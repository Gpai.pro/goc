#include "FeedbackManager.h"
#include "../GlobalDefine/EnumType.h"
#include "../GlobalDefine/Player.h"
#include "cocostudio/DictionaryHelper.h"

FeedbackManager* FeedbackManager::_instance = nullptr;

FeedbackManager::FeedbackManager()
{
    _mailbox.clear();
}

FeedbackManager::~FeedbackManager()
{

}

FeedbackManager* FeedbackManager::getInstance()
{
    if (!_instance)
    {
        _instance = new FeedbackManager();
    }

    return _instance;
}

void FeedbackManager::destroyInstance()
{
    CC_SAFE_DELETE(_instance);
}

void FeedbackManager::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    rapidjson::Document document;
    document.Parse(json.c_str());
    if (!document.IsObject())
        return;

    int errorCode = -1;
    if (document["ERR_CODE"].IsInt())
        errorCode = document["ERR_CODE"].GetInt();
    if (errorCode != 1)
        return;

    FeedbackManager::getInstance()->clearMailBox();

#define FEEDBACK_DATA "feedbackData"

    rapidjson::Value::ConstMemberIterator itrm = document.FindMember(FEEDBACK_DATA);
    if (itrm == document.MemberEnd() || !itrm->value.IsObject())
        return;
    const rapidjson::Value& momber = document[FEEDBACK_DATA];
    for (itrm = momber.MemberBegin(); itrm != momber.MemberEnd(); ++itrm)
    {
        if (!itrm->value.IsObject())
            return;

        FeedbackMailData data;
        GlobalTime gloablTime;

        const rapidjson::Value& obj = itrm->value;

        auto iter = obj.FindMember("fid");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.index = iter->value.GetInt();

        iter = obj.FindMember("type");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.type = iter->value.GetInt();

        iter = obj.FindMember("status");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.status = iter->value.GetInt();

        iter = obj.FindMember("isPlayerRead");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.isRead = iter->value.GetInt() != 0;

        iter = obj.FindMember("issue");
        if (iter == obj.MemberEnd() || !iter->value.IsString())
            break;
        data.issue = iter->value.GetString();

        iter = obj.FindMember("response");
        if (iter == obj.MemberEnd() || !iter->value.IsString())
            break;
        data.response = iter->value.GetString();

        if (data.status == EFeedbackStatus::EFeedbackStatus_Replied)
        {
            iter = obj.FindMember("year");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            gloablTime.year = iter->value.GetInt();
            iter = obj.FindMember("mon");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            gloablTime.month = iter->value.GetInt();
            iter = obj.FindMember("day");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            gloablTime.day = iter->value.GetInt();
            iter = obj.FindMember("hour");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            gloablTime.hour = iter->value.GetInt();
            iter = obj.FindMember("min");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            gloablTime.minute = iter->value.GetInt();
            data.time = gloablTime;
        }

        FeedbackManager::getInstance()->addFeedbackMail(data);
    }
}

void FeedbackManager::addFeedbackMail(FeedbackMailData& mail)
{
    if (mail.status != EFeedbackStatus::EFeedbackStatus_Replied)
    {
        ++_unreadCount;
    }

    if (_unreadCount >= UNREAD_LIMIT)
        _isOverUnreadLimit = true;

    _mailbox.push_back(mail);
}

FeedbackMailData FeedbackManager::getFeedbackMailByIndex(int index)
{
    FeedbackMailData mail;
    if (index < 1 || index > (int)_mailbox.size())
        return mail;

    mail = _mailbox.at(index - 1);
    return mail;
}

FeedbackMailData FeedbackManager::getFeedbackMailByMailIndex(int mailIndex)
{
    FeedbackMailData mail;

    for (int i = 0; i < (int)_mailbox.size(); ++i)
    {
        if (_mailbox.at(i).index == mailIndex)
        {
            mail = _mailbox.at(i);
        }
    }

    return mail;
}

void FeedbackManager::clearMailBox()
{
    _mailbox.clear();
    _unreadCount = 0;
    _isOverUnreadLimit = false;
}

bool FeedbackManager::isOverDailySendLimit()
{
    return Player::getInstance()->getGameCounter(EGC_DailySendFeecbackCount) >= DAILY_SEND_LIMIT;
}

bool FeedbackManager::isMailUnread()
{
    for (int i = 0; i < (int)_mailbox.size(); ++i)
    {
        if (_mailbox.at(i).status == EFeedbackStatus::EFeedbackStatus_Replied && !_mailbox.at(i).isRead)
            return true;
    }

    return false;
}

void FeedbackManager::setMailRead(int mailIndex)
{
    for (int i = 0; i < (int)_mailbox.size(); ++i)
    {
        if (_mailbox.at(i).index == mailIndex)
            _mailbox.at(i).isRead = true;
    }
}