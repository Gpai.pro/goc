#pragma once

#include "cocos2d.h"
#include "../GlobalDefine/GameData.h"
#include "UI/CocosGUI.h"
//#include "cocostudio/CocoStudio.h"
//

using namespace cocos2d;

struct FeedbackMailData
{
    int index;
    int type;
    int status;
    bool isRead;
    std::string issue;
    std::string response;
    GlobalTime time;
};

class FeedbackManager
{
public:
    const int UNREAD_LIMIT = 3;
    const int DAILY_SEND_LIMIT = 3;

public:
    FeedbackManager();
    ~FeedbackManager();
    static FeedbackManager* getInstance();
    static void destroyInstance();

    void onDispatchResponse(const std::string& requestTag, const std::string& json);

    void addFeedbackMail(FeedbackMailData& mail);
    FeedbackMailData getFeedbackMailByIndex(int index);
    FeedbackMailData getFeedbackMailByMailIndex(int mailIndex);
    void clearMailBox();
    bool isOverUnreadLimit(){ return _isOverUnreadLimit; };
    bool isOverDailySendLimit();
    bool isMailUnread();
    int getMailCount() { return (int)_mailbox.size(); };
    void setMailRead(int mailIndex);

private:
    static FeedbackManager* _instance;

    std::vector<FeedbackMailData> _mailbox;

    int _unreadCount = 0;
    bool _isOverUnreadLimit = false;
};