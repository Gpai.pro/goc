#include "LeadersManager.h"
#include "cocostudio/DictionaryHelper.h"
#include "../Network/MessageProcess.h"
#include "../GlobalDefine/ImageDefine.h"
#include "../GlobalDefine/GameData.h"
#include "../GlobalDefine/Player.h"
#include "../GlobalDefine/GameConst.h"
#include "../UI/UIController.h"
#include "../UI/UILeadersReward.h"

LeadersManager* LeadersManager::_instance = nullptr;

LeadersManager::LeadersManager()
{
    _rewardRanks.clear();
    clearAllRankData();
    clearCheckRewardSchedule();
}

LeadersManager::~LeadersManager()
{
    clearCheckRewardSchedule();
}

LeadersManager* LeadersManager::getInstance()
{
    if (!_instance)
    {
        _instance = new LeadersManager();
    }

    return _instance;
}

void LeadersManager::destroyInstance()
{
    CC_SAFE_DELETE(_instance);
}

void LeadersManager::requestRankReward()
{
    _rewardRanks.clear();
    RequestGetRankReward();
}

void LeadersManager::requestLeadersRank()
{
    setLeadersDoneFlag(_nowRequestType, _nowRequestTimeType, false);
    setLeadersErrorCodes(_nowRequestType, _nowRequestTimeType, 0);
    setLeadersExpiryTime(_nowRequestType, _nowRequestTimeType, 0);

    RequestGetLeadersRank(_nowRequestType, _nowRequestTimeType);
}

void LeadersManager::requestLeadersTable()
{
    _leadersTable.clear();
    RequestGetLeadersTable();
}

void LeadersManager::onDispatchRankRewardResponse(const std::string& requestTag, const std::string& json)
{
    rapidjson::Document document;
    document.Parse(json.c_str());
    if (!document.IsObject())
        return;

    int errorCode = -1;
    if (document["ERR_CODE"].IsInt())
        errorCode = document["ERR_CODE"].GetInt();
    if (errorCode != 1)
    {
        return;
    }

#define RANK_INFO "rankInfo"

    rapidjson::Value::ConstMemberIterator itrm = document.FindMember(RANK_INFO);
    if (itrm == document.MemberEnd() || !itrm->value.IsObject())
        return;
    const rapidjson::Value& momber = document[RANK_INFO];
    for (itrm = momber.MemberBegin(); itrm != momber.MemberEnd(); ++itrm)
    {
        if (!itrm->value.IsObject())
            return;

        LeadersRewardRank rank;

        const rapidjson::Value& obj = itrm->value;

        auto iter = obj.FindMember("rankType");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.type = (ELeadersType)iter->value.GetInt();

        iter = obj.FindMember("timeType");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.timeType = (ELeadersTimeType)iter->value.GetInt();

        iter = obj.FindMember("rank");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.rank = iter->value.GetInt();

        iter = obj.FindMember("rewardType");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.rewardType = iter->value.GetInt();

        iter = obj.FindMember("rewardCount");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.rewardCount = iter->value.GetInt();

        iter = obj.FindMember("rewardCoin");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.rewardCoin = iter->value.GetInt();

        iter = obj.FindMember("stringID");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.stringID = iter->value.GetInt();

        iter = obj.FindMember("picFile");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.picFile = iter->value.GetInt();

        iter = obj.FindMember("disable");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        rank.disable = iter->value.GetInt();

        _rewardRanks.push_back(rank);
    }
}

void LeadersManager::onDispatchLeadersRankResponse(const std::string& requestTag, const std::string& json)
{
    clearRankData(_nowRequestType, _nowRequestTimeType);

    rapidjson::Document document;
    document.Parse(json.c_str());
    if (!document.IsObject())
        return;

    int errorCode = -1;
    if (document["ERR_CODE"].IsInt())
        errorCode = document["ERR_CODE"].GetInt();

    setLeadersErrorCodes(_nowRequestType, _nowRequestTimeType, errorCode);

    if (errorCode != 1 && errorCode != EC_EmergencyClose && errorCode != EC_LeadersEmpty)
    {
        return;
    }

    int rankType = 0, timeType = 0;
    if (document["rankType"].IsInt())
        rankType = document["rankType"].GetInt();
    if (document["timeType"].IsInt())
        timeType = document["timeType"].GetInt();

    if (rankType != (int)_nowRequestType || timeType != (int)_nowRequestTimeType)
        return;

    if (errorCode == EC_EmergencyClose)
    {
        setLeadersDoneFlag(_nowRequestType, _nowRequestTimeType, true);
        getNextLeadersRank();
        return;
    }

    GlobalTime gloablTime;
    if (document["year"].IsInt())
        gloablTime.year = document["year"].GetInt();
    if (document["mon"].IsInt())
        gloablTime.month = document["mon"].GetInt();
    if (document["day"].IsInt())
        gloablTime.day = document["day"].GetInt();
    if (document["hour"].IsInt())
        gloablTime.hour = document["hour"].GetInt();
    if (document["min"].IsInt())
        gloablTime.minute = document["min"].GetInt();
    if (document["sec"].IsInt())
        gloablTime.second = document["sec"].GetInt();
    time_t expiryTime = ConvertGlobalTimeToTimeT(gloablTime);
    setLeadersExpiryTime(_nowRequestType, _nowRequestTimeType, expiryTime);

    if (errorCode == EC_LeadersEmpty)
    {
        setLeadersDoneFlag(_nowRequestType, _nowRequestTimeType, true);
        getNextLeadersRank();
        return;
    }
    if (errorCode != 1)
    {
        //getNextLeadersRank();
        return;
    }

    LeadersRankData playerData;
    if (document["playerRank"].IsInt())
        playerData.rank = document["playerRank"].GetInt();
    if (document["playerName"].IsString())
        playerData.name = document["playerName"].GetString();
    if (document["playerInfo"].IsInt())
        playerData.info = document["playerInfo"].GetInt();
    if (document["playerScore"].IsUint64())
        playerData.score = document["playerScore"].GetUint64();
    //if (document["playerReward"].IsInt())
    //    playerData.reward = document["playerReward"].GetInt();
    if (document["playerAvatar"].IsString())
        playerData.headURL = document["playerAvatar"].GetString();
    
    playerData.rewardString = getRewardData(playerData.rank, _nowRequestType, _nowRequestTimeType).rewardString;
    playerData.id = Value(Player::getInstance()->getID()).asInt();
    pushPlayerRankData(_nowRequestType, _nowRequestTimeType, playerData);

    int id = (1 + ((int)_nowRequestType - 1) * (int)ELeadersTimeType::ELeadersTimeType_Count) + (int)_nowRequestTimeType - 1;
    //auto table = ExternalTable<LeaderData>::getRecordByID(id);
    if (id < 1 || id > _leadersTable.size())
    {
        return;
    }
    auto table = _leadersTable.at(id - 1);
    int ranksIndex = 0;
#define LEADERS_DATA "requestData"

    rapidjson::Value::ConstMemberIterator itrm = document.FindMember(LEADERS_DATA);
    if (itrm == document.MemberEnd() || !itrm->value.IsObject())
        return;
    const rapidjson::Value& momber = document[LEADERS_DATA];
    for (itrm = momber.MemberBegin(); itrm != momber.MemberEnd(); ++itrm)
    {
        if (!itrm->value.IsObject())
            return;

        LeadersRankData data;

        const rapidjson::Value& obj = itrm->value;

        auto iter = obj.FindMember("rank");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.rank = iter->value.GetInt();

        if (playerData.rank > 3 && playerData.rank == data.rank)
            continue;

        iter = obj.FindMember("id");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.id = iter->value.GetInt();

        iter = obj.FindMember("name");
        if (iter == obj.MemberEnd() || !iter->value.IsString())
            break;
        data.name = iter->value.GetString();

        iter = obj.FindMember("info");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.info = iter->value.GetInt();

        iter = obj.FindMember("score");
        if (iter == obj.MemberEnd() || !iter->value.IsUint64())
            break;
        data.score = iter->value.GetUint64();

        //iter = obj.FindMember("reward");
        //if (iter == obj.MemberEnd() || !iter->value.IsInt())
        //    break;
        //data.reward = iter->value.GetInt();

        iter = obj.FindMember("avatar");
        if (iter == obj.MemberEnd() || !iter->value.IsString())
            break;
        data.headURL = iter->value.GetString();

        if (_nowRequestTimeType == ELeadersTimeType::ELeadersTimeType_AllTime)
        {
            data.rewardString = "-";
        }
        else
        {
            if (table.DisableReward == 1)
            {
                data.rewardString = "-";//"即將開放"
            }
            else
            {
                for (; ranksIndex < RANK_AMOUNT; ++ranksIndex)
                {
                    if (data.rank >= table.Ranks[ranksIndex].RankBegin && data.rank <= table.Ranks[ranksIndex].RankEnd)
                    {
                        if ((ELeadersRewardType)table.Ranks[ranksIndex].RewardType == ELeadersRewardType::ELeadersRewardType_Coin)
                        {
                            if (data.rank <= 3)
                                data.rewardString = safeGetStringData(1212) + " $" + GetMoneyFormat(table.Ranks[ranksIndex].RewardParam);
                            else
                                data.rewardString = "$" + GetMoneyFormat(table.Ranks[ranksIndex].RewardParam);
                        }
                        else
                        {
                            if (table.Ranks[ranksIndex].RewardCoin == 0)
                                data.rewardString = composeStringData(safeGetStringData(table.Ranks[ranksIndex].RewardStringID), GetMoneyFormat(table.Ranks[ranksIndex].RewardParam));
                            else
                                data.rewardString = composeStringData(safeGetStringData(table.Ranks[ranksIndex].RewardStringID), GetMoneyFormat(table.Ranks[ranksIndex].RewardParam), "$" + GetMoneyFormat(table.Ranks[ranksIndex].RewardCoin));
                        }
                        break;
                    }
                }
            }
        }

        pushLeadersRankData(_nowRequestType, _nowRequestTimeType, data);
    }

    setLeadersDoneFlag(_nowRequestType, _nowRequestTimeType, true);

    getNextLeadersRank();

    if (_bNeedUpdateUILeader)
    {
        UIController::getInstance()->getController(EUITAG_UILEADER)->updateUI();
        _bNeedUpdateUILeader = false;
    }
}

void LeadersManager::onDispatchLeadersTableResponse(const std::string& requestTag, const std::string& json)
{
    rapidjson::Document document;
    document.Parse(json.c_str());
    if (!document.IsObject())
        return;

    int errorCode = -1;
    if (document["ERR_CODE"].IsInt())
        errorCode = document["ERR_CODE"].GetInt();
    if (errorCode != 1)
    {
        return;
    }

#define REQUEST_DATA "requestData"

    rapidjson::Value::ConstMemberIterator itrm = document.FindMember(REQUEST_DATA);
    if (itrm == document.MemberEnd() || !itrm->value.IsObject())
        return;
    const rapidjson::Value& momber = document[REQUEST_DATA];
    for (itrm = momber.MemberBegin(); itrm != momber.MemberEnd(); ++itrm)
    {
        if (!itrm->value.IsObject())
            return;

        LeaderData table;

        const rapidjson::Value& obj = itrm->value;

        auto iter = obj.FindMember("id");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        table.ID = iter->value.GetInt();

        iter = obj.FindMember("disableReward");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        table.DisableReward = iter->value.GetInt();

        iter = obj.FindMember("nameStringDataID");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        table.NameStringDataID = iter->value.GetInt();

#define RANK_DETAIL "RankDetail"
        rapidjson::Value::ConstMemberIterator detailItr = obj.FindMember(RANK_DETAIL);
        if (detailItr == obj.MemberEnd() || !detailItr->value.IsObject())
            return;
        const rapidjson::Value& dMomber = detailItr->value;
        int i = 0;
        for (detailItr = dMomber.MemberBegin(); detailItr != dMomber.MemberEnd() && i < RANK_AMOUNT; ++detailItr, ++i)
        {
            if (!detailItr->value.IsObject())
                return;

            const rapidjson::Value& obj = detailItr->value;

            auto iter = obj.FindMember("begin");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            table.Ranks[i].RankBegin = iter->value.GetInt();

            iter = obj.FindMember("end");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            table.Ranks[i].RankEnd = iter->value.GetInt();

            iter = obj.FindMember("type");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            table.Ranks[i].RewardType = iter->value.GetInt();

            iter = obj.FindMember("param");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            table.Ranks[i].RewardParam = iter->value.GetInt();

            iter = obj.FindMember("addCoin");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            table.Ranks[i].RewardCoin = iter->value.GetInt();

            iter = obj.FindMember("stringID");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            table.Ranks[i].RewardStringID = iter->value.GetInt();

            iter = obj.FindMember("picFile");
            if (iter == obj.MemberEnd() || !iter->value.IsInt())
                break;
            table.Ranks[i].RewardPicFile = iter->value.GetInt();
        }

        _leadersTable.push_back(table);
    }
}

void LeadersManager::startRequestLeadersRank()
{
    _nowRequestType = ELeadersType::ELeadersType_Richer;
    _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_Today;

    requestLeadersRank();
}

LeadersRewardData LeadersManager::getRewardData(int rank, ELeadersType type, ELeadersTimeType timeType)
{
    LeadersRewardData data;
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return data;
    int id = (1 + ((int)type - 1) * (int)ELeadersTimeType::ELeadersTimeType_Count) + (int)timeType - 1;

    //auto table = ExternalTable<LeaderData>::getRecordByID(id);
    if (id < 1 || id > _leadersTable.size())
    {
        return data;
    }
    auto table = _leadersTable.at(id - 1);

    if (table.DisableReward == 1)
    {
        data.rewardString = "-";//"即將開放"
        data.disableReward = false;
        return data;
    }

    data.disableReward = true;
    for (int i = 0; i < RANK_AMOUNT; ++i)
    {
        if (rank >= table.Ranks[i].RankBegin && rank <= table.Ranks[i].RankEnd)
        {
            data.rewardType = (ELeadersRewardType)table.Ranks[i].RewardType;

            if (timeType == ELeadersTimeType::ELeadersTimeType_AllTime)
            {
                data.rewardString = "-";
            }
            else
            {
                if (data.rewardType == ELeadersRewardType::ELeadersRewardType_Coin)
                {
                    if (rank <= 3)
                        data.rewardString = safeGetStringData(1212) + " $" + GetMoneyFormat(table.Ranks[i].RewardParam);
                    else
                        data.rewardString = "$" + GetMoneyFormat(table.Ranks[i].RewardParam);
                }
                else
                {
                    if (table.Ranks[i].RewardCoin == 0)
                        data.rewardString = composeStringData(safeGetStringData(table.Ranks[i].RewardStringID), GetMoneyFormat(table.Ranks[i].RewardParam));
                    else
                        data.rewardString = composeStringData(safeGetStringData(table.Ranks[i].RewardStringID), GetMoneyFormat(table.Ranks[i].RewardParam), "$" + GetMoneyFormat(table.Ranks[i].RewardCoin));
                }
            }
            if (table.Ranks[i].RewardPicFile != 0)
                data.rewardPicPath = StringUtils::format(GOCIMAGE(LeadersAward%.4d), table.Ranks[i].RewardPicFile);
            else
                data.rewardPicPath = "";
            break;
        }
    }

    return data;
}

LeadersRewardData LeadersManager::getRewardDataByRange(int range, ELeadersType type, ELeadersTimeType timeType)
{
    LeadersRewardData data;
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count ||
        range < 1 || range >= RANK_AMOUNT)
        return data;

    int id = (1 + ((int)type - 1) * (int)ELeadersTimeType::ELeadersTimeType_Count) + (int)timeType - 1;

    //auto table = ExternalTable<LeaderData>::getRecordByID(id);
    if (id < 1 || id > _leadersTable.size())
    {
        return data;
    }
    auto table = _leadersTable.at(id - 1);

    if (table.DisableReward == 1)
    {
        data.rewardString = "-";//"即將開放"
        data.disableReward = false;
        return data;
    }

    data.disableReward = true;
    int i = range - 1;
    data.rewardType = (ELeadersRewardType)table.Ranks[i].RewardType;
    if (timeType == ELeadersTimeType::ELeadersTimeType_AllTime)
    {
        data.rewardString = "-";
    }
    else
    {
        if (data.rewardType == ELeadersRewardType::ELeadersRewardType_Coin)
        {
            if (range <= 3)
                data.rewardString = safeGetStringData(1212) + " $" + GetMoneyFormat(table.Ranks[i].RewardParam);
            else
                data.rewardString = "$" + GetMoneyFormat(table.Ranks[i].RewardParam);
        }
        else
        {
            if (table.Ranks[i].RewardCoin == 0)
                data.rewardString = composeStringData(safeGetStringData(table.Ranks[i].RewardStringID), GetMoneyFormat(table.Ranks[i].RewardParam));
            else
                data.rewardString = composeStringData(safeGetStringData(table.Ranks[i].RewardStringID), GetMoneyFormat(table.Ranks[i].RewardParam), "$" + GetMoneyFormat(table.Ranks[i].RewardCoin));
        }
    }
    if (table.Ranks[i].RewardPicFile != 0)
        data.rewardPicPath = StringUtils::format(GOCIMAGE(LeadersAward%.4d), table.Ranks[i].RewardPicFile);
    else
        data.rewardPicPath = "";

    return data;
}

int LeadersManager::getLeadersErrorCodes(ELeadersType type, ELeadersTimeType timeType)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return -1;

    return _leadersErrorCodes[type - 1][timeType - 1];
}

int LeadersManager::getLeadersExpiiryTime(ELeadersType type, ELeadersTimeType timeType)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return -1;

    return _leadersExpiryTime[type - 1][timeType - 1];
}

bool LeadersManager::isLeadersDataDone(ELeadersType type, ELeadersTimeType timeType)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return false;

    if (!_bLeadersDataDone[type - 1][timeType - 1])
        _bNeedUpdateUILeader = true;

    return _bLeadersDataDone[type - 1][timeType - 1];
}

bool LeadersManager::getPlayerRankData(ELeadersType type, ELeadersTimeType timeType, std::vector<LeadersRankData>& data)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return false;

    data = _playerRankData[type - 1][timeType - 1];
    return true;
}

bool LeadersManager::getLeadersRankData(ELeadersType type, ELeadersTimeType timeType, std::vector<LeadersRankData>& data)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return false;

    data = _leadersRankData[type - 1][timeType - 1];
    return true;
}

bool LeadersManager::openUILeadersReward()
{
    if (_rewardRanks.empty())
        return false;

    auto controller = UIController::getInstance()->getController<UILeadersReward*>(EUITAG_UILEADERS_REWARD);
    for (int i = 0; i < (int)_rewardRanks.size(); ++i)
    {
        controller->setData(_rewardRanks.at(i));
    }

    _rewardRanks.clear();
    controller->openUILater();
    return true;
}

int LeadersManager::getNameStringIDByTableID(int tableID)
{
    if (tableID < 1 || tableID > _leadersTable.size())
        return -1;

    return _leadersTable.at(tableID - 1).NameStringDataID;
}

void LeadersManager::clearAllRankData()
{
    for (int i = 0; i < (int)ELeadersType::ELeadersType_Count; ++i)
    for (int j = 0; j < (int)ELeadersTimeType::ELeadersTimeType_Count; ++j)
    {
        _playerRankData[i][j].clear();
        _leadersRankData[i][j].clear();
    }

    memset(_leadersErrorCodes, 0, sizeof(_leadersErrorCodes));
    memset(_bLeadersDataDone, 0, sizeof(_bLeadersDataDone));
    memset(_leadersExpiryTime, 0, sizeof(_leadersExpiryTime));
}

void LeadersManager::clearRankData(ELeadersType type, ELeadersTimeType timeType)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return;

    _playerRankData[type - 1][timeType - 1].clear();
    _leadersRankData[type - 1][timeType - 1].clear();
}

void LeadersManager::setLeadersErrorCodes(ELeadersType type, ELeadersTimeType timeType, int error)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return;

    _leadersErrorCodes[type - 1][timeType - 1] = error;
}

void LeadersManager::setLeadersDoneFlag(ELeadersType type, ELeadersTimeType timeType, bool flag)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return;

    _bLeadersDataDone[type - 1][timeType - 1] = flag;
}

void LeadersManager::setLeadersExpiryTime(ELeadersType type, ELeadersTimeType timeType, int time)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return;

    _leadersExpiryTime[type - 1][timeType - 1] = time;
}

void LeadersManager::pushPlayerRankData(ELeadersType type, ELeadersTimeType timeType, LeadersRankData data)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return;

    _playerRankData[type - 1][timeType - 1].push_back(data);
}

void LeadersManager::pushLeadersRankData(ELeadersType type, ELeadersTimeType timeType, LeadersRankData data)
{
    if (type < ELeadersType::ELeadersType_Richer || type > ELeadersType::ELeadersType_Count ||
        timeType < ELeadersTimeType::ELeadersTimeType_Today || timeType > ELeadersTimeType::ELeadersTimeType_Count)
        return;

    _leadersRankData[type - 1][timeType - 1].push_back(data);
}

void LeadersManager::getNextLeadersRank()
{
    if (_nowRequestType == ELeadersType::ELeadersType_None)
        return;

    int type = (int)_nowRequestType;
    int timeType = (int)_nowRequestTimeType + 1;

    if (timeType == (int)ELeadersTimeType::ELeadersTimeType_None)
    {
        ++type;
        timeType = (int)ELeadersTimeType::ELeadersTimeType_Today;
    }

    // 取得所有旁行榜
    if (type == (int)ELeadersType::ELeadersType_None)
    {
        _nowRequestType = ELeadersType::ELeadersType_None;
        _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_None;
        setCheckRewardSchedule();
        return;
    }

    _nowRequestType = (ELeadersType)type;
    _nowRequestTimeType = (ELeadersTimeType)timeType;
    requestLeadersRank();
}

void LeadersManager::clearCheckRewardSchedule()
{
    Director::getInstance()->getScheduler()->unschedule(schedule_selector(LeadersManager::checkRankReward), this);
}

void LeadersManager::setCheckRewardSchedule()
{
    if (Director::getInstance()->getScheduler()->isScheduled(schedule_selector(LeadersManager::checkRankReward), this))
        return;

    time_t nowTime = ConvertGlobalTimeToTimeT(Player::getInstance()->getGameTime());
    time_t expiryTime = ONE_WEEK_SECOND;

    for (int i = 0; i < ELeadersType::ELeadersType_Count; ++i)
    for (int j = 0; j < ELeadersTimeType::ELeadersTimeType_Count; ++j)
    {
        int leftTime = _leadersExpiryTime[i][j] - nowTime;

        if (leftTime > 0 && leftTime < expiryTime)
        {
            expiryTime = leftTime;
        }
    }

    int delayTime = ONE_MINUTE_SECOND * 5;
    Director::getInstance()->getScheduler()->schedule(schedule_selector(LeadersManager::checkRankReward), this, 0.0f, 0, expiryTime + delayTime, false);
}

void LeadersManager::checkRankReward(float dt)
{
    requestRankReward();

    clearCheckRewardSchedule();
}