#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "../GlobalDefine/EnumType.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataCenter/ExternalTable.h"

using namespace cocos2d;

struct LeadersRankData
{
    int id;
    int rank;   //排名
    int info;   //等級、機台...
    uint64 score;    //排名數據
    std::string rewardString;    //獎賞
    std::string name;   //玩家名稱
    std::string headURL;    //玩家頭圖URL

    LeadersRankData()
    {
        id = 0;
        rank = 0;
        info = 0;
        score = 0;
        rewardString = "-";
        name = "";
        headURL = "";
    }
};

struct LeadersRewardData
{
    ELeadersRewardType rewardType;
    //int rewardCount;
    //int rewardStringID;
    std::string rewardString;
    std::string rewardPicPath;
    bool disableReward;

    LeadersRewardData()
    {
        rewardType = ELeadersRewardType::ELeadersRewardType_Coin;
        rewardString = "-";
        rewardPicPath = "";
        disableReward = true;
    }
};

struct LeadersRewardRank
{
    int rank;
    ELeadersType type;
    ELeadersTimeType timeType;
    int rewardType;
    int rewardCount;
    int rewardCoin;
    int stringID;
    int picFile;
    int disable;    // 0:開放、1:即將開放

    LeadersRewardRank()
    {
        rank = 0;
        rewardType = 0;
        rewardCount = 0;
        stringID = 0;
        picFile = 0;
        type = ELeadersType::ELeadersType_None;
        timeType = ELeadersTimeType::ELeadersTimeType_None;
    }
};

class LeadersManager : public Ref
{
public:
    LeadersManager();
    ~LeadersManager();
    static LeadersManager* getInstance();
    static void destroyInstance();

    void requestRankReward();
    void requestLeadersRank();
    void requestLeadersTable();
    void onDispatchRankRewardResponse(const std::string& requestTag, const std::string& json);
    void onDispatchLeadersRankResponse(const std::string& requestTag, const std::string& json);
    void onDispatchLeadersTableResponse(const std::string& requestTag, const std::string& json);

    void startRequestLeadersRank();
    LeadersRewardData getRewardData(int rank, ELeadersType type, ELeadersTimeType timeType);
    LeadersRewardData getRewardDataByRange(int range, ELeadersType type, ELeadersTimeType timeType);

    int getLeadersErrorCodes(ELeadersType type, ELeadersTimeType timeType);
    int getLeadersExpiiryTime(ELeadersType type, ELeadersTimeType timeType);
    bool isLeadersDataDone(ELeadersType type, ELeadersTimeType timeType);
    bool getPlayerRankData(ELeadersType type, ELeadersTimeType timeType, std::vector<LeadersRankData>& data);
    bool getLeadersRankData(ELeadersType type, ELeadersTimeType timeType, std::vector<LeadersRankData>& data);

    bool openUILeadersReward();

    int getNameStringIDByTableID(int tableID);

private:
    void clearAllRankData();
    void clearRankData(ELeadersType type, ELeadersTimeType timeType);

    void setLeadersErrorCodes(ELeadersType type, ELeadersTimeType timeType, int error);
    void setLeadersDoneFlag(ELeadersType type, ELeadersTimeType timeType, bool flag);
    void setLeadersExpiryTime(ELeadersType type, ELeadersTimeType timeType, int time);
    void pushPlayerRankData(ELeadersType type, ELeadersTimeType timeType, LeadersRankData data);
    void pushLeadersRankData(ELeadersType type, ELeadersTimeType timeType, LeadersRankData data);

    void getNextLeadersRank();

    void clearCheckRewardSchedule();
    void setCheckRewardSchedule();
    void checkRankReward(float dt);

private:
    static LeadersManager* _instance;
    std::vector<LeadersRewardRank>  _rewardRanks;

    std::vector<LeadersRankData> _playerRankData[ELeadersType::ELeadersType_Count][ELeadersTimeType::ELeadersTimeType_Count];
    std::vector<LeadersRankData> _leadersRankData[ELeadersType::ELeadersType_Count][ELeadersTimeType::ELeadersTimeType_Count];

    ELeadersType _nowRequestType = ELeadersType::ELeadersType_Richer;
    ELeadersTimeType _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_Today;
    int _leadersErrorCodes[ELeadersType::ELeadersType_Count][ELeadersTimeType::ELeadersTimeType_Count];
    bool _bLeadersDataDone[ELeadersType::ELeadersType_Count][ELeadersTimeType::ELeadersTimeType_Count];
    bool _bNeedUpdateUILeader = false;

    int _leadersExpiryTime[ELeadersType::ELeadersType_Count][ELeadersTimeType::ELeadersTimeType_Count];

    std::vector<LeaderData> _leadersTable;
};