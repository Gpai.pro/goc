#include "JniNative.h"
#include "cocos2d.h"
#include "../GlobalDefine/Player.h"
#include "../Network/MessageProcess.h"
#include "../UI/UIInvite.h"
#include "../UI/UIController.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID 
extern "C"
{
    JNIEXPORT void Java_org_cocos2dx_cpp_FacebookActivity_FacebookLoginOnSuccess(JNIEnv * env, jobject obj)
    {
        log("Java_org_cocos2dx_cpp_FacebookActivity_FacebookLoginResponse");

        Player::getInstance()->facebookLoginOnSuccess();
    }

    JNIEXPORT void Java_org_cocos2dx_cpp_FacebookActivity_FacebookLoginFail(JNIEnv * env, jobject obj)
    {
        log("Java_org_cocos2dx_cpp_FacebookActivity_FacebookLoginFail");

        Player::getInstance()->facebookLoginFail();
    }

    JNIEXPORT void Java_org_cocos2dx_cpp_FacebookActivity_FacebookInviteFriend(JNIEnv * env, jobject obj, jboolean bSuccess)
    {
        log("Java_org_cocos2dx_cpp_FacebookActivity_FacebookInviteFriend");
        if(bSuccess)
            UIController::getInstance()->getController<UIInvite*>(EUITAG_INVITE)->successInviteFriend();

    }

    JNIEXPORT void Java_org_cocos2dx_cpp_PaymentActivity_RequestPurchase(
        JNIEnv * env, jobject obj
        , jstring originJson
        , jstring signature
        , jstring currency
        , jstring price 
        , jstring productID)
    {
        const char *nOriginJson = env->GetStringUTFChars(originJson, JNI_FALSE);
        const char *nSignature = env->GetStringUTFChars(signature, JNI_FALSE);
        const char *nCurrency = env->GetStringUTFChars(currency, JNI_FALSE);
        const char *nPrice= env->GetStringUTFChars(price, JNI_FALSE);
        const char *nProductID = env->GetStringUTFChars(productID, JNI_FALSE);

        GooglePurchaseRequestData data;
        data.paymentType = EPaymentType::EP_Google;
        data.productID = nProductID;
        data.currency = nCurrency;
        data.signature = nSignature;
        data.original_json = nOriginJson;
        data.price = nPrice;

        RequestGooglePurchase(data);

        env->ReleaseStringUTFChars(originJson, nOriginJson);
        env->ReleaseStringUTFChars(signature, nSignature);
        env->ReleaseStringUTFChars(currency, nCurrency);
        env->ReleaseStringUTFChars(price, nPrice);
        env->ReleaseStringUTFChars(productID, nProductID);
    }


    JNIEXPORT void Java_org_cocos2dx_cpp_PaymentActivity_RequestPurchaseResend(
        JNIEnv * env, jobject obj
        , jstring originJson
        , jstring signature
        , jstring currency
        , jstring price 
        , jstring productID)
    {
        const char *nOriginJson = env->GetStringUTFChars(originJson, JNI_FALSE);
        const char *nSignature = env->GetStringUTFChars(signature, JNI_FALSE);
        const char *nCurrency = env->GetStringUTFChars(currency, JNI_FALSE);
        const char *nPrice= env->GetStringUTFChars(price, JNI_FALSE);
        const char *nProductID = env->GetStringUTFChars(productID, JNI_FALSE);

        GooglePurchaseRequestData data;
        data.paymentType = EPaymentType::EP_Google;
        data.productID = nProductID;
        data.currency = nCurrency;
        data.signature = nSignature;
        data.original_json = nOriginJson;
        data.price = nPrice;

        RequestGooglePurchaseResend(data);

        env->ReleaseStringUTFChars(originJson, nOriginJson);
        env->ReleaseStringUTFChars(signature, nSignature);
        env->ReleaseStringUTFChars(currency, nCurrency);
        env->ReleaseStringUTFChars(price, nPrice);
        env->ReleaseStringUTFChars(productID, nProductID);
    }

    JNIEXPORT jstring Java_org_cocos2dx_cpp_PaymentActivity_GetPublicKey(JNIEnv * env, jobject obj)
    {
        jstring key = env->NewStringUTF("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5XMyGjPkl7t5qn07dXw1TXvLNLeHnEO7inQE6AOs0xjKsLJR483P/zKsyT7hm6vf+DcnvuEEmIHR5gjyjS9LbbTMjwV8Zyep3c90d0wHhhgZX6ypCecTBR3hpk4/T7mH08GrIVYzzpfe0qjdRnA813K4cmG0eXklDEFpKgO35VCmKLBonq+cs2owOhZtJXfTGPFf19LuA0yoU9mCq1zEluvgVjUFAciJ12ueMoQgfj7pU2SXB5zqSBrqTT3jgzjxDTD1XJgHjgUgXiOFgi5NL6frX+92/cRnBbKrGR2OeP8a9HOCh718+C5m/b2ACW0YSc8l3pq/2Xon9fp4YDcXPQIDAQAB");
        return key;
    }

    JNIEXPORT jstring Java_org_cocos2dx_cpp_AppActivity_ConfirmExitGame(JNIEnv * env, jobject obj)
    {
        Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}

#endif