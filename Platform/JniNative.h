#ifndef __JNI_NATIVE_H__
#define __JNI_NATIVE_H__

#include "cocos2d.h"

USING_NS_CC;

/********************
JNI Types	Java Type
void	    void
jboolean	boolean
jbyte   	byte
jchar	    char
jshort	    short
jint	    int
jlong	    long
jfloat  	float
jdouble 	double

example:
.java
private static native String nativeExample();
private static native String nativeFunc(int x);

.cpp
jstring Java_org_cocos2dx_lib_Cocos2dxActivity_nativeExample(JNIEnv * env, jobject thiz)
{
    return env->NewStringUTF("Hello from C++");
}

jstring Java_org_cocos2dx_lib_Cocos2dxActivity_nativeExample(JNIEnv * env, jobject thiz, jint input)
{
    int num = input;
}


**********************/

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID 
#include "platform/android/jni/JniHelper.h"


extern "C"
{
    JNIEXPORT void Java_org_cocos2dx_cpp_FacebookActivity_FacebookLoginOnSuccess(JNIEnv * env, jobject obj);
    JNIEXPORT void Java_org_cocos2dx_cpp_FacebookActivity_FacebookLoginFail(JNIEnv * env, jobject obj);
    JNIEXPORT void Java_org_cocos2dx_cpp_FacebookActivity_FacebookInviteFriend(JNIEnv * env, jobject obj, jboolean bSuccess);

    JNIEXPORT void Java_org_cocos2dx_cpp_PaymentActivity_RequestPurchase(JNIEnv * env, jobject obj, jstring originJson, jstring signature, jstring currency, jstring price,jstring productID);
    JNIEXPORT void Java_org_cocos2dx_cpp_PaymentActivity_RequestPurchaseResend(JNIEnv * env, jobject obj, jstring originJson, jstring signature, jstring currency, jstring price,jstring productID);

    JNIEXPORT jstring Java_org_cocos2dx_cpp_PaymentActivity_GetPublicKey(JNIEnv * env, jobject obj);

    JNIEXPORT jstring Java_org_cocos2dx_cpp_AppActivity_ConfirmExitGame(JNIEnv * env, jobject obj);
}
#endif
#endif