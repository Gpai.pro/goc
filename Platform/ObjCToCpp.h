//
//  ObjCToCpp.h
//  projectGOC
//
//  Created by Yu jen Chen on 2016/10/7.
//
//

#ifndef ObjCToCpp_h
#define ObjCToCpp_h


@interface ObjCToCpp : NSObject

+(void)RequestPurchase:(NSString*) receipt :(NSString*)currency :(NSString*)price :(NSString*) productID;

+(void)FacebookLoginSuccess;
+(void)FacebookLoginFail;
+(void)FacebookInviteFriend: (BOOL) bSuccess;

@end

#endif /* Header_h */
