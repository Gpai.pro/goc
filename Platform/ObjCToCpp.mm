//
//  ObjCToCpp.m
//  projectGOC
//
//  Created by Yu jen Chen on 2016/10/7.
//
//

#import <Foundation/Foundation.h>
#import "ObjCToCpp.h"
#include "MessageProcess.h"
#include "../GlobalDefine/Player.h"
#include "../UI/UIInvite.h"
#include "../UI/UIController.h"

@interface ObjCToCpp ()

@end

@implementation ObjCToCpp

+(void)RequestPurchase:(NSString*) receipt :(NSString*)currency :(NSString*)price :(NSString*) productID
{
        std::string std_receipt = std::string([receipt cStringUsingEncoding:NSUTF8StringEncoding]);
        std::string std_currency = std::string([currency cStringUsingEncoding:NSUTF8StringEncoding]);
        std::string std_price = std::string([price cStringUsingEncoding:NSUTF8StringEncoding]);
        std::string std_productID = std::string([productID cStringUsingEncoding:NSUTF8StringEncoding]);
    
        IOSPurchaseRequestData data;
        data.price = std_price;
        data.currency = std_currency;
        data.productID = std_productID;
        data.receipt = std_receipt;
        RequestIOSPurchase(data);
}

+(void)FacebookLoginSuccess
{
    Player::getInstance()->facebookLoginOnSuccess();
}

+(void)FacebookLoginFail
{
     Player::getInstance()->facebookLoginFail();
}

+(void)FacebookInviteFriend: (BOOL) bSuccess
{
    if(bSuccess)
        UIController::getInstance()->getController<UIInvite*>(EUITAG_INVITE)->successInviteFriend();
}

@end
