#include "LoginScene.h"
#include "../Gameplay/GameLogic.h"
#include "network/HttpClient.h"
#include "../GlobalDefine/Player.h"
#include "../Network/MessageProcess.h"
#include "../UI/CustomParticleSystemGroup.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataCenter/DownloadManager.h"
#include "AudioManager.h"
#include "../Auth/PlatformProxy.h"
#include "../UI/UICommon.h"
#include "../GlobalDefine/md5.h"
#include "../Animation/ProcedureFX.h"

using namespace network;
using namespace ProcedureFx;

#define BGM_COMMOM_LOGIN 1
#define HASH_KEY "f2aa9d3e1fc90c51e2afd7607398c393"

LoginScene::LoginScene()
{

}

LoginScene::~LoginScene()
{
    if (this->isScheduled(schedule_selector(LoginScene::updateCheckPerSecond)))
    {
        this->unschedule(schedule_selector(LoginScene::updateCheckPerSecond));
    }
}

Scene* LoginScene::createScene()
{
	auto scene = Scene::create();

	auto layer = LoginScene::create();

	layer->setTag(LOGINSCENE_TAG);

	scene->addChild(layer);

	return scene;
}

bool LoginScene::init()
{
	if (!Layer::init())
	{
		CCLOG("Error : LoginScene init faild");
		return false;
	}

    AudioManager::getInstance()->playBGMusic(BGM_COMMOM_LOGIN, true);

	_mainLayout = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UILogin/UILogin.ExportJson"));
	this->addChild(_mainLayout);

	// play animation
	_bulbLight = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "Lightbulb");
	_bulbLight->setLoop(true);
	_bulbLight->play();

    _coinkira1 = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "CoinKira_1");
    _coinkira1->setLoop(true);
    _coinkira1->play();

    _coinkira2 = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "CoinKira_2");
    _coinkira2->setLoop(true);
    _coinkira2->play();

    _coinLight = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "Coinlight");
    _coinLight->setLoop(true);
    _coinLight->play();

    _casinoIn = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "Casino_in");
    //_casinoIn->setLoop(true);
    _casinoIn->play();

    _casino = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "Casino_loop");
    _casino->setLoop(true);
    //_casino->play();

    _tapToPlay = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "TapToPlay");
    _tapToPlay->setLoop(true);
    _tapToPlay->play();

	// loading bar
	_loadingLayout = _mainLayout->getChildByName<Layout*>("layout_loading");
    _loadingLayout->setVisible(false);
    _loadingBar = _loadingLayout->getChildByName<LoadingBar*>("bar_loading");
	_loadingBar->setPercent(_loadingPercent);
    _loadText = _loadingLayout->getChildByName<Text*>("txt_info");
    _loadText->enableOutline(Color4B::BLACK, 3);

	setLoadingBarVisible(false);

    // start
    _intoGameLayout = _mainLayout->getChildByName<Layout*>("layout_start");
    _intoGameLayout->setVisible(true);
    _intoGameLayout->setTouchEnabled(true);
    _intoGameLayout->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchStart, this));
    _intoGameLayout->setSoundID(EFF_COMMON_ENTER);

	// login button
    _btnLayout = _mainLayout->getChildByName<Layout*>("layout_button");
    _btnLayout->setVisible(true);
    _guestLoginBtn = _btnLayout->getChildByName<Button*>("btn_guest");
	_guestLoginBtn->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchGuestLogin, this));

    _facebookBtn = _btnLayout->getChildByName<Button*>("btn_facebook");
	_facebookBtn->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchFacebook, this));

    auto mailLayout = _btnLayout->getChildByName<Layout*>("layout_mail");
    _checkBindGoogleMail = mailLayout->getChildByName<CheckBox*>("checkbox_mail");
    _checkBindGoogleMail->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchMailCheckbox, this));
    _bindingGoogleMail = mailLayout->getChildByName<Text*>("txt_mail");
    _bindingGoogleMail->enableOutline(Color4B::BLACK, 3);

    //

    // Sync
    _syncLayout = _mainLayout->getChildByName<Layout*>("layout_sync");
    auto syncText = _syncLayout->getChildByName<Text*>("txt_info");
    syncText->setString(safeGetStringData(5));
    syncText->enableOutline(Color4B::BLACK, 4);
    ActionObject* syncAction = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILogin.ExportJson", "Waiting");
    syncAction->play();
    _syncLayout->setVisible(false);
    //
	auto bgnode = _mainLayout->getChildByName<Layout*>("layout_bg");

    auto background = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UILogin/UILoginBackground.ExportJson"));
    bgnode->addChild(background, 1);

    _fireworkNode = background->getChildByName("Parallel03");


    for (int i = 1; i < 100; i++)
    {
        char name[30];
        sprintf(name, "Parallel%.2d", i);
        ActionObject* action = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILoginBackground.ExportJson", name);
        if (action == nullptr)
            break;
        action->play();
    }

    for (int i = 1; i < 100; i++)
    {
        char name[30];
        sprintf(name, "Animation%.2d", i);
        ActionObject* action = ActionManagerEx::getInstance()->getActionByName("GUI/UILogin/UILoginBackground.ExportJson", name);
        if (action == nullptr)
            break;
        action->play();
    }

    cocos2d::Vector<Texture2D*> list;
	list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin01.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin02.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin03.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin04.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin05.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin06.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin07.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin08.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin09.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin10.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin11.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin12.png"));

    auto fxnode = bgnode->getChildByName<Layout*>("PT_fx002");
	auto particle = CustomParticleSystemGroup::create("fx002.plist", list, 3);
	particle->setSpeed(5, 2);
	particle->setFrameVariance(12);
    fxnode->addChild(particle, 10);
    fxnode = bgnode->getChildByName<Layout*>("PT_fx005");
    particle = CustomParticleSystemGroup::create("fx005.plist", list, 3);
    particle->setSpeed(5, 2);
    particle->setFrameVariance(12);
    fxnode->addChild(particle);

    setLoginButtonVisible(false);
    setStartButtonVivible(false);

    this->schedule(schedule_selector(LoginScene::updateCheckPerSecond), 1);

    // select language
    auto globalLayout = _mainLayout->getChildByName<Layout*>("layout_global");
    UICommon::getInstance()->fixUIPosition(globalLayout, EFixUIPositionType::EFP_TOP);
    _globalBtn = globalLayout->getChildByName<Button*>("btn_global");
    _globalBtn->setTouchEnabled(true);
    _globalBtn->setVisible(false);
    _globalBtn->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchGlobal, this));

    _globalLayout = _mainLayout->getChildByName<Layout*>("layout_language");
    _globalLayout->setVisible(false);
    auto languageLayout = _globalLayout->getChildByName<Layout*>("i18N");
    for (int i = 0; i < LANGUAGE_TYPE_COUNT; ++i)
    {
        _languageBtns[i] = languageLayout->getChildByName<Button*>(StringUtils::format("btn_language_%d", i + 1));
        _languageTexts[i] = languageLayout->getChildByName<Text*>(StringUtils::format("txt_language_%d", i + 1));
        _languageTexts[i]->setString(safeGetStringData(LANGUAGE_STRINGID_BEGIN + i));
        _languageTexts[i]->enableOutline(Color4B::BLACK, 4);

        auto touchPanel = languageLayout->getChildByName<Layout*>(StringUtils::format("layout_touch_%d", i + 1));
        touchPanel->setTouchEnabled(true);
        touchPanel->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchSelectLanguage, this, i));
        touchPanel->setSoundID(EFF_COMMON_BUTTON_CLICK);
    }
    _languageSelectOK = languageLayout->getChildByName<Button*>("btn_ok");
    _languageSelectOK->setTouchEnabled(true);
    _languageSelectOK->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchSelectLanguageOK, this));
    int _selectLanguage = 1;

    // Version
    auto versionLayout = _mainLayout->getChildByName<Layout*>("layout_version");
    UICommon::getInstance()->fixUIPosition(versionLayout, EFixUIPositionType::EFP_TOP);
    Text* vesion = versionLayout->getChildByName<Text*>("txt_version");
    vesion->setString(PlatformProxy::getInstance()->getVersionName());
    vesion->enableOutline(Color4B::BLACK, 3);

    // term of service
    auto termsOfServiceNode = _termsOfServiceEnter = _mainLayout->getChildByName<ImageView*>("Terms_of_Service");
    _termsOfServiceEnter->setTouchEnabled(true);
    _termsOfServiceEnter->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchTermsOfService, this));
    _termsOfServiceEnter->setVisible(true);
    _termsOfServiceEnter->setSoundID(EFF_COMMON_BUTTON_CLICK);
    auto tmpText = termsOfServiceNode->getChildByName<Text*>("txt_01");
    tmpText->setString(safeGetStringData(674));
    tmpText = termsOfServiceNode->getChildByName<Text*>("txt_02");
    tmpText->setString(safeGetStringData(675));
    auto underlineText = termsOfServiceNode->getChildByName<Text*>("underline");
    underlineText->setString("__________________");
    underlineText->setVisible(true);
    underlineText->setScaleX(tmpText->getContentSize().width / underlineText->getContentSize().width);

    _termsOfService = _mainLayout->getChildByName<Layout*>("layout_Terms_of_Service");
    _termsOfService->setVisible(false);
    auto termsExitButton = _termsOfService->getChildByName<Button*>("Button_X");
    termsExitButton->setTouchEnabled(true);
    termsExitButton->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchCloseTermsOfService, this));
    termsExitButton->setSoundID(EFF_COMMON_CLOSE_WINDOW);
    auto termsScrollView = _termsOfService->getChildByName<ScrollView*>("content_ScrollView");
    Text* content = (Text*)termsScrollView->getChildByName("content");
    content->setTextAreaSize(Size(content->getContentSize().width, 0));
    content->ignoreContentAdaptWithSize(true);
    content->setString(safeGetStringData(97));

    Text* content2 = (Text*)content->clone();
    content2->setTextAreaSize(Size(content->getContentSize().width, 0));
    content2->ignoreContentAdaptWithSize(true);
    content2->setString(safeGetStringData(98));
    termsScrollView->addChild(content2);

    content2->setPosition(Vec2(2, content2->getContentSize().height + 5));
    content->setPosition(Vec2(2, content->getContentSize().height + content2->getContentSize().height));
    termsScrollView->setInnerContainerSize(content->getContentSize() + content2->getContentSize() + Size(0, 10));

    initSelectLanguage();
    initMessageWindow();

    auto pKeybackListner = EventListenerKeyboard::create();
    pKeybackListner->onKeyReleased = CC_CALLBACK_2(LoginScene::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(pKeybackListner, this);

	return true;
}

void LoginScene::updateLoadingBar(float dt)
{
#ifdef DOWNLOAD_UPDATE
    float downloadPercent = DownloadManager::getInstance()->getDownloadPercent();

    if(downloadPercent <= 0.0f)
        return;

    if (_loadingPercent < downloadPercent)
    {
        _loadingPercent += 2;
    }
#else
    _loadingPercent += 2;
#endif
	if (_loadingPercent >= 100.0f)
	{
		_loadingPercent = 100.0f;
#ifndef DOWNLOAD_UPDATE
        onSuccess();
#endif
        if(_loadingSuccess)
            onSuccess();
	}
	onProgress(_loadingPercent);
}

void LoginScene::touchGuestLogin(Ref* pSender, Widget::TouchEventType type)
{
	if (type != Widget::TouchEventType::ENDED)
		return;

    setLoginStepType(ECheckLoginTType::CheckAuthExist);
}

void LoginScene::touchFacebook(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    
    loginFacebook();

    setLoginButtonVisible(false);
}

void LoginScene::touchGlobal(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _globalLayout->setVisible(true);
}

void LoginScene::touchStart(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (Player::getInstance()->getLoginType() == ELoginType::ELT_Guest &&
        _checkBindGoogleMail->isSelected() &&
        _checkBindGoogleMail->isBright())
        RequestGuestBindingGoogleMail(_accountConnect, _guestAuth);

    GameLogic::getInstance()->EnterMainScene();
}

void LoginScene::touchSelectLanguage(Ref* pSender, Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (index < 0 || index > LANGUAGE_TYPE_COUNT)
        return;

    setLanguageBtnBright(index);
}

void LoginScene::touchSelectLanguageOK(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    // 存檔
    LanguageType lType = LANGUAGE_TYPE[_selectLanguageIndex];
    int oriType;
    Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Language], oriType);
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Language], (int)lType);
    Player::getInstance()->setLanguageType(lType);

    _globalLayout->setVisible(false);

    if ((LanguageType)oriType != lType)
        GameLogic::getInstance()->EnterRestartScene();
}

void LoginScene::touchMailCheckbox(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
}


void LoginScene::touchTermsOfService(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    _termsOfService->setVisible(true);
}

void LoginScene::touchCloseTermsOfService(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    _termsOfService->setVisible(false);
}

void LoginScene::touchCloseMessageWindow(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    if (_messageWindowCloseCallback)
    {
        _messageWindowCloseCallback();
        _messageWindowCloseCallback = nullptr;
    }
    _messageWindow->setVisible(false);
}

void LoginScene::startLoading()
{
    GameLogic::getInstance()->StartDownloadResource();

    _loadingStart = true;
    setLoginButtonVisible(false);
	setLoadingBarVisible(true);	

	if (!this->isScheduled(schedule_selector(LoginScene::updateLoadingBar)))
	{
		this->schedule(schedule_selector(LoginScene::updateLoadingBar), 0.03f);
	}
}

void LoginScene::setLoadingBarVisible(bool visible)
{
    _loadingLayout->setVisible(visible);
}

void LoginScene::setLoginButtonVisible(bool visible)
{
    _btnLayout->setVisible(visible);
}

void LoginScene::setStartButtonVivible(bool visible)
{
    _intoGameLayout->setVisible(visible);
}

void LoginScene::onProgress(int percent)
{
	_loadingBar->setPercent(_loadingPercent);
    _loadText->setString(safeGetStringData(4) + StringUtils::toString(percent) + "%");
}

void LoginScene::onSuccess()
{
    _loadingSuccess = true;
    if(GetPlatform() == PROTOCOL_PLATFORM_IOS && !isFacebookLogin())
    {
        if(!PlatformProxy::getInstance()->isLoginGameCenterDone())
            return;
    }
    if (this->isScheduled(schedule_selector(LoginScene::updateLoadingBar)))
    {
        this->unschedule(schedule_selector(LoginScene::updateLoadingBar));
    }

    setLoadingBarVisible(false);
    _globalBtn->setVisible(true);

    setStep(ELoginStep::CheckGoogleMailBind);

    // play loop animation
    _casino->play();
    initFirework();
}

void LoginScene::sendGuestRequest()
{
	RequestGuestLogin(GetPlatform(), _guestAuth);
}

void LoginScene::guestResponse(std::string auth)
{
    log("guest login Respones");

    Player::getInstance()->setLoginType(ELoginType::ELT_Guest);
    if (_guestAuth.empty())
	{
        _guestAuth = auth;
        Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::GuestAuth], auth);
	}

    setStep(ELoginStep::CheckLoginEnd);
}

void LoginScene::facebookLoginResponse(std::string auth)
{
    log("facebook login Respones");

    Player::getInstance()->setLoginType(ELoginType::ELT_Facebook);
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FacebookAuth], auth);

    setStep(ELoginStep::CheckLoginEnd);
}

void LoginScene::checkStep()
{
    switch (_loginStep)
    {
    case CheckVersion:
        // 檢查版本
        checkVersion();
        checkLoginGameCenter();
        break;
    case CheckDownload:
        // 開始更新
        if (!_loadingStart)
        {
            startLoading();
        }
        break;
    case CheckGoogleMailBind:
        checkGoogleMailBind();
        break;
    case StartLogin:
        GameLogic::getInstance()->StartLogin();
        break;
    case CheckLogin:
        if (isFacebookLogin())
        {
            setLoginStepType(ECheckLoginTType::CheckFacebookLogin);
        }
        else
        {
            if (isGuestLogin())
                setLoginStepType(CheckGuestLogin);
            else
                setLoginStepType(ECheckLoginTType::ChoiceLoginType);
        }
        break;
    case CheckLoginEnd:
        GameLogic::getInstance()->LoginEnd();
        _syncLayout->setVisible(true);
        break;
    case CheckAllDone:
        _syncLayout->setVisible(false);
        setLoginButtonVisible(false);
        setStartButtonVivible(true);

        break;
    default:
        break;
    }
}

void LoginScene::checkLogingType()
{
    switch (_checkLoginType)
    {
    case CheckFacebookLogin:
        loginFacebook();
        break;
    case ChoiceLoginType:
        setLoginButtonVisible(true);
        break;
    case CheckAuthExist:
        if (isGuestLogin())
            setLoginStepType(CheckGuestLogin);
        else
        {
            // New Guest
            sendRequestNewGuest();
        }
        break;
    case CheckGuestLogin:
        loginGuest();
        setLoginButtonVisible(false);
        break;
    default:
        break;
    }
}

bool LoginScene::checkVersion()
{
    GameLogic::getInstance()->CheckVersion();
    return true;
}

bool LoginScene::checkLoginGameCenter()
{
    if(GetPlatform() == PROTOCOL_PLATFORM_IOS && !isFacebookLogin())
    {
        PlatformProxy::getInstance()->loginGameCenter();
        return true;
    }
    
    return false;
}

void LoginScene::checkGoogleMailBind()
{
    sendCheckGoogleMailBind();
}

bool LoginScene::checkFacebookLogin()
{
    if (!_facebookLoginSuccess)
    {
        log("Facebook login failed");
        showShortMessage(safeGetStringData(11));
        Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FacebookLogin], false);
        Player::getInstance()->removeData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FacebookAuth]);
        setLoginStepType(ECheckLoginTType::ChoiceLoginType);

        return false;
    }
    else
    {
        log("Facebook login success");
        Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FacebookLogin], true);
        sendFacebookLoginRequest();

        return true;
    }
}

bool LoginScene::checkNewComer()
{
    return false;
}

void LoginScene::setStep(ELoginStep step)
{
    _loginStep = step;

    checkStep();
}

void LoginScene::getServerTimeDone()
{
    if (_loginStep == ELoginStep::CheckAllDone)
        return;

    setStep(ELoginStep::CheckLogin);
}

void LoginScene::setLoginStepType(ECheckLoginTType type)
{
    _checkLoginType = type;

    checkLogingType();
}

void LoginScene::updateCheckPerSecond(float dt)
{
    if (!_checkFacebookLogin)
        return;
    auto platfromProxy = PlatformProxy::getInstance();
    if (platfromProxy->isFacebookLoginFinish())
    {
        log("Facebook Login Finish");
        _facebookLoginFinish = true;
        _checkFacebookLogin = false;
        _facebookLoginSuccess = platfromProxy->isFacebookLoginSuccess();

        checkFacebookLogin();
    }
}

void LoginScene::loginFacebook()
{
    log("Facebook login");
    _checkFacebookLogin = true;
    _facebookLoginFinish = false;
    setLoginButtonVisible(false);
    PlatformProxy::getInstance()->facebookLogin();
}

void LoginScene::loginGuest()
{
    log("Guest login");
    sendGuestRequest();
}

void LoginScene::sendFacebookLoginRequest()
{
    log("facebook login Request");
    std::string token = PlatformProxy::getInstance()->getFacebookAccessToken();
    RequestFacebookLogin(token, GetPlatform());
}

void LoginScene::initSelectLanguage()
{
    LanguageType type = Player::getInstance()->getLanguageType();

    int index = getLanguageTypeIndexByType(type);

    setLanguageBtnBright(index);
}

void LoginScene::setLanguageBtnBright(int index)
{
    if (index < 0 || index > LANGUAGE_TYPE_COUNT)
        return;

    _selectLanguageIndex = index;

    for (int i = 0; i < LANGUAGE_TYPE_COUNT; ++i)
    {
        if (i == _selectLanguageIndex)
            _languageBtns[i]->setBright(false);
        else
            _languageBtns[i]->setBright(true);
    }
}

bool LoginScene::isFacebookLogin()
{
    bool data;
    Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FacebookLogin], data);
    return data;
}

bool LoginScene::isGuestLogin()
{
    if (Player::getInstance()->checkHasData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::GuestAuth]))
    {
        _guestAuth.clear();
        Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::GuestAuth], _guestAuth);

        if (!_guestAuth.empty())
            return true;
    }

    return false;
}

void LoginScene::sendCheckGoogleMailBind()
{
    _accountGameCenterName = PlatformProxy::getInstance()->getGameCenterName();
    _accountConnect = PlatformProxy::getInstance()->getConnectAccount();
    Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::GuestAuth], _guestAuth);
    RequestCheckGoogleMailBind(_accountConnect, _guestAuth);
}

void LoginScene::updateBindingMailStatus()
{
    _checkBindGoogleMail->setVisible(true);
    _bindingGoogleMail->setVisible(true);
    if (_guestBindingGoogleMail.empty()) // 沒綁定過帳號
    {
        if (!_accountConnect.empty() && _accountGoogleMailStatus == 0) // 手機有抓到googleMail而且此mail未綁定
        {
            //_checkBindGoogleMail->setBright(false);
            _checkBindGoogleMail->setSelected(true);
            if(GetPlatform() == PROTOCOL_PLATFORM_IOS)
                _bindingGoogleMail->setString(_accountGameCenterName);
            else if(GetPlatform() == PROTOCOL_PLATFORM_ANDROID)
                _bindingGoogleMail->setString(_accountConnect);
            else
                _bindingGoogleMail->setString("");
        }
        else
        {
            _checkBindGoogleMail->setVisible(false);
            _bindingGoogleMail->setVisible(false);
        }
    }
    else
    {
        _checkBindGoogleMail->setBright(false);
        _checkBindGoogleMail->setTouchEnabled(false);
        _checkBindGoogleMail->setSelected(true);
        _bindingGoogleMail->setString(_guestBindingGoogleMail);
    }
}

void LoginScene::sendRequestNewGuest()
{
    log("guest sendRequestNewGuest");
    std::string rand = getNewGusetRand();
    time_t result = ConvertGlobalTimeToTimeT(Player::getInstance()->getGameTime());
    std::string time = StringUtils::toString(result);
    std::string hash = md5(rand + time + HASH_KEY);
   
    RequestNewGuest(rand, time, hash);
}

void LoginScene::initMessageWindow()
{
    // message box
    _messageWindow = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/SharedUI/Message2.ExportJson"));
    _mainLayout->addChild(_messageWindow, 100);

    _messageContent = _messageWindow->getChildByName<Text*>("text01");
    _messageContent->setTextAreaSize(Size(600, 0));
    _messageContent->ignoreContentAdaptWithSize(true);
    _messageTitle = _messageWindow->getChildByName<Text*>("title");
    auto buyNowButton = _messageWindow->getChildByName<Button*>("Button_1");
    buyNowButton->addTouchEventListener(CC_CALLBACK_2(LoginScene::touchCloseMessageWindow, this));
    
    _messageWindow->setVisible(false);

    // short message
    _shortMessage = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/SharedUI/Message.ExportJson"));
    _shortMessage->setVisible(false);
    _mainLayout->addChild(_shortMessage, 100);
    _shortMessage->setTouchEnabled(false);
    setTraversalCascadeOpacityEnabled(_shortMessage, true);
    _shortMessageText = _shortMessage->getChildByName<Text*>("Label_6");
    _shortMessageAppear = ActionManagerEx::getInstance()->getActionByName("GUI/SharedUI/Message.ExportJson", "Animation01");
}

void LoginScene::openMessageWindow(std::string title, std::string content, std::function<void()> exitCallback)
{
    if (_messageWindow)
        _messageWindow->setVisible(true);

    _messageContent->setString(content);
    _messageTitle->setString(title);
    _messageWindowCloseCallback = exitCallback;
}

void LoginScene::showShortMessage(std::string message)
{
    _shortMessage->setVisible(true);
    _shortMessageText->setString(message);
    _shortMessageAppear->updateToFrameByTime(0);
    _shortMessageAppear->play();
}

void LoginScene::recvNewGuest(const std::string& auth)
{
    _guestAuth = auth;
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::GuestAuth], auth);

    setLoginStepType(CheckGuestLogin);
}

void LoginScene::recvCheckGoogleMailBind(int errorCode, int googleMailStatus, const std::string& auth, const std::string& bindGoogleMail)
{
    _accountGoogleMailStatus = googleMailStatus;
    _guestAuth = auth;
    _guestBindingGoogleMail = bindGoogleMail;

    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::GuestAuth], auth);
    updateBindingMailStatus();
    setStep(ELoginStep::StartLogin);
}

void  LoginScene::onEnter()
{
    Layer::onEnter();
    // 檢查流程
    setStep(ELoginStep::CheckVersion);
}

void LoginScene::initFirework()
{
    Vector<FiniteTimeAction*> arrayOfActions;
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1000, 0), Vec2(1000, 800), 2.0f, BigRedFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(240, 0), Vec2(240, 700), 1.0f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(340, 0), Vec2(340, 600), 2.0f, BigYellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 600), 1.0f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1100, 0), Vec2(1100, 400), 1.5f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1140, 300), 2.0f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(200, 700), 1.5f, YellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1240, 600), 1.0f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(900, 0), Vec2(1000, 800), 1.0f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 1.3f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(300, 0), Vec2(340, 600), 1.5f, BigRedFirework); }));
    arrayOfActions.pushBack(DelayTime::create(2.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(990, 0), Vec2(1140, 800), 1.5f, RedFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(890, 0), Vec2(940, 700), 1.5f, YellowFirework); }));
    arrayOfActions.pushBack(DelayTime::create(1.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1140, 200), 1.3f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1300, 0), Vec2(1240, 600), 1.6f, YellowFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 700), 1.5f, BigYellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1140, 400), 1.8f, YellowFirework); }));
    arrayOfActions.pushBack(DelayTime::create(0.5f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(340, 0), Vec2(340, 450), 1.7f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(140, 700), 0.8f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(900, 0), Vec2(1000, 800), 1.0f, BigYellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 450), 2.0f, BigYellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(300, 0), Vec2(340, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(0.5));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(890, 0), Vec2(940, 600), 1.5f, BigRedFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1000, 0), Vec2(940, 400), 1.8f, GreenFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.3f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1100, 400), 1.8f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 2.0f, RedFirework); }));
    arrayOfActions.pushBack(DelayTime::create(0.5f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1000, 460), 1.3f, YellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1240, 600), 2.6f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(0.5f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(40, 0), Vec2(340, 450), 1.7f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(140, 600), 0.8f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 750), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1100, 400), 1.8f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(900, 0), Vec2(900, 800), 2.0f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 450), 2.0f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(300, 0), Vec2(340, 600), 1.5f, RedFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.3f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1240, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1000, 0), Vec2(940, 400), 1.8f, YellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 2.0f, YellowFirework); }));
    arrayOfActions.pushBack(DelayTime::create(2.3f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1100, 400), 1.8f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1000, 300), 1.3f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 2.0f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1240, 600), 0.6f, BigRedFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.3f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(240, 0), Vec2(240, 450), 1.7f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 2.0f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1240, 600), 0.6f, RedFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(140, 600), 0.8f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1100, 400), 1.8f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1000, 460), 1.3f, BigYellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1240, 600), 2.6f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(0.5f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(40, 0), Vec2(340, 450), 1.7f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(140, 600), 0.8f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 750), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1100, 400), 1.8f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(900, 0), Vec2(900, 800), 2.0f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 450), 2.0f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(300, 0), Vec2(340, 600), 1.5f, RedFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.3f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1240, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1000, 0), Vec2(940, 400), 1.8f, YellowFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 2.0f, BigYellowFirework); }));
    arrayOfActions.pushBack(DelayTime::create(2.3f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1100, 400), 1.8f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1140, 0), Vec2(1000, 300), 1.3f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 2.0f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1240, 600), 0.6f, BigRedFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.3f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(240, 0), Vec2(240, 450), 1.7f, GreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(240, 700), 2.0f, BigGreenFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1240, 600), 0.6f, RedFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(200, 0), Vec2(140, 600), 0.8f, VioletFirework); }));
    arrayOfActions.pushBack(DelayTime::create(3.0f));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1200, 0), Vec2(1200, 600), 1.5f, VioletFirework); }));
    arrayOfActions.pushBack(CallFunc::create([this](){ FireworkParticle(_fireworkNode, Vec2(1240, 0), Vec2(1100, 400), 1.8f, VioletFirework); }));

    auto actionTrack = RepeatForever::create( Sequence::create(arrayOfActions));
    _fireworkNode->runAction(actionTrack);
}

void LoginScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* pEvent)
{
    if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    {
        PlatformProxy::getInstance()->openExitGame();
    }
}