#ifndef __LOGINSCENE_H__
#define __LOGINSCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "network/HttpRequest.h"
#include "../GlobalDefine/GameConst.h"
#include "../UI/UIMessageWindow.h"

//#include "cocos2d/extensions/cocos-ext.h"

using namespace cocos2d;
using namespace ui;
using namespace cocostudio;
//using namespace extension;

#define LOGINSCENE_TAG 10000

enum ELoginStep
{
    CheckVersion,
    CheckDownload,
    CheckGoogleMailBind,
    StartLogin,
    CheckLogin,
    CheckLoginEnd,
    CheckAllDone
};

enum ECheckLoginTType
{
    CheckFacebookLogin,
    ChoiceLoginType,
    CheckAuthExist,
    CheckGuestLogin,
    NONE_CHECK
};

class LoginScene : public cocos2d::Layer//, public AssetsManagerDelegateProtocol
{
public:
    LoginScene();

    ~LoginScene();

	static cocos2d::Scene* createScene();

	CREATE_FUNC(LoginScene);

	virtual bool init();

	void guestResponse(std::string auth);

    void facebookLoginResponse(std::string auth);

    void onSuccess();

    void recvCheckGoogleMailBind(int errorCode, int googleMailStatus, const std::string& auth, const std::string& bindGoogleMail);

    void recvNewGuest(const std::string& auth);

    void setStep(ELoginStep step);

    void getServerTimeDone();

    void openMessageWindow(std::string title, std::string content, std::function<void()> exitCallback);

    void showShortMessage(std::string message);

    void initFirework();

private:

    virtual void onEnter() override;

	void updateLoadingBar(float dt);

	void touchGuestLogin(Ref* pSender, Widget::TouchEventType type);

	void touchFacebook(Ref* pSender, Widget::TouchEventType type);

    void touchGlobal(Ref* pSender, Widget::TouchEventType type);

    void touchStart(Ref* pSender, Widget::TouchEventType type);

    void touchSelectLanguage(Ref* pSender, Widget::TouchEventType type, int index);

    void touchSelectLanguageOK(Ref* pSender, Widget::TouchEventType type);

    void touchMailCheckbox(Ref* pSender, Widget::TouchEventType type);

    void touchTermsOfService(Ref* pSender, Widget::TouchEventType type);

    void touchCloseTermsOfService(Ref* pSender, Widget::TouchEventType type);

    void touchCloseMessageWindow(Ref* pSender, Widget::TouchEventType type);

	void startLoading();

	void setLoadingBarVisible(bool visible);

	void setLoginButtonVisible(bool visible);

    void setStartButtonVivible(bool visible);

	void onProgress(int percent);

	void sendGuestRequest();

    void checkStep();

    void checkLogingType();

    bool checkVersion();
    
    bool checkLoginGameCenter();

    void checkGoogleMailBind();

    bool checkFacebookLogin();

    bool checkNewComer();

    void setLoginStepType(ECheckLoginTType type);

    void updateCheckPerSecond(float dt);

    void loginFacebook();

    void loginGuest();

    void sendFacebookLoginRequest();

    void initSelectLanguage();
    void setLanguageBtnBright(int index);

    bool isFacebookLogin();
    bool isGuestLogin();
    void sendCheckGoogleMailBind();

    void updateBindingMailStatus();

    void sendRequestNewGuest();

    void initMessageWindow();

    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* pEvent);

private:
	Layout* _mainLayout = nullptr;
    Layout* _loadingLayout = nullptr;
    LoadingBar* _loadingBar = nullptr;
    Text* _loadText = nullptr;
    Layout* _btnLayout = nullptr;
    Button* _facebookBtn = nullptr;
    Button* _guestLoginBtn = nullptr;
    Layout* _intoGameLayout = nullptr;
    Layout* _syncLayout = nullptr;
    ActionObject* _bulbLight = nullptr;
    ActionObject* _coinkira1 = nullptr;
    ActionObject* _coinkira2 = nullptr;
    ActionObject* _coinLight = nullptr;
    ActionObject* _casinoIn = nullptr;
    ActionObject* _casino = nullptr;
    ActionObject* _tapToPlay = nullptr;
    Layout* _globalLayout = nullptr;
    Button* _globalBtn = nullptr;
    Button* _languageBtns[LANGUAGE_TYPE_COUNT];
    Button* _languageSelectOK = nullptr;
    Text* _languageTexts[LANGUAGE_TYPE_COUNT];
    Layout* _termsOfService = nullptr;
    ImageView* _termsOfServiceEnter = nullptr;
    Node* _fireworkNode = nullptr;

	float _loadingPercent = 0.0f;
    bool _loadingStart = false;
    bool _loadingSuccess = false;
    bool _checkFacebookLogin = false;
    bool _facebookLoginFinish = false;
    bool _facebookLoginSuccess = false;

    ELoginStep _loginStep = CheckVersion;
	std::string _guestAuth;

    int _selectLanguageIndex = 1;

    ECheckLoginTType _checkLoginType = ECheckLoginTType::NONE_CHECK;
    int _accountGoogleMailStatus = 0;
    std::string _accountConnect;
    std::string _accountGameCenterName;
    std::string _guestBindingGoogleMail;

    CheckBox* _checkBindGoogleMail = nullptr;
    Text* _bindingGoogleMail = nullptr;

    Layout* _messageWindow = nullptr;
    Text* _messageContent = nullptr;
    Text* _messageTitle = nullptr;
    std::function<void()> _messageWindowCloseCallback = nullptr;

    Layout* _shortMessage = nullptr;
    Text* _shortMessageText = nullptr;
    cocostudio::ActionObject* _shortMessageAppear = nullptr;
};

#endif