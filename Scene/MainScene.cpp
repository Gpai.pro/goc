#include "MainScene.h"
#include "../UI/UIController.h"

USING_NS_CC;


MainScene::MainScene()
:_SlotMachine(nullptr)
{

}

MainScene::~MainScene()
{

}

Scene* MainScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
	if (!Layer::init())
	{
		return false;
	}


	UIController::getInstance()->RegisterParent(this);

    auto pKeybackListner = EventListenerKeyboard::create();

    pKeybackListner->onKeyReleased = CC_CALLBACK_2(MainScene::onKeyReleased, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(pKeybackListner, this);



	return true;
}



void MainScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* pEvent)
{
    if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    {
        UIController::getInstance()->goPrevious();
    }
}
