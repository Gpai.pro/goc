#include "RestartScene.h"
#include "../UI/UIController.h"
#include "../Gameplay/GameLogic.h"

USING_NS_CC;


RestartScene::RestartScene()
: _delayTime(1.f)
{

}

RestartScene::~RestartScene()
{

}

Scene* RestartScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = RestartScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool RestartScene::init()
{
    if (!Layer::init())
    {
        return false;
    }

    scheduleUpdate();
    return true;
}

void RestartScene::update(float dt)
{
    _delayTime -= dt;
    if (_delayTime <= 0)
        GameLogic::getInstance()->RestartGame();
}