#ifndef __RESTART_SCENE_H__
#define __RESTART_SCENE_H__

#include "cocos2d.h"

class RestartScene : public cocos2d::Layer
{
public:
    RestartScene();
    ~RestartScene();
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(RestartScene);

private:
    virtual void update(float dt) override;
private:
    float _delayTime;
};

#endif // __MAIN_SCENE_H__
