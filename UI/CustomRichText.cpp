#include "CustomRichText.h"
#include "../DataCenter/ExternalTable.h"

void CustomRichText::setCustomRichText(Text* text, float width, std::vector<int> stringDataIDs)
{
    RichText* richText = RichText::create();
    richText->setContentSize(Size(width, 0.0f));
    richText->ignoreContentAdaptWithSize(false);

    if (!text)
        return;

    auto fontSize = text->getFontSize();
    auto fontName = text->getFontName();
    auto anchorPoint = text->getAnchorPoint();
    auto position = text->getPosition();
    auto fontColor = text->getColor();
    int tag = 0;

    for (int i = 0; i < (int)stringDataIDs.size(); ++i)
    {
        StringData* pData = ExternalTable<StringData>::getRecordByID(stringDataIDs.at(i));

        if (!pData)
        {
            CCLOG("CustomRichText Error : StringData ID = %d", stringDataIDs.at(i));
            continue;
        }

        std::string stringData = pData->Content;

        int pos = stringData.find("%");

        if (pos == -1)
        {
            //RichElementText* elementText = RichElementText::create(++tag, Color3B::WHITE, 255, stringData, fontName, fontSize);
            //richText->pushBackElement(elementText);

            Text* text = Text::create();
            text->setString(stringData);
            text->setFontSize(fontSize);
            text->setFontName(fontName);
            text->setTextAreaSize(Size(width, 0.0f));
            text->ignoreContentAdaptWithSize(true);
            text->setColor(fontColor);
            RichElementCustomNode* customNode = RichElementCustomNode::create(++tag, Color3B::WHITE, 255, text);
            richText->pushBackElement(customNode);
        }
        else
        {
            while (stringData.find("%") != -1)
            {
                if (pos != 0)
                {
                    std::string newStr;
                    newStr.assign(stringData, 0, pos);
                    stringData.erase(stringData.begin(), stringData.begin() + pos);

                    //RichElementText* elementText = RichElementText::create(++tag, Color3B::WHITE, 255, newStr, fontName, fontSize);
                    //richText->pushBackElement(elementText);

                    Text* text = Text::create();
                    text->setString(newStr);
                    text->setFontSize(fontSize);
                    text->setFontName(fontName);
                    text->setTextAreaSize(Size(width, 0.0f));
                    text->ignoreContentAdaptWithSize(true);
                    text->setColor(fontColor);
                    RichElementCustomNode* customNode = RichElementCustomNode::create(++tag, Color3B::WHITE, 255, text);
                    richText->pushBackElement(customNode);
                }

                std::string color;
                color.assign(stringData, 0, 2);
                stringData.erase(stringData.begin(), stringData.begin() + 2);

                pos = stringData.find("%");
                std::string newStr;

                if (pos != -1)
                {
                    // 取目前這段顏色到下一段顏色
                    newStr.assign(stringData, 0, pos);
                    stringData.erase(stringData.begin(), stringData.begin() + pos);
                }
                else
                {
                    //  最後一段
                    newStr = stringData;
                    stringData.clear();
                }

                // 設顏色
                if (color == "%R")
                {
                    // Color Red
                    //RichElementText* elementText = RichElementText::create(++tag, Color3B::RED, 255, newStr, fontName, fontSize);
                    //richText->pushBackElement(elementText);

                    Text* text = Text::create();
                    text->setString(newStr);
                    text->setFontSize(fontSize);
                    text->setFontName(fontName);
                    text->setTextAreaSize(Size(width, 0.0f));
                    text->ignoreContentAdaptWithSize(true);
                    text->setColor(Color3B::RED);
                    RichElementCustomNode* customNode = RichElementCustomNode::create(++tag, Color3B::WHITE, 255, text);
                    richText->pushBackElement(customNode);
                }
                else if (color == "%G")
                {
                    // Color Green
                    //RichElementText* elementText = RichElementText::create(++tag, Color3B::GREEN, 255, newStr, fontName, fontSize);
                    //richText->pushBackElement(elementText);

                    Text* text = Text::create();
                    text->setString(newStr);
                    text->setFontSize(fontSize);
                    text->setFontName(fontName);
                    text->setTextAreaSize(Size(width, 0.0f));
                    text->ignoreContentAdaptWithSize(true);
                    text->setColor(Color3B::GREEN);
                    RichElementCustomNode* customNode = RichElementCustomNode::create(++tag, Color3B::GREEN, 255, text);
                    richText->pushBackElement(customNode);
                }
                else if (color == "%W")
                {
                    // Color White
                    //RichElementText* elementText = RichElementText::create(++tag, Color3B::GREEN, 255, newStr, "", 22);
                    //richText->pushBackElement(elementText);

                    Text* text = Text::create();
                    text->setString(newStr);
                    text->setFontSize(fontSize);
                    text->setFontName(fontName);
                    text->setTextAreaSize(Size(width, 0.0f));
                    text->ignoreContentAdaptWithSize(true);
                    text->setColor(Color3B::WHITE);
                    RichElementCustomNode* customNode = RichElementCustomNode::create(++tag, Color3B::GREEN, 255, text);
                    richText->pushBackElement(customNode);
                }
            }
        }

        richText->pushBackElement(RichElementNewLine::create(tag, Color3B::WHITE, 255));
    }

    if (richText)
    {
        auto parent = text->getParent();

        richText->setPosition(position);
        richText->setAnchorPoint(anchorPoint);
        text->setVisible(false);

        parent->addChild(richText, text->getLocalZOrder());
    }
}