#ifndef __CUSTOM_RICHTEXT_H__
#define __CUSTOM_RICHTEXT_H__

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "ui/UIRichText.h"

using namespace cocos2d;
using namespace ui;

class CustomRichText
{
public:
    static void setCustomRichText(Text* text, float width, std::vector<int> stringDataIDs);
};

#endif