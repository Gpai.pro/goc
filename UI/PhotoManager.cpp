#include "PhotoManager.h"
#include "../GlobalDefine/ImageDefine.h"
using namespace ui;

PhotoManager* PhotoManager::_instance = nullptr;


PhotoManager::PhotoManager()
: _isRequesting(false)
{
    _photoSpriteFrames.clear();
}

PhotoManager::~PhotoManager()
{
    //for (auto iter : _photoSpriteFrames)
    //{
    //    //auto texture = iter.second;
    //    CC_SAFE_RELEASE_NULL(iter.second);
    //}
    _photoSpriteFrames.clear();
}

PhotoManager* PhotoManager::getInstance()
{
    if (!_instance)
        _instance = new PhotoManager();
    return _instance;
}

void PhotoManager::destroyInstance()
{
    CC_SAFE_DELETE(_instance);
}

SpriteFrame* PhotoManager::getSpriteFrameByID(int ID)
{
    if (_photoSpriteFrames.find(ID) == _photoSpriteFrames.end())
        return nullptr;
    return _photoSpriteFrames.find(ID)->second;
}

void PhotoManager::setSpriteFrameByID(ui::ImageView* imageView, int ID, std::string url)
{
    auto sprite = dynamic_cast<ui::Scale9Sprite*>(imageView->getVirtualRenderer())->getSprite();
    auto texture = getSpriteFrameByID(ID);
    if (texture == nullptr)
    {
        if (url != "")
        {
            sprite->retain();
            if (url != "")
                RequestPhoto(ID, url,
                [sprite](SpriteFrame* spriteFrame){
                if (spriteFrame)
                {
                    sprite->setSpriteFrame(spriteFrame);
                }
                sprite->release();
            });
        }
        return;
    }

    sprite->setSpriteFrame(texture);
}

void PhotoManager::setSpriteFrameByID(Sprite* sprite, int ID, std::string url)
{
    auto texture = getSpriteFrameByID(ID);
    if (texture == nullptr)
    {
        if (url != "")
        {
            sprite->retain();
            if (url != "")
                RequestPhoto(ID, url,
                [sprite](SpriteFrame* spriteFrame){
                if (spriteFrame)
                {
                    sprite->setSpriteFrame(spriteFrame);
                }
                    
                sprite->release();
            });
        }
        else
        {
            auto spriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(GOCIMAGE(SharedFrame_GuestIcon));
            if (spriteFrame)
            {
                _photoSpriteFrames.insert(ID, spriteFrame);
                sprite->setSpriteFrame(spriteFrame);
            }
        }
        return;
    }

    sprite->setSpriteFrame(texture);
}

void PhotoManager::getSpriteFrameByID(int ID, std::function<void(SpriteFrame*)> callback, std::string url)
{
    auto texture = getSpriteFrameByID(ID);
    if (texture)
    {
        if (callback)
            callback(texture);
    }

    if (url != "")
    {
        RequestPhoto(ID, url, callback);
    }
    else
    {
        //SharedFrame_GuestIcon
        auto spriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(GOCIMAGE(SharedFrame_GuestIcon));
        if (spriteFrame)
        {
            _photoSpriteFrames.insert(ID, spriteFrame);
            callback(spriteFrame);
        }
    }
}

void PhotoManager::RequestPhoto(int ID, std::string url, std::function<void(SpriteFrame*)> callback)
{

    _photoRequests.insert(std::pair<int, std::string>(ID, url));
    _photoRequestCallback.push_back(std::pair<int, std::function<void(SpriteFrame*)>>(ID, callback));

    if (!_isRequesting)
        sendRequestPhoto();
}

void PhotoManager::sendRequestPhoto()
{
    if (_photoRequests.size() <= 0)
    {
        _isRequesting = false;
        return;
    }

    _isRequesting = true;
    auto iter = _photoRequests.begin();
    int ID = iter->first;
    std::string url = iter->second;

    log("request Facebook photo , ID : %d, url : %s", ID, url.c_str());
    cocos2d::network::HttpRequest* request = new (std::nothrow) cocos2d::network::HttpRequest();

    request->setUrl(url.c_str());
    request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(PhotoManager::ResponsePhoto, this, ID));
    request->setTag("GET PHOTO");
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();

    _photoRequests.erase(iter);
}

void PhotoManager::ResponsePhoto(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response, int ID)
{
    if (!response)
    {
        log("PhotoResponse - No Response");

        CallBackByID(ID, nullptr);
        sendRequestPhoto();
        return;
    }
    if (!response->isSucceed())
    {
        log("PhotoResponse - Response failed");
        log("PhotoResponse - Error buffer: %s", response->getErrorBuffer());

        CallBackByID(ID, nullptr);
        sendRequestPhoto();
        return;
    }

    std::vector<char> *buffer = response->getResponseData();
    const char* file_char = buffer->data();
    Image * image = new  Image();
    if (image->initWithImageData(reinterpret_cast<const unsigned char*>(&(buffer->front())), buffer->size()))
    {
        Texture2D* texture = new  Texture2D();
        if (texture->initWithImage(image))
        {
            /*if (_photos.find(ID) != _photos.end())
            {
            if (_photos.find(ID)->second != nullptr)
            CC_SAFE_RELEASE_NULL(_photos[ID]);
            }*/
            texture->autorelease();

            auto spriteFrame = SpriteFrame::createWithTexture(texture, Rect(Vec2::ZERO, texture->getContentSize()));
            _photoSpriteFrames.insert(ID, spriteFrame);

            CallBackByID(ID, spriteFrame);
        }
        else
            CC_SAFE_DELETE(texture);
    }
    CC_SAFE_RELEASE(image);

    sendRequestPhoto();
}

void PhotoManager::CallBackByID(int ID, SpriteFrame* spriteFrame)
{

    auto iter = _photoRequestCallback.begin();
    while (iter != _photoRequestCallback.end())
    {
        auto pair = *iter;
        if (ID == pair.first)
        {
            auto callback = pair.second;

            if (callback)
                callback(spriteFrame);
            iter = _photoRequestCallback.erase(iter);
            continue;
        }
        iter++;
    }
}