#pragma once

#include "cocos2d.h"
#include "../GlobalDefine/EnumType.h"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"
USING_NS_CC;

#define FBPHOTO_WIDTH 160.0f
#define FBPHOTO_HEIGHT 160.0f
#define FBPHOTO_SIZE Size(FBPHOTO_WIDTH, FBPHOTO_HEIGHT)

class PhotoManager : public Ref
{
public:
    PhotoManager();
    ~PhotoManager();

    static PhotoManager* getInstance();

    static void destroyInstance();

    void cleanPhotoSpriteFrames(){ _photoSpriteFrames.clear(); }

    void setSpriteFrameByID(ui::ImageView* imageView, int ID, std::string url = "");

    void setSpriteFrameByID(Sprite* imageView, int ID, std::string url = "");

    void getSpriteFrameByID(int ID, std::function<void(SpriteFrame*)> callback, std::string url);

private:
    SpriteFrame* getSpriteFrameByID(int ID);

    void RequestPhoto(int ID, std::string url, std::function<void(SpriteFrame*)> callback);

    void sendRequestPhoto();

    void ResponsePhoto(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response, int ID);
    void CallBackByID(int ID, SpriteFrame* spriteFrame);

private:

    static PhotoManager* _instance;

    Map<int, SpriteFrame*> _photoSpriteFrames;
    std::map<int, std::string> _photoRequests;
    std::vector<std::pair<int,std::function<void(SpriteFrame*)>>> _photoRequestCallback;

    bool _isRequesting;

};
