#include "UIBonusGamePoke.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataCenter/ExternalTable.h"
#include "../Network/MessageProcess.h"
#include "../Animation/CustomActions.h"
#include "AudioManager.h"
#include "UISlotUtils.h"
#include "UIController.h"
#include "UISlotPirate.h"
#include "UIMessage.h"
#include "../GlobalDefine/Player.h"

#define EFF_SLOTPIRATE_BONUSGAME_HIT 1103

UIBonusGamePoke* UIBonusGamePoke::create()
{
    UIBonusGamePoke* ui = new UIBonusGamePoke();
    if (ui && ui->init())
    {
        return ui;
    }

    CC_SAFE_DELETE(ui);
    return nullptr;
}

UIBonusGamePoke::UIBonusGamePoke()
{
    setUIShowType(EUISHOWTYPE::EUIST_BONUSGAME);
}

UIBonusGamePoke::~UIBonusGamePoke()
{

}

bool UIBonusGamePoke::openUI(int bonusGameID, int bet)
{
    _bonusGameID = bonusGameID;
    _bet = bet;

    if (!UIBasicUnit::openUI())
    {
        return false;
    }

    return true;
}

void UIBonusGamePoke::setGameInfo(int bonusGameID, int bet, bool autoSpin)
{
    _bonusGameID = bonusGameID;
    _bet = bet;
    _isAutoSpin = autoSpin;
}

void UIBonusGamePoke::notifyOpenUI()
{
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
}

void UIBonusGamePoke::notifyCloseUI()
{
    auto cSlotPirate = UIController::getInstance()->getController<UISlotPirate*>(EUITAG::EUITAG_SLOT_PIRATE);
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (!cSlotPirate->isFreeSpining())
        AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

void UIBonusGamePoke::destroyUI()
{

}

void UIBonusGamePoke::updateUI()
{
    setData();
}

void UIBonusGamePoke::UpdatePerSecond(float dt)
{
    if (!_isAutoSpin)
        return;

    ++_counter;

    switch (_step)
    {
    case eBonusGamePokeStep::START:
        if (_counter >= 3)
        {
            setStep(eBonusGamePokeStep::GAMEING);
            _counter = 0;
        }
        break;
    case eBonusGamePokeStep::INTO_ANOMATION:
        break;
    case eBonusGamePokeStep::GAMEING:
        if (_counter < 2)
            break;

        if (_pokeChance == 0)
        {
            setStep(eBonusGamePokeStep::RESULT);
            break;
        }

        if (_pokeChance <= 0 || _isWaitResponse)
            break;

        _touchIndex = RandomHelper::random_int(0, (int)_treasurePoint.size() - 1);

        while (_treasurePoint.at(_touchIndex)->getTag() == _buttonDoneTag)
        {
            _touchIndex = RandomHelper::random_int(0, (int)_treasurePoint.size() - 1);
        }
        _particlePos = _treasurePoint.at(_touchIndex)->getWorldPosition();

        _isWaitResponse = true;

        requestPokeResult();

        break;
    case eBonusGamePokeStep::RESULT:
       // setStep(eBonusGamePokeStep::END);
        break;
    case eBonusGamePokeStep::END:
        break;
    default:
        break;
    }
}

bool UIBonusGamePoke::createUI()
{
    // Start
    _bonusGameStart = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Pirate/UISlot_PirateBonusGame_Start.ExportJson"));
    _bonusGameStart->setVisible(true);
    _bonusGameStart->setTouchEnabled(true);
    _mainUINode->addChild(_bonusGameStart);

    _startButton = _bonusGameStart->getChildByName<Button*>("Button");
    _startButton->addTouchEventListener(CC_CALLBACK_2(UIBonusGamePoke::touchStartButton, this));


    _startContent = _bonusGameStart->getChildByName<Text*>("Text");
    _startContent->setString(safeGetStringData(10008));

    // Game
    _bonusGame = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Pirate/UISlot_PirateBonusGame_Game.ExportJson"));
    _mainUINode->addChild(_bonusGame);
    //this->scheduleUpdate();

    auto mapLayout = _bonusGame->getChildByName<Layout*>("Panel_bonus_scene");

    _picksLeft = mapLayout->getChildByName<TextBMFont*>("BMf_picksLeft");
    _totalWin = mapLayout->getChildByName<TextBMFont*>("BMf_totalWin");

    _pointLayout = _bonusGame->getChildByName<Layout*>("panel_btn_point");
    _pointLayout->setVisible(true);

    _boatActoin = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_Pirate/UISlot_PirateBonusGame_Game.ExportJson", "pic_boat");
    _boatActoin->setLoop(true);
    _boatActoin->play();

    _treeAction = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_Pirate/UISlot_PirateBonusGame_Game.ExportJson", "pic_tree");
    _treeAction->setLoop(true);
    _treeAction->play();

    _caveAction = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_Pirate/UISlot_PirateBonusGame_Game.ExportJson", "pic_cave_light");
    _caveAction->setLoop(true);
    _caveAction->play();

    _sharkAction = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_PirateBonusGame/UISlot_PirateBonusGame_Game.ExportJson", "pic_shark");
    _sharkAction->setLoop(true);
    _sharkAction->play();

    // End
    _bonusGameEnd = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Pirate/UISlot_PirateBonusGame_Result.ExportJson"));
    _bonusGameEnd->setVisible(false);
    _bonusGameEnd->setTouchEnabled(false);
    _mainUINode->addChild(_bonusGameEnd);

    _endButton = _bonusGameEnd->getChildByName<Button*>("Button");
    _endButton->addTouchEventListener(CC_CALLBACK_2(UIBonusGamePoke::touchEndButton, this));

    auto textLayout = _bonusGameEnd->getChildByName<Layout*>("text");
    _endContent = textLayout->getChildByName<Text*>("content");
    _endContent->setString(safeGetStringData(10011));

    _endResult = textLayout->getChildByName<TextBMFont*>("type_01");

    _endCalculate = textLayout->getChildByName<TextBMFont*>("type_02");

    // particle
    _particleDummy = _bonusGame->getChildByName<Layout*>("layout_particle_dummy");

    return true;
}

bool UIBonusGamePoke::init()
{
    return true;
}

void UIBonusGamePoke::setStep(eBonusGamePokeStep step)
{
    if (_step == step)
        return;

    _step = step;

    switch (step)
    {
    case eBonusGamePokeStep::START:
        break;
    case eBonusGamePokeStep::INTO_ANOMATION:
        break;
    case eBonusGamePokeStep::GAMEING:
        if (_pokeChance == 0)
        {
            setStep(eBonusGamePokeStep::RESULT);
            break;
        }

        _bonusGame->setVisible(true);
        setTraversalCascadeOpacityEnabled(_bonusGame, true);
        _bonusGame->setOpacity(0.0f);
        _bonusGame->runAction(Sequence::create(FadeIn::create(1.0f),
            CallFunc::create(CC_CALLBACK_0(Widget::setTouchEnabled, _bonusGame, true)), nullptr));

        _bonusGameStart->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create(CC_CALLBACK_0(Widget::setVisible, _bonusGameStart, false)), nullptr));
        //setTraversalCascadeOpacityEnabled(_bonusGameStart, true);
        break;
    case eBonusGamePokeStep::RESULT:
    {
        setEndResult(_totalWinCoin * _bet);
        setCalculate(_totalWinCoin, _bet);

        _bonusGameEnd->setVisible(true);
        setTraversalCascadeOpacityEnabled(_bonusGameEnd, true);
        _bonusGameEnd->setOpacity(0.0f);

        Sequence* actionsSeq(nullptr);
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.clear();
        arrayOfActions.pushBack(DelayTime::create(0.7f));
        arrayOfActions.pushBack(FadeIn::create(1.0f));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Widget::setTouchEnabled, _bonusGameEnd, true)));
        if (_isAutoSpin)
        {
            arrayOfActions.pushBack(DelayTime::create(1.0f));
            arrayOfActions.pushBack(CallFunc::create([this](){setStep(eBonusGamePokeStep::END); }));
        }
        actionsSeq = Sequence::create(arrayOfActions);

        _bonusGameEnd->runAction(actionsSeq);

        _bonusGame->runAction(Sequence::create(DelayTime::create(1.7f), CallFunc::create(CC_CALLBACK_0(Widget::setVisible, _bonusGame, false)), nullptr));
    }
        break;
    case eBonusGamePokeStep::END:
        Player::getInstance()->setCoin(_playerCoin);
        closeUI();
        break;
    default:
        break;
    }
}

void UIBonusGamePoke::Update(float dt)
{
    //if (_nowWinCoin == _totalWinCoin)
    //    return;

    //_nowWinCoin = GetEaseCount(_nowWinCoin, _totalWinCoin, 10, 1);

    //setTotalWin(_nowWinCoin);
}

int UIBonusGamePoke::getPokeChance()
{
    PokeGameData* pData = ExternalTable<PokeGameData>::getRecordByID(_bonusGameID);
    if (!pData)
    {
        CCLOG("UIBonusGamePoke :: Table BonusGame1Data ID = %d is Faild!!", _bonusGameID);
        return 0;
    }

    return pData->Chance;
}

int UIBonusGamePoke::getMaxChance()
{
    int chanceCount = 0;
    PokeGameData* pData = ExternalTable<PokeGameData>::getRecordByID(_bonusGameID);
    if (!pData)
    {
        CCLOG("UIBonusGamePoke :: Table BonusGame1Data ID = %d is Faild!!", _bonusGameID);
        return 0;
    }
    for (int i = 0; i < BONUSITEMCOUNT_MAX; ++i)
    {
        for (int j = 0; j < pData->items[i].Count; ++j)
            _coins.push_back(pData->items[i].Coin);

        chanceCount += pData->items[i].Count;
    }

    return chanceCount;
}

std::vector<int> UIBonusGamePoke::shuffle(int len)
{
    std::vector<int> indexVec;
    for (int i = 0; i < len; ++i)
        indexVec.push_back(i + 1);

    for (int i = len - 1; i > 0; --i)
    {
        int j = rand() % i + 1;
        int temp = indexVec[i];
        indexVec[i] = indexVec[j];
        indexVec[j] = temp;
    }

    std::vector<int> result(indexVec);
    return result;
}

void UIBonusGamePoke::pokeTreasure(int index, int coinCount)
{
    _treasureShine.at(index)->stopAllActions();
    _treasureShine.at(index)->runAction(FadeOut::create(0.5f));
    _treasureShine.at(index)->setTouchEnabled(false);

    _treasurePoint.at(index)->stopAllActions();
    _treasurePoint.at(index)->runAction(FadeOut::create(0.5f));
    _treasurePoint.at(index)->setTouchEnabled(false);
    _treasurePoint.at(index)->setTag(_buttonDoneTag);

    TextBMFont* winCoin = TextBMFont::create(Value(coinCount).asString(), "MachineFont.fnt");
    winCoin->setPosition(_treasurePoint.at(index)->getWorldPosition());
    _jumpMoneys.push_back(winCoin);
    _bonusGame->addChild(winCoin, 50);

    winCoin->setOpacity(0);
    winCoin->runAction(Spawn::createWithTwoActions(MoveBy::create(0.7f, Vec2(0.0f, 20.0f)), FadeIn::create(0.7f)));

    _bonusGame->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create(CC_CALLBACK_0(UIBonusGamePoke::updateTotalWin, this)), nullptr));
}

void UIBonusGamePoke::shinePointAction(Node* node)
{
    auto actionFadeIn = FadeIn::create(0.7f);
    auto actionFadeOut = FadeOut::create(0.7f);
    auto delayTime = DelayTime::create(0.3f);
    auto sequence = Sequence::create(actionFadeIn, delayTime, actionFadeOut, nullptr);

    node->runAction(RepeatForever::create(sequence));
}

void UIBonusGamePoke::setPickLeft(int left)
{
    _picksLeft->setString(Value(left).asString());
}

void UIBonusGamePoke::setTotalWin(int total)
{
    _totalWin->setString(Value(total).asString());
}

void UIBonusGamePoke::updateTotalWin()
{
    _totalWin->stopAllActions();
    CountTextBMFontToUint64Num* action = CountTextBMFontToUint64Num::create(1.0f, _nowWinCoin, _totalWinCoin);
    action->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    _totalWin->runAction(action);
}

void UIBonusGamePoke::erasePokeCoin(int coin)
{
    auto it = std::find(_coins.begin(), _coins.end(), coin);

    if (it != _coins.end())
    {
        _coins.erase(it);
    }
    else
    {
        _coins.pop_back();
    }
}

void UIBonusGamePoke::pokeDone()
{
    for (int i = 0; i < (int)_treasureShine.size(); ++i)
    {
        _treasureShine.at(i)->stopAllActions();
        _treasureShine.at(i)->runAction(FadeOut::create(0.5f));
        _treasureShine.at(i)->setTouchEnabled(false);
    }

    for (int i = 0; i < (int)_treasurePoint.size(); ++i)
    {
        if (_treasurePoint.at(i)->getTag() == _buttonDoneTag)
            continue;

        _treasurePoint.at(i)->setTouchEnabled(false);
        //_treasurePoint.at(i)->setColor(Color3B::GRAY);
        _treasurePoint.at(i)->runAction(TintTo::create(0.7f, Color3B::GRAY));


        TextBMFont* winCoin = TextBMFont::create(Value(_coins.back()).asString(), "MachineFont.fnt");
        winCoin->setPosition(_treasurePoint.at(i)->getWorldPosition());
        _jumpMoneys.push_back(winCoin);
        _bonusGame->addChild(winCoin, 100);
        _coins.pop_back();

        winCoin->setScale(0.7f);
        winCoin->setOpacity(0);
        auto spawn = Spawn::create(MoveBy::create(0.5f, Vec2(0.0f, 20.0f)), FadeTo::create(0.7f, 200), nullptr);
        winCoin->runAction(Sequence::create(DelayTime::create(0.7f), spawn, DelayTime::create(1.0f),
            CallFunc::create(CC_CALLBACK_0(UIBonusGamePoke::setStep, this, eBonusGamePokeStep::RESULT)), nullptr));
    }
}

void UIBonusGamePoke::touchTreasurePoint(Ref* pSender, Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_isAutoSpin)
    {
        UIController::getInstance()->getController<UIShortMessage*>(EUITAG::EUITAG_MESSAGE)->pushMessage(safeGetStringData(680));
        return;
    }

    if (_pokeChance <= 0 || _isWaitResponse)
        return;

    Widget* touch = (Widget*)pSender;
    _particlePos = touch->getTouchEndPosition();

    _isWaitResponse = true;

    _touchIndex = index;
    requestPokeResult();
}

void UIBonusGamePoke::touchStartButton(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_isAutoSpin)
    {
        UIController::getInstance()->getController<UIShortMessage*>(EUITAG::EUITAG_MESSAGE)->pushMessage(safeGetStringData(680));
        return;
    }

    setStep(eBonusGamePokeStep::GAMEING);
}

void UIBonusGamePoke::touchEndButton(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_isAutoSpin)
    {
        UIController::getInstance()->getController<UIShortMessage*>(EUITAG::EUITAG_MESSAGE)->pushMessage(safeGetStringData(680));
        return;
    }

    setStep(eBonusGamePokeStep::END);

    Player::getInstance()->setCoin(_playerCoin);
}

void UIBonusGamePoke::setEndResult(unsigned int result)
{
    _endResult->setString(cocos2d::StringUtils::toString(result));
}

void UIBonusGamePoke::setCalculate(unsigned int bonusWin, unsigned int bet)
{
    _endCalculate->setString(cocos2d::StringUtils::format("%d * %d = %d", bonusWin, bet, bonusWin * bet));
}

void UIBonusGamePoke::setData()
{
    // Start
    setStep(eBonusGamePokeStep::START);
    _pokeChance = 0;
    _nowWinCoin = 0;
    _totalWinCoin = 0;
    _counter = 0;

    _bonusGameStart->setOpacity(255);
    _bonusGameStart->setVisible(true);
    
    // Game
    _bonusGame->setOpacity(255);
    _bonusGame->setVisible(false);
    _bonusGame->setTouchEnabled(false);

    _pokeChance = getPokeChance();
    _picksLeft->setString(Value(_pokeChance).asString());
    _totalWin->setString(StringUtils::toString(_totalWinCoin));

    std::vector<int> indexVec = shuffle(getMaxChance());

    for (int i = 0; i < _pointLayout->getChildrenCount(); ++i)
        _pointLayout->getChildren().at(i)->setVisible(false);

    _treasurePoint.clear();
    _treasureShine.clear();
    for (int i = 0; i < (int)indexVec.size(); ++i)
    {
        std::string str = StringUtils::format("%02d", indexVec.at(i));
        Button* button = _pointLayout->getChildByName<Button*>("btn_point_" + str);
        button->setVisible(true);
        button->addTouchEventListener(CC_CALLBACK_2(UIBonusGamePoke::touchTreasurePoint, this, i));
        button->setTag(0);
        button->setTouchEnabled(true);
        button->setOpacity(255);
        button->setSoundID(EFF_NONE);
        _treasurePoint.push_back(button);

        ImageView* image = _pointLayout->getChildByName<ImageView*>("btn_point_glow_" + str);
        image->setVisible(true);
        image->setOpacity(0.0f);
        shinePointAction(image);
        _treasureShine.push_back(image);
    }

    for (int i = 0; i < (int)_jumpMoneys.size(); ++i)
    {
        _jumpMoneys.at(i)->removeFromParentAndCleanup(true);
    }
    _jumpMoneys.clear();

    // End
    _bonusGameEnd->setOpacity(255);
    _bonusGameEnd->setVisible(false);

    _endResult->setString("0");
    _endCalculate->setString("0 * 0 = 0");

    // particle
    stopTouchParticle();
}

void UIBonusGamePoke::requestPokeResult()
{
    RequestPokeResult(_pokeChance, _bonusGameID);
}

void UIBonusGamePoke::responsePokeResult(int action, int score, int bet, uint64 totalScore, uint64 playerCoin)
{
    _playerCoin = playerCoin;
    _isWaitResponse = false;

    --_pokeChance;
    setPickLeft(_pokeChance);

    int pokeCoin = 0;

    _nowWinCoin = _totalWinCoin;
    _totalWinCoin = totalScore;

    pokeTreasure(_touchIndex, score);
    erasePokeCoin(score);

    if (action == 1)//if (_pokeChance <= 0)
    {
        _bet = bet;
        _bonusGame->runAction(Sequence::create(DelayTime::create(1.5f), CallFunc::create(CC_CALLBACK_0(UIBonusGamePoke::pokeDone, this)), nullptr));
    }

    // play particle
    playTouchParticle(_particlePos);
    AudioManager::getInstance()->playEffectWithFileID(EFF_SLOTPIRATE_BONUSGAME_HIT, false);
}

void UIBonusGamePoke::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    int errorCode = 0;
    int action = 0;
    int gift = 0;
    int bet = 0;
    uint64 total_gift = 0;
    uint64 playerCoin = 0;

    if (!UISlotUtils::parsePokeResultResponse(json, errorCode, action, gift, bet, total_gift, playerCoin))
    {
        CCLOG("poke BonusGame response pasing error");
        return;
    }

    responsePokeResult(action, gift, bet, total_gift, playerCoin);
}

void UIBonusGamePoke::playTouchParticle(Vec2 pos)
{
    ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("Particle/piarte_bonusgame_fx01.plist");
    pParticle1->setPosition(pos);
    _particleDummy->addChild(pParticle1);
    ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("Particle/piarte_bonusgame_fx02.plist");
    pParticle2->setPosition(pos);
    _particleDummy->addChild(pParticle2);
}

void UIBonusGamePoke::stopTouchParticle()
{
    _particleDummy->removeAllChildren();
}