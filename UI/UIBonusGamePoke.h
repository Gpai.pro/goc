#ifndef __UI_BONUSGAMEPOKE_H__
#define __UI_BONUSGAMEPOKE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "../GlobalDefine//EnumType.h"
#include "../GlobalDefine/CustomTypeDef.h"

using namespace cocos2d;
using namespace ui;
using namespace cocostudio;

/*
step1: show bonus game animation
step2: message box show game description
step3: game play(show pick left, total win)
step4: show result
*/

class UIBonusGamePoke : public UIBasicUnit
{
public:
    static UIBonusGamePoke* create();

    UIBonusGamePoke();
    ~UIBonusGamePoke();

    bool openUI(int bonusGameID, int bet);

    void setGameInfo(int bonusGameID, int bet, bool autoSpin);

    virtual void notifyOpenUI() override;

    virtual void notifyCloseUI() override;

    virtual void destroyUI() override;

    virtual void updateUI() override;

    virtual void UpdatePerSecond(float dt) override;

    void onDispatchResponse(const std::string& requestTag, const std::string& json);

    void responsePokeResult(int action, int score, int bet, uint64 totalScore, uint64 playerCoin);

private:
    virtual bool createUI() override;

    bool init();

    void setStep(eBonusGamePokeStep step);

    // Game
    virtual void Update(float dt) override;

    int getPokeChance();

    int getMaxChance();

    std::vector<int> shuffle(int len);

    void pokeTreasure(int index, int coinCount);

    void shinePointAction(Node* node);

    void setPickLeft(int left);

    void setTotalWin(int total);

    void updateTotalWin();

    void erasePokeCoin(int coin);

    void pokeDone();

    void touchTreasurePoint(Ref* pSender, Widget::TouchEventType type, int index);

    // Start
    void touchStartButton(Ref* pSender, Widget::TouchEventType type);

    // End
    void touchEndButton(Ref* pSender, Widget::TouchEventType type);

    void setEndResult(unsigned int result);

    void setCalculate(unsigned int bonusWin, unsigned int bet);

    void setData();

    void requestPokeResult();

    // particle
    void playTouchParticle(Vec2 pos);
    void stopTouchParticle();

private:
    // Game
    Layout* _bonusGame = nullptr;

    Layout* _pointLayout = nullptr;

    std::vector<Button*> _treasurePoint;

    std::vector<ImageView*> _treasureShine;

    std::vector<int> _coins;

    TextBMFont* _picksLeft = nullptr;

    TextBMFont* _totalWin = nullptr;

    eBonusGamePokeStep _step = eBonusGamePokeStep::START;

    int _bonusGameID = 0;

    int _bet = 0;

    int _pokeChance = 0;

    uint64 _nowWinCoin = 0;

    uint64 _totalWinCoin = 0;

    uint64 _playerCoin = 0;

    const int _buttonDoneTag = 777;

    std::vector<TextBMFont*> _jumpMoneys;

    bool _isWaitResponse = false;

    int _touchIndex = 0;

    // Start
    Layout* _bonusGameStart = nullptr;

    Button* _startButton = nullptr;

    Text* _startContent = nullptr;

    // End
    Layout* _bonusGameEnd = nullptr;

    Button* _endButton = nullptr;

    TextBMFont* _endResult = nullptr;

    TextBMFont* _endCalculate = nullptr;

    Text* _endContent = nullptr;

    // Action
    ActionObject* _boatActoin = nullptr;

    ActionObject* _treeAction = nullptr;

    ActionObject* _caveAction = nullptr;

    ActionObject* _sharkAction = nullptr;

    // particle
    Layout* _particleDummy = nullptr;
    Vec2 _particlePos;
    int _slotID = 1;

    // Auto
    bool _isAutoSpin = false;
    int _counter = 0;
};

#endif