#include "UIConnectFBBenefit.h"
#include "../Auth/PlatformProxy.h"
#include "../GlobalDefine/GameFunc.h"
#include "AudioManager.h"
#include "../UI/UIController.h"

UIConnectFBBenefit* UIConnectFBBenefit::create()
{
    UIConnectFBBenefit* _ui = new UIConnectFBBenefit();

    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UIConnectFBBenefit::UIConnectFBBenefit()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}

UIConnectFBBenefit::~UIConnectFBBenefit()
{

}

void UIConnectFBBenefit::destroyUI()
{

}

void UIConnectFBBenefit::notifyCloseUI()
{

}

void UIConnectFBBenefit::updateUI()
{

}

bool UIConnectFBBenefit::init()
{
    return true;
}

bool UIConnectFBBenefit::createUI()
{
    _uiConnectFBBenefit = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/SharedUI/ConnectWithFBBenefit.ExportJson"));
    _uiConnectFBBenefit->setVisible(true);
    _uiConnectFBBenefit->setTouchEnabled(true);
    _mainUINode->addChild(_uiConnectFBBenefit);

    _titleBM = _uiConnectFBBenefit->getChildByName<TextBMFont*>("title");
    _titleBM->setString(safeGetStringData(654));
    _moneyBM = _uiConnectFBBenefit->getChildByName<TextBMFont*>("text_money");
    _moneyBM->setString(safeGetStringData(655));

    _connectFBBtn = _uiConnectFBBenefit->getChildByName<Button*>("Button_01");
    _connectFBBtn->setTouchEnabled(true);
    _connectFBBtn->addTouchEventListener(CC_CALLBACK_2(UIConnectFBBenefit::touchConnectFB, this));
    
    for (int i = 0; i < 3; ++i)
    {
        auto text = _uiConnectFBBenefit->getChildByName<Text*>(StringUtils::format("text_%d", i + 1));
        text->setString(safeGetStringData(651 + i));

        text->enableOutline(Color4B::BLACK, 3);
    }

    _laterBtn = _uiConnectFBBenefit->getChildByName<Button*>("Button_02");
    _laterBtn->setTouchEnabled(true);
    _laterBtn->addTouchEventListener(CC_CALLBACK_2(UIConnectFBBenefit::touchLater, this));
    _laterBtn->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    auto anim = ActionManagerEx::getInstance()->getActionByName("GUI/SharedUI/ConnectWithFBBenefit.ExportJson", "connect");
    anim->updateToFrameByTime(0);
    anim->play();

    return true;
}

void UIConnectFBBenefit::setMoney(int money)
{
    _moneyBM->setString(StringUtils::toString(money));
}

void UIConnectFBBenefit::touchConnectFB(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    PlatformProxy::getInstance()->facebookLogin();
    UIController::getInstance()->getController(EUITAG_LOADING)->openUI();

    closeUI();
}

void UIConnectFBBenefit::touchLater(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}