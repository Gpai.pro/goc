#ifndef __UI_CONNECTFBBENEFIT_H__
#define __UI_CONNECTFBBENEFIT_H__

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"

USING_NS_CC;

using namespace ui;
using namespace cocostudio;

class UIConnectFBBenefit : public UIBasicUnit
{
public:
    static UIConnectFBBenefit* create();

public:
    UIConnectFBBenefit();
    ~UIConnectFBBenefit();

    virtual void destroyUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

private:
    bool init();
    virtual bool createUI() override;

    void setMoney(int money);

    void touchConnectFB(Ref* sender, Widget::TouchEventType type);
    void touchLater(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _uiConnectFBBenefit = nullptr;
    TextBMFont* _moneyBM = nullptr;
    TextBMFont* _titleBM = nullptr;
    Button* _connectFBBtn = nullptr;
    Button* _laterBtn = nullptr;
};

#endif