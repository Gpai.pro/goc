#include "UIFBLoginBonus.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/EnumType.h"
#include "../Network/MessageProcess.h"
#include "../UI/UICoinExplosion.h"
#include "../UI/UIController.h"

UIFBLoginBonus* UIFBLoginBonus::create()
{
    UIFBLoginBonus* _ui = new UIFBLoginBonus();

    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UIFBLoginBonus::UIFBLoginBonus()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UIFBLoginBonus::~UIFBLoginBonus()
{

}

void UIFBLoginBonus::destroyUI()
{

}

void UIFBLoginBonus::notifyCloseUI()
{

}

void UIFBLoginBonus::updateUI()
{

}

bool UIFBLoginBonus::init()
{
    return true;
}

bool UIFBLoginBonus::createUI()
{
    _uiFBLoginBonus = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/SharedUI/FBLoginBonus.ExportJson"));
    _uiFBLoginBonus->setVisible(true);
    _uiFBLoginBonus->setTouchEnabled(true);
    _mainUINode->addChild(_uiFBLoginBonus);

    auto enjoy = _uiFBLoginBonus->getChildByName<TextBMFont*>("bmf_enjoy");
    enjoy->setString(safeGetStringData(663));

    auto money = _uiFBLoginBonus->getChildByName<TextBMFont*>("bmf_money");
    money->setString(safeGetStringData(664));

    auto bonus = _uiFBLoginBonus->getChildByName<TextBMFont*>("bmf_bonus");
    bonus->setString(safeGetStringData(665));

    auto hint = _uiFBLoginBonus->getChildByName<Text*>("txt_cn");
    hint->setString(safeGetStringData(666));

    _collectBtn = _uiFBLoginBonus->getChildByName<Button*>("btn_collect");
    _collectBtn->setTouchEnabled(true);
    _collectBtn->addTouchEventListener(CC_CALLBACK_2(UIFBLoginBonus::touchCollectBtn, this));

    auto mask = _uiFBLoginBonus->getChildByName<Layout*>("mask");
    mask->setTouchEnabled(true);
    mask->addTouchEventListener(CC_CALLBACK_2(UIFBLoginBonus::onTouchWindow, this));

    auto anim = ActionManagerEx::getInstance()->getActionByName("GUI/SharedUI/FBLoginBonus.ExportJson", "FBLoginBonus");
    anim->updateToFrameByTime(0);
    anim->play();

    return true;
}

void UIFBLoginBonus::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    rapidjson::Document document;
    document.Parse(json.c_str());
    if (!document.IsObject())
        return;

    int errorCode = -1;
    if (document["ERR_CODE"].IsInt())
        errorCode = document["ERR_CODE"].GetInt();
    if (errorCode != 1)
    {
        closeUI();
        return;
    }

    int clickID = -1;
    if (document["clickID"].IsInt())
        clickID = document["clickID"].GetInt();
    else
    {
        closeUI();
        return;
    }

    if (clickID != EClickEventEntry::ECE_FB_Connect_Bonus)
    {
        return;
    }

    auto controller = UIController::getInstance()->getController<UICoinExplode*>(EUITAG::EUITAG_COINEXPLOSION);
    controller->openUI(_collectBtn->getWorldPosition(), 10);
    closeUI();
}

void UIFBLoginBonus::touchCollectBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    RequestClickEvent(EClickEventEntry::ECE_FB_Connect_Bonus);
}

void UIFBLoginBonus::onTouchWindow(Ref* sender, Widget::TouchEventType type)
{
    Layout* pLayoutListener = static_cast<Layout*>(sender);
    //按下視窗如同按下按鈕
    if (type == Widget::TouchEventType::BEGAN)
    {
        _collectBtn->setHighlighted(true);
    }
    else if (type == Widget::TouchEventType::MOVED)
    {
        _collectBtn->setHighlighted(pLayoutListener->isHighlighted());
    }
    else if (type == Widget::TouchEventType::ENDED)
    {
        _collectBtn->setHighlighted(false);
        touchCollectBtn(nullptr, ui::Widget::TouchEventType::ENDED);
    }
    else if (type == Widget::TouchEventType::CANCELED)
    {
        _collectBtn->setHighlighted(false);
    }
}