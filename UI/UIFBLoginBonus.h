#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"

USING_NS_CC;

using namespace ui;
using namespace cocostudio;

class UIFBLoginBonus : public UIBasicUnit
{
public:
    static UIFBLoginBonus* create();

public:
    UIFBLoginBonus();
    ~UIFBLoginBonus();

    virtual void destroyUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

    virtual void onDispatchResponse(const std::string& requestTag, const std::string& json);

private:
    bool init();
    virtual bool createUI() override;

    void touchCollectBtn(Ref* sender, Widget::TouchEventType type);

    void onTouchWindow(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _uiFBLoginBonus = nullptr;
    Button* _collectBtn = nullptr;
};