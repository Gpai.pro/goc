#include "UIFeedback.h"
#include "UICommon.h"
#include "UIFeedbackResponseComponent.h"
#include "UIFeedbackResponseMail.h"
#include "UIFeedbackMailHint.h"
#include "UIController.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/Player.h"
#include "../DataManager/FeedbackManager.h"
#include "../Network/MessageProcess.h"
#include "AudioManager.h"

using namespace cocostudio;

UIFeedback* UIFeedback::create()
{
    UIFeedback* _ui = new UIFeedback();
    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UIFeedback::UIFeedback()
{
    setUIShowType(EUISHOWTYPE::EUIST_HIGHER_NORMAL);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}
    
UIFeedback::~UIFeedback()
{

}

void UIFeedback::destroyUI()
{

}
    
void UIFeedback::notifyOpenUI()
{
    UICommon::setFocusPage(0, _pageButtons, _pages);
}

void UIFeedback::notifyCloseUI()
{
    _pActionInput->updateToFrameByTime(0);
}

void UIFeedback::updateUI()
{
    // update red ball
    setRedBallVisible(FeedbackManager::getInstance()->isMailUnread());

    // update feedback component
    updateFeedbackComponent();

    if (FeedbackManager::getInstance()->isOverUnreadLimit() ||
        FeedbackManager::getInstance()->isOverDailySendLimit())
    {
        UICommon::setFocusPage(1, _pageButtons, _pages);
    }

    _pDownList->setVisible(false);
}

void UIFeedback::setRedBallVisible(bool visible)
{
    _pImgRedBall->setVisible(visible);
}

void UIFeedback::sendFeedbackMailSuccess()
{
    initFeedback();

    UICommon::setFocusPage(1, _pageButtons, _pages);
}

bool UIFeedback::init()
{
    return true;
}

bool UIFeedback::createUI()
{
    _uiFeedback = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UIFeedback/UIFeedback.ExportJson"));
    _uiFeedback->setTouchEnabled(true);
    _mainUINode->addChild(_uiFeedback);

    _pBtnClose = _uiFeedback->getChildByName<Button*>("btn_close");
    _pBtnClose->setTouchEnabled(true);
    _pBtnClose->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchCloseBtn, this));
    _pBtnClose->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    Button* btnFeedback = _uiFeedback->getChildByName<Button*>("btn_01");
    btnFeedback->setTouchEnabled(true);
    btnFeedback->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchFeedbackBtn, this));
    _pageButtons.push_back(btnFeedback);
    Button* btnResponse = _uiFeedback->getChildByName<Button*>("btn_02");
    btnResponse->setTouchEnabled(true);
    btnResponse->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchResponseBtn, this));
    _pageButtons.push_back(btnResponse);

    UICommon::initTabStyle(_pageButtons);

    auto mask = _uiFeedback->getChildByName<Layout*>("mask");
    mask->setTouchEnabled(true);
    mask->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchMask, this));

    _pLayoutFeedback = _uiFeedback->getChildByName<Layout*>("layout_feedback");
    _pages.push_back(_pLayoutFeedback);
    _pLayoutResponse = _uiFeedback->getChildByName<Layout*>("layout_response");
    _pages.push_back(_pLayoutResponse);

    _pImgRedBall = _uiFeedback->getChildByName<ImageView*>("img_redball");

    // feedback
    _pTxtDate = _pLayoutFeedback->getChildByName<Text*>("txt_date_content");

    _pTxtIssueType = _pLayoutFeedback->getChildByName<Text*>("txt_issue_type");
    _pTxtIssueType->setString(safeGetStringData(266));

    _pLayoutInoutListener = _pLayoutFeedback->getChildByName<Layout*>("layout_input_listener");
    _pLayoutInoutListener->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchInputIssue, this));
    _pLayoutInoutListener->setVisible(true);
    _pTextFieldIssue = _pLayoutFeedback->getChildByName<TextField*>("textfield_issue_input");
    _pTextFieldIssue->setTouchEnabled(false);
    _pTextFieldIssue->setPlaceHolder(safeGetStringData(267));
    _pTextFieldIssue->setTextVerticalAlignment(TextVAlignment::TOP);
    _pTextFieldIssue->setTextHorizontalAlignment(TextHAlignment::LEFT);
    _pTextFieldIssue->setTextAreaSize(Size(950.0f, 160.0f));
    _pTextFieldIssue->ignoreContentAdaptWithSize(false);
    _pTextFieldIssue->setContentSize(Size(950.0f, 160.0f));
    _pTextFieldIssue->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    _pTextFieldIssue->setMaxLengthEnabled(true);
    _pTextFieldIssue->setMaxLength(500);
    _pTextFieldIssue->setFontSize(24);
    _pTextFieldIssue->setColor(Color3B::WHITE);
    _pTextFieldIssue->setPlaceHolderColor(Color3B(110, 56, 128));
    _pTextFieldIssue->setString("");
    _pTextFieldIssue->addEventListener(CC_CALLBACK_2(UIFeedback::onTouchInputFeedback, this));

    _pActionInput = ActionManagerEx::getInstance()->getActionByName("GUI/UIFeedback/UIFeedback.ExportJson", "inputAnim");
    //_pActionInput->updateToFrameByTime(0);
    _pActionEndInput = ActionManagerEx::getInstance()->getActionByName("GUI/UIFeedback/UIFeedback.ExportJson", "endInputAnim");
    //_pActionEndInput->updateToFrameByTime(0);

    auto tempText = _pLayoutFeedback->getChildByName<Text*>("txt_title_01");
    tempText->setString(safeGetStringData(262));
    tempText = _pLayoutFeedback->getChildByName<Text*>("txt_title_02");
    tempText->setString(safeGetStringData(263));
    tempText = _pLayoutFeedback->getChildByName<Text*>("txt_date_title");
    tempText->setString(safeGetStringData(264));
    tempText = _pLayoutFeedback->getChildByName<Text*>("txt_issue_title");
    tempText->setString(safeGetStringData(265));

    _pBtnSend = _pLayoutFeedback->getChildByName<Button*>("btn_send");
    _pBtnSend->setTouchEnabled(true);
    _pBtnSend->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchSend, this));

    _pBtnSelectIssueType = _pLayoutFeedback->getChildByName<Button*>("btn_issue_type");
    _pBtnSelectIssueType->setTouchEnabled(true);
    _pBtnSelectIssueType->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchSelectIssueType, this));

    _pLayoutIssueListener = _pLayoutFeedback->getChildByName<Layout*>("layout_type_listener");
    _pLayoutIssueListener->setTouchEnabled(true);
    _pLayoutIssueListener->addTouchEventListener(CC_CALLBACK_2(UIFeedback::onTouchSelectIssueType, this));

    _pDownList = UIDownList::create(_pTxtIssueType, Size(400, 200));
    _pDownList->setPosition(Vec2(577, 258));
    _pDownList->setVisible(false);
    _pDownList->setTouchItemCallback(CC_CALLBACK_1(UIFeedback::selectIssueTypeCB, this));
    _pLayoutFeedback->addChild(_pDownList, 100);

    // init downlist
    for (int i = 1; i < (int)EFeedbackType::EFeedbackType_None; ++i)
        _pDownList->addItem(getIssueTypeString((EFeedbackType)i));
    //

    // response
    _pScrollViewResponse = _pLayoutResponse->getChildByName<ScrollView*>("scrollview_mail");
    _pScrollViewResponse->setBounceEnabled(true);

    tempText = _pLayoutResponse->getChildByName<Text*>("txt_hint");
    tempText->setString(safeGetStringData(288));
    tempText = _pLayoutResponse->getChildByName<Text*>("txt_no");
    tempText->setString(safeGetStringData(289));
    tempText = _pLayoutResponse->getChildByName<Text*>("txt_type");
    tempText->setString(safeGetStringData(290));
    tempText = _pLayoutResponse->getChildByName<Text*>("txt_issues");
    tempText->setString(safeGetStringData(291));
    tempText = _pLayoutResponse->getChildByName<Text*>("txt_status");
    tempText->setString(safeGetStringData(292));
    tempText = _pLayoutResponse->getChildByName<Text*>("txt_view");
    tempText->setString(safeGetStringData(293));

    initFeedback();

    return true;
}

void UIFeedback::initFeedback()
{
    setDate(Player::getInstance()->getGameTime());
    _issueType = EFeedbackType::EFeedbackType_None;
    _pTxtIssueType->setString(safeGetStringData(266));

    _pTextFieldIssue->setPlaceHolder(safeGetStringData(267));
    _pTextFieldIssue->setString("");
}

void UIFeedback::updateFeedbackComponent()
{
    int totalCount = FeedbackManager::getInstance()->getMailCount();

    _pScrollViewResponse->setInnerContainerSize(Size(_pScrollViewResponse->getContentSize().width, totalCount * COMPONENT_HEIGHT));
    _pScrollViewResponse->removeAllChildren();

    int scrollViewHeight = _pScrollViewResponse->getInnerContainerSize().height;
    for (int i = 0; i < totalCount; ++i)
    {
        FeedbackMailData data = FeedbackManager::getInstance()->getFeedbackMailByIndex(i + 1);

        auto com = UIFeedbackResponseComponent::create();
        com->setData(data.index, (EFeedbackType)data.type, data.issue, (EFeedbackStatus)data.status, data.isRead);
        com->setPosition(Vec2(0.0f, scrollViewHeight - ((i + 1) * COMPONENT_HEIGHT)));
        com->setCallback(CC_CALLBACK_1(UIFeedback::responseComponentCB, this));
        _pScrollViewResponse->addChild(com);
    }
}

void UIFeedback::setDate(GlobalTime globalTime)
{
    _feedbackTime = globalTime;
    //std::string weekday = getWeekdayString(globalTime.weekday);
    std::string month = getMonthString(globalTime.month);
    std::string day = getDayString(globalTime.day);
    std::string utc = getUTCString();
    _pTxtDate->setString(StringUtils::format("%s %s %d %d:%d", month.c_str(), day.c_str(), globalTime.year, globalTime.hour, globalTime.minute) + utc); // Mondy,May 9 2016 01:25(UTC+8)
}

std::string UIFeedback::getIssueTypeString(EFeedbackType type)
{
    switch (type)
    {
    case EFeedbackType_Account:
        return safeGetStringData(277);
        break;
    case EFeedbackType_Purchasing:
        return safeGetStringData(278);
        break;
    case EFeedbackType_Game:
        return safeGetStringData(280);
        break;
    case EFeedbackType_Other:
        return safeGetStringData(281);
        break;
    case EFeedbackType_None:
    default:
        return "";
        break;
    }
}

void UIFeedback::onTouchCloseBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}

void UIFeedback::onTouchFeedbackBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (FeedbackManager::getInstance()->isOverUnreadLimit() ||
        FeedbackManager::getInstance()->isOverDailySendLimit())
    {
        auto controller = UIController::getInstance()->getController<UIFeedbackMailHint*>(EUITAG_FEEDBACK_MAIL_HINT);
        controller->openUI();
        controller->setHintType(EFeedbackMailType::Hint);
        return;
    }

    Button* pBtn = (Button*)sender;
    UICommon::setFocusPage(pBtn->getTag(), _pageButtons, _pages);
}

void UIFeedback::onTouchResponseBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    Button* pBtn = (Button*)sender;
    UICommon::setFocusPage(pBtn->getTag(), _pageButtons, _pages);
}

void UIFeedback::onTouchInputIssue(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    if (_pActionInput->isPlaying() || _pActionEndInput->isPlaying())
        return;

    if (_attachIME)
        _pTextFieldIssue->detachWithIME();
    else
        _pTextFieldIssue->attachWithIME();
}

void UIFeedback::onTouchSend(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;         

    if (_attachIME)
        _pTextFieldIssue->detachWithIME();

    if (FeedbackManager::getInstance()->isOverUnreadLimit() ||
        FeedbackManager::getInstance()->isOverDailySendLimit())
    {
        auto controller = UIController::getInstance()->getController<UIFeedbackMailHint*>(EUITAG_FEEDBACK_MAIL_HINT);
        controller->openUI();
        controller->setHintType(EFeedbackMailType::Hint);
        return;
    }

    if (_issueType == EFeedbackType::EFeedbackType_None ||
        _pTextFieldIssue->getString().empty())
    {
        auto controller = UIController::getInstance()->getController<UIFeedbackMailHint*>(EUITAG_FEEDBACK_MAIL_HINT);
        controller->openUI();
        controller->setHintType(EFeedbackMailType::Send_Error);
        return;
    }

    std::string issue = _pTextFieldIssue->getString();

    // send request
    RequestSendFeedbackMail(_issueType, issue);
}

void UIFeedback::onTouchSelectIssueType(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _pDownList->setVisible(!_pDownList->isVisible());
}

void UIFeedback::responseComponentCB(int mailIndex)
{
    FeedbackMailData data = FeedbackManager::getInstance()->getFeedbackMailByMailIndex(mailIndex);
    auto controller = UIController::getInstance()->getController<UIFeedbackResponseMail*>(EUITAG_FEEDBACK_RESPONSE_MAIL);
    controller->openUI();
    controller->setIssue(data.issue);
    controller->setResponse(data.response);
    controller->setResponseTime(data.time);
    if (data.status == EFeedbackStatus::EFeedbackStatus_Replied)
    {
        controller->setResponseVisible(true);

        if (!data.isRead)
        {
            RequestPlayerReadMail(mailIndex);
            FeedbackManager::getInstance()->setMailRead(mailIndex);

            setRedBallVisible(FeedbackManager::getInstance()->isMailUnread());
        }
    }
    else
        controller->setResponseVisible(false);
}

void UIFeedback::onTouchInputFeedback(Ref *pSender, TextField::EventType type)
{
    switch (type)
    {
    case TextField::EventType::ATTACH_WITH_IME:
        _pActionInput->updateToFrameByTime(0);
        _pActionInput->play();
        _pTextFieldIssue->setTouchEnabled(false);
        _attachIME = true;
        log("UIFeedback ATTACH_WITH_IME");
        break;
    case TextField::EventType::DETACH_WITH_IME:
        _pActionEndInput->updateToFrameByTime(0);
        _pActionEndInput->play();
        _pTextFieldIssue->setTouchEnabled(true);
        _attachIME = false;
        log("UIFeedback DETACH_WITH_IME");
        break;
    case TextField::EventType::INSERT_TEXT:
    case TextField::EventType::DELETE_BACKWARD:
        break;
    default:
        break;
    }
}

void UIFeedback::onTouchMask(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_attachIME)
        _pTextFieldIssue->detachWithIME();
}

void UIFeedback::selectIssueTypeCB(int typeIndex)
{
    _issueType = (EFeedbackType)(typeIndex);
}