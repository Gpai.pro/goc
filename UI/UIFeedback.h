#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "UIDownList.h"
#include "../GlobalDefine/GameData.h"

USING_NS_CC;

using namespace ui;

class UIFeedback : public UIBasicUnit
{
public:
    static UIFeedback* create();

public:
    UIFeedback();
    ~UIFeedback();

    virtual void destroyUI() override;
    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;
    
    void setRedBallVisible(bool visible);
    void sendFeedbackMailSuccess();

private:
    bool init();
    virtual bool createUI() override;

    void initFeedback();
    void updateFeedbackComponent();
    void setDate(GlobalTime globalTime);
    std::string getIssueTypeString(EFeedbackType type);
private:
    void onTouchCloseBtn(Ref* sender, Widget::TouchEventType type);
    void onTouchFeedbackBtn(Ref* sender, Widget::TouchEventType type);
    void onTouchResponseBtn(Ref* sender, Widget::TouchEventType type);
    void onTouchInputIssue(Ref* sender, Widget::TouchEventType type);
    void onTouchSend(Ref* sender, Widget::TouchEventType type);
    void onTouchSelectIssueType(Ref* sender, Widget::TouchEventType type);
    void onTouchInputFeedback(Ref *pSender, TextField::EventType type);
    void onTouchMask(Ref* sender, Widget::TouchEventType type);
    void responseComponentCB(int mailIndex);
    void selectIssueTypeCB(int typeIndex);

private:
    Layout* _uiFeedback = nullptr;
    Button* _pBtnClose = nullptr;

    std::vector<Layout*> _pages;
    std::vector<Button*> _pageButtons;

    // feedback
    Layout* _pLayoutFeedback = nullptr;
    Text* _pTxtDate = nullptr;
    Text* _pTxtIssueType = nullptr;
    EditBox* _pEditBoxInputIssue = nullptr;
    TextField* _pTextFieldIssue = nullptr;
    Layout* _pLayoutInoutListener = nullptr;
    Button* _pBtnSend = nullptr;
    Button* _pBtnSelectIssueType = nullptr;
    UIDownList* _pDownList = nullptr;
    EFeedbackType _issueType = EFeedbackType::EFeedbackType_None;
    GlobalTime _feedbackTime;
    Layout* _pLayoutIssueListener = nullptr;

    cocostudio::ActionObject* _pActionInput = nullptr;
    cocostudio::ActionObject* _pActionEndInput = nullptr;
    bool _attachIME = false;

    // response
    Layout* _pLayoutResponse = nullptr;
    ScrollView* _pScrollViewResponse = nullptr;
    const int COMPONENT_HEIGHT = 73;
    ImageView* _pImgRedBall = nullptr;
};