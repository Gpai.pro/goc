#include "UIFeedbackMailHint.h"
#include "../GlobalDefine/GameFunc.h"
#include "AudioManager.h"

using namespace cocostudio;

UIFeedbackMailHint* UIFeedbackMailHint::create()
{
    UIFeedbackMailHint* _ui = new UIFeedbackMailHint();
    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UIFeedbackMailHint::UIFeedbackMailHint()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}

UIFeedbackMailHint::~UIFeedbackMailHint()
{

}

void UIFeedbackMailHint::destroyUI()
{

}

void UIFeedbackMailHint::notifyCloseUI()
{

}

void UIFeedbackMailHint::updateUI()
{

}

bool UIFeedbackMailHint::init()
{
    return true;
}

bool UIFeedbackMailHint::createUI()
{
    _mainLayout = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UIFeedback/UIFeedback_Hint.ExportJson"));
    _mainLayout->setTouchEnabled(true);
    _mainUINode->addChild(_mainLayout);

    auto okBtn = _mainLayout->getChildByName<Button*>("btn_ok");
    okBtn->setTouchEnabled(true);
    okBtn->addTouchEventListener(CC_CALLBACK_2(UIFeedbackMailHint::onTouchOKBtn, this));

    // success
    _pLayoutSuccess = _mainLayout->getChildByName<Layout*>("layout_success");
    _pLayoutSuccess->setVisible(false);
    _pTxtSuccessTitle = _pLayoutSuccess->getChildByName<Text*>("txt_title");

    auto successContent = _pLayoutSuccess->getChildByName<Text*>("txt_content");
    successContent->setTextAreaSize(Size(successContent->getContentSize().width, 0.0f));
    successContent->ignoreContentAdaptWithSize(true);
    successContent->setString(safeGetStringData(285));
    //

    // error
    _pLayoutError = _mainLayout->getChildByName<Layout*>("layout_error");
    _pLayoutError->setVisible(false);

    auto errorTitle = _pLayoutError->getChildByName<Text*>("txt_title");
    errorTitle->setString(safeGetStringData(287));
    auto errorContent = _pLayoutError->getChildByName<Text*>("txt_content");
    errorContent->setTextAreaSize(Size(errorContent->getContentSize().width, 0.0f));
    errorContent->ignoreContentAdaptWithSize(true);
    errorContent->setString(safeGetStringData(282));
    //

    _pLayoutHint = _mainLayout->getChildByName<Layout*>("layout_hint");
    _pLayoutHint->setVisible(false);
    auto hintContent = _pLayoutHint->getChildByName<Text*>("txt_content");
    hintContent->setTextAreaSize(Size(hintContent->getContentSize().width, 0.0f));
    hintContent->ignoreContentAdaptWithSize(true);
    hintContent->setString(safeGetStringData(283));

    return true;
}

void UIFeedbackMailHint::setHintType(EFeedbackMailType type)
{
    switch (type)
    {
    case Send_Success:
        _pLayoutSuccess->setVisible(true);
        _pLayoutError->setVisible(false);
        _pLayoutHint->setVisible(false);
        break;
    case Send_Error:
        _pLayoutSuccess->setVisible(false);
        _pLayoutError->setVisible(true);
        _pLayoutHint->setVisible(false);
        break;
    case Hint:
        _pLayoutSuccess->setVisible(false);
        _pLayoutError->setVisible(false);
        _pLayoutHint->setVisible(true);
        break;
    default:
        break;
    }
}

void UIFeedbackMailHint::setSendSuccessMailIndex(int index)
{
    _pTxtSuccessTitle->setString(safeGetStringData(287) + StringUtils::toString(index));
}

void UIFeedbackMailHint::onTouchCloseBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}

void UIFeedbackMailHint::onTouchOKBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}