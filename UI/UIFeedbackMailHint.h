#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"

USING_NS_CC;

using namespace ui;

enum EFeedbackMailType
{
    Send_Success,
    Send_Error,
    Hint,
};

class UIFeedbackMailHint : public UIBasicUnit
{
public:
    static UIFeedbackMailHint* create();

public:
    UIFeedbackMailHint();
    ~UIFeedbackMailHint();

    virtual void destroyUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

public:
    void setHintType(EFeedbackMailType type);
    void setSendSuccessMailIndex(int index);

private:
    bool init();
    virtual bool createUI() override;

private:
    void onTouchCloseBtn(Ref* sender, Widget::TouchEventType type);
    void onTouchOKBtn(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _mainLayout = nullptr;
    Button* _pBtnClose = nullptr;
    Text* _pTxtSuccessTitle = nullptr;

    Layout* _pLayoutSuccess = nullptr;
    Layout* _pLayoutError = nullptr;
    Layout* _pLayoutHint = nullptr;
};