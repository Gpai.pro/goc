#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/EnumType.h"

USING_NS_CC;

using namespace ui;

class UIFeedbackResponseComponent : public Widget
{
public:
    static UIFeedbackResponseComponent* create();
    UIFeedbackResponseComponent();
    ~UIFeedbackResponseComponent();

public:
    bool init();
    void setCallback(std::function<void(int)> callback){ _callback = callback; };
    void setData(const int& index, const EFeedbackType& type, const std::string& issue, const EFeedbackStatus& status, bool isRead);

private:
    void updateData();
    std::string getFeedbackTypeString(EFeedbackType type);
    std::string getFeedbackStatusString(EFeedbackStatus status);
    void onTouchGoBtn(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _mainLayout = nullptr;
    Button* _pBtnGo = nullptr;
    Text* _pTextIndex = nullptr;
    Text* _pTextType = nullptr;
    Text* _pTextIssue = nullptr;
    Text* _pTextStatus = nullptr;
    ImageView* _pImgRedBall = nullptr;
    std::function<void(int)> _callback = nullptr;
    
    int _index;
    EFeedbackType _type;
    EFeedbackStatus _status;
    std::string _issue;
};