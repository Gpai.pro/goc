#include "UIFeedbackResponseComponent.h"
#include "../GlobalDefine/GameFunc.h"
using namespace cocostudio;

UIFeedbackResponseComponent* UIFeedbackResponseComponent::create()
{
    UIFeedbackResponseComponent* _ui = new UIFeedbackResponseComponent();
    if (_ui && _ui->init())
    {
        _ui->autorelease();
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UIFeedbackResponseComponent::UIFeedbackResponseComponent()
{

}

UIFeedbackResponseComponent::~UIFeedbackResponseComponent()
{

}

bool UIFeedbackResponseComponent::init()
{
    _mainLayout = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UIFeedback/UIFeedback_Response_Component.ExportJson"));
    _mainLayout->setTouchEnabled(true);
    this->addChild(_mainLayout);
    this->setContentSize(_mainLayout->getContentSize());

    _pBtnGo = _mainLayout->getChildByName<Button*>("btn_go");
    _pBtnGo->setTouchEnabled(true);
    _pBtnGo->addTouchEventListener(CC_CALLBACK_2(UIFeedbackResponseComponent::onTouchGoBtn, this));
    _pTextIndex = _mainLayout->getChildByName<Text*>("txt_index");
    _pTextType = _mainLayout->getChildByName<Text*>("txt_type");
    _pTextIssue = _mainLayout->getChildByName<Text*>("txt_issues");
    _pTextStatus = _mainLayout->getChildByName<Text*>("txt_status");


    _pImgRedBall = _mainLayout->getChildByName<ImageView*>("img_redball");
    _pImgRedBall->setVisible(false);

    return true;
}

void UIFeedbackResponseComponent::setData(const int& index, const EFeedbackType& type, const std::string& issue, const EFeedbackStatus& status, bool isRead)
{
    _index = index;
    _type = type;
    _status = status;
    _issue = issue;

    bool unread = (_status == EFeedbackStatus::EFeedbackStatus_Replied && !isRead);
    _pImgRedBall->setVisible(unread);

    updateData();
}

void UIFeedbackResponseComponent::updateData()
{
    if (_index < 1000)
        _pTextIndex->setString(StringUtils::format("%04d", _index));
    else
        _pTextIndex->setString(StringUtils::format("%d", _index));
    _pTextType->setString(getFeedbackTypeString(_type));
    _pTextStatus->setString(getFeedbackStatusString(_status));

    // �n�_��[�W...
    if ((int)_issue.size() > 17)
    {
        _issue.resize(17);
        _issue.append("...");
    }
    else
    {

    }
    _pTextIssue->setString(_issue);
}

std::string UIFeedbackResponseComponent::getFeedbackTypeString(EFeedbackType type)
{
    switch (type)
    {
    case EFeedbackType_Account:
        return safeGetStringData(1051);
        break;
    case EFeedbackType_Purchasing:
        return safeGetStringData(1052);
        break;
    case EFeedbackType_Game:
        return safeGetStringData(1053);
        break;
    case EFeedbackType_Other:
        return safeGetStringData(1054);
        break;
    case EFeedbackType_None:
    default:
        return "";
        break;
    }
}

std::string UIFeedbackResponseComponent::getFeedbackStatusString(EFeedbackStatus status)
{
    switch (status)
    {
    case EFeedbackStatus_Unread:
        return safeGetStringData(294);
        break;
    case EFeedbackStatus_Confirming:
        return safeGetStringData(295);
        break;
    case EFeedbackStatus_Replied:
        return safeGetStringData(296);
        break;
    default:
        return "";
        break;
    }
}

void UIFeedbackResponseComponent::onTouchGoBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (this->_callback)
    {
        _pImgRedBall->setVisible(false);
        this->_callback(_index);
    }
}