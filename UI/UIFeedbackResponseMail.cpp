#include "UIFeedbackResponseMail.h"
#include "UIController.h"
#include "../GlobalDefine/GameFunc.h"
#include "AudioManager.h"

using namespace cocostudio;

UIFeedbackResponseMail* UIFeedbackResponseMail::create()
{
    UIFeedbackResponseMail* _ui = new UIFeedbackResponseMail();
    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UIFeedbackResponseMail::UIFeedbackResponseMail()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}

UIFeedbackResponseMail::~UIFeedbackResponseMail()
{

}

void UIFeedbackResponseMail::destroyUI()
{

}

void UIFeedbackResponseMail::notifyCloseUI()
{

}
    
void UIFeedbackResponseMail::updateUI()
{

}

bool UIFeedbackResponseMail::init()
{
    return true;
}
    
bool UIFeedbackResponseMail::createUI()
{
    _mainLayout = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UIFeedback/UIFeedback_Response_Mail.ExportJson"));
    _mainLayout->setTouchEnabled(true);
    _mainUINode->addChild(_mainLayout);

    auto tempText = _mainLayout->getChildByName<Text*>("txt_issue");
    tempText->setString(safeGetStringData(297));
    tempText = _mainLayout->getChildByName<Text*>("txt_response");
    tempText->setString(safeGetStringData(298));

    _pScrollViewIssue = _mainLayout->getChildByName<ScrollView*>("scrollview_issue");
    _pScrollViewIssue->setClippingEnabled(true);
    _pScrollViewIssue->setBounceEnabled(true);
    _pTxtIssue = (Text*)_pScrollViewIssue->getChildByName("txt_issue_content");
    _pTxtIssue->setTextAreaSize(Size(_pTxtIssue->getContentSize().width, 0));
    _pTxtIssue->ignoreContentAdaptWithSize(true);

    _pScrollViewResponse = _mainLayout->getChildByName<ScrollView*>("scrollview_response");
    _pScrollViewResponse->setClippingEnabled(true);
    _pScrollViewResponse->setBounceEnabled(true);
    _pTxtResponse = (Text*)_pScrollViewResponse->getChildByName("txt_response_content");
    _pTxtResponse->setTextAreaSize(Size(_pTxtResponse->getContentSize().width, 0));
    _pTxtResponse->ignoreContentAdaptWithSize(true);
    _pTxtResponseTime = _mainLayout->getChildByName<Text*>("txt_date");

    _pBtnClose = _mainLayout->getChildByName<Button*>("btn_close");
    _pBtnClose->setTouchEnabled(true);
    _pBtnClose->addTouchEventListener(CC_CALLBACK_2(UIFeedbackResponseMail::onTouchCloseBtn, this));
    _pBtnClose->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    _pBtnOK = _mainLayout->getChildByName<Button*>("btn_ok");
    _pBtnOK->setTouchEnabled(true);
    _pBtnOK->addTouchEventListener(CC_CALLBACK_2(UIFeedbackResponseMail::onTouchOKBtn, this));

    return true;
}

void UIFeedbackResponseMail::setIssue(std::string& issue)
{
    _pTxtIssue->setString(issue);
    auto textContentSize = _pTxtIssue->getContentSize();
    auto scrollviewContentSize = _pScrollViewIssue->getContentSize();

    if (textContentSize.height < scrollviewContentSize.height)
    {
        _pTxtIssue->setPosition(Vec2(0.0f, scrollviewContentSize.height));
        _pScrollViewIssue->setInnerContainerSize(scrollviewContentSize);
    }
    else
    {
        _pTxtIssue->setPosition(Vec2(0.0f, _pTxtIssue->getContentSize().height));
        _pScrollViewIssue->setInnerContainerSize(_pTxtIssue->getContentSize());
    }
}

void UIFeedbackResponseMail::setResponse(std::string& response)
{
    _pTxtResponse->setString(response);
    auto textContentSize = _pTxtResponse->getContentSize();
    auto scrollviewContentSize = _pScrollViewResponse->getContentSize();

    if (textContentSize.height < scrollviewContentSize.height)
    {
        _pTxtResponse->setPosition(Vec2(0.0f, scrollviewContentSize.height));
        _pScrollViewResponse->setInnerContainerSize(scrollviewContentSize);
    }
    else
    {
        _pTxtResponse->setPosition(Vec2(0.0f, _pTxtResponse->getContentSize().height));
        _pScrollViewResponse->setInnerContainerSize(_pTxtResponse->getContentSize());
    }
}

void UIFeedbackResponseMail::setResponseTime(GlobalTime time)
{
    std::string month = getMonthString(time.month);
    std::string day = getDayString(time.day);
    std::string utc = getUTCString();
    _pTxtResponseTime->setString(StringUtils::format("%s, %s %d %d:%d", month.c_str(), day.c_str(), time.year, time.hour, time.minute) + utc);
}

void UIFeedbackResponseMail::setResponseVisible(bool visible)
{
    _pTxtResponse->setVisible(visible);
    _pTxtResponseTime->setVisible(visible);
}

void UIFeedbackResponseMail::onTouchCloseBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}
    
void UIFeedbackResponseMail::onTouchOKBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}