#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "../GlobalDefine/GameData.h"

USING_NS_CC;

using namespace ui;

class UIFeedbackResponseMail : public UIBasicUnit
{
public:
    static UIFeedbackResponseMail* create();

public:
    UIFeedbackResponseMail();
    ~UIFeedbackResponseMail();

    virtual void destroyUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

public:
    void setIssue(std::string& issue);
    void setResponse(std::string& response);
    void setResponseTime(GlobalTime time);
    void setResponseVisible(bool visible);

private:
    bool init();
    virtual bool createUI() override;

private:
    void onTouchCloseBtn(Ref* sender, Widget::TouchEventType type);
    void onTouchOKBtn(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _mainLayout = nullptr;
    Button* _pBtnClose = nullptr;
    Button* _pBtnOK = nullptr;
    Text* _pTxtIssue = nullptr;
    Text* _pTxtResponse = nullptr;
    Text* _pTxtResponseTime = nullptr;
    ScrollView* _pScrollViewIssue = nullptr;
    ScrollView* _pScrollViewResponse = nullptr;
};