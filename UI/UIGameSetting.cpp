#include "UIGameSetting.h"
#include "../GlobalDefine/Player.h"
#include "AudioManager.h"
#include "../DataCenter/ExternalTable.h"
#include "../Auth/PlatformProxy.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Gameplay/GameLogic.h"
#include "UIInviteCode.h"
#include "UIController.h"
#include "UIMainMenu.h"
#include "UICommon.h"
#include "UIMessage.h"

using namespace cocostudio;

UIGameSetting* UIGameSetting::create()
{
    UIGameSetting* unit = new UIGameSetting();

    if (unit && unit->init())
    {
        return unit;
    }

    CC_SAFE_DELETE(unit);
    return nullptr;
}

bool UIGameSetting::init()
{
    return true;
}

UIGameSetting::UIGameSetting()
{
    setUIShowType(EUISHOWTYPE::EUIST_HIGHER_NORMAL);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}

UIGameSetting::~UIGameSetting()
{

}

void UIGameSetting::destroyUI()
{

}

void UIGameSetting::notifyCloseUI()
{

}

void UIGameSetting::updateUI()
{
    updateInfo();

    // Settting flag
    if (Player::getInstance()->checkHasData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::BGMusic]))
    {
        Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::BGMusic], _isBGMusicOn);
    }
    setSound(_isBGMusicOn);

    if (Player::getInstance()->checkHasData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::EffectMusic]))
    {
        Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::EffectMusic], _isEffectOn);
    }
    setEffect(_isEffectOn);

    if (Player::getInstance()->checkHasData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Jackpot]))
    {
        Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Jackpot], _isJackpotOn);
    }
    setJackpotAlerm(_isJackpotOn);

    if (Player::getInstance()->checkHasData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Notifications]))
    {
        Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Notifications], _isNotificationOn);
    }
    setNotifications(_isNotificationOn);

    setLanguageBtnText(getLanguageStringDataIDByType(Player::getInstance()->getLanguageType()));

    setVersion(PlatformProxy::getInstance()->getVersionName());
    setMyID(Player::getInstance()->getID());
    setInviteCode(Player::getInstance()->getInviteCode());

    // check version
    if (PlatformProxy::getInstance()->getVersionCode() < Player::getInstance()->getLatestVersionCode())
        _updateBtn->setEnabled(true);
    else
        _updateBtn->setEnabled(false);
}

bool UIGameSetting::createUI()
{
    _uiGameSetting = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UIGameSetting/UIGameSetting.ExportJson"));
    _uiGameSetting->setTouchEnabled(true);
    _mainUINode->addChild(_uiGameSetting);

    auto mainButton = _uiGameSetting->getChildByName<Layout*>("Window")->getChildByName<Layout*>("Bookmark_button");
    auto _pageInfoBtn = mainButton->getChildByName<Button*>("Bookmark_button_01");
    _pageInfoBtn->setTitleText(safeGetStringData(251));
    _pageInfoBtn->setTouchEnabled(true);
    _pageInfoBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchInfoPage, this));
    _pageButtons.push_back(_pageInfoBtn);

    auto _pageSettingBtn = mainButton->getChildByName<Button*>("Bookmark_button_02");
    _pageSettingBtn->setTitleText(safeGetStringData(255));
    _pageSettingBtn->setTouchEnabled(true);
    _pageSettingBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchSettingPage, this));
    _pageButtons.push_back(_pageSettingBtn);

    _closeBtn = _uiGameSetting->getChildByName<Layout*>("Window")->getChildByName<Button*>("window_x_button");
    _closeBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchClose, this));
    _closeBtn->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    auto window = _uiGameSetting->getChildByName<Layout*>("Window");
    auto btnWords = window->getChildByName<Layout*>("Bookmark_button_words");

    // Page Info
    auto _pageInfo = _uiGameSetting->getChildByName<Layout*>("Page_Info");
    _pageInfo->setTouchEnabled(true);
    _pages.push_back(_pageInfo);

    auto version = _pageInfo->getChildByName<Text*>("text_version");
    version->setString(safeGetStringData(254));
    _versionCode = _pageInfo->getChildByName<Text*>("text_version_code");

    auto pageInfoWords = _pageInfo->getChildByName<Layout*>("panel_words");

    _fbName = pageInfoWords->getChildByName<Text*>("text_fb");
    _guest = pageInfoWords->getChildByName<Text*>("text_guest");
    _guest->setString(safeGetStringData(222));
    _guestMail = pageInfoWords->getChildByName<Text*>("text_guest_mail");
    _guestHint = pageInfoWords->getChildByName<Text*>("text_guest_hint");
    _guestHint->setString(safeGetStringData(300));

    auto id = pageInfoWords->getChildByName<Text*>("text_id");
    id->setString(safeGetStringData(252));
    _myID = pageInfoWords->getChildByName<Text*>("text_id_value");

    auto inviteCode = pageInfoWords->getChildByName<Text*>("text_invite_code");
    inviteCode->setString(safeGetStringData(253));
    _inviteCode = pageInfoWords->getChildByName<Text*>("text_invite_code_value");

    auto pageInfoBtns = _pageInfo->getChildByName<Layout*>("panel_button");
    _logoutBtn = pageInfoBtns->getChildByName<Button*>("btn_logout");
    _logoutBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchLogout, this));

    _fbConnect = pageInfoBtns->getChildByName<Button*>("btn_fb_connect");
    _fbConnect->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchConnectFB, this));

    _inviteCodeEvent = pageInfoBtns->getChildByName<Button*>("btn_invite_event");
    _inviteCodeEvent->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchInviteCodeConfirm, this));

    _copyIDBtn = pageInfoBtns->getChildByName<Button*>("btn_copy");
    _copyIDBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchCopyID, this));

    _updateBtn = pageInfoBtns->getChildByName<Button*>("btn_update");
    _updateBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchUpdate, this));

    // Page Setting
    auto _pageSetting = _uiGameSetting->getChildByName<Layout*>("Page_Settings");
    _pageSetting->setTouchEnabled(true);
    _pages.push_back(_pageSetting);

    _settingScrollView = _pageSetting->getChildByName<ScrollView*>("Page_Settings_scrowing");
    _settingScrollView->setBounceEnabled(true);

    auto pageSettingBtns = static_cast<Layout*>(_settingScrollView->getChildByName("panel_button"));
    _musicSwitch = pageSettingBtns->getChildByName<Button*>("btn_music");
    _musicSwitch->setTouchEnabled(true);
    _musicSwitch->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchMusicSwitch, this));

    _effectSwitch = pageSettingBtns->getChildByName<Button*>("btn_effect");
    _effectSwitch->setTouchEnabled(true);
    _effectSwitch->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchEffectSwitch, this));

    _jackpotAlarmSwitch = pageSettingBtns->getChildByName<Button*>("btn_jackpot");
    _jackpotAlarmSwitch->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchJackpotSwitch, this));

    _notificationSwitch = pageSettingBtns->getChildByName<Button*>("btn_notification");
    _notificationSwitch->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchNotificationSwitch, this));

    _languageBtn = pageSettingBtns->getChildByName<Button*>("btn_language");
    _languageBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchLanguageBtn, this));
    _languageBtn->getTitleRenderer()->enableOutline(Color4B::BLACK, 3);

    _rateBtn = pageSettingBtns->getChildByName<Button*>("btn_rate_us");
    _rateBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchRateBtn, this));

    _fansBtn = pageSettingBtns->getChildByName<Button*>("btn_fans");
    _fansBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchFansPageBtn, this));

    _feedbackBtn = pageSettingBtns->getChildByName<Button*>("btn_feedback");
    _feedbackBtn->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchFeedbackBtn, this));

    auto  pageSettingButtonWords = static_cast<Layout*>(_settingScrollView->getChildByName("panel_words"));
    auto music = pageSettingButtonWords->getChildByName<Text*>("text_music");
    music->setString(safeGetStringData(256));
    auto effect = pageSettingButtonWords->getChildByName<Text*>("text_effect");
    effect->setString(safeGetStringData(221));
    auto jackpot = pageSettingButtonWords->getChildByName<Text*>("text_jackpot_alarm");
    jackpot->setString(safeGetStringData(257));
    auto notifications = pageSettingButtonWords->getChildByName<Text*>("text_notifications");
    notifications->setString(safeGetStringData(258));
    auto language = pageSettingButtonWords->getChildByName<Text*>("text_language");
    language->setString(safeGetStringData(223));
    auto rate = pageSettingButtonWords->getChildByName<Text*>("text_rate_us");
    rate->setString(safeGetStringData(259));
    auto fansPage = pageSettingButtonWords->getChildByName<Text*>("text_fan_page");
    fansPage->setString(safeGetStringData(261));
    auto feedback = pageSettingButtonWords->getChildByName<Text*>("text_feedback");
    feedback->setString(safeGetStringData(260));

    // Language
    _pageLanguage = _pageSetting->getChildByName<Layout*>("layout_language");
    _pageLanguage->setVisible(false);
    auto languageLayout = _pageLanguage->getChildByName<Layout*>("i18N");
    for (int i = 0; i < LANGUAGE_TYPE_COUNT; ++i)
    {
        _languageBtns[i] = languageLayout->getChildByName<Button*>(StringUtils::format("btn_language_%d", i + 1));
        _languageTexts[i] = languageLayout->getChildByName<Text*>(StringUtils::format("txt_language_%d", i + 1));
        _languageTexts[i]->setString(safeGetStringData(LANGUAGE_STRINGID_BEGIN + i));
        _languageTexts[i]->enableOutline(Color4B::BLACK, 4);

        auto touchPanel = languageLayout->getChildByName<Layout*>(StringUtils::format("layout_touch_%d", i + 1));
        touchPanel->setTouchEnabled(true);
        touchPanel->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchLanguageSelect, this, i));
        touchPanel->setSoundID(EFF_COMMON_BUTTON_CLICK);
    }
    auto languageOK = languageLayout->getChildByName<Button*>("btn_ok");
    languageOK->setTouchEnabled(true);
    languageOK->addTouchEventListener(CC_CALLBACK_2(UIGameSetting::touchLanguageOK, this));

    UICommon::initTabStyle(_pageButtons);

    return true;
}

void UIGameSetting::updateInfo()
{
    bool fbLoginSuccess = false;
    fbLoginSuccess = PlatformProxy::getInstance()->isFacebookLoginSuccess();

    _fbConnect->setVisible(!fbLoginSuccess);
    _logoutBtn->setVisible(fbLoginSuccess);
    _fbName->setVisible(fbLoginSuccess);
    _guest->setVisible(!fbLoginSuccess);
    _guestMail->setVisible(!fbLoginSuccess);
    _guestHint->setVisible(!fbLoginSuccess);
    
    if (fbLoginSuccess)
    {
        std::string name = "";
        name = PlatformProxy::getInstance()->getFacebookName();
        
        _fbName->setString(name);
    }
    else
    {
        std::string account = PlatformProxy::getInstance()->getConnectAccount();

        if (account.empty())
        {
            _guestMail->setString(safeGetStringData(23));
        }
        else
        {
            _guestMail->setString(account);
        }
    }

    UICommon::setFocusPage(0, _pageButtons, _pages);
}

void UIGameSetting::setVersion(std::string version)
{
    _versionCode->setString(version);
}

void UIGameSetting::setMyID(std::string id)
{
    int data = Value(id).asInt();
    _myID->setString(StringUtils::format("%010d", data));
}

void UIGameSetting::setInviteCode(std::string code)
{
    _inviteCode->setString(code);
}

void UIGameSetting::setSound(bool soundSwitch)
{
    _isBGMusicOn = soundSwitch;

    _musicSwitch->setBright(_isBGMusicOn);

    AudioManager::getInstance()->setBGMusicSwitch(_isBGMusicOn);
}

void UIGameSetting::setEffect(bool effectSwitch)
{
    _isEffectOn = effectSwitch;

    _effectSwitch->setBright(_isEffectOn);

    AudioManager::getInstance()->setEffectSwitch(_isEffectOn);
}

void UIGameSetting::setJackpotAlerm(bool jackpotSwitch)
{
    _isJackpotOn = jackpotSwitch;

    _jackpotAlarmSwitch->setBright(_isJackpotOn);

    UIController::getInstance()->getController<UIMainMenu*>(EUITAG::EUITAG_MAINMENU)->setJackpotSwitch(_isJackpotOn);
}

void UIGameSetting::setNotifications(bool notifySwitch)
{
    _isNotificationOn = notifySwitch;

    _notificationSwitch->setBright(_isNotificationOn);

    Player::getInstance()->setNotificationEnable(_isNotificationOn);
}

void UIGameSetting::setLanguageBtnText(int stringDataID)
{
    _languageBtn->setTitleText(safeGetStringData(stringDataID));
}

void UIGameSetting::initSelectLanguage()
{
    LanguageType type = Player::getInstance()->getLanguageType();

    int index = getLanguageTypeIndexByType(type);

    setLanguageBtnBright(index);
}

void UIGameSetting::setLanguageBtnBright(int index)
{
    if (index < 0 || index > LANGUAGE_TYPE_COUNT)
        return;

    _selectLanguageIndex = index;

    for (int i = 0; i < LANGUAGE_TYPE_COUNT; ++i)
    {
        if (i == _selectLanguageIndex)
            _languageBtns[i]->setBright(false);
        else
            _languageBtns[i]->setBright(true);
    }
}

void UIGameSetting::touchInfoPage(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    Button* pBtn = (Button*)sender;
    UICommon::setFocusPage(pBtn->getTag(), _pageButtons, _pages);
}

void UIGameSetting::touchSettingPage(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    Button* pBtn = (Button*)sender;
    UICommon::setFocusPage(pBtn->getTag(), _pageButtons, _pages);
}

void UIGameSetting::touchClose(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::BGMusic], _isBGMusicOn);
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::EffectMusic], _isEffectOn);
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Jackpot], _isJackpotOn);
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Notifications], _isNotificationOn);

    this->closeUI();
}

void UIGameSetting::touchLogout(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FacebookAuth], "");
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FacebookLogin], false);
    Player::getInstance()->setLoginType(ELoginType::ELT_None);

    PlatformProxy::getInstance()->facebookLogout();
    PlatformProxy::getInstance()->facebookRestart();

    GameLogic::getInstance()->EnterRestartScene();
}

void UIGameSetting::touchConnectFB(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    PlatformProxy::getInstance()->facebookLogin();
    UIController::getInstance()->getController(EUITAG_LOADING)->openUI();
}

void UIGameSetting::touchCopyID(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    int data = Value(Player::getInstance()->getID()).asInt();
    PlatformProxy::getInstance()->copyToClipboard(StringUtils::format("%010d", data));
    UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE)->pushMessage(safeGetStringData(91));
}

void UIGameSetting::touchUpdate(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    PlatformProxy::getInstance()->openStore();
}

void UIGameSetting::touchInviteCodeConfirm(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIController::getInstance()->getController<UIInviteCode*>(EUITAG::EUITAG_INVITE_CODE)->openUI();
}

void UIGameSetting::touchMusicSwitch(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    
    setSound(!_isBGMusicOn);
}

void UIGameSetting::touchEffectSwitch(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    setEffect(!_isEffectOn);
}

void UIGameSetting::touchJackpotSwitch(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    setJackpotAlerm(!_isJackpotOn);
}

void UIGameSetting::touchNotificationSwitch(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    setNotifications(!_isNotificationOn);
}

void UIGameSetting::touchLanguageBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    initSelectLanguage();
    _pageLanguage->setVisible(true);
}

void UIGameSetting::touchRateBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
 
    PlatformProxy::getInstance()->openStore();
}

void UIGameSetting::touchFansPageBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    Application::getInstance()->openURL("https://www.facebook.com/godofcasino/");
}

void UIGameSetting::touchFeedbackBtn(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //PlatformProxy::getInstance()->sendEmail();
    UIController::getInstance()->getController(EUITAG_FEEDBACK)->openUI();
}

void UIGameSetting::touchLanguageSelect(Ref* sender, Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (index < 0 || index > LANGUAGE_TYPE_COUNT)
        return;

    setLanguageBtnBright(index);
}

void UIGameSetting::touchLanguageOK(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    // �s��
    LanguageType lType = LANGUAGE_TYPE[_selectLanguageIndex];
    int oriType;
    Player::getInstance()->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Language], oriType);
    Player::getInstance()->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::Language], (int)lType);
    Player::getInstance()->setLanguageType(lType);

    //_pageLanguage->setVisible(false);

    if ((LanguageType)oriType != lType)
        GameLogic::getInstance()->EnterRestartScene();

    _pageLanguage->setVisible(false);
}

void UIGameSetting::touchCloseLanguage(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _pageLanguage->setVisible(false);
}