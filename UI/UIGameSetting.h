#ifndef __UI_GAMESETTING_H__
#define __UI_GAMESETTING_H__

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "../GlobalDefine/GameConst.h"

USING_NS_CC;

using namespace ui;

class UIGameSetting : public UIBasicUnit
{
public:
    static UIGameSetting* create();

public:
    UIGameSetting();
    ~UIGameSetting();

    virtual void destroyUI() override;

    virtual void notifyCloseUI() override;

    virtual void updateUI() override;

    void updateInfo();

private:
    bool init();

    virtual bool createUI() override;

    void setVersion(std::string version);
    void setMyID(std::string id);
    void setInviteCode(std::string code);

    void setSound(bool soundSwitch);
    void setEffect(bool effectSwitch);
    void setJackpotAlerm(bool jackpotSwitch);
    void setNotifications(bool notifySwitch);
    void setLanguageBtnText(int stringDataID);

    void initSelectLanguage();
    void setLanguageBtnBright(int index);

    void touchInfoPage(Ref* sender, Widget::TouchEventType type);

    void touchSettingPage(Ref* sender, Widget::TouchEventType type);

    void touchClose(Ref* sender, Widget::TouchEventType type);

    void touchLogout(Ref* sender, Widget::TouchEventType type);

    void touchConnectFB(Ref* sender, Widget::TouchEventType type);

    void touchCopyID(Ref* sender, Widget::TouchEventType type);

    void touchUpdate(Ref* sender, Widget::TouchEventType type);

    void touchInviteCodeConfirm(Ref* sender, Widget::TouchEventType type);

    void touchMusicSwitch(Ref* sender, Widget::TouchEventType type);

    void touchEffectSwitch(Ref* sender, Widget::TouchEventType type);

    void touchJackpotSwitch(Ref* sender, Widget::TouchEventType type);

    void touchNotificationSwitch(Ref* sender, Widget::TouchEventType type);

    void touchLanguageBtn(Ref* sender, Widget::TouchEventType type);

    void touchRateBtn(Ref* sender, Widget::TouchEventType type);

    void touchFansPageBtn(Ref* sender, Widget::TouchEventType type);

    void touchFeedbackBtn(Ref* sender, Widget::TouchEventType type);

    void touchLanguageSelect(Ref* sender, Widget::TouchEventType type, int index);

    void touchLanguageOK(Ref* sender, Widget::TouchEventType type);

    void touchCloseLanguage(Ref* sender, Widget::TouchEventType type);

private:

    Layout* _uiGameSetting = nullptr;

    std::vector<Layout*> _pages;
    std::vector<Button*> _pageButtons;

    Button* _closeBtn = nullptr;

    // Page Info
    Text* _fbName = nullptr;

    Text* _guest = nullptr;

    Text* _guestMail = nullptr;

    Text* _guestHint = nullptr;

    Text* _myID = nullptr;

    Text* _inviteCode = nullptr;

    Text* _versionCode = nullptr;

    Button* _fbConnect = nullptr;

    Button* _copyIDBtn = nullptr;

    Button* _logoutBtn = nullptr;

    Button* _inviteCodeEvent = nullptr;

    Button* _updateBtn = nullptr;

    // Page Setting
    ScrollView* _settingScrollView = nullptr;

    Button* _musicSwitch = nullptr;

    Button* _effectSwitch = nullptr;

    Button* _jackpotAlarmSwitch = nullptr;

    Button* _notificationSwitch = nullptr;

    Button* _languageBtn = nullptr;

    Button* _rateBtn = nullptr;

    Button* _fansBtn = nullptr;

    Button* _feedbackBtn = nullptr;

    bool _isBGMusicOn = true;

    bool _isEffectOn = true;

    bool _isJackpotOn = true;

    bool _isNotificationOn = true;

    // language
    Layout* _pageLanguage = nullptr;
    Button* _languageBtns[LANGUAGE_TYPE_COUNT];
    Text* _languageTexts[LANGUAGE_TYPE_COUNT];
    int _selectLanguageIndex = 0;

};

#endif