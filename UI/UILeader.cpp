#include "UILeader.h"
#include "UICommon.h"
#include "UILeadersRules.h"
#include "UIController.h"
#include "UIMessage.h"
#include "UIProfile.h"
#include "PhotoManager.h"
#include "AudioManager.h"
#include "../GlobalDefine/GameConst.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataManager/LeadersManager.h"
#include "../GlobalDefine/Player.h"
#include "../Network/NetDefine.h"
#include "../DataCenter/ExternalTable.h"

using namespace cocostudio;

// UILeaderCom
UILeaderCom* UILeaderCom::create()
{
    UILeaderCom* _ui = new UILeaderCom();
    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UILeaderCom::UILeaderCom()
{

}

UILeaderCom::~UILeaderCom()
{

}

bool UILeaderCom::init()
{
    _mainLayout = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UILeader/UILeader_th.ExportJson"));
    _mainLayout->setTouchEnabled(true);
    this->addChild(_mainLayout);
    this->setContentSize(_mainLayout->getContentSize());

    _pLayoutHead = _mainLayout->getChildByName<Layout*>("layout_head");
    _pLayoutHead->setTouchEnabled(true);
    _pLayoutHead->setClippingType(Layout::ClippingType::SCISSOR);
    _pTxtRank = _mainLayout->getChildByName<Text*>("txt_01");
    _pTxtInfo = _mainLayout->getChildByName<Text*>("txt_02");
    _pTxtName = _mainLayout->getChildByName<Text*>("txt_03");
    _pTxtScore = _mainLayout->getChildByName<Text*>("txt_04");
    _pTxtReward = _mainLayout->getChildByName<Text*>("txt_05");

    return true;
}

void UILeaderCom::setData(int id, std::string url, std::string rank, std::string info, std::string name, std::string score, std::string reward)
{
    _pLayoutHead->removeAllChildren();
    Sprite* sprite = Sprite::create();
    sprite->setScale(_pLayoutHead->getContentSize().height / 160);
    sprite->setAnchorPoint(Vec2::ZERO);
    _pLayoutHead->addChild(sprite);
    _pLayoutHead->addTouchEventListener(CC_CALLBACK_2(UILeaderCom::onTouchProfile, this, id));
    PhotoManager::getInstance()->setSpriteFrameByID(sprite, id, url);

    _pTxtRank->setString(rank);
    _pTxtInfo->setString(info);
    _pTxtName->setString(name);
    _pTxtScore->setString(score);
    _pTxtReward->setString(reward);
}

void UILeaderCom::onTouchProfile(Ref* sender, Widget::TouchEventType type, int id)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    auto controller = UIController::getInstance()->getController<UIProfile*>(EUITAG_PROFILE);
    if (id == Value(Player::getInstance()->getID()).asInt())
        controller->openUIByType(UIProfile::ProfileType::PROFILETYPE_SELF, id);
    else
        controller->openUIByType(UIProfile::ProfileType::PROFILETYPE_OTHERPLAYER, id);
}

// UILeader
UILeader* UILeader::create()
{
    UILeader* _ui = new UILeader();
    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UILeader::UILeader()
{
    setUIShowType(EUISHOWTYPE::EUIST_HIGHER_NORMAL);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}
    
UILeader::~UILeader()
{

}

void UILeader::destroyUI()
{

}

void UILeader::notifyOpenUI()
{
    _nowRequestType = ELeadersType::ELeadersType_Richer;
    _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_Weekly;

    std::vector<LeadersRankData> leadersRankData;
    LeadersManager::getInstance()->getLeadersRankData(_nowRequestType, _nowRequestTimeType, leadersRankData);

    if (leadersRankData.size() < 3)
    {
        _nowRequestType = ELeadersType::ELeadersType_Gamblers;
    }

    UICommon::setFocusPage((int)_nowRequestType - 1, _pageButtons);
}

void UILeader::notifyCloseUI()
{

}

void UILeader::Update(float dt)
{
    if (!_startUpdate)
        return;

    if (!LeadersManager::getInstance()->isLeadersDataDone(_nowRequestType, _nowRequestTimeType))
    {
        UIController::getInstance()->getController(EUITAG_LOADING)->openUI();
        _startUpdate = false;
        return;
    }

    if (UIController::getInstance()->getController(EUITAG_LOADING)->isOpen())
        UIController::getInstance()->getController(EUITAG_LOADING)->closeUI();

    std::vector<LeadersRankData> leadersRankData;
    LeadersManager::getInstance()->getLeadersRankData(_nowRequestType, _nowRequestTimeType, leadersRankData);

    if (_updateIndex >= (int)leadersRankData.size())
    {
        _startUpdate = false;
        return;
    }

    LeadersRankData rankData = leadersRankData.at(_updateIndex);

    // Top 3
    if (_updateIndex < 3)
    {
        _topPlayer[_updateIndex].winnerName->setString(rankData.name);
        _topPlayer[_updateIndex].headPic->removeAllChildren();

        if (_nowRequestType == ELeadersType::ELeadersType_Richer ||
            _nowRequestType == ELeadersType::ELeadersType_Gamblers ||
            _nowRequestType == ELeadersType::ELeadersType_JP)
        {
            _topPlayer[_updateIndex].score->setString("$" + GetMoneyFormat(rankData.score));
        }
        else if (_nowRequestType == ELeadersType::ELeadersType_Level)
        {
            _topPlayer[_updateIndex].score->setString("EXP " + GetMoneyFormat(rankData.score));
        }

        Sprite* sprite = Sprite::create();
        sprite->setScale(_topPlayer[_updateIndex].headPic->getContentSize().height / 160);
        sprite->setAnchorPoint(Vec2::ZERO);
        _topPlayer[_updateIndex].headPic->addChild(sprite);
        _topPlayer[_updateIndex].headPic->addTouchEventListener(CC_CALLBACK_2(UILeader::onTouchProfile, this, rankData.id));
        PhotoManager::getInstance()->setSpriteFrameByID(sprite, rankData.id, rankData.headURL);
    }
    else
    {
        int scrollViewHeight = _pSVLeader->getInnerContainerSize().height;

        UILeaderCom* com = UILeaderCom::create();

        std::string infoString;
        if (_nowRequestType == ELeadersType::ELeadersType_JP)
        {
            auto slotMachine = ExternalTable<SlotMachineData>::getRecordByID(rankData.info);
            if (!slotMachine)
                infoString = StringUtils::toString(rankData.info);
            else
            {
                infoString = safeGetStringData(slotMachine->SlotNameID);
            }
        }
        else
        {
            infoString = StringUtils::toString(rankData.info);
        }
       
        com->setData(rankData.id, rankData.headURL, StringUtils::toString(rankData.rank), infoString, rankData.name, GetMoneyFormat(rankData.score), rankData.rewardString);
        com->setPosition(Vec2(0.0f, scrollViewHeight - _svAddHeight - ((_updateIndex - 3 + 1) * COMPONENT_HEIGHT)));
        setTraversalCascadeOpacityEnabled(com, true);
        _pSVLeader->addChild(com);
    }

    ++_updateIndex;
}

void UILeader::updateUI()
{
    _pLayoutInfo->setVisible(!(_nowRequestTimeType == ELeadersTimeType::ELeadersTimeType_AllTime));
    setTimeBtnsFocus((int)_nowRequestTimeType);
    updateRankTitle();
    clearLeadersRank();
    _startUpdate = false;

    for (int i = 0; i < 3; ++i)
    {
        LeadersRewardData reward = LeadersManager::getInstance()->getRewardData(i + 1, _nowRequestType, _nowRequestTimeType);

        if (!reward.rewardPicPath.empty())
        {
            _topPlayer[i].rewardPic->loadTexture(reward.rewardPicPath, Widget::TextureResType::PLIST);
            _topPlayer[i].rewardPic->setVisible(true);
        }
        _topPlayer[i].rewardName->setString(reward.rewardString);
    }

    if (!checkErrorCode())
    {
        return;
    }

    // 先檢查資料是否Done，如果false就轉圈，Manager回call，關閉轉圈並更新資料
    if (!LeadersManager::getInstance()->isLeadersDataDone(_nowRequestType, _nowRequestTimeType))
    {
        UIController::getInstance()->getController(EUITAG_LOADING)->openUI();
        return;
    }

    std::vector<LeadersRankData> playerRankData;
    LeadersManager::getInstance()->getPlayerRankData(_nowRequestType, _nowRequestTimeType, playerRankData);
    std::vector<LeadersRankData> leadersRankData;
    LeadersManager::getInstance()->getLeadersRankData(_nowRequestType, _nowRequestTimeType, leadersRankData);

    if (playerRankData.empty() || leadersRankData.empty())
        return;

    LeadersRankData playerData = playerRankData.at(0);
    bool isShowPlayerOnTop = false;

    if (playerData.rank > 3)
    {
        // 玩家不在前三名，固定show在最上面
        _pLayoutPlayerRank->setVisible(true);

        if (playerData.rank > 500)
            _pTxtPlayerRank->setString(">500");
        else
            _pTxtPlayerRank->setString(StringUtils::toString(playerData.rank));
        _pTxtPlayerInfo->setString(StringUtils::toString(playerData.info));
        _pTxtPlayerScroe->setString(GetMoneyFormat(playerData.score));
        _pTxtPlayerName->setString(playerData.name);

        // reward
        LeadersRewardData reward = LeadersManager::getInstance()->getRewardData(playerData.rank, _nowRequestType, _nowRequestTimeType);
        _pTxtPlayerReward->setString(reward.rewardString);

        // head pic
        Sprite* sprite = Sprite::create();
        sprite->setScale(_pLayoutPlayerHead->getContentSize().height / 160);
        sprite->setAnchorPoint(Vec2::ZERO);
        _pLayoutPlayerHead->addChild(sprite);
        PhotoManager::getInstance()->setSpriteFrameByID(sprite, playerData.id, playerData.headURL);

        isShowPlayerOnTop = true;
    }
    else
        _pLayoutPlayerRank->setVisible(false);

    _pSVLeader->setInnerContainerSize(_pSVLeader->getContentSize());

    _svAddHeight = _rankTitleHeight;
    if ((int)leadersRankData.size() > 3)
    {
        if (isShowPlayerOnTop)
            _svAddHeight += COMPONENT_HEIGHT;
        int totalRankCount = (int)leadersRankData.size() - 3;
        _pSVLeader->setInnerContainerSize(Size(_pSVLeader->getInnerContainerSize().width, totalRankCount * COMPONENT_HEIGHT + _svAddHeight));
    }
    
    _pSVLeader->jumpToTop();
    _startUpdate = true;
    _updateIndex = 0;
}

void UILeader::UpdatePerSecond(float dt)
{
    if (!LeadersManager::getInstance()->isLeadersDataDone(_nowRequestType, _nowRequestTimeType))
    {
        return;
    }

    if (_nowRequestTimeType == ELeadersTimeType::ELeadersTimeType_AllTime)
        return;
    else
    {
        time_t nowTime = ConvertGlobalTimeToTimeT(Player::getInstance()->getGameTime());
        time_t expiryTime = LeadersManager::getInstance()->getLeadersExpiiryTime(_nowRequestType, _nowRequestTimeType);
        if (expiryTime == 0 || nowTime > expiryTime)
        {
            //auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
            //messageController->clearMessage();
            //messageController->pushMessage(safeGetStringData(1248));

            //clearLeadersRank();
            //_pBMFLeftTime->setString("00:00:00");
            return;
        }

        int countTime = expiryTime - nowTime;

        int h = countTime / ONE_HOUR_SECOND;
        int m = (countTime % ONE_HOUR_SECOND) / ONE_MINUTE_SECOND;
        int s = countTime % ONE_MINUTE_SECOND;

        if (h > 99)
            _pBMFLeftTime->setString(StringUtils::format("%03d:%02d:%02d", h, m, s));
        else
            _pBMFLeftTime->setString(StringUtils::format("%02d:%02d:%02d", h, m, s));

        if (countTime <= 0)
        {
            //LeadersManager::getInstance()->startRequestLeadersRank();
            auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
            messageController->clearMessage();
            messageController->pushMessage(safeGetStringData(1248));
            clearLeadersRank();
            _pBMFLeftTime->setString("00:00:00");
            return;
        }
    }
}

void UILeader::setLeftTime(int time)
{
    int h = time / ONE_HOUR_SECOND;
    int m = (time % ONE_HOUR_SECOND) / ONE_MINUTE_SECOND;
    int s = time % ONE_MINUTE_SECOND;

    _pBMFLeftTime->setString(StringUtils::format("%02d:%02d:%02d", h, m, s));
}

bool UILeader::init()
{
    return true;
}

bool UILeader::createUI()
{
    _uiLeader = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UILeader/UILeaders.ExportJson"));
    _uiLeader->setTouchEnabled(true);
    _mainUINode->addChild(_uiLeader);

    auto window = _uiLeader->getChildByName<Layout*>("windows");
    
    auto closeBtn = window->getChildByName<Button*>("btn_close");
    closeBtn->setTouchEnabled(true);
    closeBtn->addTouchEventListener(CC_CALLBACK_2(UILeader::onTouchClose, this));
    closeBtn->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    for (int i = 0; i < 4; ++i)
    {
        Button* pageBtn = window->getChildByName<Button*>(StringUtils::format("Button_0%d", i + 1));
        pageBtn->setTouchEnabled(true);
        pageBtn->addTouchEventListener(CC_CALLBACK_2(UILeader::onTouchLeaderType, this, i + 1));
        pageBtn->setTitleText(safeGetStringData(1201 + i));
        _pageButtons.push_back(pageBtn);
    }
    UICommon::initTabStyle(_pageButtons);

    auto page = _uiLeader->getChildByName<Layout*>("Page_01");
    _pBtnToday = page->getChildByName<Button*>("btn_today");
    _pBtnToday->setTouchEnabled(true);
    _pBtnToday->addTouchEventListener(CC_CALLBACK_2(UILeader::onTouchToday, this));
    _pBtnToday->setTitleText(safeGetStringData(1205));
    _timeButtons.push_back(_pBtnToday);

    _pBtnWeakly = page->getChildByName<Button*>("btn_Weakly");
    _pBtnWeakly->setTouchEnabled(true);
    _pBtnWeakly->addTouchEventListener(CC_CALLBACK_2(UILeader::onTouchWeekly, this));
    _pBtnWeakly->setTitleText(safeGetStringData(1206));
    _timeButtons.push_back(_pBtnWeakly);

    _pBtnAllTime = page->getChildByName<Button*>("btn_Hall");
    _pBtnAllTime->setTouchEnabled(true);
    _pBtnAllTime->addTouchEventListener(CC_CALLBACK_2(UILeader::onTouchAllTime, this));
    _pBtnAllTime->setTitleText(safeGetStringData(1207));
    _timeButtons.push_back(_pBtnAllTime);

    _pSVLeader = page->getChildByName<ScrollView*>("scrollview_rank");
    _pSVLeader->setClippingEnabled(true);
    _pSVLeader->setBounceEnabled(true);
    _pSVLeader->setClippingType(Layout::ClippingType::STENCIL);

    for (int i = 0; i < 3; ++i)
    {
        _topPlayer[i].headPic = page->getChildByName<Layout*>(StringUtils::format("img_head_%d", i + 1));
        _topPlayer[i].headPic->setTouchEnabled(true);
        _topPlayer[i].score = page->getChildByName<Text*>(StringUtils::format("txt_score_%d", i + 1));
        _topPlayer[i].rewardName = page->getChildByName<Text*>(StringUtils::format("txt_reward_%d", i + 1));
        _topPlayer[i].rewardPic = page->getChildByName<ImageView*>(StringUtils::format("img_reward_%d", i + 1));
        _topPlayer[i].winnerName = page->getChildByName<Text*>(StringUtils::format("txt_name_%d", i + 1));
    }

    auto titleLayout = page->getChildByName<Layout*>("RankTitle");
    _pTxtTitleRank = titleLayout->getChildByName<Text*>("txt_01");
    _pTxtTitleInfo = titleLayout->getChildByName<Text*>("txt_02");
    _pTxtTitleName = titleLayout->getChildByName<Text*>("txt_03");
    _pTxtTitleScore = titleLayout->getChildByName<Text*>("txt_04");
    _pTxtTitleReward = titleLayout->getChildByName<Text*>("txt_05");

    _pLayoutPlayerRank = page->getChildByName<Layout*>("RankPlayer");
    _pLayoutPlayerRank->setVisible(false);
    _pLayoutPlayerHead = _pLayoutPlayerRank->getChildByName<Layout*>("layout_head");
    _pTxtPlayerRank = _pLayoutPlayerRank->getChildByName<Text*>("txt_01");
    _pTxtPlayerInfo = _pLayoutPlayerRank->getChildByName<Text*>("txt_02");
    _pTxtPlayerName = _pLayoutPlayerRank->getChildByName<Text*>("txt_03");
    _pTxtPlayerScroe = _pLayoutPlayerRank->getChildByName<Text*>("txt_04");
    _pTxtPlayerReward = _pLayoutPlayerRank->getChildByName<Text*>("txt_05");
    
    _rankTitleHeight = page->getChildByName<Layout*>("RankTitle")->getContentSize().height;

    _pLayoutInfo = page->getChildByName<Layout*>("Layout_info");
    _pBMFLeftTime = _pLayoutInfo->getChildByName<TextBMFont*>("bmf_left_time");
    _pBtnRules = _pLayoutInfo->getChildByName<Button*>("btn_rules");
    _pBtnRules->setTouchEnabled(true);
    _pBtnRules->addTouchEventListener(CC_CALLBACK_2(UILeader::onTouchRules, this));

    auto pTxtTimeLeft = _pLayoutInfo->getChildByName<Text*>("txt_time_left");
    pTxtTimeLeft->setString(safeGetStringData(1239));

    _pTxtInfo = page->getChildByName<Text*>("LeadersInfo");
    _pTxtInfo->enableOutline(UICommon::TABTEXT_OUTLINE_COLOR, UICommon::TABTEXT_OUTLINE_SIZE);
    _pTxtHint = page->getChildByName<Text*>("LeadersHint");
    _pTxtHint->enableOutline(UICommon::TABTEXT_OUTLINE_COLOR, UICommon::TABTEXT_OUTLINE_SIZE);

    auto particles0 = CCParticleSystemQuad::create("point.plist");
    particles0->setPosition(Vec2(650, 450));
    particles0->setScale(0.8f);
    page->addChild(particles0, 1);
    auto particles1 = CCParticleSystemQuad::create("star.plist");
    particles1->setPosition(Vec2(650, 450));
    particles1->setScale(0.8f);
    page->addChild(particles1, 1);

    return true;
}

void UILeader::setTimeBtnsFocus(int index)
{
    for (int i = 0; i < (int)_timeButtons.size(); ++i)
    {
        if (index - 1 == i)
        {
            _timeButtons.at(i)->setBright(false);
        }
        else
        {
            _timeButtons.at(i)->setBright(true);
        }
    }
}

void UILeader::updateRankTitle()
{
    switch (_nowRequestType)
    {
    case ELeadersType_Richer:
        _pTxtTitleRank->setString(safeGetStringData(1208));
        _pTxtTitleInfo->setString(safeGetStringData(1209));
        _pTxtTitleName->setString(safeGetStringData(1210));
        _pTxtTitleScore->setString(safeGetStringData(1211));
        _pTxtTitleReward->setString(safeGetStringData(1212));

        _pTxtInfo->setString(safeGetStringData(1250));
        break;
    case ELeadersType_Gamblers:
        _pTxtTitleRank->setString(safeGetStringData(1208));
        _pTxtTitleInfo->setString(safeGetStringData(1209));
        _pTxtTitleName->setString(safeGetStringData(1210));
        _pTxtTitleScore->setString(safeGetStringData(1211));
        _pTxtTitleReward->setString(safeGetStringData(1212));

        _pTxtInfo->setString(safeGetStringData(1251));
        break;
    case ELeadersType_Level:
        _pTxtTitleRank->setString(safeGetStringData(1208));
        _pTxtTitleInfo->setString(safeGetStringData(1209));
        _pTxtTitleName->setString(safeGetStringData(1210));
        _pTxtTitleScore->setString(safeGetStringData(1214));
        _pTxtTitleReward->setString(safeGetStringData(1212));

        _pTxtInfo->setString(safeGetStringData(1252));
        break;
    case ELeadersType_JP:
        _pTxtTitleRank->setString(safeGetStringData(1208));
        _pTxtTitleInfo->setString(safeGetStringData(1213));
        _pTxtTitleName->setString(safeGetStringData(1210));
        _pTxtTitleScore->setString(safeGetStringData(1215));
        _pTxtTitleReward->setString(safeGetStringData(1212));

        _pTxtInfo->setString(safeGetStringData(1253));
        break;
    case ELeadersType_None:
        break;
    default:
        break;
    }
}

void UILeader::clearLeadersRank()
{
    _pLayoutPlayerRank->setVisible(false);
    _pSVLeader->removeAllChildren();
    _pBMFLeftTime->setString("00:00:00");
    for (int i = 0; i < 3; ++i)
    {
        _topPlayer[i].rewardPic->setVisible(false);
        _topPlayer[i].rewardName->setString("");
        _topPlayer[i].winnerName->setString("");
        _topPlayer[i].score->setString("");
        _topPlayer[i].headPic->removeAllChildren();
    }
}

bool UILeader::checkErrorCode()
{
    int errorCode = LeadersManager::getInstance()->getLeadersErrorCodes(_nowRequestType, _nowRequestTimeType);
    if (errorCode == ErrorCode::EC_Success)
    {
        _pTxtHint->setString("");
        _pTxtHint->setVisible(false);

        if (_nowRequestTimeType != ELeadersTimeType::ELeadersTimeType_AllTime)
        {
            time_t nowTime = ConvertGlobalTimeToTimeT(Player::getInstance()->getGameTime());
            time_t expiryTime = LeadersManager::getInstance()->getLeadersExpiiryTime(_nowRequestType, _nowRequestTimeType);
            if (expiryTime == 0 || nowTime > expiryTime)
            {
                auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
                messageController->clearMessage();
                messageController->pushMessage(safeGetStringData(1248));

                _pBMFLeftTime->setString("00:00:00");
                return false;
            }
        }

        return true;
    }
    else if (errorCode == ErrorCode::EC_LeadersEmpty)
    {
        _pTxtHint->setString(safeGetStringData(1233));
        _pTxtHint->setVisible(true);
        return false;
    }
    else if (errorCode == ErrorCode::EC_EmergencyClose)
    {
        _pTxtHint->setString(safeGetStringData(1254));
        _pTxtHint->setVisible(true);
        return false;
    }
    else
    {
        _pTxtHint->setString(safeGetStringData(1232));
        _pTxtHint->setVisible(true);
        return false;
    }
}

void UILeader::onTouchRules(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    auto controller = UIController::getInstance()->getController<UILeadersRules*>(EUITAG_UILEADERS_RULES);
    controller->setType(_nowRequestType, _nowRequestTimeType);
    controller->openUI();
}

void UILeader::onTouchToday(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    if (_nowRequestTimeType == ELeadersTimeType::ELeadersTimeType_Today)
        return;

    _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_Today;

    updateUI();
}

void UILeader::onTouchWeekly(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    if (_nowRequestTimeType == ELeadersTimeType::ELeadersTimeType_Weekly)
        return;

    _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_Weekly;

    updateUI();
}

void UILeader::onTouchAllTime(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    if (_nowRequestTimeType == ELeadersTimeType::ELeadersTimeType_AllTime)
        return;

    _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_AllTime;

    updateUI();
}
    
void UILeader::onTouchLeaderType(Ref* sender, Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _nowRequestType = (ELeadersType)index;
    _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_Weekly;

    updateUI();
    Button* pBtn = (Button*)sender;
    UICommon::setFocusPage(pBtn->getTag(), _pageButtons);
}

void UILeader::onTouchClose(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}

void UILeader::onTouchProfile(Ref* sender, Widget::TouchEventType type, int id)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    auto controller = UIController::getInstance()->getController<UIProfile*>(EUITAG_PROFILE);
    if (id == Value(Player::getInstance()->getID()).asInt())
        controller->openUIByType(UIProfile::ProfileType::PROFILETYPE_SELF, id);
    else
        controller->openUIByType(UIProfile::ProfileType::PROFILETYPE_OTHERPLAYER, id);
}