#pragma once
#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"

USING_NS_CC;

using namespace ui;

// component
class UILeaderCom : public Widget
{
public:
    static UILeaderCom* create();
    UILeaderCom();
    ~UILeaderCom();

public:
    bool init();
    void setData(int id, std::string url, std::string rank, std::string info, std::string name, std::string score, std::string reward);

private:
    void onTouchProfile(Ref* sender, Widget::TouchEventType type, int id);

private:
    Layout* _mainLayout = nullptr;
    Layout* _pLayoutHead = nullptr;
    Text* _pTxtRank = nullptr;
    Text* _pTxtInfo = nullptr;
    Text* _pTxtName = nullptr;
    Text* _pTxtScore = nullptr;
    Text* _pTxtReward = nullptr;
};

enum LeaderType
{
    Leader_1,   // 大咖富豪
    Leader_2,   // 財神
    Leader_3,   // 彩金
    Leader_4,   // 戰績
};

// 前三名
struct TopPlayer
{
    ImageView* rewardPic;
    Layout* headPic;
    Text* winnerName;
    Text* score;
    Text* rewardName;

    TopPlayer()
    {
        rewardPic = nullptr;
        headPic = nullptr;
        winnerName = nullptr;
        score = nullptr;
        rewardName = nullptr;
    }
};

// UI
class UILeader : public UIBasicUnit
{
public:
    static UILeader* create();

public:
    UILeader();
    ~UILeader();

    virtual void destroyUI() override;
    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void Update(float dt) override;
    virtual void updateUI() override;
    virtual void UpdatePerSecond(float dt);

    void setLeftTime(int time);

private:
    bool init();
    virtual bool createUI() override;
    void setTimeBtnsFocus(int index);
    void updateRankTitle();
    void clearLeadersRank();
    bool checkErrorCode();

private:
    void onTouchRules(Ref* sender, Widget::TouchEventType type);
    void onTouchToday(Ref* sender, Widget::TouchEventType type);
    void onTouchWeekly(Ref* sender, Widget::TouchEventType type);
    void onTouchAllTime(Ref* sender, Widget::TouchEventType type);
    void onTouchLeaderType(Ref* sender, Widget::TouchEventType type, int index);
    void onTouchClose(Ref* sender, Widget::TouchEventType type);
    void onTouchProfile(Ref* sender, Widget::TouchEventType type, int id);

private:
    Layout* _uiLeader = nullptr;
    std::vector<Button*> _pageButtons;
    Button* _pBtnToday = nullptr;
    Button* _pBtnWeakly = nullptr;
    Button* _pBtnAllTime = nullptr;
    std::vector<Button*> _timeButtons;
    ScrollView* _pSVLeader = nullptr;
    //std::vector<UILeaderCom> _playerComs;
    TopPlayer _topPlayer[3];
    const int COMPONENT_HEIGHT = 41;

    // rank title
    Text* _pTxtTitleRank = nullptr;
    Text* _pTxtTitleInfo = nullptr;
    Text* _pTxtTitleName = nullptr;
    Text* _pTxtTitleScore = nullptr;
    Text* _pTxtTitleReward = nullptr;

    // player
    Layout* _pLayoutPlayerRank = nullptr;
    Layout* _pLayoutPlayerHead = nullptr;
    Text* _pTxtPlayerRank = nullptr;
    Text* _pTxtPlayerInfo = nullptr;
    Text* _pTxtPlayerName = nullptr;
    Text* _pTxtPlayerScroe = nullptr;
    Text* _pTxtPlayerReward = nullptr;

    int _rankTitleHeight = 0;
    ELeadersType _nowRequestType = ELeadersType::ELeadersType_Richer;
    ELeadersTimeType _nowRequestTimeType = ELeadersTimeType::ELeadersTimeType_Today;

    bool _startUpdate = false;
    int _updateIndex = 0;
    int _svAddHeight = 0;

    // Info
    Layout* _pLayoutInfo = nullptr;
    Button* _pBtnRules = nullptr;
    TextBMFont* _pBMFLeftTime = nullptr;

    // info
    Text* _pTxtInfo = nullptr;
    Text* _pTxtHint = nullptr;
};