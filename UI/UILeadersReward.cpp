#include "UILeadersReward.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/ImageDefine.h"

using namespace cocostudio;

UILeadersReward* UILeadersReward::create()
{
    UILeadersReward* _ui = new UILeadersReward();
    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}


UILeadersReward::UILeadersReward()
{
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
}

UILeadersReward::~UILeadersReward()
{

}

void UILeadersReward::destroyUI()
{

}

void UILeadersReward::notifyOpenUI()
{

}

void UILeadersReward::notifyCloseUI()
{

}

void UILeadersReward::updateUI()
{
    if (_datas.empty())
    {
        closeUI();
        return;
    }

    LeadersRewardRank data = _datas.back();
    _datas.pop_back();

    switch (data.type)
    {
    case ELeadersType_Richer:
        if (data.timeType == ELeadersTimeType::ELeadersTimeType_Today)
            _pTxtRankContent->setString(safeGetStringData(1216));
        else if (data.timeType == ELeadersTimeType::ELeadersTimeType_Weekly)
            _pTxtRankContent->setString(safeGetStringData(1217));
        break;
    case ELeadersType_Gamblers:
        if (data.timeType == ELeadersTimeType::ELeadersTimeType_Today)
            _pTxtRankContent->setString(safeGetStringData(1218));
        else if (data.timeType == ELeadersTimeType::ELeadersTimeType_Weekly)
            _pTxtRankContent->setString(safeGetStringData(1219));
        break;
    case ELeadersType_Level:
        if (data.timeType == ELeadersTimeType::ELeadersTimeType_Today)
            _pTxtRankContent->setString(safeGetStringData(1220));
        else if (data.timeType == ELeadersTimeType::ELeadersTimeType_Weekly)
            _pTxtRankContent->setString(safeGetStringData(1221));
        break;
    case ELeadersType_JP:
        if (data.timeType == ELeadersTimeType::ELeadersTimeType_Today)
            _pTxtRankContent->setString(safeGetStringData(1222));
        else if (data.timeType == ELeadersTimeType::ELeadersTimeType_Weekly)
            _pTxtRankContent->setString(safeGetStringData(1223));
        break;
    case ELeadersType_None:
        break;
    default:
        break;
    }

    _pTxtRank->setString(StringUtils::toString(data.rank));

    bool disable = data.disable == 0;
    std::string rewardString;
    std::string rewardPicPath;

    if (disable)
    {
        if ((ELeadersRewardType)data.rewardType == ELeadersRewardType::ELeadersRewardType_Coin)
        {
            rewardString = "$" + GetMoneyFormat(data.rewardCount);
        }
        else
        {
            if (data.rewardCoin == 0)
                rewardString = composeStringData(safeGetStringData(data.stringID), GetMoneyFormat(data.rewardCount));
            else
                rewardString = composeStringData(safeGetStringData(data.stringID), GetMoneyFormat(data.rewardCount), "$" + GetMoneyFormat(data.rewardCoin));
        }

        rewardPicPath = StringUtils::format(GOCIMAGE(LeadersAward%.4d), data.picFile);

        _pImgRewardPic->loadTexture(rewardPicPath, Widget::TextureResType::PLIST);

        if ((ELeadersRewardType)data.rewardType == ELeadersRewardType::ELeadersRewardType_Coin)
            _pTxtHint->setString(safeGetStringData(1225));
        else
            _pTxtHint->setString(safeGetStringData(1226));
    }

    _pTxtReward->setString(rewardString);
    _pTxtReward->setVisible(disable);
    _pImgRewardPic->setVisible(disable);
    _pTxtHint->setVisible(disable);
    _pTxtYouWon->setVisible(disable);
}

void UILeadersReward::setData(LeadersRewardRank data)
{
    _datas.push_back(data);
}

bool UILeadersReward::init()
{
    return true;
}

bool UILeadersReward::createUI()
{
    _uiLeadersReward = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UILeader/UILeader_DailyLeader.ExportJson"));
    _uiLeadersReward->setTouchEnabled(true);
    _mainUINode->addChild(_uiLeadersReward);

    _pImgRewardPic = _uiLeadersReward->getChildByName<ImageView*>("img_reward");

    _pTxtRankContent = _uiLeadersReward->getChildByName<Text*>("txt_content");
    _pTxtRank = _uiLeadersReward->getChildByName<Text*>("txt_rank");
    _pTxtReward = _uiLeadersReward->getChildByName<Text*>("txt_reward");
    _pTxtHint = _uiLeadersReward->getChildByName<Text*>("txt_hint");

    _pTxtYouWon = _uiLeadersReward->getChildByName<Text*>("txt_you_won");
    _pTxtYouWon->setString(safeGetStringData(1224)); // you won

    auto btnOK = _uiLeadersReward->getChildByName<Button*>("Btn_ok");
    btnOK->setTouchEnabled(true);
    btnOK->addTouchEventListener(CC_CALLBACK_2(UILeadersReward::onTouchOK, this));

    return true;
}

void UILeadersReward::onTouchOK(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_datas.empty())
    {
        closeUI();
    }
    else
    {
        updateUI();
        playEnterUIAction();
    }
}