#pragma once
#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "../GlobalDefine/EnumType.h"
#include "../DataManager/LeadersManager.h"

USING_NS_CC;

using namespace ui;

class UILeadersReward : public UIBasicUnit
{
public:
    static UILeadersReward* create();

public:
    UILeadersReward();
    ~UILeadersReward();

    virtual void destroyUI() override;
    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

    void setData(LeadersRewardRank data);

private:
    bool init();
    virtual bool createUI() override;

private:
    void onTouchOK(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _uiLeadersReward = nullptr;
    Text* _pTxtRankContent = nullptr;
    Text* _pTxtRank = nullptr;
    Text* _pTxtReward = nullptr;
    Text* _pTxtHint = nullptr;
    Text* _pTxtYouWon = nullptr;
    ImageView* _pImgRewardPic = nullptr;

    std::vector<LeadersRewardRank> _datas;
    //int _rank = 0;
    //ELeadersType _leadersType = ELeadersType::ELeadersType_Richer;
    //ELeadersTimeType _leadersTimeType = ELeadersTimeType::ELeadersTimeType_Today;
};