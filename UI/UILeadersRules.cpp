#include "UILeadersRules.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataManager/LeadersManager.h"
#include "AudioManager.h"

using namespace cocostudio;

UILeadersRules* UILeadersRules::create()
{
    UILeadersRules* _ui = new UILeadersRules();
    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}


UILeadersRules::UILeadersRules()
{
    setUIShowType(EUISHOWTYPE::EUIST_HIGHER_NORMAL);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}

UILeadersRules::~UILeadersRules()
{

}

void UILeadersRules::destroyUI()
{

}

void UILeadersRules::notifyOpenUI()
{

}

void UILeadersRules::notifyCloseUI()
{

}

void UILeadersRules::updateUI()
{
    for (int i = 0; i < 3; ++i)
    {
        LeadersRewardData data = LeadersManager::getInstance()->getRewardDataByRange(i + 1, _leadersType, _leadersTimeType);

        _pImgReward[i]->loadTexture(data.rewardPicPath, Widget::TextureResType::PLIST);
        _pTxtReward[i]->setString(data.rewardString);
    }

    for (int i = 0; i < OTHER_RANGE_COUNT; ++i)
    {
        _pTxtRange[i]->setString(safeGetStringData(1227 + i));

        LeadersRewardData data = LeadersManager::getInstance()->getRewardDataByRange(i + 4, _leadersType, _leadersTimeType);
        _pRangeReward[i]->setString(data.rewardString);
    }

    _pTxtRules->setString(safeGetStringData(1231));

    int adjustHeight = _pTxtRules->getContentSize().height - MAX_RULES_HEIGHT;
    _pScrollView->setInnerContainerSize(_pScrollView->getContentSize() + Size(0.0f, adjustHeight));
    _pScrollView->setClippingEnabled(true);
    _pScrollView->setBounceEnabled(true);
    _pLayoutSVContainer->setPosition(Vec2(0.0f, _pScrollView->getInnerContainerSize().height));
}

void UILeadersRules::setType(ELeadersType type, ELeadersTimeType timeType)
{
    _leadersType = type;
    _leadersTimeType = timeType;
}

bool UILeadersRules::init()
{
    return true;
}

bool UILeadersRules::createUI()
{
    _uiLeadersRules = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UILeader/UILeaders_GameRulles.ExportJson"));
    _uiLeadersRules->setTouchEnabled(true);
    _mainUINode->addChild(_uiLeadersRules);

    _pScrollView = _uiLeadersRules->getChildByName<ScrollView*>("ScrollView");
    _pLayoutSVContainer = (Layout*)_pScrollView->getChildByName("layout_container");
    
    _pTxtRules = _pLayoutSVContainer->getChildByName<Text*>("txt_rules");

    for (int i = 0; i < 3; ++i)
    {
        _pImgReward[i] = _pLayoutSVContainer->getChildByName<ImageView*>(StringUtils::format("img_reward_%d", i + 1));
        _pTxtReward[i] = _pLayoutSVContainer->getChildByName<Text*>(StringUtils::format("txt_reward_%d", i + 1));
    }

    for (int i = 0; i < OTHER_RANGE_COUNT; ++i)
    {
        _pTxtRange[i] = _pLayoutSVContainer->getChildByName<Text*>(StringUtils::format("txt_range_%d", i + 1));
        _pRangeReward[i] = _pLayoutSVContainer->getChildByName<Text*>(StringUtils::format("txt_range_%d_reward", i + 1));
    }

    auto closeBtn = _uiLeadersRules->getChildByName<Layout*>("windows")->getChildByName<Button*>("btn_close");
    closeBtn->setTouchEnabled(true);
    closeBtn->addTouchEventListener(CC_CALLBACK_2(UILeadersRules::onTouchClose, this));
    closeBtn->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    return true;
}

void UILeadersRules::onTouchClose(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}