#pragma once
#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "../GlobalDefine/EnumType.h"

USING_NS_CC;

using namespace ui;

class UILeadersRules : public UIBasicUnit
{
public:
    static const int OTHER_RANGE_COUNT = 4;
public:
    static UILeadersRules* create();

public:
    UILeadersRules();
    ~UILeadersRules();

    virtual void destroyUI() override;
    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

    void setType(ELeadersType type, ELeadersTimeType timeType);

private:
    bool init();
    virtual bool createUI() override;

private:
    void onTouchClose(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _uiLeadersRules = nullptr;
    Text* _pTxtRules = nullptr;
    ScrollView* _pScrollView = nullptr;
    Layout* _pLayoutSVContainer = nullptr;

    ImageView* _pImgReward[3];
    Text* _pTxtReward[3];
    
    Text* _pTxtRange[OTHER_RANGE_COUNT];
    Text* _pRangeReward[OTHER_RANGE_COUNT];

    ELeadersType _leadersType = ELeadersType::ELeadersType_Richer;
    ELeadersTimeType _leadersTimeType = ELeadersTimeType::ELeadersTimeType_Today;

    const int MAX_RULES_HEIGHT = 160;
};