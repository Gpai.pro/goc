#include "UIMainMenu.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UICommon.h"
#include "../GlobalDefine/Player.h"
#include "AudioManager.h"
#include "UIController.h"
#include "../GlobalDefine/ImageDefine.h"
#include "../GlobalDefine/GameConst.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Game/Gameplay/GameLogic.h"
#include "UIGameSetting.h"
#include "../UI/UIVip.h"
#include "../UI/PhotoManager.h"
#include "CustomParticleSystem.h"
#include "../Auth/PlatformProxy.h"
#include "../Animation/CustomActions.h"
#include "UIProfile.h"
#include "../UI/UITutorialController.h"
#include "../UI/UIFeedback.h"
#include "../UI/UIPopBanner.h"
#include "../Network/MessageProcess.h"
#include "../DataManager/FeedbackManager.h"
#include "../DataManager/LeadersManager.h"
#include "../Network/MessageCenter.h"

#define JACKPOT_MUSIC 0004

using namespace cocostudio;

const float MAINMENU_HINT_UP_POSY = 400.0f;
const float MAINMENU_HINT_DOWN_POSY = 150.0f;

UIMainMenu* UIMainMenu::create()
{
	UIMainMenu* unit = new UIMainMenu();

	if (unit && unit->init())
	{
		return unit;
	}

	CC_SAFE_DELETE(unit);
	return nullptr;
}

bool UIMainMenu::init()
{
	return true;
}

UIMainMenu::UIMainMenu()
{
	setUIShowType(EUISHOWTYPE::EUIST_STAYALONE);

    _jackpotNoticeData.clear();
    _lastTimeJPDatas.clear();
}

UIMainMenu::~UIMainMenu()
{
}

void UIMainMenu::notifyCloseUI()
{

}

bool UIMainMenu::createUI()
{
	_uiMainMenu = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UIMainMenu/UIMainMenu.ExportJson"));
    _uiMainMenu->setTouchEnabled(false);
	_mainUINode->addChild(_uiMainMenu);
    setUIZOrder(ELZO_MAINMENU);

    _touchMask = _uiMainMenu->getChildByName<Layout*>("touchMaskPanel");
    _touchMask->setTouchEnabled(true);

	auto groupNode = _uiMainMenu->getChildByName("Group");
	
	_buyCoin1Layout = groupNode->getChildByName<Layout*>("buy_coins");

    _buyCoin1Btn = _buyCoin1Layout->getChildByName<Button*>("UIMainMenu_button_1");
    _buyCoin1Btn->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchBuyCoin1, this));
    _buyCoin1Btn->setSoundID(EFF_NONE);

    _buyCoin2Layout = groupNode->getChildByName<Layout*>("buy_coins_2");

	_buyCoin2Btn = _buyCoin2Layout->getChildByName<Button*>("Button_02");
	_buyCoin2Btn->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchBuyCoin2Buy, this));
    _buyCoin2Btn->setSoundID(EFF_NONE);

    _dealBtnTimer = _buyCoin2Layout->getChildByName<Button*>("Button_03");
    _dealBtnTimer->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchBuyCoin3Deal, this));

    _dealBtn = _buyCoin2Layout->getChildByName<Button*>("Button_04");
    _dealBtn->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchBuyCoin4Deal, this));

    _dealRemainTimeFont = _buyCoin2Layout->getChildByName<TextBMFont*>("remainTimeBMFont");

    // LV
    auto lv = groupNode->getChildByName<Layout*>("lv");
    _expBar = lv->getChildByName<LoadingBar*>("UIMainMenu_exp");
    _expBar->setPercent(_levelBarPercent);

    _lvTouchPanel = lv->getChildByName<Layout*>("lv_touchPanel");
    _lvTouchPanel->setTouchEnabled(true);
    _lvTouchPanel->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchLv, this));

    _particleMask = lv->getChildByName<Layout*>("particle_mask");
    ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("Particle/mainmenu_exp_bar.plist");
    pParticle1->setPosition(Vec2(50.0f, _particleMask->getContentSize().height / 2));
    _particleMask->addChild(pParticle1);

    _profile = groupNode->getChildByName<ImageView*>("UIMainMenu_photo");
    _profileBtn = groupNode->getChildByName<Button*>("UIMainMenu_photoFrame");
    _profileBtn->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchProfile, this));
    _profileBtn->setTouchEnabled(true);
    
    _lvFont = lv->getChildByName<TextBMFont*>("lvBMFont");
    _expPercentFont = lv->getChildByName<TextBMFont*>("expPercentBMFont");

    _lobby = groupNode->getChildByName<Button*>("UIMainMenu_lobby");
    _lobby->setTouchEnabled(true);
    _lobby->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchLobby, this));

    _setting = groupNode->getChildByName<Button*>("UIMainMenu_setup");
    _setting->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchSetting, this));
    _setting->setTouchEnabled(true);

    _feedback = groupNode->getChildByName<Button*>("UIMainMenu_feedback");
    _feedback->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchFeedback, this));
    _feedback->setTouchEnabled(true);

    _feedbackRedball = _feedback->getChildByName<ImageView*>("img_redball");
    _feedbackRedball->setVisible(false);

    groupNode->getChildByName<Layout*>("LvDetailPanel")->setClippingEnabled(true);
    _lvDetail = groupNode->getChildByName<Layout*>("LvDetailPanel")->getChildByName<Layout*>("LvDetail");
    
    _lvDetailExp = _lvDetail->getChildByName<Text*>("text_exp");
    _lvDetailExp->setTextHorizontalAlignment(TextHAlignment::LEFT);

    _lvDetailPointToNextLv = _lvDetail->getChildByName<Text*>("txt_next_level");
    _lvDetailPointToNextLv->setTextHorizontalAlignment(TextHAlignment::RIGHT);

    for (int i = 0; i < 6; ++i)
    {
        _lvDetailText[i] = _lvDetail->getChildByName<Text*>(StringUtils::format("txt_title_%d", i + 1));
        _lvDetailText[i]->setTextHorizontalAlignment(TextHAlignment::LEFT);
    }
    
    for (int i = 0; i < 6; ++i)
    {
        _lvDetailData[i] = _lvDetail->getChildByName<Text*>(StringUtils::format("txt_data_%d", i + 1));
        _lvDetailData[i]->setTextHorizontalAlignment(TextHAlignment::RIGHT);
    }

    _maskLayout = Layout::create();
    _mainUINode->addChild(_maskLayout, 100);
    _maskLayout->setTouchEnabled(false);
    _maskLayout->setAnchorPoint(Vec2::ZERO);
    _maskLayout->setContentSize(DESIGN_SIZE);
    _maskLayout->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchLv, this));
    //

    _messageLayout = groupNode->getChildByName<Layout*>("Marquee");
    _machineName = _messageLayout->getChildByName<Text*>("machine_name");
    _machineName->enableOutline(Color4B::WHITE, 1);
    _rewardKind = _messageLayout->getChildByName<Text*>("reward_kind");
    _rewardKind->enableOutline(Color4B::WHITE, 1);
    _scoreBM = _messageLayout->getChildByName<TextBMFont*>("reward_score");
    _headPic = _messageLayout->getChildByName<Layout*>("head_pic");
    _playerName = _messageLayout->getChildByName<Text*>("player_name");
    _playerName->enableOutline(Color4B::WHITE, 1);

    auto moneyNode = groupNode->getChildByName("money");
    _coinImage = moneyNode->getChildByName<ImageView*>("UIMainMenu_coin");

    _moneyFont = moneyNode->getChildByName<TextBMFont*>("moneyBMFont");

    // vip
    _vipCard = groupNode->getChildByName<Layout*>("vipcrad")->getChildByName<ImageView*>("vip");
    _vipCard->setTouchEnabled(true);
    _vipCard->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchVipCard, this));
    _vipCard->setSoundID(EFF_COMMON_BUTTON_CLICK);

    // Action
    _noticeAction = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "NoticeEnter");
    _noticeAction->updateToFrameByTime(0);

    auto noticeNode = groupNode->getChildByName("Notice");
    _noticeContent = noticeNode->getChildByName<Text*>("content");

    _infoBarAction = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "RewardNoticeEnter");
    _infoBarAction->updateToFrameByTime(0);

	_enterAnimation = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "EnterAnimation");

    _coinLight = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "coin_light");
    _coinLight->play();

    auto buttonLight = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "btn_reflect_light");
    buttonLight->play();

    buttonLight = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "btn_reflect_light_0");
    buttonLight->play();

    auto coinLight = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "coin_light");
    coinLight->play();

    _MargueeEnter = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "MargueeEnter");
    _MargueeEnter->updateToFrameByTime(0);

    _MargueeExit = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "MargueeExit");

    _VIPRoomEnter = ActionManagerEx::getInstance()->getActionByName("GUI/UIMainMenu/UIMainMenu.ExportJson", "VIPRoom");

	
    // Setting
    UICommon::getInstance()->fixUIPosition(_uiMainMenu, EFixUIPositionType::EFP_TOP);

    setCoin(Player::getInstance()->getCoin());
    setLevel(Player::getInstance()->getLevel());
    setExp(getLvExpPercent());
    setVipCard(Player::getInstance()->getVipLv());
    setBuyCoinVisible(true, false);

    setProfileAndLobbyVisible(true, false);
    setType(_type);

    // marquee
    _margueeLayout = groupNode->getChildByName<Layout*>("Lobby_Marquee");
    _margueeLayout->setVisible(false);

    // vip room
    _VIPRoom = groupNode->getChildByName<Layout*>("VipRoom");
    _VIPRoom->setVisible(false);

    // check pop
    _checkOnceTime = true;
    _isCheckPopUIDone = false;

    requestJackpotNoticeData();

#ifdef BUILD_DEV
    _coinImage->addTouchEventListener(CC_CALLBACK_2(UIMainMenu::touchCoin, this));
    _coinImage->setTouchEnabled(true);

#endif

    return true;
}

void UIMainMenu::destroyUI()
{

}

void UIMainMenu::updateUI()
{
    if (Player::getInstance()->getLoginType() == ELoginType::ELT_Facebook)
        facebookPictureRequest();

    auto player = Player::getInstance();

    _haveFirstBuyPop = UIPopBanner::haveFirstBuyPopBanner();
    if (_haveFirstBuyPop)
    {
        time_t nowTime = ConvertGlobalTimeToTimeT(player->getGameTime());

        if (player->checkHasData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FirstBuyPopBanner]))
        {
            player->loadAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FirstBuyPopBanner], _expiryTime);

            if (nowTime >= _expiryTime)
            {
                _expiryTime = nowTime + ONE_HOUR_SECOND;
                player->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FirstBuyPopBanner], _expiryTime);
            }
        }
        else
        {
            _expiryTime = nowTime + ONE_HOUR_SECOND;
            player->saveAccountData(PLAYER_ACCOUNTDATA_KEY[EPlayerAccountDataType::FirstBuyPopBanner], _expiryTime);
        }

        setBuyCoinVisible(false, true);
        setDealBtnTimerVisible(true);
    }
    else if (UIPopBanner::haveOnSalePopBanner())
    {
        setBuyCoinVisible(false, true);
        setDealBtnTimerVisible(false);

        _haveOnSalePop = true;
    }

    // have mail unread
    setFeedbackRedball(FeedbackManager::getInstance()->isMailUnread());
    UIPopBanner::getPopBannerList();

    LeadersManager::getInstance()->startRequestLeadersRank();
}

void UIMainMenu::setCoin(uint64 coin)
{
    if (_coin == coin)
        return;

    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(1.0f, _coin, coin);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    _moneyFont->runAction(pAction);

    _coin = coin;
}

void UIMainMenu::setLevel(const int level)
{
    if (_level == level)
        return;

    _level = level;
	_lvFont->setString(StringUtils::format("Lv.%d", level));
}

void UIMainMenu::setExp(float percent)
{
    if (_expPercent == percent)
        return;

    LoagingBarIncrease* pLoadingBarAction = LoagingBarIncrease::create(2.0f, _expPercent, percent);
    pLoadingBarAction->setMaskLayout(_particleMask);
    _expBar->runAction(pLoadingBarAction);

    CountTextBMFontToNum* pBMFontAction = CountTextBMFontToNum::create(2.0f, _expPercent, percent);
    pBMFontAction->setFormat(CountTextBMFontToNum::FORMAT_PERCENT_1DECIMAL);
    _expPercentFont->runAction(pBMFontAction);

    _expPercent = percent;
}

void UIMainMenu::setType(eMainMenuType type)
{
    if (_type == type)
        return;

    _type = type;

    switch (type)
    {
    case eMainMenuType::LOBBY:
        setProfileAndLobbyVisible(true, false);
        _VIPRoom->setVisible(false);
        if (isMarqueePlaying)
            openMarquee();
        break;
    case eMainMenuType::VIP_ROOM:
        setProfileAndLobbyVisible(false, true);
        _VIPRoom->setVisible(true);
        _VIPRoomEnter->play();
        if (isMarqueePlaying)
            openMarquee();
        break;
    case eMainMenuType::SLOT_GAME:
        setProfileAndLobbyVisible(false, true);
        _VIPRoom->setVisible(false);
        if (isMarqueePlaying)
            closeMarquee();
        break;
    case eMainMenuType::BONUS_GAME:
        setProfileAndLobbyVisible(false, true);
        _VIPRoom->setVisible(false);
        if (isMarqueePlaying)
            closeMarquee();
        break;
    case eMainMenuType::COUNT:
        break;
    default:
        break;
    }
}

Vec2 UIMainMenu::getCoinWorldPosition()
{
    if (_coinImage)
        return _coinImage->getWorldPosition();
    
    return Vec2::ZERO;
}

float UIMainMenu::getExpPercent()
{
	Player::getInstance()->getExp();
	return 50.0f;
}

void UIMainMenu::setBuyCoinVisible(bool buyCoin1Visible, bool buyCoin2Visible)
{
	_buyCoin1Layout->setVisible(buyCoin1Visible);
	_buyCoin2Layout->setVisible(buyCoin2Visible);
}

void UIMainMenu::setDealBtnTimerVisible(bool visible)
{
    _dealBtnTimer->setVisible(visible);
    _dealBtn->setVisible(!visible);
    _dealRemainTimeFont->setVisible(visible);
}

void UIMainMenu::setProfileAndLobbyVisible(bool profile, bool lobby)
{
    _profile->setVisible(profile);
    _profileBtn->setVisible(profile);
    _lobby->setVisible(lobby);
}

void UIMainMenu::touchBuyCoin1(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIController::getInstance()->getController(EUITAG::EUITAG_IAPShop)->openUI();
}

void UIMainMenu::touchBuyCoin2Buy(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIController::getInstance()->getController(EUITAG::EUITAG_IAPShop)->openUI();
}

void UIMainMenu::touchBuyCoin3Deal(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIPopBanner::openPopBannerByType(PopBannerData::EOPENTYPE_DEAL_WITH_COUNTDOWN);
    UIController::getInstance()->openStoredUI();
}

void UIMainMenu::touchBuyCoin4Deal(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIPopBanner::openPopBannerByType(PopBannerData::EOPENTYPE_DEAL);
}

void UIMainMenu::touchProfile(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    auto cProfile = UIController::getInstance()->getController<UIProfile*>(EUITAG::EUITAG_PROFILE);
    if (cProfile)
        cProfile->openUIByType(UIProfile::PROFILETYPE_SELF);
}

void UIMainMenu::touchLobby(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    UIController::getInstance()->goPrevious();
}

void UIMainMenu::touchSetting(Ref* sender, Widget::TouchEventType type)
{
	if (type != Widget::TouchEventType::ENDED)
		return; 

    UIController::getInstance()->getController<UIGameSetting*>(EUITAG::EUITAG_GAMESETTING)->openUI();
}

void UIMainMenu::touchFeedback(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIController::getInstance()->getController<UIFeedback*>(EUITAG::EUITAG_FEEDBACK)->openUI();
}

void UIMainMenu::touchLv(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _lvDetail->stopAllActions();

    // show lv detail
    if (_lvDetailOpen)
    {
        _lvDetail->runAction(MoveTo::create(0.3f, Vec2(_lvDetail->getPositionX(), MAINMENU_HINT_UP_POSY)));
        _maskLayout->setTouchEnabled(false);
    }
    else
    {
        _lvDetail->runAction(MoveTo::create(0.3f, Vec2(_lvDetail->getPositionX(), MAINMENU_HINT_DOWN_POSY)));
        _maskLayout->setTouchEnabled(true);
        setLvDetail();
    }
    
    AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_SLIDE, false);

    _lvDetailOpen = !_lvDetailOpen;
}

void UIMainMenu::touchVipCard(Ref* sender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::BEGAN)
    {
        //_vipCard->setScale(1.1f);
        _vipCard->setRotation(-7.0f);
        return;
    }
    else if (type == Widget::TouchEventType::ENDED)
    {
        //_vipCard->setScale(1.0f);
        _vipCard->setRotation(0.0f);

        UIController::getInstance()->getController<UIVip*>(EUITAG::EUITAG_VIP)->openUI();
    }
    else if (type == Widget::TouchEventType::CANCELED)
    {
        //_vipCard->setScale(1.0f);
        _vipCard->setRotation(0.0f);
    }
    else
        return;
}

void UIMainMenu::touchCoin(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
#ifdef BUILD_DEV
    if (_type == eMainMenuType::SLOT_GAME)
        UIController::getInstance()->getController(EUITAG::EUITAG_SLOTDEBUG)->openUI();
    else 
        UIController::getInstance()->getController(EUITAG::EUITAG_DASHBOARD)->openUI();
#endif
}

void UIMainMenu::setLvDetail()
{
    _lvDetailExp->setString(StringUtils::toString(GetMoneyFormat(getRamainExpToNextLv())) + safeGetStringData(419));

    _lvDetailPointToNextLv->setString(safeGetStringData(408) + " " + StringUtils::toString(Player::getInstance()->getLevel() + 1));

    int type = (eMainMenuHintType)0;
    for (int i = 0; i < MAINMENU_DETAIL_CONUT_LIMIT; ++i)
    {
        bool hasData = false;
        _lvDetailText[i]->setVisible(false);
        _lvDetailData[i]->setVisible(false);

        while (!hasData && type != eMainMenuHintType::HINT_COUNT)
        {
            switch (type)
            {
            case LEVELUP_BONUS:
                if (getLevelUpBonus() != 0)
                    hasData = true;
                break;
            case MAX_BET:
                if (getMaxBet() != 0)
                    hasData = true;
                break;
            case LOBBY_BONUS:
                if (getLobbyBonus() != 0)
                    hasData = true;
                break;
            case VIP_POINT:
                if (getVipPoint() != 0)
                    hasData = true;
                break;
            case NEW_MACHINE:
                if (getNewMachine() != "")
                    hasData = true;
                break;
            case FB_LOGIN_BONUS:
                if (getFBMoney() != 0)
                    hasData = true;
                break;
            case LOBBY_MEGA_BONUS:
                if (getLobbyMegaBonus() != 0)
                    hasData = true;
                break;
            case HINT_COUNT:
                break;
            default:
                break;
            }

            if (!hasData)
                type += 1;
        }

        if (hasData)
        {
            _lvDetailText[i]->setVisible(true);
            _lvDetailData[i]->setVisible(true);
        }

        switch (type)
        {
        case LEVELUP_BONUS:
            _lvDetailText[i]->setString(getLevelUpBonusText());
            _lvDetailData[i]->setString(StringUtils::toString(getLevelUpBonus()));
            type += 1;
            break;
        case MAX_BET:
            _lvDetailText[i]->setString(getMaxBetText());
            _lvDetailData[i]->setString(StringUtils::toString(getMaxBet()));
            type += 1;
            break;
        case LOBBY_BONUS:
            _lvDetailText[i]->setString(getLobbyBonusText());
            _lvDetailData[i]->setString(StringUtils::toString(getLobbyBonus()));
            type += 1;
            break;
        case VIP_POINT:
            _lvDetailText[i]->setString(getVipPointText());
            _lvDetailData[i]->setString(StringUtils::toString(getVipPoint()));
            type += 1;
            break;
        case NEW_MACHINE:
            _lvDetailText[i]->setString(getNewMachineText());
            _lvDetailData[i]->setString(getNewMachine());
            type += 1;
            break;
        case FB_LOGIN_BONUS:
            _lvDetailText[i]->setString(getFBMoneyText());
            _lvDetailData[i]->setString(StringUtils::toString(getFBMoney()));
            type += 1;
            break;
        case LOBBY_MEGA_BONUS:
            _lvDetailText[i]->setString(getLobbyMegaBonusText());
            _lvDetailData[i]->setString(StringUtils::toString(getLobbyMegaBonus()));
            type += 1;
            break;
        case HINT_COUNT:
            break;
        default:
            break;
        }
    }
}

void UIMainMenu::startDealTime(unsigned int remainTime)
{
    //_remainTime = remainTime;

    //if (_remainTime <= 0)
    //    return;

    //setBuyCoinVisible(false, true);

    //int h = _remainTime / ONE_HOUR_SECOND;
    //int m = (_remainTime % ONE_HOUR_SECOND) / ONE_MINUTE_SECOND;
    //int s = _remainTime % ONE_MINUTE_SECOND;

    //_dealRemainTimeFont->setString(StringUtils::format("%02d:%02d:%02d", h, m, s));
}

void UIMainMenu::countingDownDealTime(float dt)
{
    time_t nowTime = ConvertGlobalTimeToTimeT(Player::getInstance()->getGameTime());
    if (nowTime > _expiryTime || Player::getInstance()->getGameFlag(EGameFlagEntry::EGF_FirstBuy))
    {
        if (UIPopBanner::haveOnSalePopBanner())
        {
            setBuyCoinVisible(false, true);
            setDealBtnTimerVisible(false);
            _haveOnSalePop = true;
        }
        else
        {
            setBuyCoinVisible(true, false);
            _haveOnSalePop = false;
        }
        _dealRemainTimeFont->setString("00:00:00");
        _haveFirstBuyPop = false;
        return;
    }

    _firstBuyRemainTime = _expiryTime - nowTime;

    int h = _firstBuyRemainTime / ONE_HOUR_SECOND;
    int m = (_firstBuyRemainTime % ONE_HOUR_SECOND) / ONE_MINUTE_SECOND;
    int s = _firstBuyRemainTime % ONE_MINUTE_SECOND;

    _dealRemainTimeFont->setString(StringUtils::format("%02d:%02d:%02d", h, m, s));

    if (_firstBuyRemainTime < 0)
    {
        if (UIPopBanner::haveOnSalePopBanner())
        {
            setBuyCoinVisible(false, true);
            setDealBtnTimerVisible(false);
            _haveOnSalePop = true;
        }
        else
        {
            setBuyCoinVisible(true, false);
            _haveOnSalePop = false;
        }
        _dealRemainTimeFont->setString("00:00:00");
        _haveFirstBuyPop = false;
    }
}

void UIMainMenu::setSparkPos(Sprite* sprite, Vec2 vec)
{
    sprite->setPosition(vec);
}

void UIMainMenu::facebookPictureRequest()
{
    //cocos2d::network::HttpRequest* request = new (std::nothrow) cocos2d::network::HttpRequest();

    std::string facebookID = "";
    facebookID = PlatformProxy::getInstance()->getFacebookID();

    if (facebookID == "")
        return;
  
    //int width = _profile->getContentSize().width;
    //int height = _profile->getContentSize().height;
    std::string url = StringUtils::format("https://graph.facebook.com/%s/picture?width=%d&height=%d", facebookID.c_str(), 160, 160);
    
    Sprite * sprite = Sprite::create();
    sprite->setScale(_profile->getContentSize().height / 160);
    sprite->setAnchorPoint(Vec2::ZERO);
    _profile->addChild(sprite);

    PhotoManager::getInstance()->setSpriteFrameByID(sprite, Value(Player::getInstance()->getID()).asInt(), url);

}

void UIMainMenu::facebookPictureResponse(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        log("facebookPictureResponse - No Response");
        return;
    }
    if (!response->isSucceed())
    {
        log("facebookPictureResponse - Response failed");
        log("facebookPictureResponse - Error buffer: %s", response->getErrorBuffer());
        return;
    }

    std::vector<char> *buffer = response->getResponseData();
    const char* file_char = buffer->data();
    Image * image = new  Image();
    image->initWithImageData(reinterpret_cast<const unsigned char*>(&(buffer->front())), buffer->size());
    Texture2D * texture = new  Texture2D();
    texture->initWithImage(image);

    Sprite * sprite = Sprite::createWithTexture(texture);
    sprite->setAnchorPoint(Vec2::ZERO);
    _profile->addChild(sprite);

    CC_SAFE_RELEASE(image);
    CC_SAFE_RELEASE(texture);
}

unsigned int UIMainMenu::getLvUpLeftExp()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel());
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getLvUpLeftExp point nullptr");
        return 0;
    }

    return expTable->EXP - Player::getInstance()->getExp();
}

unsigned int UIMainMenu::getRamainExpToNextLv()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel());
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getLvExpPercent point nullptr");
        return 0;
    }

    return expTable->EXP - Player::getInstance()->getExp();
}

unsigned int UIMainMenu::getLvUpBonus()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel() + 1);
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getLvUpBonus point nullptr");
        return 0;
    }

    return expTable->Money;
}

unsigned int UIMainMenu::getVipPoint()
{
    if (Player::getInstance()->isMaxVipLv())
        return 0;

    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel() + 1);
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getVipPoint point nullptr");
        return 0;
    }

    return expTable->VipPoint;
}

std::string UIMainMenu::getVipPointText()
{
    return safeGetStringData(412);
}

std::string UIMainMenu::getNewMachine()
{
    std::vector<LobbyConfigData*> result;
    ExternalTable<LobbyConfigData>::getInstance()->newSQLQuery(StringUtils::format("SlotMachineID = %d", Player::getInstance()->getLevel() + 1).c_str(), &result);
    
    if (!result.empty())
    {
        auto slotTable = ExternalTable<SlotMachineData>::getRecordByID(result.at(0)->SlotMachineID);
        if (slotTable)
        {
            ExternalTable<LobbyConfigData>::getInstance()->deleteSQLQuery(&result);
            return safeGetStringData(slotTable->SlotNameID);
        }            
    }

    ExternalTable<LobbyConfigData>::getInstance()->deleteSQLQuery(&result);
    return "";


}

std::string UIMainMenu::getNewMachineText()
{
    return safeGetStringData(413);
}

unsigned int UIMainMenu::getMaxBet()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel() + 1);
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getMaxBet point nullptr");
        return 0;
    }

    return expTable->MaxBet;
}

std::string UIMainMenu::getMaxBetText()
{
    return safeGetStringData(410);
}

unsigned int UIMainMenu::getLobbyMegaBonus()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel() + 1);
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getMaxBet point nullptr");
        return 0;
    }

    return expTable->MegaLobbyBonus;
}

std::string UIMainMenu::getLobbyMegaBonusText()
{
    return safeGetStringData(416);
}

unsigned int UIMainMenu::getFBMoney()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel() + 1);
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getFBMoney point nullptr");
        return 0;
    }

    return expTable->FBMoney;
}

std::string UIMainMenu::getFBMoneyText()
{
    return safeGetStringData(415);
}

unsigned int UIMainMenu::getLevelUpBonus()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel() + 1);
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getFBMoney point nullptr");
        return 0;
    }

    return expTable->Money;
}

std::string UIMainMenu::getLevelUpBonusText()
{
    return safeGetStringData(409);
}

unsigned int UIMainMenu::getLobbyBonus()
{
    auto expTable = ExternalTable<LevelUpData>::getRecordByID(Player::getInstance()->getLevel() + 1);
    if (expTable == nullptr)
    {
        CCLOG("UIMainMenu::getFBMoney point nullptr");
        return 0;
    }

    return expTable->LobbyBonus;
}

std::string UIMainMenu::getLobbyBonusText()
{
    return safeGetStringData(411);
}

void UIMainMenu::setVipCard(int vipLv)
{
    if (_vipLv == vipLv)
        return;

    _vipLv = vipLv;
    _vipCard->loadTexture(StringUtils::format("UIVIP_vipcard_Lv%02d_2.png", vipLv), TextureResType::PLIST);
}

void UIMainMenu::addNotice(std::string content)
{
    _contentList.push_back(content);
}

void UIMainMenu::fireNotice()
{
    if (_contentList.size() > 0)
    {
        _noticeContent->setString(_contentList[0]);
        _contentList.erase(_contentList.begin());
        _noticeAction->play();

        _noticeColddown = _noticeAction->getTotalTime();
    }
}

void UIMainMenu::playJackpotNotice()
{
    if (!_bJackpotSwitch)
    {
        _jackpotNoticeData.clear();
        return;
    }

    jackpotNoticeData data = _jackpotNoticeData.back();
    _jackpotNoticeData.pop_back();

    auto slotMachine = ExternalTable<SlotMachineData>::getRecordByID(data.slotMachineID);
    if (!slotMachine)
        return;

    _machineName->setString(safeGetStringData(slotMachine->SlotNameID));
    _rewardKind->setString(safeGetStringData(data.rewardID));
    _scoreBM->setString(GetMoneyFormat(data.coin));
    _playerName->setString(data.playerName);

    Sprite * sprite = Sprite::create();
    sprite->setScale(_headPic->getContentSize().height / 160);// sprite->getContentSize().height);
    sprite->setAnchorPoint(Vec2::ZERO);
    _headPic->removeAllChildren();
    _headPic->addChild(sprite);
    PhotoManager::getInstance()->setSpriteFrameByID(sprite, data.playerID, data.headURL);
    
    float fixPos = 870.0f - (_machineName->getContentSize().width + _rewardKind->getContentSize().width + _scoreBM->getContentSize().width +
        _headPic->getContentSize().width + _playerName->getContentSize().width);
    auto dummyPos = Vec2(200.0f + (fixPos / 2), 709.0f);
    float margin = 15.0f;

    _machineName->setPosition(Vec2(dummyPos.x + _machineName->getContentSize().width, dummyPos.y));
    _rewardKind->setPosition(Vec2(_machineName->getPositionX() + margin + _rewardKind->getContentSize().width, dummyPos.y));
    _scoreBM->setPosition(Vec2(_rewardKind->getPositionX() + margin, dummyPos.y));
    _headPic->setPosition(Vec2(_scoreBM->getPositionX() + margin + _scoreBM->getContentSize().width, _headPic->getPositionY()));
    _playerName->setPosition(Vec2(_headPic->getPositionX() + margin + _headPic->getContentSize().width, dummyPos.y));

    _infoBarAction->play();
    _jackpotNoticeColddown = _infoBarAction->getTotalTime();
}

void UIMainMenu::UpdatePerSecond(float dt)
{
    if (_checkOnceTime &&
        _isCheckPopUIDone &&
        !isEnterAnimationIsPlaying() &&
        !_mainUINode->getActionByTag(MMATAG_ENTERANIM) &&
        UIController::getInstance()->isStoreUIEmpty())
    {
        _touchMask->setTouchEnabled(false);
        _checkOnceTime = false;
    }

    if (_haveFirstBuyPop)
        countingDownDealTime(dt);
    else if (_haveOnSalePop)
    {
        if (UIPopBanner::haveOnSalePopBanner())
        {
            setBuyCoinVisible(false, true);
            setDealBtnTimerVisible(false);
            _haveOnSalePop = true;
        }
        else
        {
            setBuyCoinVisible(true, false);
            _haveOnSalePop = false;
        }
    }

    if ( _contentList.size() > 0 && _noticeColddown <= 0)
    {
        fireNotice();
    }
    if (_type != LOBBY && _type != VIP_ROOM)
    {
        auto uiController = UIController::getInstance();
        if (!_jackpotNoticeData.empty() && _jackpotNoticeColddown <= 0 &&
            uiController->isStoreUIEmpty() && uiController->isUIStackEmpty())
        {
            auto slotMachine = uiController->getController(uiController->getCurrentSlotMachineTag());
            if (slotMachine && !slotMachine->IsPlayingAction())
            {
                playJackpotNotice();
            }
        }
    }
    else
    {
        if (!_lastTimeJPDatas.empty() && !isMarqueePlaying)
        {
            openMarquee();
            pushMarquee();
        }
    }

    if (_noticeColddown > 0)
        _noticeColddown -= dt;

    if (_jackpotNoticeColddown > 0)
    {
        _jackpotNoticeColddown -= dt;
    }
	
    // Update
    setCoin(Player::getInstance()->getCoin());
    setLevel(Player::getInstance()->getLevel());
    setExp(getLvExpPercent());
    setVipCard(Player::getInstance()->getVipLv());
    //setBuyCoinVisible(true, false);
}

void UIMainMenu::UpdateThirtySecond(float dt)
{
    auto uiController = UIController::getInstance();
    if (!MessageCenter::getInstance()->isNetworkCongestion())
        requestJackpotNoticeData();
}

void UIMainMenu::playEnterUIAction()
{
    _enterAnimation->updateToFrameByTime(0);
    _enterAnimation->play();

    float animaitonTime = _enterAnimation->getTotalTime();
    DelayTime* dTime = DelayTime::create(animaitonTime + 0.5f);
    auto callFunc = CallFunc::create(CC_CALLBACK_0(UIMainMenu::checkAndOpenPopUI, this));
    auto actionSeq = Sequence::create(dTime, callFunc, nullptr);
    actionSeq->setTag(MMATAG_ENTERANIM);

    _mainUINode->runAction(actionSeq);
}

void UIMainMenu::requestJackpotNoticeData()
{
    //_jackpotNoticeData.clear();
    RequestJackpotNotice();
}

void UIMainMenu::setFeedbackRedball(bool visible)
{
    _feedbackRedball->setVisible(visible);
}

void UIMainMenu::checkAndOpenPopUI()
{
    if (_isCheckPopUIDone)
        return;
        
    if (!Player::getInstance()->getGameFlag(EGameFlagEntry::EGF_Welcome_Bonus))
    {
        UIController::getInstance()->getController(EUITAG::EUITAG_WELCOME_BONUS)->openUILater();
    }

    if (Player::getInstance()->getLoginType() == ELoginType::ELT_Facebook &&
        !Player::getInstance()->getGameFlag(EGameFlagEntry::EGF_FB_Connect_Bonus))
    {
        UIController::getInstance()->getController(EUITAG::EUITAG_FBLOGIN_BONUS)->openUILater();
    }

    // 新手教學
    if (UITutorialController::getInstance()->isTutorialing())
    {
        UITutorialController::getInstance()->openTutorial();
    }
    else
    {
        if (Player::getInstance()->getLoginType() == ELoginType::ELT_Guest)
        {
            UIController::getInstance()->getController(EUITAG::EUITAG_CONNECTFBBENEFIT)->openUILater();
        }
    }

    //已經領過welcome bonus或已新手才會跳popBanner
    if (Player::getInstance()->getGameFlag(EGameFlagEntry::EGF_Welcome_Bonus) &&
        Player::getInstance()->getGameFlag(EGameFlagEntry::EGF_Tutorial_Done))
        UIPopBanner::openPopBannerByType(PopBannerData::EOPENTYPE_LOGIN_TO_LOBBY);

    LeadersManager::getInstance()->openUILeadersReward();
    UIController::getInstance()->openStoredUI();

    _isCheckPopUIDone = true;
}

void UIMainMenu::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    rapidjson::Document document;
    document.Parse(json.c_str());
    if (!document.IsObject())
        return;

    int errorCode = -1;
    if (document["ERR_CODE"].IsInt())
        errorCode = document["ERR_CODE"].GetInt();
    if (errorCode != 1)
        return;

#define JACKPOT_DATA "notice"

    std::vector<jackpotNoticeData> jpDatas;
    std::map<time_t, jackpotNoticeData> thisTimeJPDatas;
    bool newWinner = false;

    rapidjson::Value::ConstMemberIterator itrm = document.FindMember(JACKPOT_DATA);
    if (itrm == document.MemberEnd() || !itrm->value.IsObject())
        return;

    _jackpotNoticeData.clear();

    const rapidjson::Value& momber = document[JACKPOT_DATA];
    for (itrm = momber.MemberBegin(); itrm != momber.MemberEnd(); ++itrm)
    {
        if (!itrm->value.IsObject())
            return;

        jackpotNoticeData data;
        GlobalTime gloablTime;

        const rapidjson::Value& obj = itrm->value;

        auto iter = obj.FindMember("slotMachineID");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.slotMachineID = iter->value.GetInt();

        iter = obj.FindMember("rewardID");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.rewardID = iter->value.GetInt();

        iter = obj.FindMember("win");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        data.coin = iter->value.GetInt();

        // 目前沒資料一定會break
        iter = obj.FindMember("playerID");
        if (iter != obj.MemberEnd() && iter->value.IsInt())
             data.playerID = iter->value.GetInt();

        iter = obj.FindMember("avatar");
        if (iter == obj.MemberEnd() || !iter->value.IsString())
            break;
        data.headURL = iter->value.GetString();

        iter = obj.FindMember("playerName");
        if (iter == obj.MemberEnd() || !iter->value.IsString())
            break;
        data.playerName = iter->value.GetString();

        iter = obj.FindMember("year");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        gloablTime.year = iter->value.GetInt();
        iter = obj.FindMember("mon");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        gloablTime.month = iter->value.GetInt();
        iter = obj.FindMember("day");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        gloablTime.day = iter->value.GetInt();
        iter = obj.FindMember("hour");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        gloablTime.hour = iter->value.GetInt();
        iter = obj.FindMember("min");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        gloablTime.minute = iter->value.GetInt();
        iter = obj.FindMember("sec");
        if (iter == obj.MemberEnd() || !iter->value.IsInt())
            break;
        gloablTime.second = iter->value.GetInt();

        time_t timeKey = ConvertGlobalTimeToTimeT(gloablTime);
        if (_lastTimeJPDatas.find(timeKey) == _lastTimeJPDatas.end())
        {
            newWinner = true;
            jpDatas.push_back(data);
        }

        thisTimeJPDatas[timeKey] = data;
    }

    if (newWinner)
    {
        _jackpotNoticeData.clear();
        _jackpotNoticeData = jpDatas;
    }

    _lastTimeJPDatas.clear();
    _lastTimeJPDatas = thisTimeJPDatas;
}

void UIMainMenu::pushMarquee()
{
    if (_lastTimeJPDatas.empty())
    {
        isMarqueePlaying = false;
        closeMarquee();
        return;
    }
    isMarqueePlaying = true;
    if (_marqueeIndex >= (int)_lastTimeJPDatas.size())
        _marqueeIndex = 0;
    auto iter = _lastTimeJPDatas.begin();
    for (int i = 0; i < _marqueeIndex; i++)
        iter++;
    jackpotNoticeData data = iter->second;

    auto message = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UIMainMenu/UIMainMenu_Marquee.ExportJson"));
    message->setTouchEnabled(false);
    _margueeLayout->addChild(message);
    auto machineName = message->getChildByName<Text*>("machine_name");
    machineName->enableOutline(Color4B::WHITE, 1);
    auto rewardKind = message->getChildByName<Text*>("reward_kind");
    rewardKind->enableOutline(Color4B::WHITE, 1);
    auto scoreBM = message->getChildByName<TextBMFont*>("reward_score");
    auto headPic = message->getChildByName<Layout*>("head_pic");
    auto playerName = message->getChildByName<Text*>("player_name");
    playerName->enableOutline(Color4B::WHITE, 1);

    auto slotMachine = ExternalTable<SlotMachineData>::getRecordByID(data.slotMachineID);
    if (slotMachine)
        machineName->setString(safeGetStringData(slotMachine->SlotNameID));
    rewardKind->setString(safeGetStringData(data.rewardID));
    scoreBM->setString("$" + GetMoneyFormat(data.coin));
    playerName->setString(data.playerName);

    Sprite * sprite = Sprite::create();
    sprite->setScale(headPic->getContentSize().height / 160);// sprite->getContentSize().height);
    sprite->setAnchorPoint(Vec2::ZERO);
    headPic->removeAllChildren();
    headPic->addChild(sprite);
    PhotoManager::getInstance()->setSpriteFrameByID(sprite, data.playerID, data.headURL);

    auto dummyPos = Vec2(2, 25);
    float margin = 15.0f;

    machineName->setPosition(Vec2(dummyPos.x + machineName->getContentSize().width, dummyPos.y));
    rewardKind->setPosition(Vec2(machineName->getPositionX() + margin + rewardKind->getContentSize().width, dummyPos.y));
    scoreBM->setPosition(Vec2(rewardKind->getPositionX() + margin , dummyPos.y));
    headPic->setPosition(Vec2(scoreBM->getPositionX() + margin + scoreBM->getContentSize().width, headPic->getPositionY() + 8));
    playerName->setPosition(Vec2(headPic->getPositionX() + margin + headPic->getContentSize().width, dummyPos.y));

    message->setContentSize(Size(playerName->getPosition().x + playerName->getContentSize().width, message->getContentSize().height));

    message->setPosition(Vec2(1334, 0));

    float pixelPerFrame = 160;
    auto distance = message->getContentSize().width;
    auto duration = (1334 + distance) / pixelPerFrame;
    auto callNextDuration = (distance + 180.0f) / pixelPerFrame;
    auto callNext = Sequence::create(
        DelayTime::create(callNextDuration)
        , CallFunc::create(CC_CALLBACK_0(UIMainMenu::pushMarquee, this))
        , nullptr);

    Vector<FiniteTimeAction*> arrayOfActions;
    //arrayOfActions.pushBack(DelayTime::create(0.15f));
    arrayOfActions.pushBack(Spawn::create(
        MoveTo::create(duration, Vec2(-distance, 0))
        , callNext
        , nullptr));
    arrayOfActions.pushBack(RemoveSelf::create());
    auto actionsSeq = Sequence::create(arrayOfActions);
    message->runAction(actionsSeq);

    _marqueeIndex++;

}

void UIMainMenu::openMarquee()
{
    _MargueeEnter->play();
    _margueeLayout->setVisible(true);
}

void UIMainMenu::closeMarquee()
{
    _MargueeExit->play();
    _margueeLayout->runAction(
        Sequence::create(
            DelayTime::create(_MargueeExit->getTotalTime()),
            CallFunc::create(CC_CALLBACK_0(Layout::setVisible, _margueeLayout,false)),
            nullptr
        )
    );

}
