#ifndef __UI_MAIN_MENU_H__
#define __UI_MAIN_MENU_H__

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "../GlobalDefine/EnumType.h"
#include "UIBasicUnit.h"
#include "cocostudio/CocoStudio.h"
#include "network/HttpClient.h"
#include "../DataCenter/ExternalTable.h"
#include "../GlobalDefine/CustomTypeDef.h"

USING_NS_CC;

using namespace ui;
 
enum eMainMenuType
{
    LOBBY,
    VIP_ROOM,
    SLOT_GAME,
    BONUS_GAME,
    COUNT
};

enum eMainMenuHintType
{
    LEVELUP_BONUS = 0,
    MAX_BET,
    LOBBY_BONUS,
    VIP_POINT,
    NEW_MACHINE,
    FB_LOGIN_BONUS,
    LOBBY_MEGA_BONUS,
    HINT_COUNT
};

enum EMainMenuActionTag
{
    MMATAG_ENTERANIM,
};

struct jackpotNoticeData
{
    int slotMachineID;
    int rewardID;
    uint64 coin;
    int playerID;
    std::string headURL;
    std::string playerName;

    jackpotNoticeData() :slotMachineID(1), rewardID(0), coin(0), headURL(""), playerName("")
    {};
    ~jackpotNoticeData(){};
};

const int MAINMENU_DETAIL_CONUT_LIMIT = 6;

class UIMainMenu : public UIBasicUnit
{

public:
	static UIMainMenu* create();

public:

	UIMainMenu();
	~UIMainMenu();

	virtual void destroyUI() override;

	virtual void notifyCloseUI() override;

	virtual void updateUI() override;

    virtual void UpdatePerSecond(float dt) override;

    virtual void UpdateThirtySecond(float dt) override;

	//
    void setCoin(uint64 coin);

	void setLevel(const int level);

    void setExp(float percent);

    void setType(eMainMenuType type);

    Vec2 getCoinWorldPosition();

    void addNotice(std::string content);

    void fireNotice();

    void playJackpotNotice();

    void playEnterUIAction()override;

    void onDispatchResponse(const std::string& requestTag, const std::string& json);

    void responseJackpotNotice(jackpotNoticeData data);

    void setJackpotSwitch(bool jackpotSwitch){ _bJackpotSwitch = jackpotSwitch; };

    void facebookPictureRequest();

    void requestJackpotNoticeData();

    void setFeedbackRedball(bool visible);

    int getFirstBuyRemainTime(){ return _firstBuyRemainTime; };

    bool isEnterAnimationIsPlaying() { return _enterAnimation->isPlaying(); };

private:
	bool init();

	virtual bool createUI() override;

	// 
	float getExpPercent();

	void setBuyCoinVisible(bool buyCoin1Visible, bool buyCoin2Visible);

    void setDealBtnTimerVisible(bool visible);

    void setProfileAndLobbyVisible(bool profile, bool lobby);

	void touchBuyCoin1(Ref* sender, Widget::TouchEventType type);

	void touchBuyCoin2Buy(Ref* sender, Widget::TouchEventType type);

	void touchBuyCoin3Deal(Ref* sender, Widget::TouchEventType type);

    void touchBuyCoin4Deal(Ref* sender, Widget::TouchEventType type);

    void touchProfile(Ref* sender, Widget::TouchEventType type);

    void touchLobby(Ref* sender, Widget::TouchEventType type);

	void touchSetting(Ref* sender, Widget::TouchEventType type);

    void touchFeedback(Ref* sender, Widget::TouchEventType type);

    void touchLv(Ref* sender, Widget::TouchEventType type);

    void touchVipCard(Ref* sender, Widget::TouchEventType type);

    void touchCoin(Ref* sender, Widget::TouchEventType type);

    void setLvDetail();

    void startDealTime(unsigned int remainTime);

    void countingDownDealTime(float dt);

	void setSparkPos(Sprite* sprite, Vec2 vec);

    void facebookPictureResponse(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

    unsigned int getLvUpLeftExp();

    unsigned int getRamainExpToNextLv();

    unsigned int getLvUpBonus();

    unsigned int getVipPoint();

    std::string getVipPointText();

    std::string getNewMachine();

    std::string getNewMachineText();

    unsigned int getMaxBet();

    std::string getMaxBetText();

    unsigned int getLobbyMegaBonus();

    std::string getLobbyMegaBonusText();

    unsigned int getFBMoney();

    std::string getFBMoneyText();

    unsigned int getLevelUpBonus();

    std::string getLevelUpBonusText();

    unsigned int getLobbyBonus();

    std::string getLobbyBonusText();

    void setVipCard(int vipLv);

    void checkAndOpenPopUI();

    // push marguee
    void pushMarquee();
    void openMarquee();
    void closeMarquee();

private:
    uint64 _coin = 0;
    int _level = -1;
    float _expPercent =-1.0f;
    int _vipLv = -1;
	Layout* _uiMainMenu = nullptr;

    Layout* _touchMask = nullptr;

	Layout* _buyCoin1Layout = nullptr;

	Button* _buyCoin1Btn = nullptr;

	Layout* _buyCoin2Layout = nullptr;

	Button* _buyCoin2Btn = nullptr;

	Button* _dealBtnTimer = nullptr;

    Button* _dealBtn = nullptr;

    int _expiryTime = 0;

    Layout* _lvTouchPanel = nullptr;

    Layout* _lvDetail = nullptr;

    Text* _lvDetailExp = nullptr;

    Text* _lvDetailPointToNextLv = nullptr;

    Text* _lvDetailText[MAINMENU_DETAIL_CONUT_LIMIT];

    Text* _lvDetailData[MAINMENU_DETAIL_CONUT_LIMIT];

    Layout* _maskLayout = nullptr;

    bool _lvDetailOpen = false;

	float _levelBarPercent = 0.0f;

    LoadingBar* _expBar = nullptr;

    TextBMFont* _moneyFont = nullptr;

    TextBMFont* _lvFont = nullptr;

    TextBMFont* _expPercentFont = nullptr;

    TextBMFont* _dealRemainTimeFont = nullptr;

    ImageView* _profile = nullptr;

    Button* _profileBtn = nullptr;

    Button* _setting = nullptr;

    Button* _lobby = nullptr;

    Button* _feedback = nullptr;

    ImageView* _feedbackRedball = nullptr;

    Layout* _messageLayout = nullptr;

    ImageView* _pig = nullptr;    // �٨S��

    eMainMenuType _type = eMainMenuType::LOBBY;

    ImageView* _coinImage = nullptr; 

    cocostudio::ActionObject* _noticeAction = nullptr;

    Text* _noticeContent = nullptr;

    cocostudio::ActionObject* _infoBarAction = nullptr;
	
	cocostudio::ActionObject* _enterAnimation = nullptr;

    cocostudio::ActionObject* _coinLight = nullptr;

    cocostudio::ActionObject* _MargueeEnter = nullptr;

    cocostudio::ActionObject* _MargueeExit = nullptr;

    cocostudio::ActionObject* _VIPRoomEnter = nullptr;
	
    ClippingNode* _buyBtn1ClippingNode = nullptr;

    ClippingNode* _buyBtn2ClippingNode = nullptr;

    ClippingNode* _dealBtnClippingNode = nullptr;

    ImageView* _vipCard = nullptr;

    Layout* _particleMask = nullptr;

    // scrolling text marguee
    Layout* _margueeLayout = nullptr;

    bool _checkOnceTime = true;
    bool _isCheckPopUIDone = false;

    bool _haveFirstBuyPop = false;
    bool _haveOnSalePop = false;

    int _firstBuyRemainTime = 0;

private:

    std::vector<std::string> _contentList;

    float _noticeColddown = 0;

    float _jackpotNoticeColddown = 0;
    std::map<time_t, jackpotNoticeData> _lastTimeJPDatas;
    std::vector<jackpotNoticeData> _jackpotNoticeData;
    Text* _machineName = nullptr;
    Text* _rewardKind = nullptr;
    TextBMFont* _scoreBM = nullptr;
    Layout* _headPic = nullptr;
    Text* _playerName = nullptr;
    bool _bJackpotSwitch = true;

    // VIP room
    Layout* _VIPRoom = nullptr;

    // marguee 
    int _marqueeIndex = 0;
    bool isMarqueePlaying = false;
};

#endif