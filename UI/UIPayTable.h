#ifndef __UI_PAYTABLE_H__
#define __UI_PAYTABLE_H__

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "UIBasicUnit.h"
#include "cocostudio/CocoStudio.h"


USING_NS_CC;

using namespace ui;
using namespace cocostudio;

class UIPayTable : public UIBasicUnit
{
private:
    static const int TOTAL_PAGE_COUNT = 3;
    static const int SPECIAL_SYMBOL_COUNT = 3;
    static const int SPECIAL_SYMBOL_TEXT_COUNT = 3;
    static const int NORMAL_SYMBOL_COUNT = 10;
    static const int NORMAL_SYMBOL_TEXT_COUNT = 5;
    static const int MAX_PAYLINE_COUNT = 50;

public:
    struct SpecialSymbol
    {
        Layout* _symbol;
        Text* _content;// [SPECIAL_SYMBOL_TEXT_COUNT];
    };

    struct NormalSymbol
    {
        Layout* _symbol;
        TextBMFont* _content[NORMAL_SYMBOL_TEXT_COUNT];
    };

public:
    static UIPayTable* create();

public:
    UIPayTable();
    ~UIPayTable();

    virtual void destroyUI() override;

    virtual void updateUI() override;

    bool openUI(int slotID, std::string exportJsonPath);

    void setSlotMachineID(int ID);

    void setExportJsonPath(std::string exportJsonPath);

    void setPage1Data(std::vector<int>& symbolTypes,
        std::vector<int>& contentIDs_1,
        std::vector<int>& contentIDs_2,
        std::vector<int>& contentIDs_3);

private:
    bool init();

    virtual bool createUI() override;

    void setPage1();

    void setPage2();

    void setPage3();

    void setLeftButtonVisible(bool visible);

    void setRightButtonVisible(bool visible);

    void setButtonTouchEnable(bool enable);

    void touchBackToGame(Ref* pSender, Widget::TouchEventType type);

    void touchRightButton(Ref* pSender, Widget::TouchEventType type);

    void touchLeftButton(Ref* pSender, Widget::TouchEventType type);

private:
    Layout* _payTable = nullptr;

    Button* _backButton = nullptr;

    Button* _buttonLeft = nullptr;

    Button* _buttonRight = nullptr;

    ImageView* _attachment1 = nullptr;
    ImageView* _attachment2 = nullptr;

    int _nowPage = 1;

    int _slotMachineID;

    std::string _exportJsonPath;

    int _gameSymbolBeginID;

    int _gameSymbolEndID;

    int _symbolInfoBeginID;

    int _symbolInfoEndID;

    int _payLineBeginID;

    int _payLineEndID;

    Layout* _pages[TOTAL_PAGE_COUNT];

    // Page1
    SpecialSymbol _specialSymbol[SPECIAL_SYMBOL_COUNT];

    // Page2
    NormalSymbol _normalSymbol[NORMAL_SYMBOL_COUNT];

    // Page3
    ImageView* _payLinePic[MAX_PAYLINE_COUNT];

    Text* _payLineText;

    std::vector<int> _page1SymbolTypes;
    std::vector<int> _page1StringData_1;
    std::vector<int> _page1StringData_2;
    std::vector<int> _page1StringData_3;
};


#endif