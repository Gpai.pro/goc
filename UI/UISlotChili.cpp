#include "UISlotChili.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "AudioManager.h"
#include "../Network/MessageProcess.h"
#include "../GlobalDefine/Player.h"
#include "../GlobalDefine/GameConst.h"
#include "../DataCenter/ExternalTable.h"
#include "spine/spine-cocos2dx.h"
#include "../Animation/CustomSpAnim.h"
#include "UIController.h"
#include "UISlotChiliPayTable.h"
#include "../Animation/ProcedureFX.h"
#include "../Animation/CustomActions.h"
#include "UIBigWin.h"
#include "UIFiveOfAKind.h"
#include "UILevelUp.h"
#include "UIJackpot.h"
#include "UISlotChiliBonusGame.h"
#include "../GlobalDefine/GameFunc.h"
#include "UISlotChiliFreeSpinStart.h"
#include "UISlotChiliFreeSpinResult.h"
#include "UIOutOfCoin.h"
#include "UIMessage.h"
#include "../Shader/ShaderManager.h"
#include "../Gameplay/GameLogic.h"
#include "../GlobalDefine/ImageDefine.h"
#include "UIAutoSpinSetting.h"
#include "UICommon.h"

#define EFF_SLOT5_ROLE_TOUCH 5006
#define EFF_SLOT5_ROLE_WIN_1 5004
#define EFF_SLOT5_ROLE_WIN_2 5005

using namespace cocos2d::ui;

USING_NS_CC;
using namespace spine;

int UISlotChili::s_autoCountValue[AUTOCOUNTLEVEL] = { 10, 50, 100 };
const float UISlotChili::EDIDLINEBTN_HOLDTIME = 1.2f;
const float UISlotChili::ONCE_ALLPAYLINE_TIME = 3.0f;
const float UISlotChili::REPEAT_SINGLEPAYLINE_TIME = 2.5f;
const float UISlotChili::DROPDOWNLIST_FADETIME = 0.2f;
const float UISlotChili::PERFORMSPIN_MINTIME = 1.0f;
const Color3B UISlotChili::BETSELECT_DARKCOLOR = Color3B(128, 128, 128);
const Color3B UISlotChili::PAYLINEBG_DARKCOLOR = Color3B(100, 100, 100);
const Color4B UISlotChili::PAYLINEBG_DARKCOLOR_WITHEXPECTBG = Color4B(255, 255, 255, 80);
const Size SYMBOLSIZE = Size(170, 170);

UISlotChili* UISlotChili::create(int slotMachineID)
{
    UISlotChili* unit = new UISlotChili();

    if (unit && unit->init(slotMachineID))
    {
        return unit;
    }

    CC_SAFE_DELETE(unit);
    return nullptr;
}

bool UISlotChili::init(int slotMachineID)
{
    _slotID = slotMachineID;
    return true;
}

bool UISlotChili::createUI()
{
    //set model data
    _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);

    //設定PayLine最大值, 最小值
    _maxLines = _pSlotData->PayLineEndID - _pSlotData->PayLineBeginID + 1;
    _minLines = (_pSlotData->isLockedPayLine == VALUESENSE_TRUE) ? _maxLines : 1;
    _beginPayLineID = _pSlotData->PayLineBeginID;
    _endPayLineID = _pSlotData->PayLineEndID;

    _isFixedBet = _pSlotData->FixedBetBeginID>0;
    if (_isFixedBet)
    {
        for (int i = _pSlotData->FixedBetBeginID; i <= _pSlotData->FixedBetEndID; i++)
        {
            FixedBetData* pFixBetData = ExternalTable<FixedBetData>::getRecordByID(i);
            if (pFixBetData)
                _betLevelList.push_back(pFixBetData->Bet);
        }
    }
    else
    {
        std::map<int, LevelUpData*>& table = ExternalTable<LevelUpData>::raw();
        for (auto itr : table)
        {
            LevelUpData* curData = itr.second;
            if (curData->MaxBet)
                _betLevelList.push_back(curData->MaxBet);
        }
    }

    _scatterID = _bonusID = _stickyWildID = 0;
    for (int id = _pSlotData->SymbolInfoBeginID; id <= _pSlotData->SymbolInfoEndID; id++)
    {
        SymbolInfoData* pSymbolInfo = ExternalTable<SymbolInfoData>::getRecordByID(id);
        if (!pSymbolInfo)
            continue;
        if (pSymbolInfo->Type == SymbolInfoData::SYMBOLTYPE_SCATTER)
            _scatterID = pSymbolInfo->ID;
        else if (pSymbolInfo->Type == SymbolInfoData::SYMBOLTYPE_BONUS)
            _bonusID = pSymbolInfo->ID;
        else if (pSymbolInfo->Type == SymbolInfoData::SYMBOLTYPE_WILD)
            _stickyWildID = pSymbolInfo->ID;
        // _stickyID = XX;
    }
    _minScatterCombo = _minBonusCombo = 0;
    _minScatterCombo = UISlotUtils::getMinHitCombo(_slotID, _scatterID);
    _minBonusCombo = UISlotUtils::getMinHitCombo(_slotID, _bonusID);

    _reelCount = _pSlotData->ReelEndID - _pSlotData->ReelBeginID + 1;
    for (int reelID = _pSlotData->ReelBeginID; reelID <= _pSlotData->ReelEndID; reelID++)
    {
        ReelData* pReelData = ExternalTable<ReelData>::getRecordByID(reelID);
        if (!pReelData)
            _viewFrameCounts.push_back(0);
        else
            _viewFrameCounts.push_back(pReelData->ViewFrameSize);
    }

    _pRoot = static_cast<ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("UISlot_DL_Machine0005_Chili/UISlot_Chili.ExportJson"));
    CCASSERT(_pRoot, "");
    _mainUINode->addChild(_pRoot);

    UICommon::getInstance()->fixUIPosition(_pRoot, EFP_DOWN);

    Layout* pSlot = static_cast<Layout*>(_pRoot->getChildByName("layout_slot"));
    CCASSERT(pSlot, "");

    Layout* pLayoutSceneBg = static_cast<Layout*>(_pRoot->getChildByName("layout_scenebg"));
    CCASSERT(pLayoutSceneBg, "");

    Layout* pLayoutSlotBackground = static_cast<Layout*>(pSlot->getChildByName("layout_slot_background"));
    CCASSERT(pLayoutSlotBackground, "");

    _pLayoutSlotReel = static_cast<Layout*>(getChildByNodePath(pSlot, "layout_slot_reel"));
    CCASSERT(_pLayoutSlotReel, "");

    Layout* pLayoutSlotForeground = static_cast<Layout*>(pSlot->getChildByName("layout_slot_foreground"));
    CCASSERT(pLayoutSlotForeground, "");

    Layout* pLayoutControls = static_cast<Layout*>(pSlot->getChildByName("layout_controls"));
    CCASSERT(pLayoutControls, "");

    _pLayoutFreeSpin = static_cast<Layout*>(pLayoutControls->getChildByName("layout_freespin"));
    CCASSERT(_pLayoutFreeSpin, "");
    setTraversalCascadeOpacityEnabled(_pLayoutFreeSpin, true);
    _pLayoutFreeSpin->setVisible(true);
    _pLayoutFreeSpin->setOpacity(0);

    _pBMFFreeSpinRemain = static_cast<TextBMFont*>(_pLayoutFreeSpin->getChildByName("bmf_value_freespin"));
    CCASSERT(_pBMFFreeSpinRemain, "");
    _pBMFFreeSpinRemain->setString("");

    _pBMFWinMoneyAmin = static_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_winmoney_anim"));
    _pBMFWinMoneyAmin->setOpacity(0);
    _pLayoutDummyWinMoneyParticle = static_cast<Layout*>(pLayoutControls->getChildByName("layout_dummy_winmoney_particle"));

    //expectBg
    Layout* _pLayoutExpect = static_cast<Layout*>(_pLayoutSlotReel->getChildByName("layout_expect"));
    CCASSERT(_pLayoutExpect, "");

    Layout* pParticleLayout = pSlot->getChildByName<Layout*>("layout_expect_particle");
    for (int i = 0; i < _reelCount; i++)
    {
        ExpectBGGroup bgGroup;
        bgGroup.pScatterBg = static_cast<ImageView*>(_pLayoutExpect->getChildByName(StringUtils::format("img_expectbg_scatter_%d", i + 1)));
        bgGroup.pBonusBg = static_cast<ImageView*>(_pLayoutExpect->getChildByName(StringUtils::format("img_expectbg_bonus_%d", i + 1)));
        bgGroup.pHybridBg = static_cast<ImageView*>(_pLayoutExpect->getChildByName(StringUtils::format("img_expectbg_hybrid_%d", i + 1)));
        bgGroup.pParticles = static_cast<Layout*>(pParticleLayout->getChildByName(StringUtils::format("particle_%d", i + 1)));
        //bgGroup.pParticles->setClippingType(Layout::ClippingType::SCISSOR);
        _expectBgs.push_back(bgGroup);
    }
    CCASSERT(_expectBgs.size() == _reelCount, "");
    _isExpectBgSoundPlayed.clear();
    for (int i = 0; i < _reelCount; i++)
        _isExpectBgSoundPlayed.push_back(false);
    _isMiddleExpectSoundPlayed.clear();
    for (int i = 0; i < _reelCount; i++)
        _isMiddleExpectSoundPlayed.push_back(false);
    _middleExpectSoundHandle = 0;
    
    // freespin ribbon
    Layout* freespinParticle = pSlot->getChildByName<Layout*>("layout_freespin_particle");
    CCASSERT(freespinParticle, "");
    auto visibleSize = Director::getInstance()->getVisibleSize();
    freespinParticle->setPosition(Vec2(freespinParticle->getPosition().x, freespinParticle->getPosition().y - (DESIGN_SIZE.height - visibleSize.height - 70.0f)));

    for (int i = 0; i < RIBBON_PARTICLE_COUNT; ++i)
    {
        Layout* particle = freespinParticle->getChildByName<Layout*>(StringUtils::format("particle_%d", i + 1));
        _ribbonParticles.push_back(particle);
    }

    
    //clipping layout for reels
    _pLayoutReels = static_cast<Layout*>(_pLayoutSlotReel->getChildByName("layout_reels"));
    _pLayoutReels->setClippingEnabled(true);
    _pLayoutReels->setClippingType(Layout::ClippingType::SCISSOR);

    _pLayoutSymbolAnim = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_dummy_symbolanim"));
    CCASSERT(_pLayoutSymbolAnim, "");

    _pLayoutStickySymbolAnim = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_dummy_stickystmbol"));
    CCASSERT(_pLayoutStickySymbolAnim, "");

    iniPayLineComponent();

    //setup event of UI Component
    _pImgSpinText = static_cast<ImageView*>(pLayoutControls->getChildByName("img_spintext"));
    CCASSERT(_pImgSpinText, "");
    _pImgStopText = static_cast<ImageView*>(pLayoutControls->getChildByName("img_stoptext"));
    CCASSERT(_pImgStopText, "");
    _pImgSpinTextGray = static_cast<ImageView*>(pLayoutControls->getChildByName("img_spintextgray"));
    CCASSERT(_pImgSpinTextGray, "");
    _pImgSpinTextGray->setVisible(false);
    _pImgStopTextGray = static_cast<ImageView*>(pLayoutControls->getChildByName("img_stoptextgray"));
    CCASSERT(_pImgStopTextGray, "");
    _pImgStopTextGray->setVisible(false);

    _pLayoutAutoDisable = static_cast<Layout*>(pLayoutControls->getChildByName("layout_auto_disable"));
    CCASSERT(_pLayoutAutoDisable, "");

    _pBMFAutoCount = static_cast<TextBMFont*>(_pLayoutAutoDisable->getChildByName("bmf_autocount"));
    CCASSERT(_pBMFAutoCount, "");

    _pBtnSpin = static_cast<Button*>(pLayoutControls->getChildByName("btn_spin"));
    CCASSERT(_pBtnSpin, "");
    _pBtnSpin->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchSpin, this));

    _pBtnAuto = static_cast<Button*>(pLayoutControls->getChildByName("btn_auto"));
    CCASSERT(_pBtnAuto, "");
    _pBtnAuto->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchAuto, this));

    _pBtnMaxBet = static_cast<Button*>(pLayoutControls->getChildByName("btn_maxbet"));
    CCASSERT(_pBtnMaxBet, "");
    _pBtnMaxBet->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchMaxBet, this));
    _pBtnMaxBet->setSoundID(EFF_NONE);

    _pBtnBet = static_cast<Button*>(pLayoutControls->getChildByName("btn_bet"));
    CCASSERT(_pBtnBet, "");
    _pBtnBet->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchBet, this));

    _pBtnLines = static_cast<Button*>(pLayoutControls->getChildByName("btn_lines"));
    CCASSERT(_pBtnLines, "");
    _pBtnLines->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchLines, this));

    _pBtnPayTable = static_cast<Button*>(pLayoutControls->getChildByName("btn_paytable"));
    CCASSERT(_pBtnPayTable, "");
    _pBtnPayTable->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchPayTable, this));
    
    _pLayoutCloseDropdownListener = static_cast<Layout*>(pLayoutControls->getChildByName("layout_closedropdown_listener"));
    _pLayoutCloseDropdownListener->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchDropDownListBG, this));
    //debug draw
    //_pLayoutCloseDropdownListener->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
    //_pLayoutCloseDropdownListener->setBackGroundColorOpacity(125);
    //_pLayoutCloseDropdownListener->setBackGroundColor(Color3B(0, 255, 0));

    _pLayoutAutoDropdownList = static_cast<Layout*>(pLayoutControls->getChildByName("layout_dropdown_auto"));
    CCASSERT(_pLayoutAutoDropdownList, "");
    setTraversalCascadeOpacityEnabled(_pLayoutAutoDropdownList, true);

    for (int i = 0; i < AUTOCOUNTLEVEL; ++i)
    {
        auto btnAuto = _pLayoutAutoDropdownList->getChildByName<Button*>(StringUtils::format("btn_autonum_%d", i + 1));
        CCASSERT(btnAuto, "");
        btnAuto->setSoundID(EFF_NONE);
        btnAuto->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchAutoValue, this));
        btnAuto->setTitleText(safeGetStringData(681 + i));
    }

    auto btnAutoUnlimited = _pLayoutAutoDropdownList->getChildByName<Button*>("btn_autonum_4");
    CCASSERT(btnAutoUnlimited, "");
    btnAutoUnlimited->setSoundID(EFF_NONE);
    btnAutoUnlimited->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onAutoUnlimitedClick, this));
    btnAutoUnlimited->setTitleText(safeGetStringData(684));

    auto btnAutoSpinSetting = _pLayoutAutoDropdownList->getChildByName<Button*>("btn_autonum_5");
    CCASSERT(btnAutoSpinSetting, "");
    btnAutoSpinSetting->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onAutoSpinSetting, this));
    btnAutoSpinSetting->setTitleText(safeGetStringData(685));

    _pLayoutBetDropdownList = static_cast<Layout*>(pLayoutControls->getChildByName("layout_dropdown_bet"));
    CCASSERT(_pLayoutBetDropdownList, "");

    _pNextBtn = static_cast<Button*>(_pLayoutBetDropdownList->getChildByName("btn_next_bet"));
    CCASSERT(_pNextBtn, "");
    _pNextBtn->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchNextBet, this));

    _pPreviousBtn = static_cast<Button*>(_pLayoutBetDropdownList->getChildByName("btn_previous_bet"));
    CCASSERT(_pPreviousBtn, "");
    _pPreviousBtn->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchPreviousBet, this));

    Layout* pLayoutBetLevel = static_cast<Layout*>(_pLayoutBetDropdownList->getChildByName("layout_betlevel"));
    CCASSERT(pLayoutBetLevel, "");

    _pListBetLevels = static_cast<ListView*>(_pLayoutBetDropdownList->getChildByName("list_betlevel_list"));
    CCASSERT(_pListBetLevels, "");
    _pListBetLevels->setTouchEnabled(false);
    _pListBetLevels->setScrollBarEnabled(false);
    _pListBetLevels->setBounceEnabled(true);

    _pLayoutLinesDropdownList = static_cast<Layout*>(pLayoutControls->getChildByName("layout_dropdown_lines"));
    CCASSERT(_pLayoutLinesDropdownList, "");
    setTraversalCascadeOpacityEnabled(_pLayoutLinesDropdownList, true);

    Button* pBtnAddLine = static_cast<Button*>(_pLayoutLinesDropdownList->getChildByName("btn_addline"));
    CCASSERT(pBtnAddLine, "");
    pBtnAddLine->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchAddLines, this));

    Button* pBtnMinusLine = static_cast<Button*>(_pLayoutLinesDropdownList->getChildByName("btn_minusline"));
    CCASSERT(pBtnMinusLine, "");
    pBtnMinusLine->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchMinusLines, this));

    _pBMFLineNumEdit = static_cast<TextBMFont*>(_pLayoutLinesDropdownList->getChildByName("bmf_linenum_edit"));
    CCASSERT(_pBMFLineNumEdit, "");

    _pBMFWinNum = dynamic_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_win_num"));
    CCASSERT(_pBMFWinNum, "");

    _pBMFTotlaBetNum = static_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_totalbet_num"));
    CCASSERT(_pBMFTotlaBetNum, "");

    _pBMFCurrentBet = static_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_currentbet_num"));
    CCASSERT(_pBMFCurrentBet, "");

    _pBMFCurrentLines = static_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_currentlines_num"));
    CCASSERT(_pBMFCurrentLines, "");

    _pLayoutJPBanner = static_cast<Layout*>(pLayoutControls->getChildByName("layout_jpbanner"));
    CCASSERT(_pLayoutJPBanner, "");

    _pBMFJPNum = static_cast<TextBMFont*>(_pLayoutJPBanner->getChildByName("bmf_jp_num"));
    CCASSERT(_pBMFJPNum, "");

    _pImgJPCheck = _pLayoutJPBanner->getChildByName<ImageView*>("img_check");
    _pImgJPUncheck = _pLayoutJPBanner->getChildByName<ImageView*>("img_uncheck");

    _sceneObjMan = CustomSpAnim::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_man", "");
    _sceneObjMan->setMix("idle", "win01", 0.2f);
    _sceneObjMan->setMix("idle", "win02_in", 0.2f);
    _sceneObjMan->setMix("idle", "touch", 0.2f);

    _sceneObjMan->setMix("win01", "idle", 0.2f);
    _sceneObjMan->setMix("win01", "touch", 0.2f);

    //_sceneObjMan->setMix("win02_in", "win02_loop", 0.2f);
    _sceneObjMan->setMix("win02_loop", "win02_out", 0.2f);
    _sceneObjMan->setMix("win02_out", "idle", 0.2f);

    _sceneObjMan->setMix("win02_in", "touch", 0.2f);
    _sceneObjMan->setMix("win02_loop", "touch", 0.2f);
    _sceneObjMan->setMix("win02_out", "touch", 0.2f);

    _sceneObjMan->setMix("win02_in", "idle", 0.2f);
    _sceneObjMan->setMix("win02_loop", "idle", 0.2f);

    _sceneObjMan->setMix("touch", "idle", 0.2f);

    performSceneAnim(SCENEANIM_ROLEIDLE);
    Layout* pLayoutDummySceneobjMan = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_dummy_sceneobj_man"));
    pLayoutDummySceneobjMan->setPosition(pLayoutDummySceneobjMan->getPosition() - Vec2(130, 20));
    CCASSERT(pLayoutDummySceneobjMan, "");
    pLayoutDummySceneobjMan->addChild(_sceneObjMan);

    _pLayoutListenerSceneobj1 = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_listener_man1"));
    _pLayoutListenerSceneobj2 = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_listener_man2"));
    _pLayoutListenerSceneobj1->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchBeganSceneObj1, this));
    _pLayoutListenerSceneobj2->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchBeganSceneObj1, this));

    //setup initial property of UI Component
    closeDropDownList(false);
    _pLayoutAutoDisable->setVisible(false);

    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
    setAutoSpinStatus(AUTOSPINSTATUS_OFF);
    setFreeSpinStatus(FREESPINSTATUS_OFF);

    //setup Reel
    UIReel* pReel;
    std::vector<SymbolSpec> symbolSpecs;
    for (int id = _pSlotData->SymbolInfoBeginID; id <= _pSlotData->SymbolInfoEndID; id++)
    {
        SymbolInfoData* pSymbolInfo = ExternalTable<SymbolInfoData>::getRecordByID(id);
        if (!pSymbolInfo)
            continue;
        std::string spriteFile = StringUtils::format(GOCIMAGE(%s), pSymbolInfo->SpriteFile.c_str());
        std::string animFile("");
        if (!pSymbolInfo->AnimFile.empty())
            animFile = _pSlotData->FolderName + "/Spine/" + pSymbolInfo->AnimFile;
        std::string animMaskFile("");
        if (!pSymbolInfo->AnimMaskFile.empty())
            animMaskFile = _pSlotData->FolderName + "/Spine/" + pSymbolInfo->AnimMaskFile;
        std::string blurFile("");
        if (pSymbolInfo->BlurType == REPLCESPRITE)
            blurFile = StringUtils::format(GOCIMAGE(%s_blur), pSymbolInfo->SpriteFile.c_str());

        symbolSpecs.push_back(SymbolSpec((SymbolInfoData::ESymbolType)pSymbolInfo->Type, spriteFile, SPRITEFRAME, pSymbolInfo->AnchorPt, pSymbolInfo->SymbolSize, animFile, animMaskFile, (EBlurType)pSymbolInfo->BlurType, blurFile, pSymbolInfo->SoundID));
        
        if (_stickyWildID == id)
            _stickySymbol = SymbolSpec((SymbolInfoData::ESymbolType)pSymbolInfo->Type, spriteFile, SPRITEFRAME, pSymbolInfo->AnchorPt, pSymbolInfo->SymbolSize, animFile, animMaskFile, (EBlurType)pSymbolInfo->BlurType, blurFile, pSymbolInfo->SoundID);
    }

    Layout* pLayoutReelDummy(nullptr);
    for (int i = 0; i < _reelCount; i++)
    {
        int reelOrder = i + 1;
        pLayoutReelDummy = static_cast<Layout*>(_pLayoutReels->getChildByName(StringUtils::format("layout_dummy_reel_%d", reelOrder)));
        CCASSERT(pLayoutReelDummy, "");

        pReel = UIReel::createWithReelView(Size(170, 510), 3, symbolSpecs, SYMBOLSIZE);
        if (pReel)
        {
            pReel->setReelOrder(reelOrder);
            pReel->setPosition(pLayoutReelDummy->getPosition());
            pReel->setOnSingleReelEnterMiddleExpectCB(CC_CALLBACK_1(UISlotChili::onSingleReelEnterMiddleExpectCB, this));
            pReel->setOnSingleReelEnterEndCB(CC_CALLBACK_1(UISlotChili::onSingleReelEnterEndCB, this));
            pReel->setOnSingleReelPendingStopCB(CC_CALLBACK_1(UISlotChili::onSingleReelPendingStopCB, this));
            pReel->setOnSingleReelStopCB(CC_CALLBACK_1(UISlotChili::onSingleReelStopCB, this));
            pReel->setOnSingleReelTriggerStopSoundCB(CC_CALLBACK_1(UISlotChili::onSingleReelTriggerStopSoundCB, this));
            _pLayoutReels->addChild(pReel);
            _reels.push_back(pReel);
        }
    }

    for (int i = (int)_betLevelList.size()-1; i >= 0; i--)
    {
        Layout* pPrototypeBtn = static_cast<Layout*>(pLayoutBetLevel->clone());
        Button* pBtn = static_cast<Button*>(pPrototypeBtn->getChildByName("btn_betlevel"));
        ImageView* pJPImg = static_cast<ImageView*>(pPrototypeBtn->getChildByName("img_jpmark"));
        ImageView* pLockImg = static_cast<ImageView*>(pPrototypeBtn->getChildByName("img_lock"));
        pBtn->addTouchEventListener(CC_CALLBACK_2(UISlotChili::onTouchBetValue, this));
        int betValue = _betLevelList.at(i);
        pBtn->setTitleText(Value(betValue).asString());
        pBtn->setTag(i);
        pPrototypeBtn->setTag(i);
        _pListBetLevels->pushBackCustomItem(pPrototypeBtn);
        pJPImg->setVisible(false);
        pLockImg->setVisible(false);
    }
    setTraversalCascadeOpacityEnabled(_pLayoutBetDropdownList, true);
    pLayoutBetLevel->setVisible(false);

    setMaxLineAndBet();
    _pListBetLevels->jumpToBottom();

    setAutoCount(0);
    setFreeSpinCount(0);//read from slotMachine Stat
    setWin(0);
    setJackpot(0);

    // particle
    cocos2d::Vector<Texture2D*> list;
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin01.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin02.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin03.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin04.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin05.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin06.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin07.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin08.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin09.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin10.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin11.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin12.png"));

    _coinParticle = CustomParticleSystemGroup::create("normalHit.plist", list, 1);
    _coinParticle->setSpeed(5, 2);
    _coinParticle->setFrameVariance(12);
    _coinParticle->stopSystem();
    _pLayoutDummyWinMoneyParticle->addChild(_coinParticle);

    for (int i = 0; i < _reelCount; ++i)
        _isStopExpectBG.push_back(false);
    // animation
    //_pActionEnterAnim = ActionManagerEx::getInstance()->getActionByName("UISlot_DL_Machine0005_Chili/UISlot_Chili.ExportJson", "slot_in");
    //_pActionEnterAnim->updateToFrameByTime(0);

    return true;
}

UISlotChili::UISlotChili()
{
    setUIShowType(EUISHOWTYPE::EUIST_SLOTMACHINE);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotChili::~UISlotChili()
{

}

void UISlotChili::destroyUI()
{
    _pRoot->removeFromParent();
    _pRoot = nullptr;
}

void UISlotChili::notifyOpenUI()
{
    AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

void UISlotChili::notifyCloseUI()
{
    stopAutoSpin();
    stopPerformRibbon();
    performStopSuddendly();
    stopPerformSpinResult();

    SlotMachineInfo info = Player::getInstance()->getSlotMachineInfo(_slotID);
    Player::getInstance()->setSlotMachineInfo(_slotID, info);
    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
    setAutoSpinStatus(AUTOSPINSTATUS_OFF);
    setFreeSpinStatus(FREESPINSTATUS_OFF);

    performEnterSceneAnim(false);

    _coinParticle->stopSystem();

    closeDropDownList();
    clearStickySymbol();
}

void UISlotChili::updateUI()
{
    SlotMachineInfo machineInfo = Player::getInstance()->getSlotMachineInfo(_slotID);

    //清除上次資料
    setWin(0);

    //update jackpot
    setJackpot(machineInfo.jackpot);
    setJPEnable(machineInfo.isJackpotEnable);
    updateJackpotHint();

    //update Bet
    setMaxLineAndBet();

    _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (_pSlotData->DefaultBet > 0)
    {
        int index = getBetIndexByBetValue(_pSlotData->DefaultBet);
        setCurBet(index);
    }

    // update FreeSpin
    if (machineInfo.freeSpin > 0)
    {
        showFreeSpinCount(true);
        setFreeSpinCount(machineInfo.freeSpin);
        setAutoCount(machineInfo.freeSpin);
        setAutoSpinStatus(AUTOSPINSTATUS_ON);
        setFreeSpinStatus(FREESPINSTATUS_ON);
        AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);

        // sticky wild
        std::vector<SlotSelectData> slotSelectData = Player::getInstance()->getSlotSelectData();
        std::vector<cocos2d::Vec2> stickyPos;
        for (int i = 0; i < (int)slotSelectData.size(); ++i)
            stickyPos.push_back(slotSelectData.at(i).symbolPos);
        playStickyWild(stickyPos);
        performSceneAnim(ESceneAnim::SCENEANIM_ROLEWIN_1);
    }
    else
    {
        showFreeSpinCount(false);
        setFreeSpinCount(machineInfo.freeSpin);
        setAutoCount(machineInfo.freeSpin);
        setAutoSpinStatus(AUTOSPINSTATUS_OFF);
        setFreeSpinStatus(FREESPINSTATUS_OFF);
    }

    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
}

void UISlotChili::playEnterUIAction()
{
    performEnterSceneAnim(true);
}

void UISlotChili::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    if (!isOpen())
        return;

    EMessageTag tagNum = (EMessageTag)Value(requestTag).asInt();
    if (tagNum == EMT_Slot_Spin || tagNum == EMT_Cheat_Codes)
    {
        if (tagNum == EMT_Cheat_Codes && !isScrolling())
        {
            setWin(0);
            stopPerformSpinResult();
            setSlotButtonStatus(SLOTBTNSTATUS_WAITINGRESPONSE);
            performSpin();
            _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
        }

        int errCode = -1;
        SlotIDList slotIDListRcv;
        SpinResponse_Overall overallRcv;
        std::vector<SpinResponse_PayLine> paylineRcv;
        SpinResponse_JP jpRcv;
        SpinResponse_Win winRcv;
        SpinResponse_FreeSpin freeSpinRcv;
        SpinResponse_Bonus bonusRcv;
        SpinResponse_Bonuswild bonuswildRcv;
        SpinResponse_Profile profileRcv;
        SpinResponse_LevelUp levelupRcv;
        SlotIDList specialSlotListRcv;

        if (!UISlotUtils::parseResponse(
            _slotID,
            json,
            errCode,
            slotIDListRcv,
            overallRcv,
            paylineRcv,
            jpRcv,
            winRcv,
            freeSpinRcv,
            bonusRcv,
            bonuswildRcv,
            profileRcv,
            levelupRcv,
            specialSlotListRcv
            ))
        {
            CCLOG("spin response pasing error");
            return;
        }

        clearLastResult();
        saveRcv(
            slotIDListRcv,
            overallRcv,
            paylineRcv,
            jpRcv,
            winRcv,
            freeSpinRcv,
            bonusRcv,
            bonuswildRcv,
            profileRcv,
            levelupRcv,
            specialSlotListRcv
            );
        Player::getInstance()->setLevel(profileRcv.level);
        Player::getInstance()->setExp(profileRcv.exp);
        Player::getInstance()->setVipLv(profileRcv.vip_level);
        Player::getInstance()->setVipPoint(profileRcv.vip_point);

        auto info = Player::getInstance()->getSlotMachineInfo(_slotID);
        info.jackpot = _jpRcv.winnings;
        Player::getInstance()->setSlotMachineInfo(_slotID, info);
        setJackpot(_jpRcv.winnings);

        performStopSpinWithResult(slotIDListRcv);
    }
}

void UISlotChili::Update(float fDelta)
{
    if (_buttonStatus == SLOTBTNSTATUS_ONCELINEANDSTOREUI)  //正在播中獎結果視窗storeUI
    {
        if (!isPerformSpinResult(RESULTTYPE_BTNSTATUSIDLE)) //已播完中獎結果視窗storeUI,可以開始下一次spin
            setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
    }

    if (_autoSpinStatus == AUTOSPINSTATUS_ON && _buttonStatus == SLOTBTNSTATUS_IDLE)
    {
        //正在播AutoSpin結果 或 正在進場動畫 或 正在轉場Loading畫面
        if (!isPerformSpinResult(RESULTTYPE_AUTOSPIN) && /*!_pActionEnterAnim->isPlaying() &&*/ !UIController::getInstance()->getController(EUITAG_UITRANSITIOIN)->isOpen())
        {
            stopPerformSpinResult();
            if (isOpen())
                AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
            requestSpin();
        }
    }

    // Update Jackpot
    SlotMachineInfo machineInfo = Player::getInstance()->getSlotMachineInfo(_slotID);
    if (machineInfo.isJackpotEnable)
        setJackpot(machineInfo.jackpot);

}

void UISlotChili::UpdatePerSecond(float dt)
{
    auto info = Player::getInstance()->getSlotMachineInfo(_slotID);
    setJackpot(info.jackpot);

    if (isFreeSpining())
    {
        performRibbon();
    }
}

void UISlotChili::onTouchSpin(Ref *pSender, Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::BEGAN)
    {
        _pBtnSpin->setScale(1.05f);
        _pImgSpinText->setScale(1.05f);
        _pImgStopText->setScale(1.05f);
        _pImgSpinTextGray->setScale(1.05f);
        _pImgStopTextGray->setScale(1.05f);
        return;
    }

    if (type == Widget::TouchEventType::MOVED)
    {
        if (_pBtnSpin->isHighlighted())
        {
            _pBtnSpin->setScale(1.05f);
            _pImgSpinText->setScale(1.05f);
            _pImgStopText->setScale(1.05f);
            _pImgSpinTextGray->setScale(1.05f);
            _pImgStopTextGray->setScale(1.05f);
        }
        else
        {
            _pBtnSpin->setScale(1.0f);
            _pImgSpinText->setScale(1.0f);
            _pImgStopText->setScale(1.0f);
            _pImgSpinTextGray->setScale(1.0f);
            _pImgStopTextGray->setScale(1.0f);
        }
        return;
    }

    if (type == Widget::TouchEventType::CANCELED)
    {
        _pBtnSpin->setScale(1.0f);
        _pImgSpinText->setScale(1.0f);
        _pImgStopText->setScale(1.0f);
        _pImgSpinTextGray->setScale(1.0f);
        _pImgStopTextGray->setScale(1.0f);
        return;
    }
    _pBtnSpin->setScale(1.0f);
    _pImgSpinText->setScale(1.0f);
    _pImgStopText->setScale(1.0f);
    _pImgSpinTextGray->setScale(1.0f);
    _pImgStopTextGray->setScale(1.0f);

    if (_autoSpinStatus == AUTOSPINSTATUS_ON && _freeSpinStatus == FREESPINSTATUS_OFF)
    {
        stopAutoSpin();
        return;
    }

    if (_buttonStatus == SLOTBTNSTATUS_STOPINGREEL)
    {
        performStopSuddendly();
        return;
    }

    //停止播放spin result
    stopPerformSpinResult();

    closeDropDownList();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);

    requestSpin();
}

void UISlotChili::onTouchAuto(Ref *pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (isScrolling())
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    stopPerformSpinResult();

    bool isAlreadyOpened = _pLayoutAutoDropdownList->isVisible();

    closeDropDownList();

    if (!_pLayoutAutoDropdownList->isVisible())
    {
        _pLayoutAutoDropdownList->stopAllActions();
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutAutoDropdownList, true)));
        arrayOfActions.pushBack(FadeIn::create(DROPDOWNLIST_FADETIME));
        auto actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutAutoDropdownList->runAction(actionsSeq);
        _pLayoutCloseDropdownListener->setVisible(true);
    }
}

void UISlotChili::onTouchMaxBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (isAutoSpining())
        return;

    if (isScrolling())
        return;

    //停止播放spin result
    stopPerformSpinResult();

    closeDropDownList();

    setMaxLineAndBet();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);

    requestSpin();

}

void UISlotChili::onTouchBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    stopPerformSpinResult();

    if (!_pLayoutBetDropdownList)
        return;

    bool isAlreadyOpened = _pLayoutBetDropdownList->isVisible();

    closeDropDownList();

    
    for (auto pBetLevelLayout : _pListBetLevels->getChildren())
    {
        Button* pBtn = static_cast<Button*>(pBetLevelLayout->getChildByName("btn_betlevel"));
        ImageView* pJPImg = static_cast<ImageView*>(pBetLevelLayout->getChildByName("img_jpmark"));
        ImageView* pLockImg = static_cast<ImageView*>(pBetLevelLayout->getChildByName("img_lock"));
        int betValue = _betLevelList.at(pBtn->getTag());
        pJPImg->setVisible(betValue >= _jpBetLimit && _pLayoutJPBanner->isVisible());
        if ((_isFixedBet && !UISlotUtils::isFixedBetUsable(_slotID, betValue)) || (!_isFixedBet && !Player::getInstance()->isBetUsable(betValue)))
            pLockImg->setVisible(true);
            //pBtn->setEnabled(false);
        else
            pLockImg->setVisible(false);
            //pBtn->setEnabled(true);
    }

    if (!_pLayoutBetDropdownList->isVisible())
    {
        _pLayoutBetDropdownList->stopAllActions();
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutBetDropdownList, true)));
        arrayOfActions.pushBack(FadeIn::create(DROPDOWNLIST_FADETIME));
        auto actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutBetDropdownList->runAction(actionsSeq);
        _pLayoutCloseDropdownListener->setVisible(true);

        //將目前bet設到ListView中間
        SetBetLevelListCurPos(_betLevelList.size() -1 - _curBet -1);
    }
    setCurBet(_curBet);
}

void UISlotChili::onTouchLines(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    stopPerformSpinResult();

    if (_pSlotData->isLockedPayLine)
        return;

    bool isAlreadyOpened = _pLayoutLinesDropdownList->isVisible();

    closeDropDownList();

    if (!_pLayoutLinesDropdownList->isVisible())
    {
        _pLayoutLinesDropdownList->stopAllActions();
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutLinesDropdownList, true)));
        arrayOfActions.pushBack(FadeIn::create(DROPDOWNLIST_FADETIME));
        auto actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutLinesDropdownList->runAction(actionsSeq);
        _pLayoutCloseDropdownListener->setVisible(true);
        showPaylineLessThanPayLineOrder(_curLines, true);
    }
    setCurLines(_curLines);
}

void UISlotChili::onTouchPayTable(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    closeDropDownList();
    UISlotChiliPayTable* pPayTable = dynamic_cast<UISlotChiliPayTable*>(UIController::getInstance()->getController(EUITAG_CHILI_PAYTABLE));
    if (!pPayTable)
        return;
    std::vector<int> symbolTypes = { SymbolInfoData::ESymbolType::SYMBOLTYPE_BONUS, SymbolInfoData::ESymbolType::SYMBOLTYPE_SCATTER, SymbolInfoData::ESymbolType::SYMBOLTYPE_WILD };
    std::vector<int> stringData1 = { 10036 };
    std::vector<int> stringData2 = { 10002, 10003, 10004, 10005 };
    std::vector<int> stringData3 = { 10007 };
    pPayTable->setPage1Data(symbolTypes, stringData1, stringData2, stringData3);
    pPayTable->openUI(_slotID, "UISlot_DL_Machine0005_Chili/UISlot_Chili_PayTable.ExportJson");
}

void UISlotChili::onTouchAutoValue(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    //停止播放spin result
    stopPerformSpinResult();

    //檢查button所代表值是否超出範圍
    Button* valuBtn = dynamic_cast<Button*>(pSender);
    int valueIndex = valuBtn->getTag();
    if (valueIndex <= 0 || valueIndex > AUTOCOUNTLEVEL)
        return;

    //設定autospin倒數, 關閉DropDownList
    setAutoCount(s_autoCountValue[valueIndex-1]);
    setAutoSpinStatus(AUTOSPINSTATUS_ON);

    closeDropDownList();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
    requestSpin();
}

void UISlotChili::onAutoUnlimitedClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    _isAutoSpinUnlimited = true;

    //停止播放spin result
    stopPerformSpinResult();

    //設定autospin倒數, 關閉DropDownList
    setAutoCount(0);
    setAutoSpinStatus(AUTOSPINSTATUS_ON);

    closeDropDownList();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
    requestSpin();
}

void UISlotChili::onAutoSpinSetting(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIController::getInstance()->getController(EUITAG_UIAUTOSPIN_SETTING)->openUI();
}

void UISlotChili::onTouchNextBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    SetBetLevelListCurPos(_betLevelListCurPos - 1);
}

void UISlotChili::onTouchPreviousBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    SetBetLevelListCurPos(_betLevelListCurPos+1);
}

void UISlotChili::onTouchBetValue(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //選擇Bet
    int betIdx = static_cast<Button*>(pSender)->getTag();
    int betValue = _betLevelList.at(betIdx);
    if (_isFixedBet && !UISlotUtils::isFixedBetUsable(_slotID, betValue))
    {
        auto unlockLevel = UISlotUtils::getFixedBetUnlockLevel(_slotID, betValue);
        auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
        messageController->clearMessage();
        messageController->pushMessage(composeStringData(safeGetStringData(34), StringUtils::toString(unlockLevel)));
        return;
    }
    else if (!_isFixedBet && !Player::getInstance()->isBetUsable(betValue))
    {
        auto unlockLevel = UISlotUtils::getBetUnlockLevel(betValue);
        auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
        messageController->clearMessage();
        messageController->pushMessage(composeStringData(safeGetStringData(34), StringUtils::toString(unlockLevel)));
        return;
    }

    //關閉DropDownList
    closeDropDownList();

    setCurBet(betIdx);
}

void UISlotChili::onTouchAddLines(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        //編輯Lines
        if (_curLines < _maxLines)
        {
            setCurLines(_curLines + 1);
            showPaylineLessThanPayLineOrder(_curLines, true);
            std::vector<cocos2d::Vec2> ptList = getPaylinePtList(_pLayoutPaylineRoot, _curLines);
            ProcedureFx::moveParticleAlongSegment(ptList, _pLayoutDummyPaylineParticle);
        }

        _mainUINode->stopActionByTag(TAG_SETPAYTOMAX);
    }
    else if (type == Widget::TouchEventType::BEGAN)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(EDIDLINEBTN_HOLDTIME));
        arrayOfActions.pushBack(CallFunc::create(
        [this]()
        {   setCurLines(_maxLines);
            showPaylineLessThanPayLineOrder(_curLines, true);
        }
        ));
        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_SETPAYTOMAX);
        _mainUINode->runAction(actionsSeq);
    }
    else if (type == Widget::TouchEventType::CANCELED)
    {
        _mainUINode->stopActionByTag(TAG_SETPAYTOMAX);
    }
}

void UISlotChili::onTouchMinusLines(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::ENDED)
    {
        //編輯Lines
        if (_curLines > _minLines)
        {
            setCurLines(_curLines - 1);
            showPaylineLessThanPayLineOrder(_curLines, true);
        }

        _mainUINode->stopActionByTag(TAG_SETPAYTOMIN);
    }
    else if (type == Widget::TouchEventType::BEGAN)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(EDIDLINEBTN_HOLDTIME));
        arrayOfActions.pushBack(CallFunc::create(
        [this]()
        {   setCurLines(_minLines);
            showPaylineLessThanPayLineOrder(_curLines, true);
        }
        ));
        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_SETPAYTOMIN);
        _mainUINode->runAction(actionsSeq);
    }
    else if (type == Widget::TouchEventType::CANCELED)
    {
        _mainUINode->stopActionByTag(TAG_SETPAYTOMIN);
    }
}

void UISlotChili::onTouchDropDownListBG(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeDropDownList();
}

void UISlotChili::onTouchBeganSceneObj1(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    performSceneAnim(SCENEANIM_ROLETOUCH);
}

void UISlotChili::onSingleReelEnterMiddleExpectCB(int reelOrder)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !isOpen())//slot depend
        return;

    if (!_isMiddleExpectSoundPlayed.at(reelIndex))
    {
        _isMiddleExpectSoundPlayed.at(reelIndex) = true;
        _middleExpectSoundHandle = AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_MIDDLEEXPECT, false);
    }
}

void UISlotChili::onSingleReelEnterEndCB(int reelOrder)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !isOpen())//slot depend
        return;

    //加速或停止下一滾輪
    //必須在EnterEndCB處理, 若在PendingStopCB處理, 下一滾輪會多等一圈
    if (reelOrder != _reelCount)
    {
        int scatterCounter(0);
        bool haveChanceScatter = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _scatterID, _minScatterCombo, SymbolInfoData::SYMBOLTYPE_SCATTER, reelOrder, scatterCounter);

        int bonusCounter(0);
        bool haveChanceBonus = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _bonusID, _minBonusCombo, SymbolInfoData::SYMBOLTYPE_BONUS, reelOrder, bonusCounter);


        auto pNextReel = _reels.at(reelIndex + 1);
        //若已停輪或快停輪則不處理 "加速或停止下一滾輪"
        if (pNextReel->getSpinState() == ESPIN_INFINITE_RUNNING || pNextReel->getSpinState() == ESPIN_INFINITE_RUNNING_EXPECT)
        {
            bool expectHitScatter = haveChanceScatter && scatterCounter >= _minScatterCombo - 1;
            bool expectgHitBonus = haveChanceBonus && bonusCounter >= _minBonusCombo - 1;
            //聽牌
            if (expectHitScatter || expectgHitBonus)
            {
                Vector<FiniteTimeAction*> arrayOfActions;
                arrayOfActions.pushBack(DelayTime::create(UIReel::st4_secPerRound));
                arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::beginInfiniteExpect, pNextReel)));
                arrayOfActions.pushBack(DelayTime::create(2.0f));
                arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, pNextReel)));
                Sequence* actionsSeq = Sequence::create(arrayOfActions);
                actionsSeq->setTag(TAG_STOPSINGLEREEL);
                pNextReel->runAction(actionsSeq);
            }
            else
            {
                //action for delay a frame
                //若沒有延遲一個frame, 全部輪會一起停止, 
                //一個frame足以讓下個reel進入下一圈, 或可設定一個DelayTime
                auto delayTime = DelayTime::create(0.3f);
                CallFunc* pStopNextReelAction = CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, pNextReel));
                pStopNextReelAction->setTag(TAG_STOPSINGLEREEL);
                Sequence* actionsSeq = Sequence::create(delayTime, pStopNextReelAction, nullptr);
                pNextReel->runAction(actionsSeq);
            }
        }
    }
}

void UISlotChili::onSingleReelPendingStopCB(int reelOrder)
{
    if (!isOpen())
        return;
    //整理已停止Reel的ExpectBG
    //reelOrder前的reel都已停止
    //最新停止的一輪需要播放音效
    refineAllReelExpectBGByChance(reelOrder);
    //最後一輪停止
    if (reelOrder == _reelCount)
    {
        if (_buttonStatus == SLOTBTNSTATUS_STOPSUDDENDLY)//快速停輪過程不表現ExpectBG, 待停輪時一次表現
            refineGivenReelExpectBGByHit(isHitScatter(), isHitBonus(), _reelCount, false);
    }
}

void UISlotChili::onSingleReelStopCB(int reelOrder)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !isOpen())//slot depend
        return;

    //最後一輪停止
    if (reelOrder == _reelCount)
    {
        //有中獎表現中獎
        performSpinResult(_paylineRcv, _jpRcv, _winRcv, _freeSpinRcv, _bonusRcv, _profileRcv, _levelupRcv, _specialSlotListRcv);
        //沒中獎如果正在autoSpin則繼續(沒中獎可能有升級界面)
        //if (!isHit())
        //{
        //    if (isAutoSpining())
        //    {
        //        Vector<FiniteTimeAction*> arrayOfActions;
        //        arrayOfActions.pushBack(DelayTime::create(1.0f));
        //        arrayOfActions.pushBack(CallFunc::create(
        //            [this](){
        //            stopPerformSpinResult();
        //            requestSpin();
        //        }
        //        ));
        //        Sequence* actionsSeq = Sequence::create(arrayOfActions);
        //        _mainUINode->runAction(actionsSeq);
        //    }
        //};
        Player::getInstance()->setCoin(_profileRcv.coin);//停輪後才更新coin

        //若中scatter, bonus設定特殊背景
        refineGivenReelExpectBGByHit(isHitScatter(), isHitBonus(), _reelCount, true);//on this moment reelOrder==_reeCount
        if (!isHit())
            colorAllSymbol(Color4B::WHITE);

    }
}

void UISlotChili::onSingleReelTriggerStopSoundCB(int reelOrder)
{
    if (isOpen())
    {
        //快速停輪時只播最後一輪停輪聲
        if (_buttonStatus == SLOTBTNSTATUS_STOPSUDDENDLY)
        {
            if(reelOrder == _reelCount)
                AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_REEL_STOP, false);
        }
        //一般停輪播放每輪停輪聲
        else
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_REEL_STOP, false);
    }
}

void UISlotChili::refineAllReelExpectBGByChance(int reelOrder)
{
    if (_buttonStatus == SLOTBTNSTATUS_STOPSUDDENDLY)//快速停輪不表現ExpectBG
        return;

    int scatterCounter(0);
    bool haveChanceScatter = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _scatterID, _minScatterCombo, SymbolInfoData::SYMBOLTYPE_SCATTER, reelOrder, scatterCounter);

    int bonusCounter(0);
    bool haveChanceBonus = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _bonusID, _minBonusCombo, SymbolInfoData::SYMBOLTYPE_BONUS, reelOrder, bonusCounter);

    refineGivenReelExpectBGByHit(haveChanceScatter, haveChanceBonus, reelOrder, false);
}

void UISlotChili::refineGivenReelExpectBGByHit(bool hitScatter, bool hitBonus, int maxReelOrder, bool isResult)
{
    int maxReelIndex = maxReelOrder - 1;
    if (maxReelIndex < 0 || maxReelIndex >= (int)_slotIDListRcv.size())//slot depend
        return;

    int beingSymbolInfoID = _pSlotData->SymbolInfoBeginID;
    bool haveChanceHybrid = hitScatter && hitBonus;
    bool noChance = !hitScatter && !hitBonus;
    for (int i = 0; i < (int)_reels.size(); i++)
    {
        auto& reel = _reels.at(i);

        int performReelOrder = i + 1;
        //不可能中Scatter和Bonus
        if (noChance)
        {
            performExpectBG(performReelOrder, EXPECT_NONE);
            if (isResult)
                reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            continue;
        }

        //可能中, 但還未停輪的reel不需背景
        if (i > maxReelIndex)
        {
            performExpectBG(performReelOrder, EXPECT_NONE);
            if (isResult)
                reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            continue;
        }

        //可能中, 已停輪的reel判斷需要顯示什麼背景
        bool haveScatter = false;
        bool haveBonus = false;
        if (!isResult)
        {
            for (auto id : _slotIDListRcv.at(i))
            {
                int symbolID = id + beingSymbolInfoID;
                if (symbolID == _scatterID)
                    haveScatter = true;
                else if (symbolID == _bonusID)
                    haveBonus = true;
            }
        }
        else
        {
            for (auto hitCoord : _freeSpinRcv.hitCoord)
            {
                if (hitCoord.x - 1 == i)
                {
                    haveScatter = true;
                    break;
                }
            }
            if (!haveBonus)
            {
                for (auto hitCoord : _bonusRcv.hitCoord)
                {
                    if (hitCoord.x - 1 == i)
                    {
                        haveBonus = true;
                        break;
                    }
                }
            }
            if (!haveBonus)
            {
                for (auto hitCoord : _bonuswildRcv.hitCoord)
                {
                    if (hitCoord.x - 1 == i)
                    {
                        haveBonus = true;
                        break;
                    }
                }
            }
        }

        bool haveHybrid = haveScatter&&haveBonus;

        if (haveChanceHybrid)       //可能中Scatter和Bonus
        {
            if (haveHybrid)
            {
                performExpectBG(performReelOrder, EXPECT_HYBRID);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else if (haveScatter)
            {
                performExpectBG(performReelOrder, EXPECT_SCATTER);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else if (haveBonus)
            {
                performExpectBG(performReelOrder, EXPECT_BONUS);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            }
            continue;
        }

        if (hitScatter)      //只可能中Scatter
        {
            if (haveScatter)
            {
                performExpectBG(performReelOrder, EXPECT_SCATTER);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            }

            continue;
        }
        else if (hitBonus)   //只可能中Bonus
        {
            if (haveBonus)
            {
                performExpectBG(performReelOrder, EXPECT_BONUS);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(Color4B(PAYLINEBG_DARKCOLOR)));
            }

            continue;
        }
    }
}

bool UISlotChili::isHitJackpot()
{
    return _jpRcv.jackpot > 0;
}

bool UISlotChili::isHitScatter()
{
    //現在不在freespin狀態, 但有freespin次數表示進入freeSpin
    return isStartFreeSpin() || isAddFreeSpin();
}

bool UISlotChili::isHitBonus()
{
    return _bonusRcv.gameID > 0 || _bonuswildRcv.gameID > 0 || _overallRcv.bonus_gameID > 0;
}

bool UISlotChili::isHitFiveOfAKind()
{
    return _winRcv.fiveOfAKindID > 0;
}

bool UISlotChili::isHitBigWin()
{
    return _winRcv.win_level > WinLevel::WINLV_NORMAL;
}

bool UISlotChili::isHit()
{
    return (
        _paylineRcv.size() > 0  ||  //中scatter, 中bonus, 中bonuswild, 中一般符號, 中jp(jp會有假payline)
        _overallRcv.overall > 0 ||  //中overal
        isHitJackpot()              //中jp
        );
}

bool UISlotChili::isShowBigWin()
{
    return !isHitJackpot() && !isHitBonus() && !isHitScatter() && isHitBigWin();
}

bool UISlotChili::isLevelUp()
{
    return _levelupRcv.upgrade > 0;
}

bool UISlotChili::isStartFreeSpin()
{
    //現在不在freespin狀態, 但有freespin次數表示進入freeSpin
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::NORMAL && _freeSpinRcv.remain_count>0;
}

bool UISlotChili::isEndFreeSpin()
{
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::RESULT;
}

bool UISlotChili::isAddFreeSpin()
{
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::ADD;
}

bool UISlotChili::isDoingFreeSpin()
{
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::INFREESPIN;
}

void UISlotChili::setAutoCount(int autoCount)
{
    if (!_pLayoutAutoDisable)
        return;

    if (_isAutoSpinUnlimited)
    {
        _pBMFAutoCount->setString("~");
        _pLayoutAutoDisable->setVisible(true);
    }
    else
    {
        _autoCount = autoCount;
        _pBMFAutoCount->setString(Value(_autoCount).asString());
        if (autoCount > 0)
        {
            _pLayoutAutoDisable->setVisible(true);
        }
        else
        {
            _pLayoutAutoDisable->setVisible(false);
        }
    }
}

void UISlotChili::stopAutoSpin()
{
    setAutoSpinStatus(AUTOSPINSTATUS_OFF);
    setAutoCount(0);
}

void UISlotChili::setWin(int win)
{
    _win = win;
    if (_pBMFWinNum)
        _pBMFWinNum->setString(StringUtils::toString(win));
}

void UISlotChili::updateTotalBet()
{
    if (_betLevelList.empty() || _curBet < 0 || _curBet >= (int)_betLevelList.size())
        return;

    _totalBet = _curLines*_betLevelList.at(_curBet);
    if (_pBMFTotlaBetNum)
        _pBMFTotlaBetNum->setString(StringUtils::toString(_totalBet));
}

void UISlotChili::SetBetLevelListCurPos(int pos)
{
    int posMin = 0, posMax = _betLevelList.size() - BETLISTVIEW_ITEMCOUNT;
    if (pos < posMin)
        pos = posMin;
    if (pos > posMax)
        pos = posMax;
    if (pos == posMin)
    {
        _pNextBtn->setEnabled(false);
        _pNextBtn->setColor(BETSELECT_DARKCOLOR);

        _pPreviousBtn->setEnabled(true);
        _pPreviousBtn->setColor(Color3B(255, 255, 255));
    }
    else if (pos == posMax)
    {
        _pNextBtn->setEnabled(true);
        _pNextBtn->setColor(Color3B(255, 255, 255));

        _pPreviousBtn->setEnabled(false);
        _pPreviousBtn->setColor(BETSELECT_DARKCOLOR);
    }
    else
    {
        _pNextBtn->setColor(Color3B(255, 255, 255));
        _pNextBtn->setEnabled(true);

        _pPreviousBtn->setColor(Color3B(255, 255, 255));
        _pPreviousBtn->setEnabled(true);
    }

    _betLevelListCurPos = pos;
    _pListBetLevels->scrollToItem(_betLevelListCurPos, Vec2::ANCHOR_TOP_LEFT, Vec2::ANCHOR_TOP_LEFT);
}

void UISlotChili::setCurBet(int bet)
{
    if (bet < 0 || bet >= (int)_betLevelList.size())
        return;
    _curBet = bet;
    int betValue = _betLevelList.at(_curBet);
    if (_pBMFCurrentBet)
        _pBMFCurrentBet->setString(StringUtils::toString(betValue));
    updateJackpotHint();
    updateTotalBet();
}

void UISlotChili::setCurLines(int lines)
{
    _curLines = lines;
    if (_pBMFCurrentLines)
        _pBMFCurrentLines->setString(StringUtils::toString(lines));
    if (_pBMFLineNumEdit)
        _pBMFLineNumEdit->setString(StringUtils::toString(lines));
    updateTotalBet();
}

void UISlotChili::setMaxLineAndBet()
{
    setCurLines(_maxLines);
    if (_isFixedBet && UISlotUtils::getUsableFixedBetLevel(_slotID).size())
        setCurBet(UISlotUtils::getUsableFixedBetLevel(_slotID).size() - 1);
    else if (!_isFixedBet && Player::getInstance()->getUsableBetLevel().size())
        setCurBet(Player::getInstance()->getUsableBetLevel().size() - 1);
    else
        setCurBet(0);
}

int UISlotChili::getPayLineOrder(int payLineID)
{
    if (payLineID < _beginPayLineID || payLineID > _endPayLineID)
        return 0;
    else
        return payLineID - _beginPayLineID + 1;
}

void UISlotChili::setJackpot(uint64 jackpot)
{
    if (_pBMFJPNum)
    {
        int jackpotActionTag = 1;
        if (_pBMFJPNum->getActionByTag(jackpotActionTag))
            return;
        CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(10.0f, _jackpot, jackpot);
        pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
        pAction->setTag(jackpotActionTag);
        _pBMFJPNum->stopAllActions();
        _pBMFJPNum->runAction(pAction);

        _jackpot = jackpot;
    }
}

void UISlotChili::setJPEnable(bool enable)
{
    _pLayoutJPBanner->setVisible(enable);
    for (auto reel : _reels)
        reel->setJPEnable(enable);
    _jpBetLimit = enable ? _pSlotData->JackpotBetLimit : 0;
}

void UISlotChili::updateJackpotHint()
{
    bool overLimit = _betLevelList.at(_curBet) >= _jpBetLimit;
    _pImgJPCheck->setVisible(overLimit);
    _pImgJPUncheck->setVisible(!overLimit);
}

void UISlotChili::clearLastResult()
{
    //清除上次ExpectBG資料
    for (int i = 0; i < (int)_isExpectBgSoundPlayed.size(); i++)
        _isExpectBgSoundPlayed[i] = false;
    for (int i = 0; i < (int)_isMiddleExpectSoundPlayed.size(); i++)
        _isMiddleExpectSoundPlayed[i] = false;

    //清除上次暫存資料
    clearRcv();
}

void UISlotChili::clearRcv()
{
    _slotIDListRcv.clear();
    _paylineRcv.clear();
    _jpRcv.clear();
    _winRcv.clear();
    _freeSpinRcv.clear();
    _bonusRcv.clear();
    _profileRcv.clear();
    _levelupRcv.clear();
    _specialSlotListRcv.clear();
}

void UISlotChili::saveRcv(
    SlotIDList& slotIDListRcv,
    SpinResponse_Overall& overallRcv,
    std::vector<SpinResponse_PayLine>& paylineRcv,
    SpinResponse_JP& jpRcv,
    SpinResponse_Win& winRcv,
    SpinResponse_FreeSpin& freeSpinRcv,
    SpinResponse_Bonus& bonusRcv,
    SpinResponse_Bonuswild& bonuswildRcv,
    SpinResponse_Profile& profileRcv,
    SpinResponse_LevelUp& levelupRcv,
    SlotIDList& specialSlotListRcv)
{
    _slotIDListRcv = slotIDListRcv;
    _overallRcv = overallRcv;
    _paylineRcv = paylineRcv;
    _jpRcv = jpRcv;
    _winRcv = winRcv;
    _freeSpinRcv = freeSpinRcv;
    _bonusRcv = bonusRcv;
    _bonuswildRcv = bonuswildRcv;
    _profileRcv = profileRcv;
    _levelupRcv = levelupRcv;
    _specialSlotListRcv = specialSlotListRcv;
}

void UISlotChili::performEnterSceneAnim(bool play)
{
    //if (play)
    //    _pActionEnterAnim->play();
    //else
    //    _pActionEnterAnim->stop();
}

void UISlotChili::closeDropDownList(bool fadeOut)
{
    _pLayoutAutoDropdownList->stopAllActions();
    _pLayoutBetDropdownList->stopAllActions();
    _pLayoutLinesDropdownList->stopAllActions();
    if (fadeOut)
    {
        Sequence* actionsSeq(nullptr);
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.clear();
        arrayOfActions.pushBack(FadeOut::create(DROPDOWNLIST_FADETIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutAutoDropdownList, false)));
        actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutAutoDropdownList->runAction(actionsSeq);

        arrayOfActions.clear();
        arrayOfActions.pushBack(FadeOut::create(DROPDOWNLIST_FADETIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutBetDropdownList, false)));
        actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutBetDropdownList->runAction(actionsSeq);

        arrayOfActions.clear();
        arrayOfActions.pushBack(FadeOut::create(DROPDOWNLIST_FADETIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutLinesDropdownList, false)));
        actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutLinesDropdownList->runAction(actionsSeq);
    }
    else
    {
        _pLayoutAutoDropdownList->setVisible(false);
        _pLayoutAutoDropdownList->setOpacity(0.0f);
        _pLayoutBetDropdownList->setVisible(false);
        _pLayoutBetDropdownList->setOpacity(0.0f);
        _pLayoutLinesDropdownList->setVisible(false);
        _pLayoutLinesDropdownList->setOpacity(0.0f);
    }

    if (_pLayoutCloseDropdownListener)
        _pLayoutCloseDropdownListener->setVisible(false);

    hideAllPayline();
}

int UISlotChili::getAutoCountValueTitleByTag(int tag)
{
    int valueIndex = tag;
    if (valueIndex <= 0 || valueIndex > AUTOCOUNTLEVEL)
        return 0;
    return s_autoCountValue[valueIndex - 1];
}

bool UISlotChili::isScrolling()
{
    for (auto reel : _reels)
    {
        if (reel && reel->isScrolling())
            return true;
    }
    return false;
}

void UISlotChili::performSpin()
{
    if (isScrolling())
        return;

    for (auto reel:_reels)
        reel->spin(0.4f, -1, SPINSTYLE_INFINITE_4);

    AudioManager::getInstance()->stopEff(_spinSoundHandle);
    if (isOpen())
        _spinSoundHandle = AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinSoundID, false);

}

void UISlotChili::performStopSpinWithResult(SlotIDList& result)
{
    //設定reel結果
    int i = 0;
    for (auto reel : _reels)
    {
        reel->setSpinResult(result.at(i));
        i++;
    }

    //開始停輪
    ActionInterval* pMinTimeAction = static_cast<ActionInterval*>(_mainUINode->getActionByTag(TAG_PERFORMSPIN_MINTIME));
    if (pMinTimeAction)
    {
        float waitTime = pMinTimeAction->getDuration() - pMinTimeAction->getElapsed();
        _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
        if (waitTime > 0.0f)
        {
            Vector<FiniteTimeAction*> arrayOfActions;
            arrayOfActions.pushBack(DelayTime::create(waitTime));
            arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, (*_reels.begin()))));
            auto actionsSeq = Sequence::create(arrayOfActions);
            actionsSeq->setTag(TAG_STOPFIRSTREEL);
            _mainUINode->runAction(actionsSeq);
        }
    }
    else
        (*_reels.begin())->stopInfinite();

    if (Player::getInstance()->getAutoSpinSetting().StopWinTimes)
    {
        if (_winRcv.win_coin >= _totalBet * Player::getInstance()->getAutoSpinSetting().WinTimes)
            _isAutoSpinUnlimited = false;
    }

    if (isAutoSpining() && Player::getInstance()->getAutoSpinSetting().StopSuddenly)
        performStopSuddendly();
    else
        setSlotButtonStatus(SLOTBTNSTATUS_STOPINGREEL);
}

void UISlotChili::performStopSuddendly()
{
    setSlotButtonStatus(SLOTBTNSTATUS_STOPSUDDENDLY);
    //for (auto& boolVaule : _isExpectBgSoundPlayed)
    //    boolVaule = true;
    AudioManager::getInstance()->stopEff(_middleExpectSoundHandle);
    _mainUINode->stopActionByTag(TAG_STOPFIRSTREEL);
    _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
    for (auto reel : _reels)
    {
        reel->stopActionByTag(TAG_STOPSINGLEREEL);
        reel->stopInfiniteSuddenly();
    }
}

void UISlotChili::requestSpin()
{
    if (isScrolling()/* || isPerformSpinResult()*/)
        return;

    stopPerformSpinResult();

    if (_freeSpinStatus == FREESPINSTATUS_OFF && !UISlotUtils::checkCoinAndOpenOutOfCoin(CC_CALLBACK_0(UISlotChili::outOfCoinKeepPlayingCB, this), _betLevelList.at(_curBet), _curLines))
    {
        if (_autoSpinStatus == AUTOSPINSTATUS_ON)
            stopAutoSpin();
        return;
    }
    setWin(0);    //清除上次win
    setSlotButtonStatus(SLOTBTNSTATUS_WAITINGRESPONSE);
    performSpin();

    _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
    DelayTime* pMinTimeAction = DelayTime::create(PERFORMSPIN_MINTIME);
    pMinTimeAction->setTag(TAG_PERFORMSPIN_MINTIME);
    _mainUINode->runAction(pMinTimeAction);

#ifdef ENABLE_OFFLINE_SLOT
    onDispatchResponse(Value(EMT_Slot_Spin).asString(), "");
    return;
#else

    RequestSlotSpin(_betLevelList.at(_curBet), _curLines);
#endif
}

void UISlotChili::showFreeSpinCount(bool show)
{
    if (show && _pLayoutFreeSpin->getOpacity()!=255)
        _pLayoutFreeSpin->runAction(FadeIn::create(0.2f));
    else if (!show && _pLayoutFreeSpin->getOpacity() != 0)
        _pLayoutFreeSpin->runAction(FadeOut::create(0.2f));
}

void UISlotChili::setFreeSpinCount(int count)
{
    _freeSpinCount = count;
    _pBMFFreeSpinRemain->setString(StringUtils::toString(count));
}

void UISlotChili::performAddFreeSpinCount(int addCount)
{
    CountTextBMFontToNum* pAction = CountTextBMFontToNum::create(0.5, _freeSpinCount - addCount, _freeSpinCount);
    pAction->setFormat(CountTextBMFontToNum::FORMAT_NONE);
    _pBMFFreeSpinRemain->runAction(pAction);
}

void UISlotChili::setSlotButtonStatus(ESlotButtonStatus status)
{
    _buttonStatus = status;

    //Paytable在任何時刻都enable
    if (_freeSpinStatus == FREESPINSTATUS_ON)
    {
        switch (status)
        {
        case SLOTBTNSTATUS_STOPINGREEL:     //只能按stop快速停輪
            _pBtnSpin->setEnabled(true);
            _pImgSpinText->setVisible(false);
            _pImgStopText->setVisible(true);
            _pImgSpinTextGray->setVisible(false);
            _pImgStopTextGray->setVisible(false);

            _pBtnAuto->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);
            _pBtnBet->setEnabled(false);
            _pBtnLines->setEnabled(false);
            break;
        default:                            //所有按鈕都disable
            _pBtnSpin->setEnabled(false);
            _pImgSpinText->setVisible(false);
            _pImgStopText->setVisible(false);
            _pImgSpinTextGray->setVisible(false);
            _pImgStopTextGray->setVisible(true);

            _pBtnAuto->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);
            _pBtnBet->setEnabled(false);
            _pBtnLines->setEnabled(false);
            break;
        }
    }
    else if (_autoSpinStatus == AUTOSPINSTATUS_ON)
    {
        //keep autoSpin Setting
    }
    else
    {
        switch (status)
        {
        case SLOTBTNSTATUS_IDLE:                //一般靜止狀態, 所有按鈕都enable
            _pBtnSpin->setEnabled(true);
            _pImgSpinText->setVisible(true);
            _pImgStopText->setVisible(false);
            _pImgSpinTextGray->setVisible(false);
            _pImgStopTextGray->setVisible(false);

            _pBtnAuto->setEnabled(true);
            _pBtnMaxBet->setEnabled(true);
            _pBtnBet->setEnabled(true);
            _pBtnLines->setEnabled(true);
            break;
        case SLOTBTNSTATUS_WAITINGRESPONSE:     //等待server回應, 所有按鈕都disable
            _pBtnSpin->setEnabled(false);
            _pImgSpinText->setVisible(false);
            _pImgStopText->setVisible(false);
            _pImgSpinTextGray->setVisible(true);
            _pImgStopTextGray->setVisible(false);

            _pBtnAuto->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);
            _pBtnBet->setEnabled(false);
            _pBtnLines->setEnabled(false);
            break;
        case SLOTBTNSTATUS_STOPINGREEL:         //server已回應, 只能按stop快速停輪
            _pBtnSpin->setEnabled(true);
            _pImgSpinText->setVisible(false);
            _pImgStopText->setVisible(true);
            _pImgSpinTextGray->setVisible(false);
            _pImgStopTextGray->setVisible(false);

            _pBtnAuto->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);
            _pBtnBet->setEnabled(false);
            _pBtnLines->setEnabled(false);
            break;
        case SLOTBTNSTATUS_STOPSUDDENDLY:       //server已回應, 按stop快速停輪中
            _pBtnSpin->setEnabled(false);
            _pImgSpinText->setVisible(false);
            _pImgStopText->setVisible(false);
            _pImgSpinTextGray->setVisible(false);
            _pImgStopTextGray->setVisible(true);

            _pBtnAuto->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);
            _pBtnBet->setEnabled(false);
            _pBtnLines->setEnabled(false);
            break;
        case SLOTBTNSTATUS_ONCELINEANDSTOREUI:  //當結果有StoreUI, 播onceAllPayLine和StoreUI時, 所有按鈕都disable
            _pBtnSpin->setEnabled(false);
            _pImgSpinText->setVisible(false);
            _pImgStopText->setVisible(false);
            _pImgSpinTextGray->setVisible(true);
            _pImgStopTextGray->setVisible(false);

            _pBtnAuto->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);
            _pBtnBet->setEnabled(false);
            _pBtnLines->setEnabled(false);
            break;

        }
    }
}

void UISlotChili::setAutoSpinStatus(EAutoSpinStatus status)
{
    _autoSpinStatus = status;

    switch (status)
    {
    case AUTOSPINSTATUS_ON:      //自動轉輪中, 只能按stop停止自動轉輪
        _autoSpinStatus = AUTOSPINSTATUS_ON;
        _pBtnSpin->setEnabled(true);
        _pImgSpinText->setVisible(false);
        _pImgStopText->setVisible(true);
        _pImgSpinTextGray->setVisible(false);
        _pImgStopTextGray->setVisible(false);

        _pBtnAuto->setEnabled(false);
        _pBtnMaxBet->setEnabled(false);
        _pBtnBet->setEnabled(false);
        _pBtnLines->setEnabled(false);
        break;
    case AUTOSPINSTATUS_OFF:   //停止轉輪中, 回復buttonStatus
        _autoSpinStatus = AUTOSPINSTATUS_OFF;
        setSlotButtonStatus(_buttonStatus);
        _isAutoSpinUnlimited = false;
        break;
    }
}

void UISlotChili::setFreeSpinStatus(EFreeSpinStatus status)
{
    _freeSpinStatus = status;

    switch (status)
    {
    case FREESPINSTATUS_ON:     //自動轉輪中, 只有在slotButton為SLOTBTNSTATUS_STOPSUDDENDLY時可快速停輪
        _freeSpinStatus = FREESPINSTATUS_ON;
        setSlotButtonStatus(_buttonStatus);
        //setAutoSpinStatus(_autoSpinStatus);
        break;
    case FREESPINSTATUS_OFF:    //停止轉輪中, 回復buttonStatus
        _freeSpinStatus = FREESPINSTATUS_OFF;
        setSlotButtonStatus(_buttonStatus);
        setAutoSpinStatus(_autoSpinStatus);
        break;
    }
}

std::vector<cocos2d::Vec2> UISlotChili::getPaylinePtList(cocos2d::ui::Layout* paylineLayout, int payLineOrder)
{
    std::vector<cocos2d::Vec2> ptList;
    ui::Layout* singlePaylineLayout = static_cast<ui::Layout*>(_pLayoutPaylineRoot->getChildByName(StringUtils::format("layout_line%02d", payLineOrder)));
    for (int i = 0; i < singlePaylineLayout->getChildrenCount(); i++)
    {
        auto pChild = singlePaylineLayout->getChildren().at(i);
        ui::ImageView* pImageView = dynamic_cast<ui::ImageView*>(pChild);
        Vec2 headPt = _pLayoutDummyPaylineParticle->convertToNodeSpace(pImageView->convertToWorldSpace(Vec2(0, pImageView->getContentSize().height / 2.0f)));
        ptList.push_back(headPt);
        Vec2 tailPt = _pLayoutDummyPaylineParticle->convertToNodeSpace(pImageView->convertToWorldSpace(Vec2(pImageView->getContentSize().width, pImageView->getContentSize().height / 2.0f)));
            ptList.push_back(tailPt);
    }

    //payline的元件沒有按照順序
    sort(ptList.begin(), ptList.end(), 
        [](const Vec2& first, const Vec2& second) {
        return (first.x < second.x);
    });

    //debugDraw
    //auto debugDraw = DrawNode::create();
    //for (auto pt : ptList)
    //    debugDraw->drawDot(pt, (float)M_PI, Color4F(1.0f, 1.0f, 1.0f, 1.0f));
    //_pLayoutSlotReel->addChild(debugDraw, 10000);

    return ptList;
}

void UISlotChili::iniPayLineComponent()
{
    _pLayoutPaylineRoot = static_cast<ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("UISlot_DL_Machine0005_Chili/UISlot_Chili_Payline.ExportJson"));
    CCASSERT(_pLayoutPaylineRoot, "");
    //setTraversalCascadeOpacityEnabled(_pLayoutPaylineRoot, true);
    for (auto pLayout : _pLayoutPaylineRoot->getChildren())
    {
        if (!pLayout)
            continue;
        pLayout->setVisible(true);
        for (auto pImageView : pLayout->getChildren())
        {
            if (!pImageView)
                continue;
            pImageView->setOpacity(0.0f);
        }
    }

    Layout* pLayoutDummySymbolAnim = static_cast<Layout*>(_pLayoutSlotReel->getChildByName("layout_dummy_payline"));
    CCASSERT(pLayoutDummySymbolAnim, "");
    pLayoutDummySymbolAnim->addChild(_pLayoutPaylineRoot);

    _pLayoutDummyPaylineParticle = static_cast<Layout*>(_pLayoutSlotReel->getChildByName("layout_dummy_payline_particle"));
    CCASSERT(_pLayoutDummyPaylineParticle, "");
}

void UISlotChili::showPayline(int payLineOrder, bool show)
{
    auto paylineLayout = static_cast<ui::Layout*>(_pLayoutPaylineRoot->getChildByName(StringUtils::format("layout_line%02d", payLineOrder)));
    if (!paylineLayout)
        return;
    for (auto pImageView : paylineLayout->getChildren())
    {
        if (!pImageView)
            continue;
        if (show)
            pImageView->runAction(FadeIn::create(0.2f));
            //pImageView->setOpacity(255);
        else
            pImageView->runAction(FadeOut::create(0.2f));
            //pImageView->setOpacity(0);
    }
}

void UISlotChili::showPaylineLessThanPayLineOrder(int payLineOrder, bool show)
{
    if (payLineOrder < _minLines || payLineOrder > _maxLines)
        return;

    for (int i = _maxLines; i >= _minLines; i--)
    {
        if (i <= payLineOrder)
            showPayline(i, show);
        else
            showPayline(i, false);
    }
}

void UISlotChili::hideAllPayline()
{
    showPaylineLessThanPayLineOrder(_maxLines, false);
}

void UISlotChili::playPayLine(int payLineID, bool play, bool Loop)
{
    if (!_pLayoutPaylineRoot)
        return;

    int payLineOrder = getPayLineOrder(payLineID);
    if (!payLineOrder)
        return;

    std::string lineAnimName = StringUtils::format("anim_line%02d", payLineOrder);
    auto lineAnim = cocostudio::ActionManagerEx::getInstance()->getActionByName("UISlot_DL_Machine0005_Chili/UISlot_Chili_Payline.ExportJson", lineAnimName.c_str());
    if (!lineAnim)
        return;
    lineAnim->setLoop(Loop);
    if (play)
        lineAnim->play();
    else
        lineAnim->stop();
}

void UISlotChili::stopPlayPayLine(int payLineID)
{
    playPayLine(payLineID, false, false);
    //若有播到一半要stopPlayPayLine的需求, 需要還原opacity為初始值0
    //目前只有一次全部stop的狀況
}

void UISlotChili::stopPlayAllPayLine()
{
    for (int i = _beginPayLineID; i <= _endPayLineID; i++)
        stopPlayPayLine(i);
    for (auto pNode : _pLayoutPaylineRoot->getChildren())
    {
        if (pNode)
        {
            for (auto pImageView : pNode->getChildren())
                pImageView->setOpacity(0.0f);
        }
    }
}

void UISlotChili::onceAllPayLines(std::vector<SpinResponse_PayLine>& payLines, SpinResponse_Win& winRcv)
{
    if (payLines.empty())
        return;

    if (isAutoSpining())
    {
        for (auto lines : payLines)
            playPayLine(lines.lineID, true, true);

        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::playAllPayLineSymbol, this, payLines)));
        arrayOfActions.pushBack(DelayTime::create(ONCE_ALLPAYLINE_TIME));//onceAllPayLines中
        //if (UIController::getInstance()->isStoreUIEmpty())
        //    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);//有payline沒彈窗, allPalyLine播完時可以自動spin
        //需等待BinWgin等彈窗關閉才可request
        //arrayOfActions.pushBack(CallFunc::create(
        //    [&, this](){
        //    stopPerformSpinResult();
        //    requestSpin(); }
        //));
        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_ONCEALLPAYLINE);
        _mainUINode->runAction(actionsSeq);
    }
    else
    {
        for (auto lines : payLines)
            playPayLine(lines.lineID, true, false);

        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::playAllPayLineSymbol, this, payLines)));
        //if (UIController::getInstance()->isStoreUIEmpty())
        //    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);//有payline沒彈窗, allPalyLine播出的瞬間開始可以按spin
        arrayOfActions.pushBack(DelayTime::create(ONCE_ALLPAYLINE_TIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::clearPayLineSymbol, this)));
        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_ONCEALLPAYLINE);
        _mainUINode->runAction(actionsSeq);
    }

    if (!isShowBigWin())//若是bigwin會在UIBigWin中顯示
        performWinMoney(winRcv.win_coin);

    //場景慶祝動畫
    if (!isFreeSpining())
    {
        if (winRcv.win_coin >= _totalBet * 2)
            performSceneAnim(SCENEANIM_ROLEWIN_2);
        else
            performSceneAnim((ESceneAnim)cocos2d::RandomHelper::random_int((int)SCENEANIM_ROLEWIN_1, (int)SCENEANIM_ROLEWIN_2));
    }

    if (isOpen())
    {
        if (isHitJackpot())
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
        else if (isHitScatter())
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
        else if (isHitBonus())
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
        else if (isHitFiveOfAKind())
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
        else if (isHitBigWin())
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
        else
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->WinSoundID, false);
    }
}

void UISlotChili::repeatSinglePayLines(std::vector<SpinResponse_PayLine>& payLines)
{
    if (payLines.empty())
        return;
    Vector<FiniteTimeAction*> arrayOfActions;
    //重覆輪播每條payline
    for (auto line : payLines)
    {
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::playPayLine, this, line.lineID, true, false)));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::playSinglePayLineSymbol, this, line)));
        arrayOfActions.pushBack(DelayTime::create(REPEAT_SINGLEPAYLINE_TIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::clearPayLineSymbol, this)));
    }

    auto actionsSeq = Sequence::create(arrayOfActions);

    //自動spin中只播一次
    if (isAutoSpining())
    {
        //改為不播單條
        //actionsSeq->setTag(TAG_REPEATSINGLEPAYLINE);
        //_mainUINode->runAction(actionsSeq);
    }
    //手動spin重覆播放
    else
    {
        auto rep = RepeatForever::create(actionsSeq);
        rep->setTag(TAG_REPEATSINGLEPAYLINE);
        _mainUINode->runAction(rep);
    }
}

void UISlotChili::playAllPayLineSymbol(std::vector<SpinResponse_PayLine>& payLines)
{
    std::map<Vec2, Vec2> focusPos;
    for (auto line:payLines)
    {
        if (/*line.lineID <= 0 || */line.lineID > _endPayLineID)//有些中獎沒有payline只有symbol
            continue;
        for (auto hitCoord : line.hitCoord)
        {
            Vec2 curPos = hitCoord;// getSymbolCoord(line.lineID, order);
            if (curPos.isZero())
                continue;
            if (focusPos.find(curPos) == focusPos.end())
                focusPos[curPos] = curPos;
        }
    }

    for (auto itr : focusPos)
    {
        Vec2 curPos = itr.second;
        Vec2 outVec = Vec2::ZERO;
        UISymbolBase* outSymbol=nullptr;
        getSymbolOnPayLine(curPos.x, curPos.y, outVec, &outSymbol);
        SymbolSpec outSymbolSpec = outSymbol->getSymbolSpec();
        outSymbol->setVisible(false);
        //symbol only use static sprite
        if (outSymbolSpec._animfile.empty())
        {
            Sprite* pSprite = Sprite::createWithSpriteFrameName(outSymbolSpec._filename);
            pSprite->setPosition(outVec);
            pSprite->setAnchorPoint(Vec2::ZERO);
            _pLayoutSymbolAnim->addChild(pSprite);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
        //symbol have animation file
        else
        {
            CustomSpAnim* pSpine = CustomSpAnim::createWithFile(outSymbolSpec._animfile, outSymbolSpec._maskfile);
            pSpine->playAnimationRoundInTime("win", ONCE_ALLPAYLINE_TIME);
            pSpine->setPosition(outVec);
            _pLayoutSymbolAnim->addChild(pSpine);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
    }
}

void UISlotChili::playSinglePayLineSymbol(SpinResponse_PayLine& payLine)
{
    std::map<Vec2, Vec2> focusPos;

    if (/*payLine.lineID <= 0 || */payLine.lineID > _endPayLineID)//有些中獎沒有payline只有symbol
        return;

    for (auto hitCoord : payLine.hitCoord)
    {
        Vec2 curPos = hitCoord;// getSymbolCoord(payLine.lineID, order);
        if (curPos.isZero())
            continue;
        if (focusPos.find(curPos) == focusPos.end())
            focusPos[curPos] = curPos;
    }

    for (auto itr : focusPos)
    {
        Vec2 curPos = itr.second;
        Vec2 outVec = Vec2::ZERO;
        UISymbolBase* outSymbol = nullptr;
        getSymbolOnPayLine(curPos.x, curPos.y, outVec, &outSymbol);
        SymbolSpec outSymbolSpec = outSymbol->getSymbolSpec();
        outSymbol->setVisible(false);
        //symbol only use static sprite
        if (outSymbolSpec._animfile.empty())
        {
            Sprite* pSprite = Sprite::createWithSpriteFrameName(outSymbolSpec._filename);
            pSprite->setPosition(outVec);
            pSprite->setAnchorPoint(Vec2::ZERO);
            _pLayoutSymbolAnim->addChild(pSprite);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
        //symbol have animation file
        else
        {
            CustomSpAnim* pSpine = CustomSpAnim::createWithFile(outSymbolSpec._animfile, outSymbolSpec._maskfile);
            pSpine->playAnimationRoundInTime("win", REPEAT_SINGLEPAYLINE_TIME);
            pSpine->setPosition(outVec);
            _pLayoutSymbolAnim->addChild(pSpine);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
    }
    int lineSymbolID = payLine.symbolID;
    SymbolInfoData* pSymbolData = ExternalTable<SymbolInfoData>::getRecordByID(lineSymbolID);
    if (!isPerformSpinResult(RESULTTYPE_SYMBOL_SOUND) && pSymbolData->SoundID && isOpen())
        AudioManager::getInstance()->playEffectWithFileID(pSymbolData->SoundID, false);
}

void UISlotChili::clearPayLineSymbol()
{
    if (!_pLayoutSymbolAnim)
        return;
    _pLayoutSymbolAnim->removeAllChildrenWithCleanup(true);
    visibleAllSymbol();
}

void UISlotChili::visibleAllSymbol()
{
    for (auto pReel : _reels)
    {
        if (!pReel)
            continue;
        for (auto pSymobl : pReel->getSymbols())
        {
            if (!pSymobl)
                continue;
            pSymobl->setVisible(true);
        }
    }
}

void UISlotChili::colorAllSymbol(const Color4B& color4B)
{
    for (auto pReel : _reels)
    {
        if (!pReel)
            continue;
        pReel->colorAllSymbol(color4B);
    }
}

void UISlotChili::performWinMoney(uint64 win)
{
    if (!_pBMFWinMoneyAmin || win <= 0)
        return;

    _pBMFWinMoneyAmin->stopAllActions();

    //數錢
    CountTextBMFontToUint64Num* pHUDWinAction = CountTextBMFontToUint64Num::create(1.0f, 0, win);
    pHUDWinAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    _pBMFWinMoneyAmin->runAction(pHUDWinAction);

    CountTextBMFontToUint64Num* pWinAction = CountTextBMFontToUint64Num::create(1.0f, 0, win);
    pWinAction->setFormat(CountTextBMFontToUint64Num::FORMAT_COMMA);
    _pBMFWinNum->runAction(pWinAction);

    //放大
    Vector<FiniteTimeAction*> arrayOfActions;
    arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setOpacity, _pBMFWinMoneyAmin, 255)));
    arrayOfActions.pushBack(CallFunc::create(
        [this](){ _pBMFWinMoneyAmin->setScale(0.55f); }
    ));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 1.0f)));
    arrayOfActions.pushBack(DelayTime::create(1.0f));
    arrayOfActions.pushBack(FadeOut::create(0.5));
    
    auto scaleSeq = Sequence::create(arrayOfActions);
    _pBMFWinMoneyAmin->runAction(scaleSeq);

    _coinParticle->resetSystem();
}

void UISlotChili::performSceneAnim(ESceneAnim anim)
{
    switch (anim)
    {
    case SCENEANIM_ROLEIDLE:
        if (_sceneObjMan->isPlayingAnimation("win01", 0))
        {
            _sceneObjMan->setToSetupPose();
            _sceneObjMan->setAnimation(0, "idle", true);
        }
        else if (_sceneObjMan->isPlayingAnimation("win02_loop", 0))
        {
            _sceneObjMan->setToSetupPose();
            _sceneObjMan->setAnimation(0, "win02_out", false);
            _sceneObjMan->addAnimation(0, "idle", true);
        }
        else if (_sceneObjMan->isPlayingAnimation("idle", 0) ||
            _sceneObjMan->isPlayingAnimation("win02_out", 0)
            )
        {
            return;
        }
        else
        {
            _sceneObjMan->setToSetupPose();
            _sceneObjMan->setAnimation(0, "idle", true);
        }
        break;
    case SCENEANIM_ROLEWIN_1:
        if (_sceneObjMan->isPlayingAnimation("win01", 0))
            break;
        _sceneObjMan->setToSetupPose();
        _sceneObjMan->setAnimation(0, "win01", true);
        if (isOpen())
            AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT5_ROLE_WIN_1, false);
        break;
    case SCENEANIM_ROLEWIN_2:
        _sceneObjMan->setToSetupPose();
        _sceneObjMan->setAnimation(0, "win02_in", false);
        _sceneObjMan->addAnimation(0, "win02_loop", true);
        if (isOpen())
            AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT5_ROLE_WIN_2, false);
        break;
    case SCENEANIM_ROLETOUCH:
        _sceneObjMan->setToSetupPose();
        _sceneObjMan->setAnimation(0, "touch", false);
        if (!isFreeSpining())
            _sceneObjMan->addAnimation(0, "idle", true);
        else
            _sceneObjMan->addAnimation(0, "win01", true);
        if (isOpen())
            AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT5_ROLE_TOUCH, false);
        break;

        break;
    default:
        break;
    }
}

void UISlotChili::performExpectBG(int reelOrder, EExpectType expectBG)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_expectBgs.size())
        return;

    ExpectBGGroup& rCurExpectBGGroup = _expectBgs.at(reelIndex);
    if (expectBG == EXPECT_NONE)
    {
        rCurExpectBGGroup.pScatterBg->setVisible(false);
        rCurExpectBGGroup.pBonusBg->setVisible(false);
        rCurExpectBGGroup.pHybridBg->setVisible(false);
        //rCurExpectBGGroup.pParticles->removeAllChildren();

        for (int i = 0; i < rCurExpectBGGroup.pParticles->getChildrenCount(); ++i)
        {
            ParticleSystemQuad* particle = dynamic_cast<ParticleSystemQuad*>(rCurExpectBGGroup.pParticles->getChildren().at(i));
            if (particle)
            {
                particle->setAutoRemoveOnFinish(true);
                particle->stopSystem();
            }
        }

        _isStopExpectBG.at(reelIndex) = true;
        return;
    }

    switch (expectBG)
    {
    case EXPECT_SCATTER:
        rCurExpectBGGroup.pScatterBg->setVisible(true);
        rCurExpectBGGroup.pBonusBg->setVisible(false);
        rCurExpectBGGroup.pHybridBg->setVisible(false);
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            if (isOpen())
                AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    case EXPECT_BONUS:
        rCurExpectBGGroup.pScatterBg->setVisible(false);
        rCurExpectBGGroup.pBonusBg->setVisible(true);
        rCurExpectBGGroup.pHybridBg->setVisible(false);
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            if (isOpen())
                AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    case EXPECT_HYBRID:
        rCurExpectBGGroup.pScatterBg->setVisible(false);
        rCurExpectBGGroup.pBonusBg->setVisible(false);
        rCurExpectBGGroup.pHybridBg->setVisible(true);
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            if (isOpen())
                AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    default:
        break;
    }

    if (rCurExpectBGGroup.pParticles->getChildrenCount() == 0 || _isStopExpectBG.at(reelIndex))
    {
        _isStopExpectBG.at(reelIndex) = false;
        rCurExpectBGGroup.pParticles->removeAllChildren();

        ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_blur_h_point.plist");
        pParticle1->setPosition(Vec2(0.0f, 250.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle1);
        ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_h_point01.plist");
        pParticle2->setPosition(Vec2(0.0f, 250.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle2);
        ParticleSystemQuad* pParticle3 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_h_point01.plist");
        pParticle3->setPosition(Vec2(0.0f, 250.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle3);

        ParticleSystemQuad* pParticle4 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_blur_h_point.plist");
        pParticle4->setPosition(Vec2(170.0f, 250.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle4);
        ParticleSystemQuad* pParticle5 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_h_point01.plist");
        pParticle5->setPosition(Vec2(170.0f, 250.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle5);
        ParticleSystemQuad* pParticle6 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_h_point01.plist");
        pParticle6->setPosition(Vec2(170.0f, 250.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle6);

        ParticleSystemQuad* pParticle7 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_w_point.plist");
        pParticle7->setPosition(Vec2(85.0f, 0.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle7);
        ParticleSystemQuad* pParticle8 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_blur_w_point.plist");
        pParticle8->setPosition(Vec2(85.0f, 0.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle8);

        ParticleSystemQuad* pParticle9 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_w_point.plist");
        pParticle9->setPosition(Vec2(85.0f, 510.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle9);
        ParticleSystemQuad* pParticle10 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_expect_blur_w_point.plist");
        pParticle10->setPosition(Vec2(85.0f, 510.0f));
        rCurExpectBGGroup.pParticles->addChild(pParticle10);

        ProcedureFx::moveParticleAlongRectByName(Rect(Vec2::ZERO, rCurExpectBGGroup.pParticles->getContentSize()), rCurExpectBGGroup.pParticles, 10, "UISlot_DL_Machine0005_Chili/Particle/chili_expect_fire02.plist", true, 0.1f);
    }
}

void UISlotChili::stopPerformExpectBG()
{
    for (auto& curBGs: _expectBgs)
    {
        curBGs.pScatterBg->setVisible(false);
        curBGs.pBonusBg->setVisible(false);
        curBGs.pHybridBg->setVisible(false);
        //curBGs.pParticles->removeAllChildren();

        for (int i = 0; i < curBGs.pParticles->getChildrenCount(); ++i)
        {
            ParticleSystemQuad* particle = dynamic_cast<ParticleSystemQuad*>(curBGs.pParticles->getChildren().at(i));
            if (particle)
            {
                particle->setAutoRemoveOnFinish(true);
                particle->stopSystem();
            }
        }
    }
    colorAllSymbol(Color4B::WHITE);
    
    for (int i = 0; i < (int)_isStopExpectBG.size(); ++i)
    {
        _isStopExpectBG.at(i) = true;
    }
}

void UISlotChili::clearStickySymbol()
{
    if (!_pLayoutStickySymbolAnim)
        return;

    _pLayoutStickySymbolAnim->removeAllChildrenWithCleanup(true);
}

void UISlotChili::playStickyWild(std::vector<cocos2d::Vec2>& stickyPos)
{
    clearStickySymbol();

    if (stickyPos.empty() || _stickyWildID == 0)
        return;
    
    // ge sticky symol
    for (auto stickyCoord : stickyPos)
    {
        int reelIndex = stickyCoord.x - 1;

        if (reelIndex < 0 || reelIndex >= (int)_reels.size() || reelIndex >= _reelCount)
        {
            CCLOG("illegal reelIndex");
            return;
        }

        UIReel* pReel = _reels.at(reelIndex);
        if (!pReel)
            return;

        cocos2d::Vector<UISymbolBase*>& symbols = pReel->getSymbols();

        int useSymbolIndex = symbols.size() - 1 - stickyCoord.y;      //12321 -> 32123
        if (useSymbolIndex<1 || useSymbolIndex>symbols.size() - 2)  //1~3
            return;

        UISymbolBase* pSymbol = symbols.at(useSymbolIndex);
        if (!pSymbol)
            return;
        Vec2 out_Position = Vec2(pReel->getPosition() + pSymbol->getPosition());

        if (_stickySymbol._animfile.empty())
        {
            Sprite* pSprite = Sprite::createWithSpriteFrameName(_stickySymbol._filename);
            pSprite->setPosition(out_Position);
            pSprite->setAnchorPoint(Vec2::ZERO);
            _pLayoutStickySymbolAnim->addChild(pSprite);
        }
        //symbol have animation file
        else
        {
            CustomSpAnim* pSpine = CustomSpAnim::createWithFile(_stickySymbol._animfile, _stickySymbol._maskfile);
            pSpine->playAnimation("win");
            pSpine->setPosition(out_Position);
            _pLayoutStickySymbolAnim->addChild(pSpine);
        }

        //particle
        ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_h02_l.plist");
        pParticle1->setPosition(out_Position + Vec2(0.0f, 85.0f));
        ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_h01.plist");
        pParticle2->setPosition(out_Position + Vec2(0.0f, 85.0f));

        ParticleSystemQuad* pParticle3 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_w01.plist");
        pParticle3->setPosition(out_Position + Vec2(85.0f, 170.0f));
        ParticleSystemQuad* pParticle4 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_w02.plist");
        pParticle4->setPosition(out_Position + Vec2(85.0f, 170.0f));

        ParticleSystemQuad* pParticle5 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_h02_r.plist");
        pParticle5->setPosition(out_Position + Vec2(170.0f, 85.0f));
        ParticleSystemQuad* pParticle6 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_h01.plist");
        pParticle6->setPosition(out_Position + Vec2(170.0f, 85.0f));

        ParticleSystemQuad* pParticle7 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_d01.plist");
        pParticle7->setPosition(out_Position + Vec2(85.0f, 0.0f));
        ParticleSystemQuad* pParticle8 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_w01.plist");
        pParticle8->setPosition(out_Position + Vec2(85.0f, 0.0f));
        ParticleSystemQuad* pParticle9 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/chili_wild_fire_w02.plist");
        pParticle9->setPosition(out_Position + Vec2(85.0f, 0.0f));


        _pLayoutStickySymbolAnim->addChild(pParticle1);
        _pLayoutStickySymbolAnim->addChild(pParticle2);
        _pLayoutStickySymbolAnim->addChild(pParticle3);
        _pLayoutStickySymbolAnim->addChild(pParticle4);
        _pLayoutStickySymbolAnim->addChild(pParticle5);
        _pLayoutStickySymbolAnim->addChild(pParticle6);
        _pLayoutStickySymbolAnim->addChild(pParticle7);
        _pLayoutStickySymbolAnim->addChild(pParticle8);
        _pLayoutStickySymbolAnim->addChild(pParticle9);
    }

    SymbolInfoData* pSymbolData = ExternalTable<SymbolInfoData>::getRecordByID(_stickyWildID);
    if (pSymbolData->SoundID && isOpen())
        AudioManager::getInstance()->playEffectWithFileID(pSymbolData->SoundID, false);
}

void UISlotChili::stickyWildEndFreeSpin()
{
    std::vector<cocos2d::Vec2> stickySymbolCoord;
    UISlotUtils::getStickyCoordByStickyID(_stickyWildID, _specialSlotListRcv, stickySymbolCoord);

    if (stickySymbolCoord.empty())
        return;

    for (int i = 0; i < (int)stickySymbolCoord.size(); ++i)
    {
        int reelIndex = stickySymbolCoord.at(i).x - 1;

        if (reelIndex < 0 || reelIndex >= (int)_reels.size() || reelIndex >= _reelCount)
        {
            CCLOG("illegal reelIndex");
            continue;
        }

        UIReel* pReel = _reels.at(reelIndex);
        if (!pReel)
            return;

        cocos2d::Vector<UISymbolBase*>& symbols = pReel->getSymbols();

        int useSymbolIndex = symbols.size() - 1 - stickySymbolCoord.at(i).y;      //12321 -> 32123
        if (useSymbolIndex<1 || useSymbolIndex>symbols.size() - 2)  //1~3
            return;

        UISymbolBase* pSymbol = symbols.at(useSymbolIndex);
        if (!pSymbol)
            return;

        pSymbol->changeSymbol(_stickySymbol, pReel->getMotionBlur());
    }
}

void UISlotChili::performRibbon()
{
    for (int i = 0; i < (int)_ribbonParticles.size(); ++i)
    {
        if (_ribbonParticles.at(i)->getChildrenCount() == 0)
        {
            int isPerform = cocos2d::RandomHelper::random_int(0, 2);
            if (isPerform == 0)
                continue;

            if (i == 0)
            {
                ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_y.plist");
                pParticle1->setPosition(Vec2(0.0f, 0.0f));
                pParticle1->setAutoRemoveOnFinish(true);
                ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_b.plist");
                pParticle2->setPosition(Vec2(0.0f, 0.0f));
                pParticle2->setAutoRemoveOnFinish(true);
                ParticleSystemQuad* pParticle3 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_g.plist");
                pParticle3->setPosition(Vec2(0.0f, 0.0f));
                pParticle3->setAutoRemoveOnFinish(true);
                _ribbonParticles.at(i)->addChild(pParticle1);
                _ribbonParticles.at(i)->addChild(pParticle2);
                _ribbonParticles.at(i)->addChild(pParticle3);
            }
            else if (i == 1)
            {
                ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_y.plist");
                pParticle1->setPosition(Vec2(0.0f, 0.0f));
                pParticle1->setAutoRemoveOnFinish(true);
                ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_b.plist");
                pParticle2->setPosition(Vec2(0.0f, 0.0f));
                pParticle2->setAutoRemoveOnFinish(true);
                ParticleSystemQuad* pParticle3 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_g.plist");
                pParticle3->setPosition(Vec2(0.0f, 0.0f));
                pParticle3->setAutoRemoveOnFinish(true);
                _ribbonParticles.at(i)->addChild(pParticle1);
                _ribbonParticles.at(i)->addChild(pParticle2);
                _ribbonParticles.at(i)->addChild(pParticle3);
            }
            if (i == 2)
            {
                ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_y.plist");
                pParticle1->setPosition(Vec2(0.0f, 0.0f));
                pParticle1->setAutoRemoveOnFinish(true);
                _ribbonParticles.at(i)->addChild(pParticle1);
            }
            else if (i == 3)
            {
                ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_y.plist");
                pParticle1->setPosition(Vec2(-22.0f, 0.0f));
                pParticle1->setAutoRemoveOnFinish(true);
                ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_b.plist");
                pParticle2->setPosition(Vec2(0.0f, 0.0f));
                pParticle2->setAutoRemoveOnFinish(true);
                ParticleSystemQuad* pParticle3 = ParticleSystemQuad::create("UISlot_DL_Machine0005_Chili/Particle/freespin_fx_ribbon_g.plist");
                pParticle3->setPosition(Vec2(14.0f, 0.0f));
                pParticle3->setAutoRemoveOnFinish(true);
                _ribbonParticles.at(i)->addChild(pParticle1);
                _ribbonParticles.at(i)->addChild(pParticle2);
                _ribbonParticles.at(i)->addChild(pParticle3);
            }
        }
    }
}

void UISlotChili::stopPerformRibbon()
{
    for (int i = 0; i < (int)_ribbonParticles.size(); ++i)
    {
        _ribbonParticles.at(i)->removeAllChildren();
    }
}

Vec2 UISlotChili::getSymbolCoord(int payLineID, int order)
{
    if (payLineID <= 0)
        return Vec2::ZERO;

    PayLineData* pPayLine = ExternalTable<PayLineData>::getRecordByID(payLineID);
    if (!pPayLine)
        return Vec2::ZERO;

    int reelIndex = order - 1;

    if (reelIndex < 0 || reelIndex >= (int)_reels.size() || reelIndex >= _reelCount)
    {
        CCLOG("illegal reelIndex");
        return Vec2::ZERO;
    }

    int symbolIndex = pPayLine->component[reelIndex].viewFrameIndex;

    return Vec2(order, symbolIndex);
}

void UISlotChili::getSymbolOnPayLine(int x, int y, Vec2& out_Position, UISymbolBase** out_Symbol)
{
    int reelIndex = x - 1;

    if (reelIndex < 0 || reelIndex >= (int)_reels.size() || reelIndex >= _reelCount)
    {
        CCLOG("illegal reelIndex");
        return;
    }

    UIReel* pReel = _reels.at(reelIndex);
    if (!pReel)
        return;

    cocos2d::Vector<UISymbolBase*>& symbols = pReel->getSymbols();
    
    int useSymbolIndex = symbols.size() - 1 - y;      //12321 -> 32123
    if (useSymbolIndex<1 || useSymbolIndex>symbols.size() - 2)  //1~3
        return;

    UISymbolBase* pSymbol = symbols.at(useSymbolIndex);
    if (!pSymbol)
        return;
    out_Position = Vec2(pReel->getPosition() + pSymbol->getPosition());
    *out_Symbol = pSymbol;
}

void UISlotChili::performSpinResult(
    std::vector<SpinResponse_PayLine>& pay_lineRcv,
    SpinResponse_JP& jpRcv,
    SpinResponse_Win& winRcv,
    SpinResponse_FreeSpin& freeSpinRcv,
    SpinResponse_Bonus& bonusRcv,
    SpinResponse_Profile& profileRcv,
    SpinResponse_LevelUp& levelupRcv,
    SlotIDList& specialSlotListRcv)
{
    //必須放在前面, autoSpin中才扣1
    //放在後面可能扣到才剛加進autoSpin中的freeSpin
    if (isAutoSpining())
        setAutoCount(_autoCount - 1);

    stopPerformSpinResult();

    bool bStoreUI = false;

    //UIJackpot
    if (isHitJackpot())
    {
        auto cJackpot = UIController::getInstance()->getController<UIJackpot*>(EUITAG::EUITAG_JACKPOT);
        if (cJackpot)
        {
            cJackpot->reset();
            cJackpot->setShowMoney(jpRcv.winnings);
            cJackpot->openUILater();
            bStoreUI = true;
        }
    }

    //UIFiveOfAKind
    if (!isHitJackpot() && !isHitBonus() && !isHitScatter() && isHitFiveOfAKind())
    {
        auto cFiveOFAKind = UIController::getInstance()->getController<UIFiveOfAKind*>(EUITAG::EUITAG_FIVEOFAKIND);
        if (cFiveOFAKind)
        {
            cFiveOFAKind->reset();
            if (isAutoSpining())
                cFiveOFAKind->setAutoCloseUITime(5.0f);
            cFiveOFAKind->setShowSymbol("UISlot_DL_Machine0005_Chili/", winRcv.fiveOfAKindID, Size(170 , 170), true);
            cFiveOFAKind->openUILater();
            bStoreUI = true;
        }
    }

    //UIBigWin
    if (isShowBigWin())
    {
        auto cBigWin = UIController::getInstance()->getController<UIBigWin*>(EUITAG::EUITAG_BIGWIN);
        if (cBigWin)
        {
            cBigWin->reset();
            if (isAutoSpining())
            {
                cBigWin->setAutoCloseUITime(5.0f);
                cBigWin->setShare(false);
            }
            cBigWin->setShowMoney(winRcv.win_coin);
            cBigWin->setSlotWinText(_pBMFWinNum);
            cBigWin->openUILater();
            bStoreUI = true;
        }
    }

    //BonusGame
    if (isHitBonus())
    {
        auto bonusGame = UIController::getInstance()->getController<UISlotChiliBonusGame*>(EUITAG::EUITAG_CHILI_BONUSGAME);
        if (bonusGame)
        {
            bonusGame->setGameInfo(bonusRcv.gameID, bonusRcv.bet, isAutoSpining() && !Player::getInstance()->getAutoSpinSetting().StopWinBonus);
            bonusGame->openUILater();
            bStoreUI = true;
        }
    }

    //FreeSpin: start
    if (isStartFreeSpin())
    {
        auto cFreeSpinStart = UIController::getInstance()->getController<UISlotChiliFreeSpinStart*>(EUITAG::EUITAG_CHILI_FREESPINSTART);
        if (cFreeSpinStart)
        {
            cFreeSpinStart->reset();
            cFreeSpinStart->setAutoCloseUITime(5.0f);
            cFreeSpinStart->setFreeSpinCounter(freeSpinRcv.remain_count);
            cFreeSpinStart->openUILater();
            bStoreUI = true;
            cFreeSpinStart->setOnCloseUICB(
                [this]()
            {
                //場景表現
                performSceneAnim(ESceneAnim::SCENEANIM_ROLEWIN_1);
            }
            );
        }
        showFreeSpinCount(true);
        setFreeSpinCount(freeSpinRcv.remain_count);
        setAutoCount(_autoCount + freeSpinRcv.remain_count);
        setAutoSpinStatus(AUTOSPINSTATUS_ON);
        setFreeSpinStatus(FREESPINSTATUS_ON);

        //performRibbon();
    }

    //FreeSpin: end
    if (isEndFreeSpin())
    {
        auto cFreeSpinReslut = UIController::getInstance()->getController<UISlotChiliFreeSpinResult*>(EUITAG::EUITAG_CHILI_FREESPINRESULT);
        if (cFreeSpinReslut)
        {
            cFreeSpinReslut->reset();
            if (isAutoSpining() && !Player::getInstance()->getAutoSpinSetting().StopWinBonus)
                cFreeSpinReslut->setAutoCloseUITime(5.0f);
            cFreeSpinReslut->setWinMoney(freeSpinRcv.coin_sum);
            cFreeSpinReslut->setFreeSpinCounter(freeSpinRcv.total_count);
            cFreeSpinReslut->openUILater();
            bStoreUI = true;
            cFreeSpinReslut->setOnCloseUICB(
            [this]()
            {   
                //場景表現
                performSceneAnim(ESceneAnim::SCENEANIM_ROLEIDLE);
                clearStickySymbol();
                stopPerformRibbon();
            }
            );
        }
        showFreeSpinCount(false);
        setFreeSpinCount(freeSpinRcv.remain_count);
        setFreeSpinStatus(FREESPINSTATUS_OFF);

        stickyWildEndFreeSpin();
    }

    //FreeSpin: add more freespin
    if (isAddFreeSpin())
    {
        auto cFreeSpinStart = UIController::getInstance()->getController<UISlotChiliFreeSpinStart*>(EUITAG::EUITAG_CHILI_FREESPINSTART);
        if (cFreeSpinStart)
        {
            cFreeSpinStart->reset();
            cFreeSpinStart->setAutoCloseUITime(5.0f);
            cFreeSpinStart->setFreeSpinCounter(freeSpinRcv.add_count);
            cFreeSpinStart->openUILater();
            bStoreUI = true;
        }
        showFreeSpinCount(true);
        setFreeSpinCount(freeSpinRcv.remain_count);
        setAutoCount(_autoCount + freeSpinRcv.add_count);
        performAddFreeSpinCount(freeSpinRcv.add_count);
    }

    //FreeSpin: do freespin
    if (isDoingFreeSpin())
    {
        showFreeSpinCount(true);
        setFreeSpinCount(freeSpinRcv.remain_count);
    }

    //UILeveUp
    if (isLevelUp())
    {
        auto cLevelUp = UIController::getInstance()->getController<UILevelUp*>(EUITAG::EUITAG_LEVELUP);
        if (cLevelUp)
        {
            cLevelUp->reset();
            if (isAutoSpining())
            {
                cLevelUp->setAutoCloseUITime(5.0f);
                cLevelUp->setShare(false);
            }

            cLevelUp->setReward(levelupRcv.award_money + levelupRcv.award_fbmoney, levelupRcv.award_vip_point);
            cLevelUp->openUILater();
            bStoreUI = true;
        }
    }

    //必須放在後面
    //放在前面可能直接停止autoSpin, 但freeSpin可能會增加autoCount
    if (!isAutoSpining())
        stopAutoSpin();


    if (bStoreUI /*|| isHit()*/)
    {
        //storeUI開玩才可idle
        /*//等onceAllPayline(performWinMoney)後才idle*/
        setSlotButtonStatus(SLOTBTNSTATUS_ONCELINEANDSTOREUI);
    }
    else
    {
        setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
    }

    // sticky
    if (isFreeSpining())
    {
        std::vector<cocos2d::Vec2> stickySymbolCoord;
        UISlotUtils::getStickyCoordByStickyID(_stickyWildID, _specialSlotListRcv, stickySymbolCoord);
        playStickyWild(stickySymbolCoord);
    }

    //有中獎play payline, play symbol animation
    if (isHit())
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::onceAllPayLines, this, pay_lineRcv, winRcv)));
        arrayOfActions.pushBack(DelayTime::create(ONCE_ALLPAYLINE_TIME));
        if (bStoreUI)
            arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIController::openStoredUI, UIController::getInstance())));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotChili::repeatSinglePayLines, this, pay_lineRcv)));

        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_PERFORMSPINRESULT);
        _mainUINode->runAction(actionsSeq);
    }
    //沒中獎但有UI要開,例如freeSpin結算, LevelUp
    else
    {
        if (bStoreUI)
            UIController::getInstance()->openStoredUI();
    }
}

void UISlotChili::stopPerformSpinResult()
{
    if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE))
        _mainUINode->stopActionByTag(TAG_ONCEALLPAYLINE);
    if (_mainUINode->getActionByTag(TAG_REPEATSINGLEPAYLINE))
        _mainUINode->stopActionByTag(TAG_REPEATSINGLEPAYLINE);
    if (_mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
        _mainUINode->stopActionByTag(TAG_PERFORMSPINRESULT);
    stopPlayAllPayLine();
    clearPayLineSymbol();
    stopPerformExpectBG();

    if (!isFreeSpining())
    {
        // clear sticky symbol
        clearStickySymbol();
        performSceneAnim(SCENEANIM_ROLEIDLE);
    }
}

bool UISlotChili::isPerformSpinResult(EPerformResultType type)
{
    switch (type)
    {
    case RESULTTYPE_NORMAL:
        if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE) ||
            _mainUINode->getActionByTag(TAG_REPEATSINGLEPAYLINE) ||
            _mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
            return true;
        break;
    case RESULTTYPE_AUTOSPIN:
        if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE) ||
            _mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
            return true;
    case RESULTTYPE_BTNSTATUSIDLE:
        if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE) ||
            _mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
            return true;
        break;
    case RESULTTYPE_SYMBOL_SOUND:
        break;
    default:
        break;
    }

    auto cJackpot = UIController::getInstance()->getController<UIJackpot*>(EUITAG::EUITAG_JACKPOT);
    if (cJackpot->isOpen())
        return true;

    auto cBigWin = UIController::getInstance()->getController<UIBigWin*>(EUITAG::EUITAG_BIGWIN);
    if (cBigWin->isOpen())
        return true;

    auto cFiveOfAKind = UIController::getInstance()->getController<UIFiveOfAKind*>(EUITAG::EUITAG_FIVEOFAKIND);
    if (cFiveOfAKind->isOpen())
        return true;

    auto cFreeSpinStart = UIController::getInstance()->getController<UISlotChiliFreeSpinStart*>(EUITAG::EUITAG_CHILI_FREESPINSTART);
    if (cFreeSpinStart->isOpen())
        return true;

    auto cFreeSpinResult = UIController::getInstance()->getController<UISlotChiliFreeSpinResult*>(EUITAG::EUITAG_CHILI_FREESPINRESULT);
    if (cFreeSpinResult->isOpen())
        return true;

    auto cLevelUp = UIController::getInstance()->getController<UILevelUp*>(EUITAG::EUITAG_LEVELUP);
    if (cLevelUp->isOpen())
        return true;

    if (GameLogic::getInstance()->GetState() == E_GameState_StayBonusGame)
        return true;

    return false;
}

//#include "cocostudio/DictionaryHelper.h"
    //std::vector<SlotIDList> requestSequence = 
    //{
    //    {
    //        { 5, 4, 5 },
    //        { 6, 13, 6 },
    //        { 7, 4, 7 },
    //        { 8, 5, 8 },
    //        { 9, 4, 9 },
    //    },
    //    {
    //        { 5, 4, 5 },
    //        { 6, 13, 6 },
    //        { 7, 4, 7 },
    //        { 8, 5, 8 },
    //        { 9, 4, 9 },
    //    }
    //};
    //static int iter = 0;
    //
    //RequestCheatCodes(5, 25, *(requestSequence.begin() + iter));
    //iter++;
    //if (iter == requestSequence.size())
    //    iter = 0;

void UISlotChili::outOfCoinKeepPlayingCB()
{
    int bet = 0;

    if (_isFixedBet)
        UISlotUtils::getSpinnableBetAndLine(bet, _curLines, UISlotUtils::getUsableFixedBetLevel(_slotID), false);
    else
        UISlotUtils::getSpinnableBetAndLine(bet, _curLines, Player::getInstance()->getUsableBetLevel(), false);

    int betIndex = getBetIndexByBetValue(bet);

    setCurLines(_curLines);
    setCurBet(betIndex);
}

int UISlotChili::getBetIndexByBetValue(int betValue)
{
    int betIndex = 0;
    for (int i = 0; i < (int)_betLevelList.size(); ++i)
    {
        if (betValue == _betLevelList.at(i))
        {
            betIndex = i;
            break;
        }
    }

    return betIndex;
}