#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIReel.h"
#include <spine/spine-cocos2dx.h>
#include "CustomParticleSystemGroup.h"
#include "../Animation/CustomSpAnim.h"
#include "UISlotUtils.h"
#include "UIBasicUnit.h"

class UISlotChili : public UIBasicUnit
{
public:

    enum ESlotActionTag
    {
        TAG_ONCEALLPAYLINE,
        TAG_REPEATSINGLEPAYLINE,
        TAG_ONCEALLPAYLINESYMBOL,
        TAG_REPEATSINGLEPAYLINESYMBOL,
        TAG_PERFORMSPINRESULT,
        TAG_SETPAYTOMAX,
        TAG_SETPAYTOMIN,
        TAG_STOPSINGLEREEL,
        TAG_STOPFIRSTREEL,
        TAG_PERFORMSPIN_MINTIME,
    };

    enum ESceneAnim
    {
        SCENEANIM_ROLEIDLE,   //閒置
        SCENEANIM_ROLEWIN_1,  //勝利1
        SCENEANIM_ROLEWIN_2,  //勝利2
        SCENEANIM_ROLETOUCH,  //點擊
    };
    static UISlotChili* create(int slotMachineID);

    static const int AUTOCOUNTLEVEL = 3;        //auto spin的階數
    static int s_autoCountValue[AUTOCOUNTLEVEL];//auto spin每階值
    static const int PAYLINECOMPONENT_MAX = 5;  //組成一條PayLine圖數量
    static const float EDIDLINEBTN_HOLDTIME;    //持續按多久add, minus可直接設成_maxLines, _minLines
    static const float ONCE_ALLPAYLINE_TIME;
    static const float REPEAT_SINGLEPAYLINE_TIME;
    static const int BETLISTVIEW_ITEMCOUNT = 3; //bet選單顯示數量
    static const float DROPDOWNLIST_FADETIME;   //選單fadeIn, fadeOut時間
    static const float PERFORMSPIN_MINTIME;     //開始滾輪到開始停輪最小時間(server已回應仍需等待最小時間, 但可快速停輪)
    static const cocos2d::Color3B BETSELECT_DARKCOLOR;    //BetList無法向上或無法向下時按鈕暗色
    static const cocos2d::Color3B PAYLINEBG_DARKCOLOR;    //表現中獎時,沒中獎符號暗色
    static const cocos2d::Color4B PAYLINEBG_DARKCOLOR_WITHEXPECTBG;    //表現中獎時,沒中獎符號若有ExpectBG時暗色
    static const int RIBBON_PARTICLE_COUNT = 4;

    UISlotChili();
    ~UISlotChili();

    virtual void destroyUI() override;
    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;
    virtual void playEnterUIAction() override;
    virtual void onDispatchResponse(const std::string& requestTag, const std::string& json);
    virtual void Update(float dt);
    virtual void UpdatePerSecond(float dt);

    void addReel(const cocos2d::Rect& viewFrame, int symbolCount, const cocos2d::Size& symbolSize);

    //event of control panel
    void onTouchSpin(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchAuto(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchMaxBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchLines(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchPayTable(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //event of auto button in drop list
    void onTouchAutoValue(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onAutoUnlimitedClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onAutoSpinSetting(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //event of bet button in drop list
    void onTouchNextBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchPreviousBet(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchBetValue(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //event of lines buttonin  drop list
    void onTouchAddLines(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchMinusLines(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //event of drop list bg
    void onTouchDropDownListBG(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //event of sceneObj1Listener
    void onTouchBeganSceneObj1(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //callback of a reelStop
    void onSingleReelEnterMiddleExpectCB(int reelOrder);
    void onSingleReelEnterEndCB(int reelOrder);
    void onSingleReelPendingStopCB(int reelOrder);      //reelOrder = 1 ~ _reelCount
    void onSingleReelStopCB(int reelOrder);             //reelOrder = 1 ~ _reelCount
    void onSingleReelTriggerStopSoundCB(int reelOrder); //reelOrder = 1 ~ _reelCount
    void refineAllReelExpectBGByChance(int reelOrder);                                                  //依目前停輪狀態的scatter, bonus中獎機會設定ExpectBG
    void refineGivenReelExpectBGByHit(bool hitScatter, bool hitBonus, int maxReelOrder, bool isResult); //依hit結果和reel中是否有scatter, bonus設定ExpectBG
    //bool haveChanceHitSpecialSymbolOnSingleReelStop(SymbolInfoData::ESymbolType symbolType, int reelOrder, int& outSymbolCounter);

    //sense of SpinResponse
    bool isHitJackpot();    //SpinResponse是否中了jackpot
    bool isHitScatter();    //SpinResponse是否中了freeSpin( isStartFreeSpin() || isAddFreeSpin() )
    bool isHitBonus();      //SpinResponse是否中了bonus
    bool isHitFiveOfAKind();//SpinResponse是否中了FiveOfAKind
    bool isHitBigWin();     //SpinResponse是否中了BigWin
    bool isHit();           //SpinResponse是否中了任何獎項(特殊符號,一般符號, 會做playAllPayLineSymbol)
    bool isShowBigWin();    //SpinResponse是否會showBigWin, isHitBigWin不一定需要show, 可能因為中其它大獎省略
    bool isLevelUp();       //SpinResponse是否升級
    bool isStartFreeSpin(); //SpinResponse是否開始freeSpin(非freespin時中了freespin)
    bool isEndFreeSpin();   //SpinResponse是否結束freeSpin
    bool isAddFreeSpin();   //SpinResponse是否增加freeSpin(在freespin裡中了freespin)
    bool isDoingFreeSpin(); //SpinResponse是否為一次freespin

    //UI operate function
    void performEnterSceneAnim(bool play);
    void closeDropDownList(bool fadeOut=true);                  //關閉所有DropList選單
    int getAutoCountValueTitleByTag(int tag);                   //依Button tag取得AutoSpin的值
    bool isScrolling();                                         //是否正在表現滾輪
    void performSpin();                                         //表現持續spin
    void performStopSpinWithResult(SlotIDList& result);         //表現spin結果
    void performStopSuddendly();                                //表現手動快速停輪
    void requestSpin();                                         //發送一次spin請求
    void showFreeSpinCount(bool show);
    bool isFreeSpining(){ return _freeSpinCount > 0; };
    void setFreeSpinCount(int count);
    void performAddFreeSpinCount(int addCount);
    void setSlotButtonStatus(ESlotButtonStatus status);         //設定按鈕可否點擊
    void setAutoSpinStatus(EAutoSpinStatus status);             //設定按鈕可否點擊
    void setFreeSpinStatus(EFreeSpinStatus status);             //設定按鈕可否點擊

    std::vector<cocos2d::Vec2> getPaylinePtList(cocos2d::ui::Layout* paylineLayout, int payLineOrder);
    void iniPayLineComponent();                                             //初始化payline元件
    void showPayline(int payLineOrder, bool show);                          //顯示單條payline靜態圖
    void showPaylineLessThanPayLineOrder(int payLineOrder, bool show);      //顯示所有小於payLineID的payline靜態圖
    void hideAllPayline();                                                  //隱藏所有payline靜態圖
    void playPayLine(int payLineID, bool play, bool Loop);                  //播放單條payline動畫
    void stopPlayPayLine(int payLineID);                                    //停止播放單條palyline動畫
    void stopPlayAllPayLine();                                              //停止播放所有palyline動畫
    void onceAllPayLines(std::vector<SpinResponse_PayLine>& payLines, SpinResponse_Win& winRcv);  //中獎播放一次全部中獎線
    void repeatSinglePayLines(std::vector<SpinResponse_PayLine>& payLines); //中獎重覆輪播單條中獎線
    void playAllPayLineSymbol(std::vector<SpinResponse_PayLine>& payLines); //播放palyline上的symbol動畫
    void playSinglePayLineSymbol(SpinResponse_PayLine& payLine);            //播放palyline上的symbol動畫
    void clearPayLineSymbol();                                              //清除palyline上的symbol動畫
    void visibleAllSymbol();                                                //設定所有symbol可見
    void colorAllSymbol(const cocos2d::Color4B& color4B);                   //對所有symbol設定color
    void performWinMoney(uint64 win);                                          //表現_pBMFWinMoneyAmin(放大, 數錢)
    void performSceneAnim(ESceneAnim anim);                                 //表現場景動畫
    void performExpectBG(int reelOrder, EExpectType expectBG);              //表現期待感背景, reelOrder = 1 ~ _reelCount
    void stopPerformExpectBG();                                             //停止表現期待感背景
    void clearStickySymbol();
    void playStickyWild(std::vector<cocos2d::Vec2>& stickyPos);
    void stickyWildEndFreeSpin();
    void performRibbon();                                                    //FreeSpin播放彩帶
    void stopPerformRibbon();                                                //清除FreeSpin彩帶
  
    cocos2d::Vec2 getSymbolCoord(int payLineID, int order);                 //回傳LineID的第x個是盤面的第x輪第y個
    void getSymbolOnPayLine(int x, int y, cocos2d::Vec2& out_Position, UISymbolBase** out_Symbol); //取得盤面第x輪第y個的Symbol

    void performSpinResult(
        std::vector<SpinResponse_PayLine>& pay_lineRcv,
        SpinResponse_JP& jpRcv,
        SpinResponse_Win& winRcv, 
        SpinResponse_FreeSpin& freeSpinRcv, 
        SpinResponse_Bonus& bonusRcv, 
        SpinResponse_Profile& profileRcv, 
        SpinResponse_LevelUp& levelupRcv,
        SlotIDList& specialSlotListRcv);                                  //表現中獎: payline, symbol動畫, 特殊中獎
    void stopPerformSpinResult();                                           //中斷表現中獎
    bool isPerformSpinResult(EPerformResultType type = RESULTTYPE_NORMAL);

    //set model data
    void setAutoCount(int autoCount);
    bool isAutoSpining(){ return _isAutoSpinUnlimited || _autoCount > 0; };
    void stopAutoSpin();
    void setWin(int win);
    void updateTotalBet();

    void SetBetLevelListCurPos(int pos);
    void setCurBet(int bet);
    void setCurLines(int lines);
    void setMaxLineAndBet();
    int getPayLineOrder(int payLineID);
    void setJPEnable(bool enable);
    void updateJackpotHint();
    int getJackpot(){ return _jackpot; };
    void setJackpot(uint64 jackpot);
    void setReelCount(int reelCount){ _reelCount = reelCount; };
    int getReelCount(){ return _reelCount; };
    const std::vector<int>& getViewFrameCounts(){ return _viewFrameCounts; };
    void clearLastResult();
    void clearRcv();
    void saveRcv(
    SlotIDList& slotIDListRcv,
    SpinResponse_Overall& overallRcv,
    std::vector<SpinResponse_PayLine>& paylineRcv,
    SpinResponse_JP& jpRcv,
    SpinResponse_Win& winRcv,
    SpinResponse_FreeSpin& freeSpinRcv,
    SpinResponse_Bonus& bonusRcv,
    SpinResponse_Bonuswild& bonuswildRcv,
    SpinResponse_Profile& profileRcv,
    SpinResponse_LevelUp& levelupRcv,
    SlotIDList& specialSlotListRcv);

    // out of coin callback
    void outOfCoinKeepPlayingCB();
    int getBetIndexByBetValue(int betValue);

private:
    bool init(int slotMachineID);
    virtual bool createUI() override;

    //particle
    cocos2d::CustomParticleSystemGroup* _coinParticle;

    //UI Component to operate
    ui::Layout* _pRoot = nullptr;
    cocos2d::ui::Layout* _pLayoutPaylineRoot=nullptr;
    cocos2d::ui::Layout* _pLayoutSeparator = nullptr;               //Reel分隔線圖
    cocos2d::ui::Layout* _pLayoutReels = nullptr;                   //put reel, clip reel
    cocos2d::ui::Layout* _pLayoutSymbolAnim = nullptr;              //a layout for play symbol spine animation
    cocos2d::ui::Layout* _pLayoutSlotReel = nullptr;                //機台中景
    cocos2d::ui::Layout* _pLayoutStickySymbolAnim = nullptr;
    cocos2d::ui::Layout* _pLayoutAutoDropdownList = nullptr;
    cocos2d::ui::Layout* _pLayoutBetDropdownList = nullptr;
    cocos2d::ui::Layout* _pLayoutLinesDropdownList = nullptr;
    cocos2d::ui::Layout* _pLayoutCloseDropdownListener = nullptr;
    cocos2d::ui::Layout* _pLayoutAutoDisable = nullptr;
    cocos2d::ui::Layout* _pLayoutFreeSpin = nullptr;
    cocos2d::ui::Layout* _pLayoutDummyWinMoneyParticle = nullptr;   //少量金幣dummy
    cocos2d::ui::Layout* _pLayoutDummyPaylineParticle = nullptr;    //payline particle dummy
    cocos2d::ui::Layout* _pLayoutJPBanner = nullptr;
    cocos2d::ui::Layout* _pLayoutListenerSceneobj1 = nullptr;
    cocos2d::ui::Layout* _pLayoutListenerSceneobj2 = nullptr;
    cocos2d::ui::TextBMFont* _pBMFAutoCount = nullptr;
    cocos2d::ui::TextBMFont* _pBMFWinNum = nullptr;
    cocos2d::ui::TextBMFont* _pBMFWinMoneyAmin = nullptr;
    cocos2d::ui::TextBMFont* _pBMFTotlaBetNum = nullptr;
    cocos2d::ui::TextBMFont* _pBMFCurrentBet = nullptr;
    cocos2d::ui::TextBMFont* _pBMFCurrentLines = nullptr;
    cocos2d::ui::TextBMFont* _pBMFLineNumEdit = nullptr;
    cocos2d::ui::TextBMFont* _pBMFJPNum = nullptr;
    cocos2d::ui::TextBMFont* _pBMFFreeSpinRemain = nullptr;
    cocos2d::ui::ImageView* _pImgStopText = nullptr;
    cocos2d::ui::ImageView* _pImgSpinText = nullptr;
    cocos2d::ui::ImageView* _pImgStopTextGray = nullptr;
    cocos2d::ui::ImageView* _pImgSpinTextGray = nullptr;
    cocos2d::ui::ImageView* _pImgJPCheck = nullptr;
    cocos2d::ui::ImageView* _pImgJPUncheck = nullptr;
    cocos2d::ui::ListView* _pListBetLevels = nullptr;
    cocos2d::ui::Button* _pBtnSpin = nullptr;
    cocos2d::ui::Button* _pBtnAuto = nullptr;
    cocos2d::ui::Button* _pBtnMaxBet = nullptr;
    cocos2d::ui::Button* _pBtnBet = nullptr;
    cocos2d::ui::Button* _pBtnLines = nullptr;
    cocos2d::ui::Button* _pNextBtn = nullptr;
    cocos2d::ui::Button* _pPreviousBtn = nullptr;
    cocos2d::ui::Button* _pBtnPayTable = nullptr;
    cocostudio::ActionObject* _pActionEnterAnim = nullptr;
    
    std::vector<ExpectBGGroup> _expectBgs;          //特殊符號背景
    std::vector<bool> _isExpectBgSoundPlayed;       //是否播過特殊符號背景音效
    std::vector<bool> _isMiddleExpectSoundPlayed;   //是否播過特殊符號聽牌音效
    unsigned int _middleExpectSoundHandle = 0;
    unsigned int _spinSoundHandle = 0;
    CustomSpAnim* _sceneObjMan = nullptr;               //場景物件 大叔
    std::vector<UIReel*> _reels;                        //weak reference for all reels

    std::vector<cocos2d::ui::Layout*> _ribbonParticles; // FreeSpin彩帶

    //model data
    SlotMachineData* _pSlotData=nullptr;
    int _slotID=1;          //ID of SlotMachine Table
    int _autoCount;         //剩餘auto次數
    int _freeSpinCount;     //剩餘freespin次數
    int _win;               //最近一次中獎金額
    int _totalBet;          //lines*bet
    int _curBet;            //目前每線壓注bet
    std::vector<int> _betLevelList; //bet選單顯示清單
    int _betLevelListCurPos;        //bet選單目前位置, 非_curBet
    int _curLines;          //目前壓注線數
    int _minLines;          //最小壓注線數
    int _maxLines;          //最大壓注線數
    int _beginPayLineID;
    int _endPayLineID;
    int _stickyWildID;          //sticky symbol ID
    int _scatterID;         //scatter symbol ID
    int _bonusID;           //bonus symbol ID
    int _minScatterCombo;   //scatter中獎最少combo
    int _minBonusCombo;     //bonus中獎最少combo
    int _jackpot;           //目前jackpot
    int _jpBetLimit;        //多少bet以上可以參加jackpot
    int _reelCount;         //slot總共幾reel
    bool _isAutoSpinUnlimited = false;
    bool _isFixedBet = false;
    std::vector<bool> _isStopExpectBG;
    ESlotButtonStatus _buttonStatus;
    EAutoSpinStatus _autoSpinStatus;
    EFreeSpinStatus _freeSpinStatus;

    SymbolSpec _stickySymbol;	// sticky symbol spec

    std::vector<int> _viewFrameCounts; //每reel各顯示幾個符號

    //暫存server回應
    SlotIDList _slotIDListRcv;
    SpinResponse_Overall _overallRcv;
    std::vector<SpinResponse_PayLine> _paylineRcv;
    SpinResponse_JP _jpRcv;
    SpinResponse_Win _winRcv;
    SpinResponse_FreeSpin _freeSpinRcv;
    SpinResponse_Bonus _bonusRcv;
    SpinResponse_Bonuswild _bonuswildRcv;
    SpinResponse_Profile _profileRcv;
    SpinResponse_LevelUp _levelupRcv;
    SlotIDList _specialSlotListRcv;

};

