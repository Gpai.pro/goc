#include "UISlotChiliBonusGame.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataCenter/ExternalTable.h"
#include "../Animation/CustomActions.h"
#include "../Network/MessageProcess.h"
#include "../Animation/CustomSpAnim.h"
#include "AudioManager.h"
#include "UISlotUtils.h"
#include "UIController.h"
#include "UIMessage.h"
#include "UICommon.h"
#include "../GlobalDefine/ImageDefine.h"
#include "../GlobalDefine/Player.h"

using namespace cocostudio;

#define EFF_CHILI_BONUSGAME_EAT 5101
#define EFF_CHILI_BONUSGAME_DOG 5104
#define EFF_CHILI_BONUSGAME_DONKEY 5102
#define EFF_CHILI_BONUSGAME_LIZARD 5103
#define EFF_CHILI_BONUSGAME_HOT 5106

UISlotChiliBonusGame* UISlotChiliBonusGame::create()
{
    UISlotChiliBonusGame* ui = new UISlotChiliBonusGame();
    if (ui && ui->init())
    {
        return ui;
    }

    CC_SAFE_DELETE(ui);
    return nullptr;
}

bool UISlotChiliBonusGame::init()
{
    return true;
}

UISlotChiliBonusGame::UISlotChiliBonusGame()
: _slotMachineID(5),
_currentState(chiliGameStatus::CGS_NONE),
_selectCharacterIndex(ECharacterIndex::ECI_DOG)
{
    setUIShowType(EUISHOWTYPE::EUIST_BONUSGAME);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotChiliBonusGame::~UISlotChiliBonusGame()
{

}

void UISlotChiliBonusGame::notifyOpenUI()
{
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotMachineID);
    if (_pSlotData)
        AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);

    setState(CGS_START);
}

void UISlotChiliBonusGame::setGameInfo(int bonusGameID, int currentBet, bool autoSpin)
{
    _bonusGameID = bonusGameID;
    _currentBet = currentBet;
    _isAutoSpin = autoSpin;
    

    setState(chiliGameStatus::CGS_INIT);
}

void UISlotChiliBonusGame::reset()
{
    showSelectArrow(ECI_CHARACTER_COUNT);
}

void UISlotChiliBonusGame::notifyCloseUI()
{
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotMachineID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

void UISlotChiliBonusGame::destroyUI()
{

}

void UISlotChiliBonusGame::updateUI()
{
}

void UISlotChiliBonusGame::UpdatePerSecond(float dt)
{
}

bool UISlotChiliBonusGame::createUI()
{
    // Start
    _SlotChiliBonusGame = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson"));
    if (!_SlotChiliBonusGame)
        return false;
    _mainUINode->addChild(_SlotChiliBonusGame);
    _selectCharacter = _SlotChiliBonusGame->getChildByName<Layout*>("Page_01");

    _selectBoard[ECI_DOG] = _selectCharacter->getChildByName<Layout*>("Panel_dog");
    _selectBoard[ECI_DOG]->addTouchEventListener(CC_CALLBACK_2(UISlotChiliBonusGame::onTouchSelectBoardEvent, this, ECI_DOG));
    _selectBoard[ECI_DOG]->setTouchEnabled(true);
    _characterSelectBoard[ECI_DOG] = spine::SkeletonAnimation::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_dog_01.json", "UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_dog_01.atlas");
    _selectBoard[ECI_DOG]->addChild(_characterSelectBoard[ECI_DOG]);

    _selectBoard[ECI_LIZARD] = _selectCharacter->getChildByName<Layout*>("Panel_lizard");
    _selectBoard[ECI_LIZARD]->addTouchEventListener(CC_CALLBACK_2(UISlotChiliBonusGame::onTouchSelectBoardEvent, this, ECI_LIZARD));
    _selectBoard[ECI_LIZARD]->setTouchEnabled(true);
    _characterSelectBoard[ECI_LIZARD] = spine::SkeletonAnimation::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_lizard01.json", "UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_lizard01.atlas");
    _selectBoard[ECI_LIZARD]->addChild(_characterSelectBoard[ECI_LIZARD]);

    _selectBoard[ECI_DONKEY] = _selectCharacter->getChildByName<Layout*>("Panel_donckey");
    _selectBoard[ECI_DONKEY]->addTouchEventListener(CC_CALLBACK_2(UISlotChiliBonusGame::onTouchSelectBoardEvent, this, ECI_DONKEY));
    _selectBoard[ECI_DONKEY]->setTouchEnabled(true);
    _characterSelectBoard[ECI_DONKEY] = spine::SkeletonAnimation::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_donkey01.json", "UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_donkey01.atlas");
    _selectBoard[ECI_DONKEY]->addChild(_characterSelectBoard[ECI_DONKEY]);

    _selectCharacter->getChildByName<Text*>("Lbl_title")->setString(safeGetStringData(10039));
    _selectCharacter->getChildByName<Text*>("Lbl_sub_title")->setString(safeGetStringData(10040));

    _changeToGameScene = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "ChangeToGame");
    _changeToGameScene->updateToFrameByTime(0);
    _arrowAnimation = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "ArrowAnimation");
    _arrowAnimation->updateToFrameByTime(0);
    _characterChili[ECI_DOG] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "GodChili");
    _characterChili[ECI_DOG]->updateToFrameByTime(0);
    _characterChili[ECI_LIZARD] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "LizardChili");
    _characterChili[ECI_LIZARD]->updateToFrameByTime(0);
    _characterChili[ECI_DONKEY] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DonckeyChili");
    _characterChili[ECI_DONKEY]->updateToFrameByTime(0);
    _awardAnimation = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "AwardAnimation");
    _awardAnimation->updateToFrameByTime(0);

    _characterChiliHead[ECI_DOG][0] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DOGChiliAnimation01");
    _characterChiliHead[ECI_DOG][1] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DOGChiliAnimation02");
    _characterChiliHead[ECI_DOG][2] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DOGChiliAnimation03");
    _characterChiliHead[ECI_DOG][3] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DOGChiliAnimation04");

    _characterChiliHead[ECI_LIZARD][0] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "LizardChiliAnimation01");
    _characterChiliHead[ECI_LIZARD][1] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "LizardChiliAnimation02");
    _characterChiliHead[ECI_LIZARD][2] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "LizardChiliAnimation03");
    _characterChiliHead[ECI_LIZARD][3] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "LizardChiliAnimation04");

    _characterChiliHead[ECI_DONKEY][0] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DonkeyChiliAnimation01");
    _characterChiliHead[ECI_DONKEY][1] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DonkeyChiliAnimation02");
    _characterChiliHead[ECI_DONKEY][2] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DonkeyChiliAnimation03");
    _characterChiliHead[ECI_DONKEY][3] = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0005_Chili/UISlot_ChiliBonusGame_Game.ExportJson", "DonkeyChiliAnimation04");


    _gameplay = _SlotChiliBonusGame->getChildByName<Layout*>("Page_02");
    UICommon::getInstance()->fixUIPosition(_gameplay,EFixUIPositionType::EFP_DOWN);

    auto arrowNode = _gameplay->getChildByName("Arrow");
    _arrowImage[ECI_DOG] = arrowNode->getChildByName<ImageView*>("img_arrow01");
    _arrowImage[ECI_LIZARD] = arrowNode->getChildByName<ImageView*>("img_arrow02");
    _arrowImage[ECI_DONKEY] = arrowNode->getChildByName<ImageView*>("img_arrow03");
    auto leaderNode = _gameplay->getChildByName("Leader");
    _awardImage[ECI_DOG] = leaderNode->getChildByName<ImageView*>("img_1st");
    _awardImage[ECI_LIZARD] = leaderNode->getChildByName<ImageView*>("img_2nd");
    _awardImage[ECI_DONKEY] = leaderNode->getChildByName<ImageView*>("img_3rd");
    setAwardVisible(false);

    auto dogChiliNode = _gameplay->getChildByName("chili_dog");
    for (int i = 0; i < 4; i++)
        _chiliImage[ECI_DOG][i] = dogChiliNode->getChildByName<ImageView*>("img_chili0" + StringUtils::toString(i+1));
    auto lizardChiliNode = _gameplay->getChildByName("chili_lizard");
    for (int i = 0; i < 4; i++)
        _chiliImage[ECI_LIZARD][i] = lizardChiliNode->getChildByName<ImageView*>("img_chili0" + StringUtils::toString(i+1));
    auto donkeyChiliNode = _gameplay->getChildByName("chili_donckey");
    for (int i = 0; i < 4; i++)
        _chiliImage[ECI_DONKEY][i] = donkeyChiliNode->getChildByName<ImageView*>("img_chili0" + StringUtils::toString(i+1));

    auto dogPanel = _gameplay->getChildByName("panel_dog");
    auto dogPanelFront = _gameplay->getChildByName("panel_dog_front");
    _characterBehind[ECI_DOG] = CustomSpAnim::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_dog_02","");
    _characterBehind[ECI_DOG]->setToSetupPose();
    _characterBehind[ECI_DOG]->playAnimation("idle");
    dogPanel->addChild(_characterBehind[ECI_DOG]);
    _characterFront[ECI_DOG] = CustomSpAnim::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_dog_03", "");
    _characterFront[ECI_DOG]->setToSetupPose();
    _characterFront[ECI_DOG]->playAnimation("idle");
    dogPanelFront->addChild(_characterFront[ECI_DOG]);

    auto lizardPanel = _gameplay->getChildByName("panel_lizard");
    auto lizardPanelFront = _gameplay->getChildByName("panel_lizard_front");
    _characterBehind[ECI_LIZARD] = CustomSpAnim::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_lizard02", "");
    _characterBehind[ECI_LIZARD]->setToSetupPose();
    _characterBehind[ECI_LIZARD]->playAnimation("idle");
    lizardPanel->addChild(_characterBehind[ECI_LIZARD]);
    _characterFront[ECI_LIZARD] = CustomSpAnim::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_lizard03", "");
    _characterFront[ECI_LIZARD]->setToSetupPose();
    _characterFront[ECI_LIZARD]->playAnimation("idle");
    lizardPanelFront->addChild(_characterFront[ECI_LIZARD]);

    auto donkeyPanel = _gameplay->getChildByName("panel_donckey");
    auto donkeyPanelFront = _gameplay->getChildByName("panel_donckey_front");
    _characterBehind[ECI_DONKEY] = CustomSpAnim::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_donkey02", "");
    _characterBehind[ECI_DONKEY]->setToSetupPose();
    _characterBehind[ECI_DONKEY]->playAnimation("idle");
    donkeyPanel->addChild(_characterBehind[ECI_DONKEY]);
    _characterFront[ECI_DONKEY] = CustomSpAnim::createWithFile("UISlot_DL_Machine0005_Chili/Spine/chili_obj_sp_donkey03", "");
    _characterFront[ECI_DONKEY]->setToSetupPose();
    _characterFront[ECI_DONKEY]->playAnimation("idle");
    donkeyPanelFront->addChild(_characterFront[ECI_DONKEY]);


    _startPage = _SlotChiliBonusGame->getChildByName<Layout*>("Page_Start");
    auto startBtn = _startPage->getChildByName<Button*>("Button");
    startBtn->addTouchEventListener(CC_CALLBACK_2(UISlotChiliBonusGame::onTouchStartEvent, this));

    _startPage->getChildByName<Text*>("Text")->setString(safeGetStringData(10042));
    _startPage->getChildByName<Text*>("Label_151")->setString(safeGetStringData(10038));

    _resultPage = _SlotChiliBonusGame->getChildByName<Layout*>("Page_result");
    auto closeBtn = _resultPage->getChildByName<Button*>("Button");
    closeBtn->addTouchEventListener(CC_CALLBACK_2(UISlotChiliBonusGame::onTouchCloseGameEvent, this));
    auto textNode = _resultPage->getChildByName("text");
    _totalWinBMFont = textNode->getChildByName<TextBMFont*>("type_01");
    _winCalculateBMFont = textNode->getChildByName<TextBMFont*>("type_02");

    _resultPage->getChildByName("text")->getChildByName<Text*>("content")->setString(safeGetStringData(10011));

    _BoardPage = _SlotChiliBonusGame->getChildByName<Layout*>("panel_board");

    auto awardBoardPanel = _gameplay->getChildByName<Layout*>("board");
    _awardCountText[0] = awardBoardPanel->getChildByName<TextBMFont*>("bmf_1st_price");
    _awardCountText[1] = awardBoardPanel->getChildByName<TextBMFont*>("bmf_2nd_price");
    _awardCountText[2] = awardBoardPanel->getChildByName<TextBMFont*>("bmf_3rd_price");

    setTraversalCascadeColorEnabled(_mainUINode, true);
    setTraversalCascadeOpacityEnabled(_mainUINode, true);
    return true;
}

void UISlotChiliBonusGame::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    int errorCode = 0;
    int action = 0;
    int gift = 0;
    int bet = 0;
    uint64 total_gift = 0;
    uint64 playerCoin = 0;

    if (!UISlotUtils::parsePokeResultResponse(json, errorCode, action, gift, bet, total_gift, playerCoin))
    {
        CCLOG("poke BonusGame response pasing error");
        closeUI();
        return;
    }
    PokeGameData* pokeGameData = ExternalTable<PokeGameData>::getRecordByID(_bonusGameID);
    for (int i = 0; i < 3; i++)
    {
        _awardCountText[i]->setString(GetMoneyFormat(pokeGameData->items[i].Coin));
        if (gift == pokeGameData->items[i].Coin)
            _gameRank[_selectCharacterIndex] = i;
    }
    _currentSeed = rand() % 4;
    bool rankFilled[3] = { false, false, false };
    rankFilled[_gameRank[_selectCharacterIndex]] = true;

    // set rank
    for (int i = 0; i < 3; i++)
    {
        if (_selectCharacterIndex == i)
            continue;
        int unpair = -1;
        if (_currentSeed % 2 == 0)
        {
            for (int j = 0; j < 3; j++)
            {
                if (rankFilled[j] == false)
                {
                    unpair = j;
                    rankFilled[j] = true;
                    break;
                }
            }
        }
        else
        {
            for (int j = 0; j < 3; j++)
            {
                if (rankFilled[j] == false)
                {
                    unpair = j;
                    rankFilled[j] = true;
                    break;
                }
            }
        }
        _gameRank[i] = unpair;
    }

    // set award image

    setAward(_gameRank[0], _gameRank[1], _gameRank[2]);

    _totalScore = total_gift;
    _playerCoin = playerCoin;
    setState(CGS_ROUND_START);
}

void UISlotChiliBonusGame::onTouchSelectBoardEvent(Ref* pSender, Widget::TouchEventType type, ECharacterIndex index)
{
    if (_isAutoSpin)
        return;
    if (_selectCharacterIndex != ECI_CHARACTER_COUNT)
        return;
    switch (type)
    {
    case cocos2d::ui::Widget::TouchEventType::BEGAN:

        highlightCharacter(index);
        break;
    case cocos2d::ui::Widget::TouchEventType::ENDED:
        //setState(CGS_WAIT_RESPONSE);
        _SlotChiliBonusGame->runAction(Sequence::create(
            DelayTime::create(2.0f),
            CallFunc::create(CC_CALLBACK_0(UISlotChiliBonusGame::setState, this, CGS_WAIT_RESPONSE)),
            nullptr));
        _selectCharacterIndex = index;
        _characterSelectBoard[_selectCharacterIndex]->setAnimation(0, "win", true);
        switch (_selectCharacterIndex)
        {
        case ECharacterIndex::ECI_DOG:
            AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_DOG, false);
            break;
        case ECharacterIndex::ECI_LIZARD:
            AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_LIZARD, false);
            break;
        case ECharacterIndex::ECI_DONKEY:
            AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_DONKEY, false);
            break;
        default:
            break;
        }
  
        break;
    case cocos2d::ui::Widget::TouchEventType::CANCELED:
        highlightCharacter(ECI_CHARACTER_COUNT);
        break;
    default:
        break;
    }
}


void UISlotChiliBonusGame::onTouchStartEvent(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    if (_isAutoSpin)
        return;
    setState(CGS_CHOOSE_CHARACTER);
}

void UISlotChiliBonusGame::onTouchCloseGameEvent(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;
    if (_isAutoSpin)
        return;

    Player::getInstance()->setCoin(_playerCoin);
    closeUI();
}

void UISlotChiliBonusGame::setState(chiliGameStatus Nextstate)
{
    switch (Nextstate)
    {
    case UISlotChiliBonusGame::CGS_NONE:
        break;
    case UISlotChiliBonusGame::CGS_INIT:

        break;
    case UISlotChiliBonusGame::CGS_START:
        _changeToGameScene->updateToFrameByTime(0);
        setAwardVisible(false); 
        showSelectArrow(ECI_CHARACTER_COUNT);
        _selectCharacter->setVisible(false);
        _gameplay->setVisible(false);
        _startPage->setVisible(true);
        _resultPage->setVisible(false);
        _BoardPage->setVisible(true);

        setChiliRound(ECI_DOG, -1);
        setChiliRound(ECI_LIZARD, -1);
        setChiliRound(ECI_DONKEY, -1);

        _characterChiliHead[ECI_DOG][0]->updateToFrameByTime(0);
        _characterChiliHead[ECI_LIZARD][0]->updateToFrameByTime(0);
        _characterChiliHead[ECI_DONKEY][0]->updateToFrameByTime(0);

        _characterSelectBoard[ECI_DOG]->setToSetupPose();
        _characterSelectBoard[ECI_DOG]->setAnimation(0, "idle", true);
        _characterSelectBoard[ECI_LIZARD]->setToSetupPose();
        _characterSelectBoard[ECI_LIZARD]->setAnimation(0, "idle", true);
        _characterSelectBoard[ECI_DONKEY]->setToSetupPose();
        _characterSelectBoard[ECI_DONKEY]->setAnimation(0, "idle", true);
        _characterBehind[ECI_DOG]->addAnimation(0, "idle", true);
        _characterFront[ECI_DOG]->addAnimation(0, "idle", true);
        _characterBehind[ECI_LIZARD]->addAnimation(0, "idle", true);
        _characterFront[ECI_LIZARD]->addAnimation(0, "idle", true);
        _characterBehind[ECI_DONKEY]->addAnimation(0, "idle", true);
        _characterFront[ECI_DONKEY]->addAnimation(0, "idle", true);

        _selectCharacterIndex = ECI_CHARACTER_COUNT;

        if (_isAutoSpin)
        {
            _SlotChiliBonusGame->runAction(Sequence::create(
                DelayTime::create(2.0f),
                CallFunc::create(CC_CALLBACK_0(UISlotChiliBonusGame::setState, this, CGS_CHOOSE_CHARACTER)),
                nullptr));
        }
        break;
    case UISlotChiliBonusGame::CGS_CHOOSE_CHARACTER:
        highlightCharacter(ECI_CHARACTER_COUNT);
        _characterSelectBoard[ECI_DOG]->setAnimation(0,"idle",true);
        _characterSelectBoard[ECI_LIZARD]->setAnimation(0,"idle",true);
        _characterSelectBoard[ECI_DONKEY]->setAnimation(0,"idle",true);
        _selectCharacter->setVisible(true);
        _gameplay->setVisible(false);
        _startPage->setVisible(false);
        _resultPage->setVisible(false);
        _BoardPage->setVisible(true);

        if (_isAutoSpin)
        {
            auto selectIndex = rand() % ECI_CHARACTER_COUNT;
            highlightCharacter((ECharacterIndex)selectIndex);
            _SlotChiliBonusGame->runAction(Sequence::create(
            DelayTime::create(2.0f),
            CallFunc::create(CC_CALLBACK_0(UISlotChiliBonusGame::setState, this, CGS_WAIT_RESPONSE)),
            nullptr));
            _selectCharacterIndex = (ECharacterIndex)selectIndex;
            _characterSelectBoard[_selectCharacterIndex]->setAnimation(0, "win", true);
            switch (_selectCharacterIndex)
            {
            case ECharacterIndex::ECI_DOG:
                AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_DOG , false);
                break;
            case ECharacterIndex::ECI_LIZARD:
                AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_LIZARD, false);
                break;
            case ECharacterIndex::ECI_DONKEY:
                AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_DONKEY, false);
                break;
            default:
                break;
            }
 
        }
        break;
    case UISlotChiliBonusGame::CGS_WAIT_RESPONSE:
        if (_currentState != CGS_CHOOSE_CHARACTER)
            return;
        RequestPokeResult(1, _bonusGameID);
        break;
    case UISlotChiliBonusGame::CGS_ROUND_START:
    {
        _changeToGameScene->play();
        _gameplay->setVisible(true);
        auto animationCallback = [this]()
        {
            _selectCharacter->setVisible(false); 
            _arrowAnimation->play();
            showSelectArrow(_selectCharacterIndex);
            _BoardPage->setVisible(false);

        };
        _SlotChiliBonusGame->runAction(Sequence::create(DelayTime::create(_changeToGameScene->getTotalTime()), 
            CallFunc::create(animationCallback), 
            // wait for animation
            DelayTime::create(3.0f),
            CallFunc::create([this](){setState(CGS_ROUND_EAT); }),
            //
            nullptr));
        
        _characterBehind[ECI_DOG]->setOpacity(0);
        _characterBehind[ECI_DOG]->runAction(Sequence::create(
            DelayTime::create(1.3f),
            FadeIn::create(0.5f),
            nullptr
            ));

        _characterFront[ECI_DOG]->setOpacity(0);
        _characterFront[ECI_DOG]->runAction(Sequence::create(
            DelayTime::create(1.3f),
            FadeIn::create(0.5f),
            nullptr
            ));

        _characterBehind[ECI_LIZARD]->setOpacity(0);
        _characterBehind[ECI_LIZARD]->runAction(Sequence::create(
            DelayTime::create(1.3f),
            FadeIn::create(0.5f),
            nullptr
            ));

        _characterFront[ECI_LIZARD]->setOpacity(0);
        _characterFront[ECI_LIZARD]->runAction(Sequence::create(
            DelayTime::create(1.3f),
            FadeIn::create(0.5f),
            nullptr
            ));

        _characterBehind[ECI_DONKEY]->setOpacity(0);
        _characterBehind[ECI_DONKEY]->runAction(Sequence::create(
            DelayTime::create(1.3f),
            FadeIn::create(0.5f),
            nullptr
            ));

        _characterFront[ECI_DONKEY]->setOpacity(0);
        _characterFront[ECI_DONKEY]->runAction(Sequence::create(
            DelayTime::create(1.3f),
            FadeIn::create(0.5f),
            nullptr
            ));

    }
       break;
    case UISlotChiliBonusGame::CGS_ROUND_EAT:

        setEatAnimation(ECI_DOG, 0);
        setEatAnimation(ECI_LIZARD, 0);
        setEatAnimation(ECI_DONKEY, 0);
        break;
    case UISlotChiliBonusGame::CGS_ROUND_END:
        _SlotChiliBonusGame->runAction(Sequence::create(
            // wait for animation
            DelayTime::create(2.0f),
            CallFunc::create([this](){setState(CGS_GAME_OVER); }),
            //
            nullptr));
        break;
    case UISlotChiliBonusGame::CGS_GAME_OVER:
        showSelectArrow(ECharacterIndex::ECI_CHARACTER_COUNT);
        _awardAnimation->updateToFrameByTime(0);
        setAwardVisible(true);
        _awardAnimation->play();

        for (int i = 0; i < ECI_CHARACTER_COUNT; i++)
        {
            if (_gameRank[i] == 0)
            {
                _characterBehind[i]->addAnimation(0, "win", true);
                _characterFront[i]->addAnimation(0, "win", true);
                switch (i)
                {
                case ECharacterIndex::ECI_DOG:
                    AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_DOG, false);
                    break;
                case ECharacterIndex::ECI_LIZARD:
                    AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_LIZARD, false);
                    break;
                case ECharacterIndex::ECI_DONKEY:
                    AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_DONKEY, false);
                    break;
                default:
                    break;
                }
            }
        }

        
        _SlotChiliBonusGame->runAction(Sequence::create(
            // wait for animation
            DelayTime::create(5.0f),
            CallFunc::create([this](){setState(CGS_RESULT); }),
            //
            nullptr));
        break;
    case UISlotChiliBonusGame::CGS_RESULT:
        _changeToGameScene->updateToFrameByTime(0);
        _selectCharacter->setVisible(false);
        _gameplay->setVisible(false);
        _startPage->setVisible(false);
        _resultPage->setVisible(true);
        _BoardPage->setVisible(true);
        {
            _totalWinBMFont->setString(GetMoneyFormat((uint64)_totalScore * _currentBet));
            auto scoreStr = GetMoneyFormat(_totalScore);
            auto currentBetStr = GetMoneyFormat(_currentBet);
            auto str = StringUtils::format("%s * %s", currentBetStr.c_str(), scoreStr.c_str());
            _winCalculateBMFont->setString(str);

            if (_isAutoSpin)
            {
                _SlotChiliBonusGame->runAction(Sequence::create(
                    DelayTime::create(2.0f),
                    CallFunc::create([this]()
                    {   
                        Player::getInstance()->setCoin(_playerCoin);
                        closeUI();
                    }),
                    nullptr));
            }
        }
        break;
    default:
        break;
    }
    _currentState = Nextstate;
}


void UISlotChiliBonusGame::highlightCharacter(ECharacterIndex index)
{
    if (index == ECI_CHARACTER_COUNT)
    {
        for (int i = 0; i < ECI_CHARACTER_COUNT; i++)
        {
            _selectBoard[i]->setColor(Color3B::WHITE);
            _characterSelectBoard[i]->setColor(Color3B::WHITE);
        }
    }
    else
    {
        for (int i = 0; i < ECI_CHARACTER_COUNT; i++)
        {
            if (i != index)
            {
                _selectBoard[i]->setColor(Color3B::GRAY);
                _characterSelectBoard[i]->setColor(Color3B::GRAY);
            }
            else
            {
                _selectBoard[i]->setColor(Color3B::WHITE);
                _characterSelectBoard[i]->setColor(Color3B::WHITE);
            }
        }
    }
}

void UISlotChiliBonusGame::showSelectArrow(ECharacterIndex index)
{
        for (int i = 0; i < ECI_CHARACTER_COUNT; i++)
        {
            _arrowImage[i]->setVisible(i == index);
        }
}
void UISlotChiliBonusGame::setAward(int dogAward, int LizardAward, int donckeyAward)
{
    switch (dogAward)
    {
    case 0:
        _awardImage[ECI_DOG]->loadTexture(GOCIMAGE(SharedFrame_1st_player), Widget::TextureResType::PLIST);
        break;
    case 1:
        _awardImage[ECI_DOG]->loadTexture(GOCIMAGE(SharedFrame_2nd_player), Widget::TextureResType::PLIST);
        break;
    case 2:
        _awardImage[ECI_DOG]->loadTexture(GOCIMAGE(SharedFrame_3rd_player), Widget::TextureResType::PLIST);
        break;
    default:
        break;
    }

    switch (LizardAward)
    {
    case 0:
        _awardImage[ECI_LIZARD]->loadTexture(GOCIMAGE(SharedFrame_1st_player), Widget::TextureResType::PLIST);
        break;
    case 1:
        _awardImage[ECI_LIZARD]->loadTexture(GOCIMAGE(SharedFrame_2nd_player), Widget::TextureResType::PLIST);
        break;
    case 2:
        _awardImage[ECI_LIZARD]->loadTexture(GOCIMAGE(SharedFrame_3rd_player), Widget::TextureResType::PLIST);
        break;
    default:
        break;
    }

    switch (donckeyAward)
    {
    case 0:
        _awardImage[ECI_DONKEY]->loadTexture(GOCIMAGE(SharedFrame_1st_player), Widget::TextureResType::PLIST);
        break;
    case 1:
        _awardImage[ECI_DONKEY]->loadTexture(GOCIMAGE(SharedFrame_2nd_player), Widget::TextureResType::PLIST);
        break;
    case 2:
        _awardImage[ECI_DONKEY]->loadTexture(GOCIMAGE(SharedFrame_3rd_player), Widget::TextureResType::PLIST);
        break;
    default:
        break;
    }
}


void UISlotChiliBonusGame::setAwardVisible(bool visible)
{
    _awardImage[ECI_DOG]->setVisible(visible);
    _awardImage[ECI_LIZARD]->setVisible(visible);
    _awardImage[ECI_DONKEY]->setVisible(visible);
}

void UISlotChiliBonusGame::setEatAnimation(ECharacterIndex index, int round)
{
    _characterBehind[index]->setToSetupPose();

    _characterBehind[index]->setAnimation(0, "eat_in", false);
    _characterBehind[index]->runAction(Sequence::create(
        DelayTime::create(0.25f),
        CallFunc::create([this, index,round]()
            {
                setChiliRound(index, round);
                _characterChili[index]->play();
            }),
        DelayTime::create(0.4f),
        CallFunc::create([this, index, round]()
            {
                AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_EAT, false);
                _characterChiliHead[index][round]->play();
            }),
        nullptr
        ));
    _characterBehind[index]->addAnimation(0, "eat_loop", false);
    _characterBehind[index]->addAnimation(0, "eat_loop", false);

    if (CHILIRESULT[_currentSeed][_gameRank[index]][round])
    {
        _characterBehind[index]->addAnimation(0, "hot_in", false);
        for (int i = 0; i < 6; i++)
            _characterBehind[index]->addAnimation(0, "hot_loop", false);
        _characterBehind[index]->addAnimation(0, "hot_out", false);
        _characterBehind[index]->addAnimation(0, "idle", true);
        _characterBehind[index]->runAction(Sequence::create(
            DelayTime::create(2.0f),
            CallFunc::create([](){  AudioManager::getInstance()->playEffectWithFileID(EFF_CHILI_BONUSGAME_HOT, false); }),
            DelayTime::create(5.0f),
            CallFunc::create(CC_CALLBACK_0(UISlotChiliBonusGame::spineAnimCompleteCB, this, index, round)),
            nullptr
            ));
    }
    else
    {
        for (int i = 0; i < 9; i++)
            _characterBehind[index]->addAnimation(0, "eat_loop", false);
        _characterBehind[index]->addAnimation(0, "idle", true);
        _characterBehind[index]->runAction(Sequence::create(
            DelayTime::create(4.5f),
            CallFunc::create(CC_CALLBACK_0(UISlotChiliBonusGame::spineAnimCompleteCB, this, index, round)),
            nullptr
            ));
    }
    // front
    _characterFront[index]->setToSetupPose();
    _characterFront[index]->setAnimation(0, "eat_in", false);
    _characterFront[index]->addAnimation(0, "eat_loop", false);
    _characterFront[index]->addAnimation(0, "eat_loop", false);

    if (CHILIRESULT[_currentSeed][_gameRank[index]][round])
    {
        _characterFront[index]->addAnimation(0, "hot_in", false);
        for (int i = 0; i < 6; i++)
            _characterFront[index]->addAnimation(0, "hot_loop", false);
        _characterFront[index]->addAnimation(0, "hot_out", false);
        _characterFront[index]->addAnimation(0, "idle", true);
    }
    else
    {
        for (int i = 0; i < 9; i++)
            _characterFront[index]->addAnimation(0, "eat_loop", false);
        _characterFront[index]->addAnimation(0, "idle", true);
    }
}


void UISlotChiliBonusGame::spineAnimCompleteCB(ECharacterIndex index, int round)
{
    if (_currentState != CGS_ROUND_EAT)
        return;
    if (round == 3)
        setState(CGS_ROUND_END);
    else
        setEatAnimation(index, round + 1);
   
}

void UISlotChiliBonusGame::setChiliRound(ECharacterIndex index, int round)
{
    switch (round)
    {
    case 0:
        _chiliImage[index][0]->setVisible(true);
        _chiliImage[index][1]->setVisible(true);
        _chiliImage[index][2]->setVisible(true);
        _chiliImage[index][3]->setVisible(false);
        break;
    case 1:
        _chiliImage[index][0]->setVisible(false);
        _chiliImage[index][1]->setVisible(true);
        _chiliImage[index][2]->setVisible(true);
        _chiliImage[index][3]->setVisible(false);
        break;
    case 2:
        _chiliImage[index][0]->setVisible(false);
        _chiliImage[index][1]->setVisible(false);
        _chiliImage[index][2]->setVisible(true);
        _chiliImage[index][3]->setVisible(false);
        break;
    case 3:
        _chiliImage[index][0]->setVisible(false);
        _chiliImage[index][1]->setVisible(false);
        _chiliImage[index][2]->setVisible(false);
        _chiliImage[index][3]->setVisible(false);
        break;

    default:
        _chiliImage[index][0]->setVisible(true);
        _chiliImage[index][1]->setVisible(true);
        _chiliImage[index][2]->setVisible(true);
        _chiliImage[index][3]->setVisible(true);
        break;
    }

}