#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "UIBasicUnit.h"
#include "cocostudio/CocoStudio.h"
#include "../Animation/CustomSpAnim.h"
#include "../GlobalDefine/CustomTypeDef.h"

USING_NS_CC;

using namespace ui;

const bool CHILIRESULT[4][3][4] = 
{
    {
        { false, false, false, false },
        { true, false, false, true },
        { false, true, true, true },
    },
    {
        { true, false, false, false },
        { false, true, true, false },
        { false, true, true, true },
    },
    {
        { true, false, false, false },
        { true, true, false, false },
        { false, true, true, true },
    },
    {
        { false, false, false, true },
        { false, true, false, true },
        { true, true, true, true },
    },
}
; // [kind][rank][round]


class UISlotChiliBonusGame : public UIBasicUnit
{
private:
    enum ECharacterIndex
    {
        ECI_DOG = 0,
        ECI_LIZARD = 1,
        ECI_DONKEY = 2,
        ECI_CHARACTER_COUNT = 3
    };

public:
    static UISlotChiliBonusGame* create();
    UISlotChiliBonusGame();
    ~UISlotChiliBonusGame();

    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void destroyUI() override;
    virtual void updateUI() override;
    virtual void UpdatePerSecond(float dt) override;

    void setGameInfo(int bonusGameID, int currentBet, bool isAutoSpin);

    void reset();
private:
    virtual bool createUI() override;

    virtual void onDispatchResponse(const std::string& requestTag, const std::string& json) override;

    bool init();

    void highlightCharacter(ECharacterIndex index);

    void showSelectArrow(ECharacterIndex index);

    void setAward(int dogAward, int LizardAward, int donckeyAward);

    void setAwardVisible(bool visible);

    void setEatAnimation(ECharacterIndex index, int round);

    void spineAnimCompleteCB(ECharacterIndex index, int round);

    void setChiliRound(ECharacterIndex index, int round);

private:
    void onTouchSelectBoardEvent(Ref* pSender, Widget::TouchEventType type, ECharacterIndex index);

    void onTouchStartEvent(Ref* pSender, Widget::TouchEventType type);

    void onTouchCloseGameEvent(Ref* pSender, Widget::TouchEventType type);

private: // bonus game status
    enum chiliGameStatus
    {
        CGS_NONE,
        CGS_INIT,
        CGS_START,
        CGS_CHOOSE_CHARACTER,
        CGS_WAIT_RESPONSE,
        CGS_ROUND_START,
        CGS_ROUND_EAT,
        CGS_ROUND_END,
        CGS_GAME_OVER,
        CGS_RESULT,
    };
    void setState(chiliGameStatus Nextstate);
    chiliGameStatus _currentState;

private: // character
    ECharacterIndex _selectCharacterIndex;

private: 
    Layout* _SlotChiliBonusGame = nullptr;
    Layout* _selectCharacter = nullptr;
    Layout* _gameplay = nullptr;
    Layout* _startPage = nullptr;
    Layout* _resultPage = nullptr;
    Layout* _BoardPage = nullptr;
    TextBMFont* _totalWinBMFont = nullptr;
    TextBMFont* _winCalculateBMFont = nullptr;
    TextBMFont* _awardCountText[3];

    Layout* _selectBoard[ECI_CHARACTER_COUNT];

    ImageView* _arrowImage[ECI_CHARACTER_COUNT];
    ImageView* _awardImage[ECI_CHARACTER_COUNT];
    ImageView* _chiliImage[ECI_CHARACTER_COUNT][4];
    
    CustomSpAnim* _characterBehind[ECI_CHARACTER_COUNT];
    CustomSpAnim* _characterFront[ECI_CHARACTER_COUNT];
    spine::SkeletonAnimation* _characterSelectBoard[ECI_CHARACTER_COUNT];
    cocostudio::ActionObject* _changeToGameScene;
    cocostudio::ActionObject* _arrowAnimation;
    cocostudio::ActionObject* _awardAnimation;
    cocostudio::ActionObject* _characterChili[ECharacterIndex::ECI_CHARACTER_COUNT];
    cocostudio::ActionObject* _characterChiliHead[ECharacterIndex::ECI_CHARACTER_COUNT][4];

private: // game data
    int _slotMachineID; 
    int _currentBet;
    int _bonusGameID;
    uint64 _totalScore;
    uint64 _playerCoin;
    bool _isAutoSpin;
    int _currentSeed;

    int _gameRank[ECI_CHARACTER_COUNT];
};