#include "UISlotChiliFreeSpinResult.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Animation/CustomActions.h"
#include "../DataCenter/ExternalTable.h"
#include "AudioManager.h"
#include "../Gameplay/GameLogic.h"


using namespace cocostudio;
using namespace ui;


UISlotChiliFreeSpinResult* UISlotChiliFreeSpinResult::create()
{
    UISlotChiliFreeSpinResult* unit = new UISlotChiliFreeSpinResult();

	if (unit && unit->init())
	{
		return unit;
	}

	CC_SAFE_DELETE(unit);
	return nullptr;
}

bool UISlotChiliFreeSpinResult::init()
{
	return true;
}

UISlotChiliFreeSpinResult::UISlotChiliFreeSpinResult()
{
	setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotChiliFreeSpinResult::~UISlotChiliFreeSpinResult()
{

}

void UISlotChiliFreeSpinResult::notifyCloseUI()
{
    _pBMFFreeSpinCounter->stopAllActions();
    _pLayoutFreeSpinResult->stopAllActions();
    if (_onCloseUICB)
    {
        _onCloseUICB();
        _onCloseUICB = nullptr;
    }

    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (GameLogic::getInstance()->GetState() != E_GameState_StayBonusGame)
        AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

bool UISlotChiliFreeSpinResult::createUI()
{
    _pLayoutFreeSpinResult = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("UISlot_DL_Machine0005_Chili/UISlot_Chili_FreeSpins_Result.ExportJson"));
    _mainUINode->addChild(_pLayoutFreeSpinResult);
    _pLayoutFreeSpinResult->setTouchEnabled(true);

    setTraversalCascadeOpacityEnabled(_mainUINode, true);

    auto layoutText = _pLayoutFreeSpinResult->getChildByName<TextBMFont*>("text");
    _pBMFWinMoney = layoutText->getChildByName<TextBMFont*>("bmf_winmoney");
    _pBMFWinMoney->setString("0");

    _pBMFFreeSpinCounter = layoutText->getChildByName<TextBMFont*>("bmf_freespin_counter");
    _pBMFFreeSpinCounter->setString("0");

    _pBtnCloseUI = _pLayoutFreeSpinResult->getChildByName<Button*>("btn_closeui");
    _pBtnCloseUI->addTouchEventListener(CC_CALLBACK_2(UISlotChiliFreeSpinResult::onTouchExitButton, this));

	return true;
}

void UISlotChiliFreeSpinResult::destroyUI()
{
    _pLayoutFreeSpinResult->removeFromParent();
    _pLayoutFreeSpinResult = nullptr;
}

void UISlotChiliFreeSpinResult::updateUI()
{
    if (_winMoney<0)//�i��0��
        return;

    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(TIME_COUNTMONEY, 0, _winMoney);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_NONE);
    _pBMFWinMoney->runAction(pAction);

    _pBMFFreeSpinCounter->setString(Value(_freeSpinCounter).asString());

    if (_autoCloseUITime > 0.0f)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(_autoCloseUITime));
        arrayOfActions.pushBack(CallFunc::create([this](){ this->closeUI(); }));
        auto actionSeq = Sequence::create(arrayOfActions);
        actionSeq->setTag(TAG_CLOSEUI);
        _pLayoutFreeSpinResult->runAction(actionSeq);
    }
}

void UISlotChiliFreeSpinResult::Update(float dt)
{
}

void UISlotChiliFreeSpinResult::onTouchExitButton(Ref* sender, ui::Widget::TouchEventType event)
{
    if (event != ui::Widget::TouchEventType::ENDED)
        return;

    _pLayoutFreeSpinResult->stopActionByTag(TAG_CLOSEUI);
    closeUI();
}