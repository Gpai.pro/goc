#include "UISlotChiliFreeSpinStart.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Animation/CustomActions.h"
#include "../DataCenter/ExternalTable.h"
#include "AudioManager.h"


using namespace cocostudio;
using namespace ui;

#define EFF_SLOT5_FREESPIN_ENTER 5105

UISlotChiliFreeSpinStart* UISlotChiliFreeSpinStart::create()
{
    UISlotChiliFreeSpinStart* unit = new UISlotChiliFreeSpinStart();

	if (unit && unit->init())
	{
		return unit;
	}

	CC_SAFE_DELETE(unit);
	return nullptr;
}

bool UISlotChiliFreeSpinStart::init()
{
	return true;
}

UISlotChiliFreeSpinStart::UISlotChiliFreeSpinStart()
{
	setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotChiliFreeSpinStart::~UISlotChiliFreeSpinStart()
{

}

void UISlotChiliFreeSpinStart::notifyOpenUI()
{
    AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT5_FREESPIN_ENTER, false);

    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
}

void UISlotChiliFreeSpinStart::notifyCloseUI()
{
    _pLayoutFreeSpinStart->stopAllActions();
    if (_onCloseUICB)
    {
        _onCloseUICB();
        _onCloseUICB = nullptr;
    }
}

bool UISlotChiliFreeSpinStart::createUI()
{
    _pLayoutFreeSpinStart = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("UISlot_DL_Machine0005_Chili/UISlot_Chili_FreeSpins_Start.ExportJson"));

    _mainUINode->addChild(_pLayoutFreeSpinStart);

    setTraversalCascadeOpacityEnabled(_mainUINode, true);

    _pLayoutCloseUIListenser = _pLayoutFreeSpinStart->getChildByName<Layout*>("layout_listener_closeui");
    CCASSERT(_pLayoutCloseUIListenser, "");
    _pLayoutCloseUIListenser->addTouchEventListener(CC_CALLBACK_2(UISlotChiliFreeSpinStart::onTouchCloseUIListener, this));

    _pBMFFreeSpinCounter = _pLayoutFreeSpinStart->getChildByName<TextBMFont*>("bmf_freespin_counter");
    CCASSERT(_pBMFFreeSpinCounter, "");
    _pBMFFreeSpinCounter->setString("0");

	return true;
}

void UISlotChiliFreeSpinStart::destroyUI()
{
    _pLayoutFreeSpinStart->removeFromParent();
    _pLayoutFreeSpinStart = nullptr;
}

void UISlotChiliFreeSpinStart::updateUI()
{
    if (_freeSpinCounter<=0)
        return;

    _pBMFFreeSpinCounter->setString(Value(_freeSpinCounter).asString());

    if (_autoCloseUITime > 0.0f)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(_autoCloseUITime));
        arrayOfActions.pushBack(CallFunc::create([this](){ this->closeUI(); }));
        auto actionSeq = Sequence::create(arrayOfActions);
        actionSeq->setTag(TAG_CLOSEUI);
        _pLayoutFreeSpinStart->runAction(actionSeq);
    }
}

void UISlotChiliFreeSpinStart::Update(float dt)
{
}

void UISlotChiliFreeSpinStart::onTouchCloseUIListener(Ref* sender, ui::Widget::TouchEventType event)
{
    if (event != Widget::TouchEventType::ENDED)
        return;
    _pLayoutFreeSpinStart->stopActionByTag(TAG_CLOSEUI);
    closeUI();
}