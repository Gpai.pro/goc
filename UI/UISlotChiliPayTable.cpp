#include "UISlotChiliPayTable.h"
#include "../DataCenter/ExternalTable.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/ImageDefine.h"
#include "CustomRichText.h"
#include "AudioManager.h"


UISlotChiliPayTable* UISlotChiliPayTable::create()
{
    UISlotChiliPayTable* uiPayTable = new UISlotChiliPayTable();

    if (uiPayTable && uiPayTable->init())
    {
        return uiPayTable;
    }

    CC_SAFE_DELETE(uiPayTable);
    return nullptr;
}

UISlotChiliPayTable::UISlotChiliPayTable()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);

    _page1SymbolTypes.clear();
    _page1StringData_1.clear();
    _page1StringData_2.clear();
    _page1StringData_3.clear();
}

UISlotChiliPayTable::~UISlotChiliPayTable()
{

}

void UISlotChiliPayTable::destroyUI()
{

}

void UISlotChiliPayTable::updateUI()
{
    _nowPage = 1;
    _pages[0]->setVisible(true);

    for (int i = 1; i < TOTAL_PAGE_COUNT; ++i)
    {
        _pages[i]->setVisible(false);
    }

    setLeftButtonVisible(false);
    setRightButtonVisible(true);

    // setting
    setPage1();
    setPage2();
    setPage3();
    setPage4();
}

bool UISlotChiliPayTable::openUI(int slotID, std::string exportJsonPath)
{
    setSlotMachineID(slotID);

    setExportJsonPath(exportJsonPath);

    if (!UIBasicUnit::openUI())
    {
        return false;
    }

    return true;
}

void UISlotChiliPayTable::setSlotMachineID(int ID)
{
    _slotMachineID = ID;

    SlotMachineData* pData = ExternalTable<SlotMachineData>::getRecordByID(_slotMachineID);
    if (!pData)
    {
        CCLOG("UISlotChiliPayTable Load SlotMachineDate ID = %d Error!!!", _slotMachineID);
        return;
    }

    _gameSymbolBeginID = pData->GameSymbolBeginID;
    _gameSymbolEndID = pData->GameSymbolEndID;

    _symbolInfoBeginID = pData->SymbolInfoBeginID;
    _symbolInfoEndID = pData->SymbolInfoEndID;

    _payLineBeginID = pData->PayLineBeginID;
    _payLineEndID = pData->PayLineEndID;
}

void UISlotChiliPayTable::setExportJsonPath(std::string exportJsonPath)
{
    _exportJsonPath = exportJsonPath;
}

void UISlotChiliPayTable::setPage1Data(std::vector<int>& symbolTypes, std::vector<int>& contentIDs_1, std::vector<int>& contentIDs_2, std::vector<int>& contentIDs_3)
{
    _page1SymbolTypes = symbolTypes;
    _page1StringData_1 = contentIDs_1;
    _page1StringData_2 = contentIDs_2;
    _page1StringData_3 = contentIDs_3;
}

bool UISlotChiliPayTable::init()
{
    return true;
}

bool UISlotChiliPayTable::createUI()
{
    if (_exportJsonPath.empty())
        return false;

    _payTable = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile(_exportJsonPath.c_str()));
    _mainUINode->addChild(_payTable);
    setUIZOrder(ELZO_NORMAL);

    auto buttonLayout = _payTable->getChildByName<Layout*>("Button");
    buttonLayout->setLocalZOrder(50);

    _backButton = buttonLayout->getChildByName<Button*>("btn_back_to_game");
    _backButton->addTouchEventListener(CC_CALLBACK_2(UISlotChiliPayTable::touchBackToGame, this));
    _backButton->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    _buttonRight = buttonLayout->getChildByName<Button*>("btn_right");
    _buttonRight->addTouchEventListener(CC_CALLBACK_2(UISlotChiliPayTable::touchRightButton, this));

    _buttonLeft = buttonLayout->getChildByName<Button*>("btn_left");
    _buttonLeft->addTouchEventListener(CC_CALLBACK_2(UISlotChiliPayTable::touchLeftButton, this));

    _attachment1 = buttonLayout->getChildByName<ImageView*>("img_chain_01");

    _attachment2 = buttonLayout->getChildByName<ImageView*>("img_chain_02");

    setLeftButtonVisible(false);

    // Page_01
    _pages[0] = _payTable->getChildByName<Layout*>("Page_01");
    _pages[0]->setVisible(true);


    for (int i = 0; i < SPECIAL_SYMBOL_COUNT; ++i)
    {
        _specialSymbol[i]._symbol = _pages[0]->getChildByName<Layout*>(StringUtils::format("Panel_%02d", i + 1));

        _specialSymbol[i]._content = _pages[0]->getChildByName<Text*>(StringUtils::format("type_%02d_%d", i + 1, 1));
        _specialSymbol[i]._content->ignoreContentAdaptWithSize(true);
        _specialSymbol[i]._content->setTextAreaSize(Size(200.0f, 0.0f));
    }


    // Page_02
    _pages[1] = _payTable->getChildByName<Layout*>("Page_02");
    _pages[1]->setVisible(false);
    for (int i = 0; i < NORMAL_SYMBOL_COUNT; ++i)
    {
        _normalSymbol[i]._symbol = _pages[1]->getChildByName<Layout*>(StringUtils::format("Panel_%02d", i + 1));
        _normalSymbol[i]._symbol->setVisible(false);

        for (int j = 0; j < NORMAL_SYMBOL_TEXT_COUNT; ++j)
        {
            _normalSymbol[i]._content[j] = _pages[1]->getChildByName<TextBMFont*>(StringUtils::format("type_%02d_%d", i + 1, j + 1));
            _normalSymbol[i]._content[j]->setVisible(false);
        }
    }

    // Page_03
    _pages[2] = _payTable->getChildByName<Layout*>("Page_03");
    _pages[2]->setVisible(false);
    for (int i = 0; i < MAX_PAYLINE_COUNT; ++i)
    {
        _payLinePic[i] = _pages[2]->getChildByName<ImageView*>(StringUtils::format("SharePaylines_%02d", i + 1));
        _payLinePic[i]->setVisible(false);
    }

    _payLineText = _pages[2]->getChildByName<Text*>("type_1");

    // Page_04
    _pages[3] = _payTable->getChildByName<Layout*>("Page_04");
    _pages[3]->setVisible(false);

    _stickyRule = _pages[3]->getChildByName<Text*>("lbl_content");

    return true;
}

void UISlotChiliPayTable::setPage1()
{
    for (int i = _symbolInfoBeginID; i <= _symbolInfoEndID; ++i)
    {
        SymbolInfoData* pData = ExternalTable<SymbolInfoData>::getRecordByID(i);
        if (!pData)
        {
            CCLOG("UISlotChiliPayTable Load SymbolInfoData ID = %d Error!!!", i);
            continue;
        }

        if (pData->Type == _page1SymbolTypes.at(0))
        {
            auto filename = StringUtils::format(GOCIMAGE(%s), pData->SpriteFile.c_str());
            _specialSymbol[0]._symbol->setBackGroundImage(filename, Widget::TextureResType::PLIST);

            CustomRichText::setCustomRichText(_specialSymbol[0]._content, 300.0f, _page1StringData_1);
        }
        else if (pData->Type == _page1SymbolTypes.at(1))
        {
            auto filename = StringUtils::format(GOCIMAGE(%s), pData->SpriteFile.c_str());
            _specialSymbol[1]._symbol->setBackGroundImage(filename, Widget::TextureResType::PLIST);

            CustomRichText::setCustomRichText(_specialSymbol[1]._content, 310.0f, _page1StringData_2);
        }
        else if (pData->Type == _page1SymbolTypes.at(2))
        {
            auto filename = StringUtils::format(GOCIMAGE(%s), pData->SpriteFile.c_str());
            _specialSymbol[2]._symbol->setBackGroundImage(filename, Widget::TextureResType::PLIST);

            CustomRichText::setCustomRichText(_specialSymbol[2]._content, 300.0f, _page1StringData_3);
        }
    }
}

void UISlotChiliPayTable::setPage2()
{
    int index = 0;
    for (int i = _gameSymbolBeginID; i <= _gameSymbolEndID; ++i)
    {
        GameSymbolData* pGameSymbolData = ExternalTable<GameSymbolData>::getRecordByID(i);
        if (!pGameSymbolData)
        {
            CCLOG("UISlotChiliPayTable Load GameSymbolData ID = %d Error!!!", i);
            continue;
        }

        SymbolInfoData* pSymbolInfoData = ExternalTable<SymbolInfoData>::getRecordByID(pGameSymbolData->SymbolInfoID[0]);
        if (!pSymbolInfoData)
        {
            CCLOG("UISlotChiliPayTable Load SymbolInfoData ID = %d Error!!!", i);
            continue;
        }

        if (pSymbolInfoData->Type == SymbolInfoData::SYMBOLTYPE_NORMAL)
        {
            _normalSymbol[index]._symbol->setVisible(true);
            auto filename = StringUtils::format(GOCIMAGE(%s), pSymbolInfoData->SpriteFile.c_str());
            _normalSymbol[index]._symbol->setBackGroundImage(filename, Widget::TextureResType::PLIST);

            for (int j = 0; j < NORMAL_SYMBOL_TEXT_COUNT; ++j)
            {
                if (pGameSymbolData->ComboPay[j] > 0)
                {
                    _normalSymbol[index]._content[NORMAL_SYMBOL_TEXT_COUNT - j - 1]->setVisible(true);
                    _normalSymbol[index]._content[NORMAL_SYMBOL_TEXT_COUNT - j - 1]->setString(StringUtils::format("%d - ", j + 1) + GetMoneyFormat(pGameSymbolData->ComboPay[j]));
                }
                else
                {
                    _normalSymbol[index]._content[NORMAL_SYMBOL_TEXT_COUNT - j - 1]->setVisible(false);
                }
            }

            ++index;
        }
    }
}

void UISlotChiliPayTable::setPage3()
{
    int index = 0;
    for (int i = _payLineBeginID; i <= _payLineEndID; ++i)
    {
        _payLinePic[index]->setVisible(true);
        ++index;
    }

    std::vector<int> stringDataIDs;
    for (int i = 10012; i <= 10014; ++i)
        stringDataIDs.push_back(i);

    CustomRichText::setCustomRichText(_payLineText, 500.0f, stringDataIDs);
}

void UISlotChiliPayTable::setPage4()
{
    _stickyRule->setString(safeGetStringData(10037));
}

void UISlotChiliPayTable::setLeftButtonVisible(bool visible)
{
    _buttonLeft->setVisible(visible);
    if (_attachment2)
        _attachment2->setVisible(visible);
}

void UISlotChiliPayTable::setRightButtonVisible(bool visible)
{
    _buttonRight->setVisible(visible);

    if (_attachment1)
        _attachment1->setVisible(visible);
}

void UISlotChiliPayTable::setButtonTouchEnable(bool enable)
{
    _buttonLeft->setTouchEnabled(enable);
    _buttonRight->setTouchEnabled(enable);
    _backButton->setTouchEnabled(enable);

    if (_attachment2)
        _attachment2->setVisible(enable);
    if (_attachment1)
        _attachment1->setVisible(enable);
}

void UISlotChiliPayTable::touchBackToGame(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}

void UISlotChiliPayTable::touchRightButton(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_nowPage >= TOTAL_PAGE_COUNT)
        return;

    _pages[_nowPage - 1]->setVisible(false);
    _pages[_nowPage]->setVisible(true);

    /* setButtonTouchEnable(false);
    _pages[_nowPage - 1]->runAction(MoveTo::create(0.7f, Vec2(1400.0f, _pages[_nowPage - 1]->getPositionY())));
    _mainUINode->runAction(Sequence::create(DelayTime::create(0.7f), CallFunc::create(CC_CALLBACK_0(UISlotChiliPayTable::setButtonTouchEnable, this, true)), nullptr));*/

    ++_nowPage;
    setLeftButtonVisible(true);

    if (_nowPage >= TOTAL_PAGE_COUNT)
        setRightButtonVisible(false);
}

void UISlotChiliPayTable::touchLeftButton(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_nowPage <= 1)
        return;

    _pages[_nowPage - 1]->setVisible(false);
    _pages[_nowPage - 2]->setVisible(true);

    /*  setButtonTouchEnable(false);
    _pages[_nowPage - 2]->runAction(MoveTo::create(0.7f, Vec2(0.0f, _pages[_nowPage - 1]->getPositionY())));
    _mainUINode->runAction(Sequence::create(DelayTime::create(0.7f), CallFunc::create(CC_CALLBACK_0(UISlotChiliPayTable::setButtonTouchEnable, this, true)), nullptr));*/

    --_nowPage;
    setRightButtonVisible(true);

    if (_nowPage <= 1)
        setLeftButtonVisible(false);
}