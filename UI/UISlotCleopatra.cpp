#include "UISlotCleopatra.h"
#include "UISlotCleopatraBonusGame.h"
#include "UISlotCleopatraFreeSpinStart.h"
#include "UISlotCleopatraFreeSpinResult.h"
#include "UIController.h"
#include "UIBigWin.h"
#include "UIFiveOfAKind.h"
#include "UIJackpot.h"
#include "UILevelup.h"
#include "UIOutOfCoin.h"
#include "UIMessage.h"
#include "../GlobalDefine/Player.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataCenter/ExternalTable.h"
#include "../Network/MessageProcess.h"
#include "../GlobalDefine/ImageDefine.h"
#include "../Animation/CustomActions.h"
#include "../Animation/CustomSpAnim.h"
#include "../Animation/ProcedureFX.h"
#include "AudioManager.h"
#include "UIPayTable.h"
#include "UILevelup.h"
#include "UIAutoSpinSetting.h"
#include "../Gameplay/GameLogic.h"

using namespace cocostudio;

const float LINEANIMTIME = 4.0f;
#define EFF_TOUCH_CLEOPATRA      3004      // 戳艷后

UISlotCleopatra* UISlotCleopatra::create(int slotMachineID)
{
    UISlotCleopatra* ui = new UISlotCleopatra();

    if (ui && ui->init(slotMachineID))
    {
        return ui;
    }

    CC_SAFE_DELETE(ui);
    return nullptr;
}

UISlotCleopatra::UISlotCleopatra()
{
    setUIShowType(EUISHOWTYPE::EUIST_SLOTMACHINE);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotCleopatra::~UISlotCleopatra()
{

}

void UISlotCleopatra::destroyUI()
{

}

void UISlotCleopatra::notifyOpenUI()
{
    AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

void UISlotCleopatra::notifyCloseUI()
{
    SlotMachineInfo info = Player::getInstance()->getSlotMachineInfo(_slotID);
    info.freeSpin = _remainFreeSpinCount;
    Player::getInstance()->setSlotMachineInfo(_slotID, info);

    stopMachine();
    stopPerformSpinResult();

    if (_isAutoPanelOn)
        setAutoPanel(!_isAutoPanelOn);
    if (_isBetOn)
        setBetPanel(!_isBetOn);
}

void UISlotCleopatra::updateUI()
{
    SlotMachineInfo machineInfo = Player::getInstance()->getSlotMachineInfo(_slotID);
    
    //update jackpot
    setJackpotScore(machineInfo.jackpot);
    setJPEnable(machineInfo.isJackpotEnable);
    updateJackpotHint();

    // update Bet
    setMaxLineAndBet();

    _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (_pSlotData->DefaultBet > 0)
    {
        _curBet = _pSlotData->DefaultBet;
        setBet(_curBet);
    }

    // update Free
    if (machineInfo.freeSpin > 0)
    {
        setSpinInUpdate(true);

        setFreeSpinCount(machineInfo.freeSpin);
        setAutoSpinRemainCount(_remainAutoCount + machineInfo.freeSpin);
        setAutoSpinStatus(AUTOSPINSTATUS_ON);
        setFreeSpinStatus(FREESPINSTATUS_ON);
        AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
    }
    else
    {
        setSpinInUpdate(false);
        setFreeSpinCount(machineInfo.freeSpin);
        setAutoSpinStatus(AUTOSPINSTATUS_OFF);
        setFreeSpinStatus(FREESPINSTATUS_OFF);
    }

    setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_IDLE);

    // set win 0
    setCurWin(0);
}

void UISlotCleopatra::UpdatePerSecond(float dt)
{
    if (_pActionEnterAnim->isPlaying())
        return;

    if (_spinBtnStatus == SLOTBTNSTATUS_ONCELINEANDSTOREUI)
    {
        if ((!_uiSlotCleopatra->getActionByTag(ETAG_PLAYALLLINE) ||
            !_uiSlotCleopatra->getActionByTag(ETAG_REPEATSINGLELINE) ||
            !_uiSlotCleopatra->getActionByTag(ETAG_PERFORMSPINRESULT) || 
            !_uiSlotCleopatra->getActionByTag(ETAG_STOPREEL)) && !isUIOpen())
            setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_IDLE);
    }

    if ((isFreeSpining() || isAutoSpining()) && _bSpinInUpdate && !isUIOpen())
    {
        setSpinInUpdate(false);
        if (isOpen())
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
        requestSpin();
    }

    SlotMachineInfo machineInfo = Player::getInstance()->getSlotMachineInfo(_slotID);

    // Update Jackpot
    if (machineInfo.isJackpotEnable)
        setJackpotScore(machineInfo.jackpot);
}

void UISlotCleopatra::playEnterUIAction()
{
    _pActionEnterAnim->play();
}

bool UISlotCleopatra::init(int slotMachineID)
{
    _slotID = slotMachineID;
    return true;
}

bool UISlotCleopatra::createUI()
{
    _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (!_pSlotData)
        return false;
    _payLineBeginID = _pSlotData->PayLineBeginID;
    _payLineEndID = _pSlotData->PayLineEndID;
    totalPayLineCount = _payLineEndID - _payLineBeginID + 1;

    for (int id = _pSlotData->SymbolInfoBeginID; id <= _pSlotData->SymbolInfoEndID; id++)
    {
        SymbolInfoData* pSymbolInfo = ExternalTable<SymbolInfoData>::getRecordByID(id);
        if (!pSymbolInfo)
            continue;
        if (pSymbolInfo->Type == SymbolInfoData::SYMBOLTYPE_SCATTER)
            _scatterID = pSymbolInfo->ID;
        else if (pSymbolInfo->Type == SymbolInfoData::SYMBOLTYPE_BONUS)
            _bonusID = pSymbolInfo->ID;
    }
    _minScatterCombo = UISlotUtils::getMinHitCombo(_slotID, _bonusID);
    _minBonusCombo = UISlotUtils::getMinHitCombo(_slotID, _scatterID);
    _reelCount = _pSlotData->ReelEndID - _pSlotData->ReelBeginID + 1;

    _uiSlotCleopatra = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Cleopatra/UISlot_Cleopatra.ExportJson"));
    _uiSlotCleopatra->setTouchEnabled(true);
    _mainUINode->addChild(_uiSlotCleopatra);

    _pLayoutGroup = _uiSlotCleopatra->getChildByName<Layout*>("Layout_Group");

    _winMoneyAmin = _pLayoutGroup->getChildByName<TextBMFont*>("bmf_winmoney_anim");
    _winMoneyAmin->setOpacity(0);

    // animation
    _pActionEnterAnim = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_Cleopatra/UISlot_Cleopatra.ExportJson", "enter_animation");
    _pActionEnterAnim->updateToFrameByTime(0);

    Layout* pLayoutDummySceneobj = static_cast<Layout*>(_pLayoutGroup->getChildByName("layout_dummy_sceneobj"));
    CCASSERT(pLayoutDummySceneobj, "");

    _sceneObjCleopatra = CustomSpAnim::createWithFile("UISlot_Cleopatra/Spine/cleopatra_obj_sp_cleopatra01", "");
    _sceneObjCleopatra->setMix("idle", "win01", 0.2f);
    _sceneObjCleopatra->setMix("idle", "win02_in", 0.2f);
    _sceneObjCleopatra->setMix("win02_in", "win02_loop", 0.2f);
    _sceneObjCleopatra->setMix("win02_loop", "win02_out", 0.2f);
    performSceneAnim(ECleopatraSceneAnim::SCENEANIM_CLEOPATRAIDLE);
    pLayoutDummySceneobj->addChild(_sceneObjCleopatra);

    Layout* sceneObjListener = _pLayoutGroup->getChildByName<Layout*>("layout_sceneobj_listener");
    sceneObjListener->setTouchEnabled(true);
    sceneObjListener->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchSceneObjCleopatra, this));


    _touchMask = _pLayoutGroup->getChildByName<Layout*>("touchMask");
    _touchMask->setVisible(true);
    _touchMask->setTouchEnabled(false);
    _touchMask->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchMainPenal, this));

    auto btns = _pLayoutGroup->getChildByName<Layout*>("btn_list");
    _payTableBtn = btns->getChildByName<Button*>("Btn_PayTable");
    _payTableBtn->setTouchEnabled(true);
    _payTableBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchPayTable, this));

    _maxBetBtn = btns->getChildByName<Button*>("Btn_MaxBet");
    _maxBetBtn->setSoundID(EFF_NONE);
    _maxBetBtn->setTouchEnabled(true);
    _maxBetBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchMaxBet, this));

    _spinBtn = btns->getChildByName<Button*>("Btn_Spin");
    _spinBtn->setTouchEnabled(true);
    _spinBtn->setSoundID(EFF_NONE);
    _spinBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchSpin, this));
    _spinImg = btns->getChildByName<ImageView*>("text_SPIN");
    _spinImgGray = btns->getChildByName<ImageView*>("text_SPIN_disable");
    _stopImg = btns->getChildByName<ImageView*>("text_STOP");
    _stopImgGray = btns->getChildByName<ImageView*>("text_STOP_disable");

    _curWinScore = _pLayoutGroup->getChildByName<TextBMFont*>("BMf_Win");
    _totalBet = _pLayoutGroup->getChildByName<TextBMFont*>("BMf_TotalBet");

    // jackpot
    _jackpotPanel = _pLayoutGroup->getChildByName<Layout*>("jackpot_panel");
    _jackpotBM = _jackpotPanel->getChildByName<TextBMFont*>("jackpot_count");
    _jackpot_hint_no = _jackpotPanel->getChildByName<ImageView*>("hint_no");
    _jackpot_hint_yes = _jackpotPanel->getChildByName<ImageView*>("hint_yes");

    _isFixedBet = _pSlotData->FixedBetBeginID > 0;
    if (_isFixedBet)
    {
        for (int i = _pSlotData->FixedBetBeginID; i <= _pSlotData->FixedBetEndID; i++)
        {
            FixedBetData* pFixBetData = ExternalTable<FixedBetData>::getRecordByID(i);
            if (pFixBetData)
                _betLevel.push_back(pFixBetData->Bet);
        }
    }
    else
    {
        _betLevel = Player::getInstance()->getBetLevel();
    }
    _curBet = _betLevel.at(0);

    _betPanel = _pLayoutGroup->getChildByName<Layout*>("drop_down_list_bet");
    _betPanel->setTouchEnabled(true);

    _betPanel->setVisible(false);
    _betBtn = btns->getChildByName<Button*>("Btn_Bet");
    _betBtn->setTouchEnabled(true);
    _betBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchBet, this));
    _betNowNum = _pLayoutGroup->getChildByName<TextBMFont*>("BMf_bet");

    _betNextBtn = _betPanel->getChildByName<Button*>("Bet_Next");
    _betNextBtn->setTouchEnabled(true);
    _betNextBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchBetAdd, this));
    _betPreBtn = _betPanel->getChildByName<Button*>("Bet_Previous");
    _betPreBtn->setTouchEnabled(true);
    _betPreBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchBetMinus, this));

    _betListView = _betPanel->getChildByName<ListView*>("betBtnList");
    _betListView->setDirection(ui::ScrollView::Direction::VERTICAL);
    _betListView->setScrollBarEnabled(false);
    _betListView->setTouchEnabled(false);
    _betListView->setBounceEnabled(true);

    _betListPos = _betLevel.size() - 3;
    auto layoutBet = _betPanel->getChildByName<Layout*>("layout_bet");
    for (int i = (int)_betLevel.size() - 1; i >= 0; i--)
    {
        auto tempLayout = layoutBet->clone();
        Button* btn = tempLayout->getChildByName<Button*>("bet_btn");
        ImageView* jp = tempLayout->getChildByName<ImageView*>("jp_img");
        ImageView* lock = tempLayout->getChildByName<ImageView*>("lock");
        int bet = _betLevel.at(i);
        bool isUsableBet = Player::getInstance()->isBetUsable(bet);
        btn->setTitleText(StringUtils::toString(bet));
        btn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchSelectBet, this, i));
        if ((_isFixedBet && !UISlotUtils::isFixedBetUsable(_slotID, bet)) || (!_isFixedBet && !isUsableBet))
        {
            //btn->setTouchEnabled(false);
            lock->setVisible(true);
        }
        else
        {
            //lock->setVisible(false);
        }
        jp->setVisible(false);
        
        _betListView->pushBackCustomItem(tempLayout);
    }
    _betListView->jumpToBottom();
    layoutBet->setVisible(false);

    // free spin
    _freeSpinPanel = _pLayoutGroup->getChildByName<Layout*>("FreeSpinCounter");
    _remainFreeSpin = _freeSpinPanel->getChildByName<TextBMFont*>("freeSpinCount");
    setFreeSpinVisible(false);

    // auto
    _autoSpinBtn = btns->getChildByName<Button*>("Btn_Auto");
    _autoSpinBtn->setTouchEnabled(true);
    _autoSpinBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchAutoSpin, this));
    _autoPanel = _pLayoutGroup->getChildByName<Layout*>("drop_down_list_auto");
    _autoPanel->setTouchEnabled(true);
    _autoPanel->setVisible(false);

    for (int i = 0; i < AUTOSPINNUM; ++i)
    {
        _autoSpinTimeBtn[i] = _autoPanel->getChildByName<Button*>(StringUtils::format("btn_autoNum_%d", i + 1));
        _autoSpinTimeBtn[i]->setTouchEnabled(true);
        _autoSpinTimeBtn[i]->setSoundID(EFF_NONE);
        _autoSpinTimeBtn[i]->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchAutoSpinTimes, this, i + 1));
        _autoSpinTimeBtn[i]->setTitleText(safeGetStringData(681 + i));
    }

    _autoSpinUnlimited = _autoPanel->getChildByName<Button*>("btn_autoNum_4");
    _autoSpinUnlimited->setTouchEnabled(true);
    _autoSpinUnlimited->setSoundID(EFF_NONE);
    _autoSpinUnlimited->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchAutoSpinUnlimited, this));
    _autoSpinUnlimited->setTitleText(safeGetStringData(684));

    _autoSpinSetting = _autoPanel->getChildByName<Button*>("btn_autoNum_5");
    _autoSpinSetting->setTouchEnabled(true);
    _autoSpinSetting->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatra::touchAutoSpinSetting, this));
    _autoSpinSetting->setTitleText(safeGetStringData(685));

    _autoSpinCountBtn = btns->getChildByName<Button*>("Btn_Auto_disable");
    _autoSpinCountBtn->setVisible(false);
    _autoSpinCountBM = _autoSpinCountBtn->getChildByName<TextBMFont*>("auto_disable_BMf");

    // setup reel
    UIReel* pReel;
    std::vector<SymbolSpec> symbolSpecs;
    const Size SYMBOLSIZE = Size(165, 165);

    //for (int i = _pSlotData->SymbolInfoBeginID; i < _pSlotData->SymbolInfoEndID + 1; ++i)
    //{
    //    SymbolInfoData* pSybolData = ExternalTable<SymbolInfoData>::getRecordByID(i);
    //    if (!pSybolData)
    //        continue;

    //    // TEST JP
    //    if (i == _pSlotData->SymbolInfoBeginID)
    //    {
    //        auto filename = StringUtils::format(GOCIMAGE(%s), pSybolData->SpriteFile.c_str());
    //        std::string maskName = "";
    //        if (pSybolData->AnimMaskFile != "")
    //            maskName = "UISlot_Cleopatra/Spine/" + pSybolData->AnimMaskFile;
    //        symbolSpecs.push_back(SymbolSpec(filename,
    //            SPRITE,
    //            Vec2(50, 0), Size(216, 165),
    //            "",
    //            "",
    //            NONEBLUR,
    //            filename));
    //        continue;
    //    }
    //    auto filename = StringUtils::format(GOCIMAGE(%s), pSybolData->SpriteFile.c_str());
    //    Sprite* pSprite = Sprite::createWithSpriteFrameName(filename);
    //    Size defaultSize = Size(165, 165);
    //    Size symbolSize = pSprite->getContentSize();
    //    Vec2 oriVec = pSprite->getContentSize() - defaultSize;
    //    std::string maskName = "";
    //    if (pSybolData->AnimMaskFile != "")
    //        maskName = "UISlot_Cleopatra/Spine/" + pSybolData->AnimMaskFile;
    //    symbolSpecs.push_back(SymbolSpec(filename,
    //        SPRITE,
    //        oriVec, pSprite->getContentSize(),
    //        "UISlot_Cleopatra/Spine/" + pSybolData->AnimFile,
    //        maskName,
    //        NONEBLUR,
    //        filename));
    //}
 
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_JACKPOT, "cleopatra_sym_st_jp.png", SPRITEFRAME, Vec2(50, 0), Size(216, 165), "", "", NONEBLUR, "", 0));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_BONUS, "cleopatra_sym_st_bonus.png", SPRITEFRAME, Vec2(9, 10), Size(174, 208), "UISlot_Cleopatra/Spine/cleopatra_sym_sp_bonus", "", NONEBLUR, "", 3201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_WILD, "cleopatra_sym_st_wild.png", SPRITEFRAME, Vec2(0, 12), Size(165, 207), "UISlot_Cleopatra/Spine/cleopatra_sym_sp_wild", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_wild", NONEBLUR, "", 0));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_SCATTER, "cleopatra_sym_st_scatter.png", SPRITEFRAME, Vec2(10, 12), Size(175, 193), "UISlot_Cleopatra/Spine/cleopatra_sym_sp_scatter", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_scatter", NONEBLUR, "", 3202));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_p1.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_p1", "", NONEBLUR, "", 3203));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_p2.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_p2", "", NONEBLUR, "", 3203));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_p3.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_p3", "", NONEBLUR, "", 3203));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_p4.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_p4", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_p4", NONEBLUR, "", 3203));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_a.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_a", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_a", NONEBLUR, "", 3204));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_k.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_k", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_k", NONEBLUR, "", 3204));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_q.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_q", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_q", NONEBLUR, "", 3204));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_j.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_j", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_j", NONEBLUR, "", 3204));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "cleopatra_sym_st_ten.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_Cleopatra/Spine/cleopatra_sym_sp_ten", "UISlot_Cleopatra/Spine/cleopatra_sym_ma_ten", NONEBLUR, "", 3204));

    _symbolBorad = _pLayoutGroup->getChildByName<Layout*>("symbol_board");
    _symbolBorad->setClippingType(Layout::ClippingType::SCISSOR);

    for (int i = 0; i < _reelCount; ++i)
    {
        Layout* pReelDummy = static_cast<Layout*>(_symbolBorad->getChildByName(StringUtils::format("reel_raw%d", i + 1)));
        CCASSERT(pReelDummy, "");

        pReel = UIReel::createWithReelView(Size(165, 520), 3, symbolSpecs, SYMBOLSIZE);
        if (pReel)
        {
            pReel->setReelOrder(i + 1);
            pReel->setOnSingleReelEnterMiddleExpectCB(CC_CALLBACK_1(UISlotCleopatra::onReelEnterExpectCB, this));
            pReel->setOnSingleReelEnterEndCB(CC_CALLBACK_1(UISlotCleopatra::onReelEnterEndCB, this));
            pReel->setOnSingleReelPendingStopCB(CC_CALLBACK_1(UISlotCleopatra::onReelPendingStopCB, this));
            pReel->setOnSingleReelStopCB(CC_CALLBACK_1(UISlotCleopatra::onReelStopCB, this));
            pReel->setOnSingleReelTriggerStopSoundCB(CC_CALLBACK_1(UISlotCleopatra::onReelTriggerStopSoundCB, this));
            pReelDummy->addChild(pReel);
            _reels.push_back(pReel);
            _reelLayouts.push_back(pReelDummy);
        }
    }

    auto expectLayout = _pLayoutGroup->getChildByName<Layout*>("layout_expect");
    for (int i = 0; i < _reelCount; ++i)
    {
        ExpectBGGroup group;
        group.pScatterBg = expectLayout->getChildByName<ImageView*>(StringUtils::format("expect_scatter_%d", i + 1));
        group.pBonusBg = expectLayout->getChildByName<ImageView*>(StringUtils::format("expect_bonus_%d", i + 1));
        group.pHybridBg = expectLayout->getChildByName<ImageView*>(StringUtils::format("expect_hybrid_%d", i + 1));
        group.pParticles = expectLayout->getChildByName<Layout*>(StringUtils::format("particle_layout_%d", i + 1));
        group.pParticles->setClippingType(Layout::ClippingType::SCISSOR);
        _reelExpect.push_back(group);
    }
    
    _isExpectBgSoundPlayed.clear();
    for (int i = 0; i < _reelCount; i++)
        _isExpectBgSoundPlayed.push_back(false);
    _isMiddleExpectSoundPlayed.clear();
    for (int i = 0; i < _reelCount; i++)
        _isMiddleExpectSoundPlayed.push_back(false);
    _middleExpectSoundHandle = 0;

    // particle
    cocos2d::Vector<Texture2D*> list;
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin01.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin02.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin03.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin04.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin05.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin06.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin07.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin08.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin09.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin10.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin11.png"));
    list.pushBack(Director::getInstance()->getTextureCache()->addImage("coin12.png"));

    _coinParticle = CustomParticleSystemGroup::create("normalHit.plist", list, 1);
    _coinParticle->setSpeed(5, 2);
    _coinParticle->setFrameVariance(12);
    _coinParticle->stopSystem();
    auto coinParticleDummy = _pLayoutGroup->getChildByName<Layout*>("coin_particle_panel");
    coinParticleDummy->addChild(_coinParticle);

    // init setting
    initPayLine();

    setCurWin(0);
    setJackpotScore(0);
    setMaxLineAndBet();

    return true;
}

void UISlotCleopatra::stopMachine()
{
    _uiSlotCleopatra->stopAllActions();
    for (int i = 0; i < _reelCount; i++)
    {
        UIReel* reel = _reels.at(i);
        if (!reel)
            continue;
        reel->stopInfiniteSuddenly();
    }

    setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_IDLE);
    setAutoSpinStatus(EAutoSpinStatus::AUTOSPINSTATUS_OFF);
    setFreeSpinStatus(EFreeSpinStatus::FREESPINSTATUS_OFF);
}

void UISlotCleopatra::startSpin()
{
    for (int i = 0; i < _reelCount; i++)
    {
        UIReel* reel = _reels.at(i);
        if (!reel)
            continue;

        reel->spin(2.0f, -9, SPINSTYLE_INFINITE_4);
    }

    AudioManager::getInstance()->stopEff(_spinSoundHandle);
    _spinSoundHandle = AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinSoundID, false);
}

void UISlotCleopatra::stopSpinWithResult(std::vector<std::vector<int>> spinResult)
{
    if (spinResult.size() != _reelCount)
        return;

    Vector<FiniteTimeAction*> arrayOfActions;
    for (int i = 0; i < _reelCount; i++)
    {
        UIReel* reel = _reels.at(i);
        if (!reel)
            continue;
        reel->setSpinResult(spinResult.at(i));
        
        //arrayOfActions.pushBack(DelayTime::create(0.5f));
        //arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, reel)));
    }

    arrayOfActions.pushBack(DelayTime::create(0.5f));
    arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, (*_reels.begin()))));

    auto actionsSeq = Sequence::create(arrayOfActions);
    actionsSeq->setTag(ETAG_STOPREEL);
    _uiSlotCleopatra->runAction(actionsSeq);

    if (isAutoSpining() && Player::getInstance()->getAutoSpinSetting().StopWinTimes)
    {
        if (_winRcv.win_coin >= _curBet * totalPayLineCount * Player::getInstance()->getAutoSpinSetting().WinTimes)
            setAutoSpinStatus(EAutoSpinStatus::AUTOSPINSTATUS_OFF);
    }

    if (isAutoSpining() && Player::getInstance()->getAutoSpinSetting().StopSuddenly)
        stopReel();
    else
        setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_STOPINGREEL);
}

void UISlotCleopatra::setCurWin(uint64 win)
{
    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(1.0f, 0, win);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    _curWinScore->runAction(pAction);

    if (win <= 0)
    {
        return;
    }

    _winMoneyAmin->stopAllActions();

    CountTextBMFontToUint64Num* pHUDWinAction = CountTextBMFontToUint64Num::create(1.0f, 0, win);
    pHUDWinAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    _winMoneyAmin->runAction(pHUDWinAction);

    //放大
    Vector<FiniteTimeAction*> arrayOfActions;
    arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setOpacity, _winMoneyAmin, 255)));
    arrayOfActions.pushBack(CallFunc::create(
        [this](){ _winMoneyAmin->setScale(0.55f); }
    ));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 1.0f)));
    arrayOfActions.pushBack(DelayTime::create(1.0f));
    arrayOfActions.pushBack(FadeOut::create(0.5));

    auto scaleSeq = Sequence::create(arrayOfActions);
    _winMoneyAmin->runAction(scaleSeq);

    if (win > 0)
        _coinParticle->resetSystem();
}

void UISlotCleopatra::setBet(int bet)
{
    _betNowNum->setString(GetMoneyFormat(bet));
    _totalBet->setString(GetMoneyFormat(bet * totalPayLineCount));
}

void UISlotCleopatra::setJPEnable(bool enable)
{
    _jackpotPanel->setVisible(enable);
    for (auto reel : _reels)
        reel->setJPEnable(enable);
    _jpLimit = enable ? _pSlotData->JackpotBetLimit : 0;
}

void UISlotCleopatra::setJackpotScore(uint64 count)
{
    int jackpotActionTag = 1;
    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(60.0f, _jackScore, count);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    pAction->setTag(jackpotActionTag);
    _jackpotBM->stopAllActions();
    _jackpotBM->runAction(pAction);

    _jackScore = count;
}

void UISlotCleopatra::updateJackpotHint()
{
    bool overLimit = _curBet >= _jpLimit;
    _jackpot_hint_yes->setVisible(overLimit);
    _jackpot_hint_no->setVisible(!overLimit);
}

void UISlotCleopatra::setBetPanel(bool onOff)
{
    _isBetOn = onOff;
    _betPanel->setVisible(_isBetOn);
}

void UISlotCleopatra::setAutoPanel(bool onOff)
{
    _isAutoPanelOn = onOff;
    _autoPanel->setVisible(_isAutoPanelOn);
}

void UISlotCleopatra::setBetListPos(int pos)
{
    int posMin = 0, posMax = _betLevel.size() - 3;
    if (pos < posMin)
        pos = posMin;
    if (pos > posMax)
        pos = posMax;

    Color3B darkColor = Color3B(128, 128, 128);;
    Color3B whiteColor = Color3B(255, 255, 255);

    if (pos == posMin)
    {
        _betNextBtn->setEnabled(false);
        _betNextBtn->setColor(darkColor);

        _betPreBtn->setEnabled(true);
        _betPreBtn->setColor(whiteColor);
    }
    else if (pos == posMax)
    {
        _betNextBtn->setEnabled(true);
        _betNextBtn->setColor(whiteColor);

        _betPreBtn->setEnabled(false);
        _betPreBtn->setColor(darkColor);
    }
    else
    {
        _betNextBtn->setColor(whiteColor);
        _betNextBtn->setEnabled(true);

        _betPreBtn->setColor(whiteColor);
        _betPreBtn->setEnabled(true);
    }

    _betListPos = pos;
    _betListView->scrollToItem(pos, Vec2::ANCHOR_TOP_LEFT, Vec2::ANCHOR_TOP_LEFT);
}

void UISlotCleopatra::setMaxLineAndBet()
{
    if (_isFixedBet && UISlotUtils::getUsableFixedBetLevel(_slotID).size())
        _curBet = UISlotUtils::getUsableFixedBetLevel(_slotID).at(UISlotUtils::getUsableFixedBetLevel(_slotID).size() - 1);
    else if (!_isFixedBet && Player::getInstance()->getUsableBetLevel().size())
        _curBet = Player::getInstance()->getUsableBetLevel().at(Player::getInstance()->getUsableBetLevel().size() - 1);
    else
        _curBet = Player::getInstance()->getUsableBetLevel().at(0);

    setBet(_curBet);
}

void UISlotCleopatra::updateBet()
{
    Vector<Widget*> items = _betListView->getItems();

    int betSize = (int)_betLevel.size();
    int itemSize = (int)items.size();
    if (betSize != itemSize)
        return;

    int pos = betSize - 1;
    for (int i = 0; i < itemSize; ++i)
    {
        auto tempLayout = items.at(i);
        Button* btn = tempLayout->getChildByName<Button*>("bet_btn");
        ImageView* jp = tempLayout->getChildByName<ImageView*>("jp_img");
        ImageView* lock = tempLayout->getChildByName<ImageView*>("lock");
        int bet = _betLevel.at(betSize - 1 - i);
        bool isUsableBet = Player::getInstance()->isBetUsable(bet);
        btn->setTitleText(GetMoneyFormat(bet));

        if (_curBet == bet)
        {
            pos = i - 1;
        }

        if ((_isFixedBet && !UISlotUtils::isFixedBetUsable(_slotID, bet)) || (!_isFixedBet && !isUsableBet))
        {
            //btn->setTouchEnabled(false);
            lock->setVisible(true);
        }
        else
        {
            //btn->setTouchEnabled(true);
            lock->setVisible(false);
        }
        jp->setVisible(bet >= _jpLimit && _jackpotPanel->isVisible());
    }

    setBetListPos(pos);
    //_betListView->jumpToBottom();
}

void UISlotCleopatra::stopReel()
{
    _uiSlotCleopatra->stopAllActions();
    for (int i = 0; i < _reelCount; i++)
    {
        UIReel* reel = _reels.at(i);
        if (!reel)
            continue;
        reel->stopInfiniteSuddenly();
    }

    setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_STOPSUDDENDLY);
}

bool UISlotCleopatra::isHit()
{
    return (
        _paylineRcv.size() > 0 ||  
        _overallRcv.overall > 0 ||  
        _jpRcv.jackpot > 0
        );
}

bool UISlotCleopatra::isHitScatter()
{
    return (_freeSpinRcv.action == SpinResponse_FreeSpin::Action::NORMAL && _freeSpinRcv.remain_count > 0) ||
            _freeSpinRcv.action == SpinResponse_FreeSpin::Action::ADD;
}

bool UISlotCleopatra::isHitBonus()
{
    return _bonusRcv.gameID > 0 || _bonuswildRcv.gameID > 0 || _overallRcv.bonus_gameID > 0;
}

void UISlotCleopatra::setSpinBtnStatus(ESlotButtonStatus status)
{
    _spinBtnStatus = status;

    if (_autoSpinStatus == EAutoSpinStatus::AUTOSPINSTATUS_ON)
        return;
    switch (status)
    {
    case SLOTBTNSTATUS_IDLE:
        if (_freeSpinStatus == EFreeSpinStatus::FREESPINSTATUS_ON || _autoSpinStatus == EAutoSpinStatus::AUTOSPINSTATUS_ON)
            break;
        _spinImg->setVisible(true);
        _stopImg->setVisible(false);
        _spinImgGray->setVisible(false);
        _stopImgGray->setVisible(false);

        _spinBtn->setTouchEnabled(true);
        _maxBetBtn->setTouchEnabled(true);
        _autoSpinBtn->setTouchEnabled(true);
        _betBtn->setTouchEnabled(true);

        _spinBtn->setEnabled(true);
        _maxBetBtn->setEnabled(true);
        _autoSpinBtn->setEnabled(true);
        _betBtn->setEnabled(true);
        break;
    case SLOTBTNSTATUS_WAITINGRESPONSE:
        _spinBtn->setTouchEnabled(false);
        _maxBetBtn->setTouchEnabled(false);
        _autoSpinBtn->setTouchEnabled(false);
        _betBtn->setTouchEnabled(false);

        _spinBtn->setEnabled(false);
        _maxBetBtn->setEnabled(false);
        _autoSpinBtn->setEnabled(false);
        _betBtn->setEnabled(false);

        if (_freeSpinStatus == EFreeSpinStatus::FREESPINSTATUS_ON || _autoSpinStatus == EAutoSpinStatus::AUTOSPINSTATUS_ON)
            break;
        _spinImg->setVisible(false);
        _stopImg->setVisible(false);
        _spinImgGray->setVisible(true);
        _stopImgGray->setVisible(false);
        break;
    case SLOTBTNSTATUS_STOPINGREEL:
        _spinImg->setVisible(false);
        _stopImg->setVisible(true);
        _spinImgGray->setVisible(false);
        _stopImgGray->setVisible(false);

        _spinBtn->setTouchEnabled(true);
        _spinBtn->setEnabled(true);
        break;
    case SLOTBTNSTATUS_STOPSUDDENDLY:
        //if (_autoSpinStatus == EAutoSpinStatus::AUTOSPINSTATUS_ON)
        //{
        //    _spinImg->setVisible(false);
        //    _stopImg->setVisible(true);
        //    _spinBtn->setTouchEnabled(true);
        //    _spinBtn->setEnabled(true);
        //    break;
        //}
        _spinImg->setVisible(false);
        _stopImg->setVisible(false);
        _spinImgGray->setVisible(false);
        _stopImgGray->setVisible(true);

        _spinBtn->setTouchEnabled(false);
        _spinBtn->setEnabled(false);
        break;
    case SLOTBTNSTATUS_ONCELINEANDSTOREUI:
        _spinImg->setVisible(false);
        _stopImg->setVisible(false);
        _spinImgGray->setVisible(true);
        _stopImgGray->setVisible(false);

        _spinBtn->setEnabled(false);
        _maxBetBtn->setEnabled(false);
        _autoSpinBtn->setEnabled(false);
        _betBtn->setEnabled(false);
        break;
    default:
        break;
    }
}

void UISlotCleopatra::setAutoSpinStatus(EAutoSpinStatus status)
{
    _autoSpinStatus = status;

    switch (status)
    {
    case AUTOSPINSTATUS_ON:
        setAutoSpinBtnStatus(true);

        _spinImg->setVisible(false);
        _stopImg->setVisible(true);
        _spinImgGray->setVisible(false);
        _stopImgGray->setVisible(false);

        _spinBtn->setTouchEnabled(true);
        _maxBetBtn->setTouchEnabled(false);
        _autoSpinBtn->setTouchEnabled(false);
        _betBtn->setTouchEnabled(false);

        _spinBtn->setEnabled(true);
        _maxBetBtn->setEnabled(false);
        _autoSpinBtn->setEnabled(false);
        _betBtn->setEnabled(false);

        break;
    case AUTOSPINSTATUS_OFF:
        setAutoSpinBtnStatus(false);
        setSpinBtnStatus(_spinBtnStatus);
        break;
    default:
        break;
    }
}

void UISlotCleopatra::setFreeSpinStatus(EFreeSpinStatus status)
{
    _freeSpinStatus = status;

    switch (status)
    {
    case FREESPINSTATUS_ON:
        setFreeSpinVisible(true);
        break;
    case FREESPINSTATUS_OFF:
        setFreeSpinVisible(false);
        setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_IDLE);
        break;
    default:
        break;
    }
}

bool UISlotCleopatra::isAutoSpining()
{
    return _isAutoSpinUnlimited || _remainAutoCount > 0;
}

void UISlotCleopatra::minusAutoConut()
{
    --_remainAutoCount;
}


bool UISlotCleopatra::isUIOpen()
{
    auto cBigWin = UIController::getInstance()->getController<UIBigWin*>(EUITAG::EUITAG_BIGWIN);
    if (cBigWin->isOpen())
        return true;

    auto cFiveOfAKind = UIController::getInstance()->getController<UIFiveOfAKind*>(EUITAG::EUITAG_FIVEOFAKIND);
    if (cFiveOfAKind->isOpen())
        return true;

    auto cJackpot = UIController::getInstance()->getController<UIJackpot*>(EUITAG::EUITAG_JACKPOT);
    if (cJackpot->isOpen())
        return true;

    auto cBonusGame = UIController::getInstance()->getController<UISlotCleopatraBonusGame*>(EUITAG::EUITAG_CLEOPATRA_BONUSGAME);
    if (cBonusGame->isOpen())
        return true;

    auto cFreeSpinStart = UIController::getInstance()->getController<UISlotCleopatraFreeSpinStart*>(EUITAG::EUITAG_CLEOPATRA_FREESPINSTART);
    if (cFreeSpinStart->isOpen())
        return true;

    auto cFreeSpinResult = UIController::getInstance()->getController<UISlotCleopatraFreeSpinResult*>(EUITAG::EUITAG_CLEOPATRA_FREESPINRESULT);
    if (cFreeSpinResult->isOpen())
        return true;

    auto cLvUp = UIController::getInstance()->getController<UILevelUp*>(EUITAG::EUITAG_LEVELUP);
    if (cLvUp->isOpen())
        return true;

    if (!UIController::getInstance()->isStoreUIEmpty())
        return true;

    return false;
}

void UISlotCleopatra::setAutoSpinRemainCount(int count)
{
    if (_isAutoSpinUnlimited)
    {
        _autoSpinCountBM->setString("~");
    }
    else
    {
        _remainAutoCount = count;
        _autoSpinCountBM->setString(StringUtils::toString(count));
    }
}

void UISlotCleopatra::setAutoSpinBtnStatus(bool start)
{
    if (!start)
    {
        _remainAutoCount = 0;
        setAutoSpinRemainCount(0);
        _isAutoSpinUnlimited = false;
    }
    else
    {
        setAutoSpinRemainCount(_remainAutoCount);
    }

    _autoSpinBtn->setVisible(!start);
    _autoSpinCountBtn->setVisible(start);
}

void UISlotCleopatra::initPayLine()
{
    _payLineAnim = static_cast<ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("UISlot_Cleopatra/UISlot_Cleopatra_Payline.ExportJson"));

    setTraversalCascadeOpacityEnabled(_payLineAnim, true);
    for (auto pNode : _payLineAnim->getChildren())
    {
        if (pNode)
        {
            pNode->setVisible(true);
            for (auto pLine : pNode->getChildren())
            {
                if (pLine)
                    pLine->setOpacity(0.0f);
            }
        }
    }

    _payLineAnim->setPosition(Vec2(-221.0f, -172.0f));
    _symbolBorad->addChild(_payLineAnim, 100);
}

int UISlotCleopatra::getPayLineOrder(int payLineID)
{
    if (payLineID < _payLineBeginID || payLineID > _payLineEndID)
        return 0;
    else
        return payLineID - _payLineBeginID + 1;
}

void UISlotCleopatra::playPayLine(int payLineID, bool play, bool Loop)
{
    if (!_payLineAnim)
        return;

    int payLineOrder = getPayLineOrder(payLineID);
    if (!payLineOrder)
        return;

    std::string lineAnimName = StringUtils::format("anim_line%02d", payLineOrder);
    auto lineAnim = cocostudio::ActionManagerEx::getInstance()->getActionByName("UISlot_Cleopatra/UISlot_Cleopatra_Payline.ExportJson", lineAnimName.c_str());
    if (!lineAnim)
        return;

    lineAnim->setLoop(Loop);
    if (play)
        lineAnim->play();
    else
        lineAnim->stop();
}

void UISlotCleopatra::playAllPayLineOnce(std::vector<SpinResponse_PayLine>& pay_line, SpinResponse_Win& winRcv, bool specialWin)
{
    if (pay_line.empty())
        return;

    for (auto lines : pay_line)
        playPayLine(lines.lineID, true, false);

    Vector<FiniteTimeAction*> arrayOfActions;
    arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::playAllPayLineSymbol, this, pay_line)));
    arrayOfActions.pushBack(DelayTime::create(LINEANIMTIME));
    arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::clearPayLineSymbol, this)));
    auto actionsSeq = Sequence::create(arrayOfActions);
    actionsSeq->setTag(ETAG_PLAYALLLINE);
    _uiSlotCleopatra->runAction(actionsSeq);

    //場景慶祝動畫
    performSceneAnim((ECleopatraSceneAnim)cocos2d::RandomHelper::random_int((int)ECleopatraSceneAnim::SCENEANIM_CLEOPATRAWIN01, (int)ECleopatraSceneAnim::SCENEANIM_CLEOPATRAWIN02));

    if (specialWin)
         AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
    else
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->WinSoundID, false);
}

void UISlotCleopatra::repeatSinglePayLines(std::vector<SpinResponse_PayLine>& pay_line)
{
    if (pay_line.empty())
        return;

    Vector<FiniteTimeAction*> arrayOfActions;
    //重覆輪播每條payline
    for (auto line : pay_line)
    {
        //playPayLine(line.lineID, true, true);
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::playPayLine, this, line.lineID, true, false)));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::playSinglePayLineSymbol, this, line)));
        arrayOfActions.pushBack(DelayTime::create(LINEANIMTIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::clearPayLineSymbol, this)));
    }

    auto actionsSeq = Sequence::create(arrayOfActions);
    actionsSeq->setTag(ETAG_REPEATSINGLELINE);

    //自動spin中只播一次
    if (isAutoSpining())
    {
        //改為不播單條
        //actionsSeq->setTag(TAG_REPEATSINGLEPAYLINE);
        //runAction(actionsSeq);
    }
    //手動spin重覆播放
    else
    {
        auto rep = RepeatForever::create(actionsSeq);
        _uiSlotCleopatra->runAction(rep);
    }
}

void UISlotCleopatra::stopPlayPayLine(int payLineID)
{
    playPayLine(payLineID, false, false);
}

void UISlotCleopatra::stopPlayAllPayLine()
{
    for (int i = _payLineBeginID; i <= _payLineEndID; i++)
        stopPlayPayLine(i);
    for (auto pNode : _payLineAnim->getChildren())
    {
        if (pNode)
        {
            for (auto pImageView : pNode->getChildren())
                pImageView->setOpacity(0.0f);
        }
    }
}

void UISlotCleopatra::playAllPayLineSymbol(std::vector<SpinResponse_PayLine>& payLines)
{
    std::map<Vec2, Vec2> focusPos;
    for (auto line : payLines)
    {
        if (line.lineID > _payLineEndID)
            continue;
        for (auto hitCoord : line.hitCoord)
        {
            Vec2 curPos = hitCoord;// getSymbolCoord(line.lineID, order);
            if (curPos.isZero())
                continue;
            if (focusPos.find(curPos) == focusPos.end())
                focusPos[curPos] = curPos;
        }
    }

    for (auto itr : focusPos)
    {
        Vec2 curPos = itr.second;
       
        int reelIndex = curPos.x - 1;
        UIReel* pReel = _reels.at(reelIndex);
        if (!pReel)
            return;

        cocos2d::Vector<UISymbolBase*>& symbols = pReel->getSymbols();
        int symbolIndex = symbols.size() - 1 - curPos.y;
        UISymbolBase* pSymbol = symbols.at(symbolIndex); // 只有一個輪,index都取1 
        if (!pSymbol)
            return;

        SymbolSpec outSymbolSpec = pSymbol->getSymbolSpec();
        pSymbol->setVisible(false);

        Vec2 symbolPos = pSymbol->getPosition();

        const Size SYMBOLSIZE = Size(165, 165);
        if (outSymbolSpec._animfile.empty())
        {
            Sprite* pSprite = Sprite::createWithSpriteFrameName(outSymbolSpec._filename);
            pSprite->setPosition(symbolPos - outSymbolSpec._anchorInPixel);
            pSprite->setAnchorPoint(Vec2::ZERO);
            _reelLayouts.at(reelIndex)->addChild(pSprite);
            ProcedureFx::moveParticleAlongRect(Rect(symbolPos, SYMBOLSIZE), _reelLayouts.at(reelIndex), 0);

            _symbolAnimations.push_back(pSprite);
        }
        //symbol have animation file
        else
        {
            CustomSpAnim* pSpine = CustomSpAnim::createWithFile(outSymbolSpec._animfile, outSymbolSpec._maskfile);
            pSpine->playAnimationRoundInTime("win", LINEANIMTIME);
            pSpine->setPosition(symbolPos);
            _reelLayouts.at(reelIndex)->addChild(pSpine);
            ProcedureFx::moveParticleAlongRect(Rect(symbolPos, SYMBOLSIZE), _reelLayouts.at(reelIndex), 0);

            _symbolAnimations.push_back(pSpine);
        }
    }
}

void UISlotCleopatra::playSinglePayLineSymbol(SpinResponse_PayLine& payLine)
{
    for (auto hitCoord : payLine.hitCoord)
    {
        Vec2 curPos = hitCoord;

        int reelIndex = curPos.x - 1;
        UIReel* pReel = _reels.at(reelIndex);
        if (!pReel)
            return;

        cocos2d::Vector<UISymbolBase*>& symbols = pReel->getSymbols();
        int symbolIndex = symbols.size() - 1 - curPos.y;
        UISymbolBase* pSymbol = symbols.at(symbolIndex); // 只有一個輪,index都取1 
        if (!pSymbol)
            return;

        SymbolSpec outSymbolSpec = pSymbol->getSymbolSpec();
        pSymbol->setVisible(false);

        Vec2 symbolPos = pSymbol->getPosition();

        const Size SYMBOLSIZE = Size(165, 165);
        if (outSymbolSpec._animfile.empty())
        {
            Sprite* pSprite = Sprite::createWithSpriteFrameName(outSymbolSpec._filename);
            pSprite->setPosition(symbolPos - outSymbolSpec._anchorInPixel);
            pSprite->setAnchorPoint(Vec2::ZERO);
            _reelLayouts.at(reelIndex)->addChild(pSprite);
            ProcedureFx::moveParticleAlongRect(Rect(symbolPos, SYMBOLSIZE), _reelLayouts.at(reelIndex), 0);

            _symbolAnimations.push_back(pSprite);
        }
        //symbol have animation file
        else
        {
            CustomSpAnim* pSpine = CustomSpAnim::createWithFile(outSymbolSpec._animfile, outSymbolSpec._maskfile);
            pSpine->playAnimationRoundInTime("win", LINEANIMTIME);
            pSpine->setPosition(symbolPos);
            _reelLayouts.at(reelIndex)->addChild(pSpine);
            ProcedureFx::moveParticleAlongRect(Rect(symbolPos, SYMBOLSIZE), _reelLayouts.at(reelIndex), 0);

            _symbolAnimations.push_back(pSpine);
        }
    }
    int lineSymbolID = payLine.symbolID;
    SymbolInfoData* pSymbolData = ExternalTable<SymbolInfoData>::getRecordByID(lineSymbolID);
    if (!isPerformSpinResult(RESULTTYPE_SYMBOL_SOUND) && pSymbolData->SoundID)
        AudioManager::getInstance()->playEffectWithFileID(pSymbolData->SoundID, false);
}

void UISlotCleopatra::clearPayLineSymbol()
{
    if (_symbolAnimations.empty())
        return;

    for (int i = 0; i < (int)_symbolAnimations.size(); ++i)
    {
        _symbolAnimations.at(i)->removeFromParentAndCleanup(true);
    }

    _symbolAnimations.clear();

    visibleAllSymbol();
}

void UISlotCleopatra::visibleAllSymbol()
{
    for (auto pReel : _reels)
    {
        if (!pReel)
            continue;
        for (auto pSymobl : pReel->getSymbols())
        {
            if (!pSymobl)
                continue;
            pSymobl->setVisible(true);
        }
    }
}

void UISlotCleopatra::setFreeSpinVisible(bool visible)
{
    _freeSpinPanel->setVisible(visible);
}

void UISlotCleopatra::setFreeSpinCount(int count)
{
    _remainFreeSpinCount = count;
    _remainFreeSpin->setString(StringUtils::toString(count));
}

void UISlotCleopatra::addFreeSpinCount(int addCount)
{
    CountTextBMFontToNum* pAction = CountTextBMFontToNum::create(1.5f, _remainFreeSpinCount - addCount + 1, _remainFreeSpinCount);
    pAction->setFormat(CountTextBMFontToNum::FORMAT_NONE);
    _remainFreeSpin->runAction(pAction);
}

bool UISlotCleopatra::isFreeSpining()
{
    return _remainFreeSpinCount > 0;
}

void UISlotCleopatra::minusFreeSpinCount()
{
    --_remainFreeSpinCount;
}

void UISlotCleopatra::performSpinResult(std::vector<SpinResponse_PayLine>& pay_lineRcv,
    SpinResponse_JP& jpRcv,
    SpinResponse_Win& winRcv,
    SpinResponse_FreeSpin& freeSpinRcv,
    SpinResponse_Bonus& bonusRcv,
    SpinResponse_Profile& profileRcv,
    SpinResponse_LevelUp& levelupRcv)
{
    bool bStoreUI = false;
    bool bSpecialWin = false;

    //UIJackpot
    if (jpRcv.jackpot > 0)
    {
        auto cJackpot = UIController::getInstance()->getController<UIJackpot*>(EUITAG::EUITAG_JACKPOT);
        if (cJackpot)
        {
            cJackpot->reset();
            cJackpot->setShowMoney(jpRcv.winnings);
            cJackpot->openUILater();
            bStoreUI = true;
            bSpecialWin = true;
        }
    }

    //BonusGame
    if (bonusRcv.gameID > 0)
    {
        auto bonusGame = UIController::getInstance()->getController<UISlotCleopatraBonusGame*>(EUITAG::EUITAG_CLEOPATRA_BONUSGAME);
        if (bonusGame)
        {
            bonusGame->setGameInfo(bonusRcv.gameID, bonusRcv.bet, isAutoSpining() && !Player::getInstance()->getAutoSpinSetting().StopWinBonus);
            bonusGame->openUILater();
            bStoreUI = true;
            bSpecialWin = true;
        }
    }

    //FreeSpinStart
    bool bFreeSpin = false;
    int freeSpinAddToAutoCount = 0;
    ActionInstant* freeSpinAction = nullptr;
    DelayTime* freeSpinActionDelayTime = DelayTime::create(3.5f);

    if (_freeSpinRcv.action == SpinResponse_FreeSpin::Action::NORMAL && _freeSpinRcv.remain_count > 0)
    {
        // 第一次進FreeSpin
        auto cFreeSpinStart = UIController::getInstance()->getController<UISlotCleopatraFreeSpinStart*>(EUITAG::EUITAG_CLEOPATRA_FREESPINSTART);
        if (cFreeSpinStart)
        {
            cFreeSpinStart->reset();
            cFreeSpinStart->setFreeSpinCounter(freeSpinRcv.remain_count);
            cFreeSpinStart->setAutoCloseUITime(5.0f);
            cFreeSpinStart->openUILater();
            bStoreUI = true;
        }

        setFreeSpinCount(freeSpinRcv.remain_count);
        setAutoSpinRemainCount(_remainAutoCount + freeSpinRcv.remain_count);
        setAutoSpinStatus(AUTOSPINSTATUS_ON);
        setFreeSpinStatus(FREESPINSTATUS_ON);
     }
    else if (_freeSpinRcv.action == SpinResponse_FreeSpin::Action::ADD)
    {       // Get more FreeSpin
        setFreeSpinCount(freeSpinRcv.remain_count);
        setAutoSpinRemainCount(_remainAutoCount + freeSpinRcv.remain_count);
        setAutoSpinStatus(AUTOSPINSTATUS_ON);
        setFreeSpinStatus(FREESPINSTATUS_ON);
    }
    else if (_freeSpinRcv.action == SpinResponse_FreeSpin::Action::INFREESPIN)
    {
        setFreeSpinCount(freeSpinRcv.remain_count);
    }
    else if (_freeSpinRcv.action == SpinResponse_FreeSpin::Action::RESULT)
    {
        setFreeSpinVisible(false);
        auto cFreeSpinResult = UIController::getInstance()->getController<UISlotCleopatraFreeSpinResult*>(EUITAG::EUITAG_CLEOPATRA_FREESPINRESULT);
        if (cFreeSpinResult)
        {
            cFreeSpinResult->reset();
            cFreeSpinResult->setFreeSpinCounter(freeSpinRcv.total_count);
            cFreeSpinResult->setWinMoney(freeSpinRcv.coin_sum);
            if (isAutoSpining() && !Player::getInstance()->getAutoSpinSetting().StopWinBonus)
                cFreeSpinResult->setAutoCloseUITime(5.0f);
            cFreeSpinResult->openUILater();
            bStoreUI = true;
        }
        setFreeSpinCount(freeSpinRcv.remain_count);
        setFreeSpinStatus(FREESPINSTATUS_OFF);
    }

    bool bFiveOrBigWin = false;
    //UIFiveOfAKind
    if (!bSpecialWin && winRcv.fiveOfAKindID>0)
    {
        auto cFiveOFAKind = UIController::getInstance()->getController<UIFiveOfAKind*>(EUITAG::EUITAG_FIVEOFAKIND);
        if (cFiveOFAKind)
        {
            cFiveOFAKind->reset();
            if (isAutoSpining())
                cFiveOFAKind->setAutoCloseUITime(5.0f);
            cFiveOFAKind->setShowSymbol("UISlot_Cleopatra/", winRcv.fiveOfAKindID, Size(165, 165), true);
            cFiveOFAKind->openUILater();
            bStoreUI = true;
        }

        bFiveOrBigWin = true;
    }

    //UIBigWin
    if (!bSpecialWin && winRcv.win_level > WinLevel::WINLV_NORMAL) // add free spin big win
    {
        auto cBigWin = UIController::getInstance()->getController<UIBigWin*>(EUITAG::EUITAG_BIGWIN);
        if (cBigWin)
        {
            cBigWin->reset();
            if (isAutoSpining())
            {
                cBigWin->setAutoCloseUITime(5.0f);
                cBigWin->setShare(false);
            }
            cBigWin->setShowMoney(winRcv.win_coin);
            cBigWin->setSlotWinText(_curWinScore);
            cBigWin->openUILater();
            bStoreUI = true;
        }

        bFiveOrBigWin = true;
    }
    else
        setCurWin(winRcv.win_coin);


    //UILeveUp
    if (_levelupRcv.upgrade > 0)
    {
        auto cLevelUp = UIController::getInstance()->getController<UILevelUp*>(EUITAG::EUITAG_LEVELUP);
        if (cLevelUp)
        {
            cLevelUp->reset();
            if (isAutoSpining())
            {
                cLevelUp->setAutoCloseUITime(5.0f);
                cLevelUp->setShare(false);
            }

            cLevelUp->setReward(levelupRcv.award_money + levelupRcv.award_fbmoney, levelupRcv.award_vip_point);
            cLevelUp->openUILater();
            bStoreUI = true;
        }
    }

    Vector<FiniteTimeAction*> arrayOfActions;
    if (!bStoreUI)
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::setSpinBtnStatus, this, ESlotButtonStatus::SLOTBTNSTATUS_IDLE)));
    else
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::setSpinBtnStatus, this, ESlotButtonStatus::SLOTBTNSTATUS_ONCELINEANDSTOREUI)));

    if (isHit())
    {
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::playAllPayLineOnce, this, pay_lineRcv, winRcv, bSpecialWin || bFiveOrBigWin)));
        arrayOfActions.pushBack(DelayTime::create(LINEANIMTIME));
    }
    else
    {
        arrayOfActions.pushBack(DelayTime::create(1.0f));
    }
    if (bStoreUI)
    {
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIController::openStoredUI, UIController::getInstance())));
    }

    if (isAutoSpining() || isFreeSpining())
    {
        if (!bStoreUI)// &&  !Player::getInstance()->getAutoSpinSetting().StopWinTimes)
        {
            arrayOfActions.pushBack(CallFunc::create(
                [this](){ 
                if (isOpen())
                    AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
            }
            ));

            arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::requestSpin, this)));
        }
        else
        {
            arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::setSpinInUpdate, this, true)));
            //arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::repeatSinglePayLines, this, pay_lineRcv)));
        }
    }
    else
    {
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotCleopatra::repeatSinglePayLines, this, pay_lineRcv)));
    }

    if (freeSpinAction != nullptr)
    {
        arrayOfActions.pushBack(freeSpinAction);
        arrayOfActions.pushBack(freeSpinActionDelayTime);
    }

    if (!isAutoSpining())
    {
        setAutoSpinStatus(EAutoSpinStatus::AUTOSPINSTATUS_OFF);
    }

   
    auto actionsSeq = Sequence::create(arrayOfActions);
    actionsSeq->setTag(ETAG_PERFORMSPINRESULT);
    _uiSlotCleopatra->runAction(actionsSeq);
}

void UISlotCleopatra::stopPerformSpinResult()
{
    _uiSlotCleopatra->stopAllActions();
    stopPlayAllPayLine();
    clearPayLineSymbol();
    stopPerformExpect();

    performSceneAnim(ECleopatraSceneAnim::SCENEANIM_CLEOPATRAIDLE);
}

bool UISlotCleopatra::isPerformSpinResult(EPerformResultType type)
{
    //switch (type)
    //{
    //case RESULTTYPE_NORMAL:
    //    if (getActionByTag(TAG_ONCEALLPAYLINE) ||
    //        getActionByTag(TAG_REPEATSINGLEPAYLINE) ||
    //        getActionByTag(TAG_PERFORMSPINRESULT))
    //        return true;
    //    break;
    //case RESULTTYPE_AUTOSPIN:
    //    if (getActionByTag(TAG_ONCEALLPAYLINE) ||
    //        getActionByTag(TAG_PERFORMSPINRESULT))
    //        return true;
    //case RESULTTYPE_BTNSTATUSIDLE:
    //    if (getActionByTag(TAG_ONCEALLPAYLINE) ||
    //        getActionByTag(TAG_PERFORMSPINRESULT))
    //        return true;
    //    break;
    //case RESULTTYPE_SYMBOL_SOUND:
    //    break;
    //default:
    //    break;
    //}

    auto cJackpot = UIController::getInstance()->getController<UIJackpot*>(EUITAG::EUITAG_JACKPOT);
    if (cJackpot->isOpen())
        return true;

    auto cBigWin = UIController::getInstance()->getController<UIBigWin*>(EUITAG::EUITAG_BIGWIN);
    if (cBigWin->isOpen())
        return true;

    auto cFiveOfAKind = UIController::getInstance()->getController<UIFiveOfAKind*>(EUITAG::EUITAG_FIVEOFAKIND);
    if (cFiveOfAKind->isOpen())
        return true;

    auto cFreeSpinStart = UIController::getInstance()->getController<UISlotCleopatraFreeSpinStart*>(EUITAG::EUITAG_CLEOPATRA_FREESPINSTART);//slot depend
    if (cFreeSpinStart->isOpen())
        return true;

    auto cFreeSpinResult = UIController::getInstance()->getController<UISlotCleopatraFreeSpinResult*>(EUITAG::EUITAG_CLEOPATRA_FREESPINRESULT);//slot depend
    if (cFreeSpinResult->isOpen())
        return true;

    auto cLevelUp = UIController::getInstance()->getController<UILevelUp*>(EUITAG::EUITAG_LEVELUP);
    if (cLevelUp->isOpen())
        return true;

    if (GameLogic::getInstance()->GetState() == E_GameState_StayBonusGame)
        return true;

    return false;
}

void UISlotCleopatra::requestSpin()
{
    if (isScrolling())
        return;

    if (_freeSpinStatus != EFreeSpinStatus::FREESPINSTATUS_ON && !UISlotUtils::checkCoinAndOpenOutOfCoin(CC_CALLBACK_0(UISlotCleopatra::outOfCoinKeepPlayingCB, this), _curBet, totalPayLineCount, false))
    {
        stopPerformSpinResult();
        if (_autoSpinStatus == EAutoSpinStatus::AUTOSPINSTATUS_ON)
        {
            setAutoSpinStatus(EAutoSpinStatus::AUTOSPINSTATUS_OFF);
            setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_IDLE);
        }
        return;
    }

    stopPerformSpinResult();
    setCurWin(0);
    startSpin();
    setSpinBtnStatus(ESlotButtonStatus::SLOTBTNSTATUS_WAITINGRESPONSE);

#ifdef BUILD_DEV
    if (_cheatsCodeList.empty())
        RequestSlotSpin(_curBet, totalPayLineCount);
    else
    {
        SlotIDList slotID = _cheatsCodeList.front();
        _cheatsCodeList.erase(_cheatsCodeList.begin());
        RequestCheatCodes(_curBet, 50, slotID);
    }
#else
    RequestSlotSpin(_curBet, totalPayLineCount);
#endif
}

void UISlotCleopatra::clearAllSpinResultData()
{
    _slotIDListRcv.clear();
    _overallRcv.clear();
    _paylineRcv.clear();
    _jpRcv.clear();
    _winRcv.clear();
    _freeSpinRcv.clear();
    _bonusRcv.clear();
    _bonuswildRcv.clear();
    _profileRcv.clear();
    _levelupRcv.clear();
}

void UISlotCleopatra::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    if (!this->isOpen())
        return;

    EMessageTag tagNum = (EMessageTag)Value(requestTag).asInt();

    if (tagNum == EMT_Slot_Spin || tagNum == EMT_Cheat_Codes)
    {
        if (tagNum == EMT_Cheat_Codes && !isScrolling())
        {
            stopPerformSpinResult();
            startSpin();
        }
            
        int errCode = -1;
        
        clearExpectSoundResult();
        clearAllSpinResultData();

        if (!UISlotUtils::parseResponse(
            _slotID,
            json,
            errCode,
            _slotIDListRcv,
            _overallRcv,
            _paylineRcv,
            _jpRcv,
            _winRcv,
            _freeSpinRcv,
            _bonusRcv,
            _bonuswildRcv,
            _profileRcv,
            _levelupRcv,
            _specialSlotListRcv
            ))
        {
            CCLOG("spin response pasing error");
            return;
        }

        // set jp
        auto info = Player::getInstance()->getSlotMachineInfo(_slotID);
        info.jackpot = _jpRcv.winnings;
        Player::getInstance()->setSlotMachineInfo(_slotID, info);
        setJackpotScore(_jpRcv.winnings);

        stopSpinWithResult(_slotIDListRcv);
    }
}

void UISlotCleopatra::refreshReelExpectBG(bool hitScatter, bool hitBonus, int maxReelOrder, bool isResult)
{
    int maxReelIndex = maxReelOrder - 1;
    if (maxReelIndex < 0 || maxReelIndex >= (int)_slotIDListRcv.size())//slot depend
        return;

    const Color3B PAYLINEBG_DARKCOLOR = Color3B(100, 100, 100);
    const Color4B PAYLINEBG_DARKCOLOR_WITHEXPECTBG = Color4B(255, 255, 255, 80);

    int beingSymbolInfoID = _pSlotData->SymbolInfoBeginID;
    bool haveChanceHybrid = hitScatter && hitBonus;
    bool noChance = !hitScatter && !hitBonus;
    for (int i = 0; i < _reelCount; i++)
    {
        auto& reel = _reels.at(i);

        int performReelOrder = i + 1;
        //不可能中Scatter和Bonus
        if (noChance)
        {
            performReelExpectBG(performReelOrder, EXPECT_NONE);
            if (isResult)
                reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            continue;
        }

        //可能中, 但還未停輪的reel不需背景
        if (i > maxReelIndex)
        {
            performReelExpectBG(performReelOrder, EXPECT_NONE);
            if (isResult)
                reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            continue;
        }

        //可能中, 已停輪的reel判斷需要顯示什麼背景
        bool haveScatter = false;
        bool haveBonus = false;
        if (!isResult)
        {
            for (auto id : _slotIDListRcv.at(i))
            {
                int symbolID = id + beingSymbolInfoID;
                if (symbolID == _scatterID)
                    haveScatter = true;
                else if (symbolID == _bonusID)
                    haveBonus = true;
            }
        }
        else
        {
            for (auto hitCoord : _freeSpinRcv.hitCoord)
            {
                if (hitCoord.x - 1 == i)
                {
                    haveScatter = true;
                    break;
                }
            }
            if (!haveBonus)
            {
                for (auto hitCoord : _bonusRcv.hitCoord)
                {
                    if (hitCoord.x - 1 == i)
                    {
                        haveBonus = true;
                        break;
                    }
                }
            }
            if (!haveBonus)
            {
                for (auto hitCoord : _bonuswildRcv.hitCoord)
                {
                    if (hitCoord.x - 1 == i)
                    {
                        haveBonus = true;
                        break;
                    }
                }
            }
        }

        bool haveHybrid = haveScatter && haveBonus;

        if (haveChanceHybrid)       //可能中Scatter和Bonus
        {
            if (haveHybrid)
            {
                performReelExpectBG(performReelOrder, EXPECT_HYBRID);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else if (haveScatter)
            {
                performReelExpectBG(performReelOrder, EXPECT_SCATTER);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else if (haveBonus)
            {
                performReelExpectBG(performReelOrder, EXPECT_BONUS);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performReelExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            }
            continue;
        }

        if (hitScatter)      //只可能中Scatter
        {
            if (haveScatter)
            {
                performReelExpectBG(performReelOrder, EXPECT_SCATTER);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performReelExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            }

            continue;
        }
        else if (hitBonus)   //只可能中Bonus
        {
            if (haveBonus)
            {
                performReelExpectBG(performReelOrder, EXPECT_BONUS);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performReelExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(Color4B(PAYLINEBG_DARKCOLOR)));
            }

            continue;
        }
    }
}

void UISlotCleopatra::refreshReelExpectBGByChance(int reelOrder)
{
    if (_spinBtnStatus == ESlotButtonStatus::SLOTBTNSTATUS_STOPSUDDENDLY)//快速停輪不表現ExpectBG
        return;

    int scatterCounter(0);
    bool haveChanceScatter = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _scatterID, _minScatterCombo, SymbolInfoData::SYMBOLTYPE_SCATTER, reelOrder, scatterCounter);

    int bonusCounter(0);
    bool haveChanceBonus = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _bonusID, _minBonusCombo, SymbolInfoData::SYMBOLTYPE_BONUS, reelOrder, bonusCounter);

    refreshReelExpectBG(haveChanceScatter, haveChanceBonus, reelOrder, false);
}

void UISlotCleopatra::onReelEnterExpectCB(int reelOrder)
{
    // Reel進入Expect狀態(Reel加速)
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !this->isOpen())//slot depend
        return;

    if (!_isMiddleExpectSoundPlayed.at(reelIndex))
    {
        _isMiddleExpectSoundPlayed.at(reelIndex) = true;
        _middleExpectSoundHandle = AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_MIDDLEEXPECT, false);
    }
}

void UISlotCleopatra::onReelEnterEndCB(int reelOrder)
{
    // Reel進入End圈，決定下一輪加速或停止
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !this->isOpen())//slot depend
        return;

    //加速或停止下一滾輪
    //必須在EnterEndCB處理, 若在PendingStopCB處理, 下一滾輪會多等一圈
    if (reelOrder != _reelCount)
    {
        int scatterCounter(0);
        bool haveChanceScatter = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _scatterID, _minScatterCombo, SymbolInfoData::SYMBOLTYPE_SCATTER, reelOrder, scatterCounter);

        int bonusCounter(0);
        bool haveChanceBonus = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _bonusID, _minBonusCombo, SymbolInfoData::SYMBOLTYPE_BONUS, reelOrder, bonusCounter);


        auto pNextReel = _reels.at(reelIndex + 1);
        //若已停輪或快停輪則不處理 "加速或停止下一滾輪"
        if (pNextReel->getSpinState() == ESPIN_INFINITE_RUNNING || pNextReel->getSpinState() == ESPIN_INFINITE_RUNNING_EXPECT)
        {
            bool expectHitScatter = haveChanceScatter && scatterCounter >= _minScatterCombo - 1;
            bool expectgHitBonus = haveChanceBonus && bonusCounter >= _minBonusCombo - 1;
            //聽牌
            if (expectHitScatter || expectgHitBonus)
            {
                Vector<FiniteTimeAction*> arrayOfActions;
                arrayOfActions.pushBack(DelayTime::create(UIReel::st4_secPerRound));
                arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::beginInfiniteExpect, pNextReel)));
                arrayOfActions.pushBack(DelayTime::create(2.0f));
                arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, pNextReel)));
                Sequence* actionsSeq = Sequence::create(arrayOfActions);
                pNextReel->runAction(actionsSeq);
            }
            else
            {
                //action for delay a frame
                //若沒有延遲一個frame, 全部輪會一起停止, 
                //一個frame足以讓下個reel進入下一圈, 或可設定一個DelayTime
                auto delayTime = DelayTime::create(0.3f);
                CallFunc* pStopNextReelAction = CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, pNextReel));
                Sequence* actionsSeq = Sequence::create(delayTime, pStopNextReelAction, nullptr);
                pNextReel->runAction(actionsSeq);
            }
        }
    }
}

void UISlotCleopatra::onReelPendingStopCB(int reelOrder)
{
    if (!this->isOpen())
        return;

    // Reel進入最後回彈，播ExpectBG跟Particle
    refreshReelExpectBGByChance(reelOrder);
    //最後一輪停止
    if (reelOrder == _reelCount)
    {
        if (_spinBtnStatus == SLOTBTNSTATUS_STOPSUDDENDLY)//快速停輪過程不表現ExpectBG, 待停輪時一次表現
            refreshReelExpectBG(isHitScatter(), isHitBonus(), _reelCount, false);
    }
}

void UISlotCleopatra::onReelStopCB(int reelOrder)
{
    if (reelOrder < 1 || reelOrder > _reelCount || !this->isOpen())
        return;

    if (reelOrder == _reelCount)
    {
        if (isFreeSpining())
        {
            //minusFreeSpinCount();
            //setFreeSpinVisible(true);
            //setFreeSpinCount(_remainFreeSpinCount);
        }
        if (isAutoSpining())
        {
            minusAutoConut();
            setAutoSpinRemainCount(_remainAutoCount);
        }

        //stopPerformExpect();
        performSpinResult(_paylineRcv, _jpRcv, _winRcv, _freeSpinRcv, _bonusRcv, _profileRcv, _levelupRcv);

        Player::getInstance()->setCoin(_profileRcv.coin);//停輪後才更新coin
        Player::getInstance()->setLevel(_profileRcv.level);
        Player::getInstance()->setExp(_profileRcv.exp);
        Player::getInstance()->setVipLv(_profileRcv.vip_level);
        Player::getInstance()->setVipPoint(_profileRcv.vip_point);

        refreshReelExpectBG(isHitScatter(), isHitBonus(), _reelCount, true);//on this moment reelOrder==_reeCount
        if (!isHit())
            colorAllSymbol(Color4B::WHITE);
    }
}

void UISlotCleopatra::onReelTriggerStopSoundCB(int reelOrder)
{
    if (isOpen())
    {
        //快速停輪時只播最後一輪停輪聲
        if (_spinBtnStatus == SLOTBTNSTATUS_STOPSUDDENDLY)
        {
            if (reelOrder == _reelCount)
                AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_REEL_STOP, false);
        }
        //一般停輪播放每輪停輪聲
        else
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_REEL_STOP, false);
    }
}

bool UISlotCleopatra::transSymbolToIndex(std::vector<std::vector<int>>& reel_symbol)
{
    //必須有5輪
    //每輪必須有3個symbol
    //symbolID介於beginSymbolInfoID~endSymbolInfoID

    int beginSymbolInfoID = _pSlotData->SymbolInfoBeginID;
    int endSymbolInfoID = _pSlotData->SymbolInfoEndID;
    int symbolIndexMax = _pSlotData->SymbolInfoEndID - _pSlotData->SymbolInfoBeginID;
    if (!ExternalTable<SymbolInfoData>::getRecordByID(beginSymbolInfoID) || !ExternalTable<SymbolInfoData>::getRecordByID(endSymbolInfoID))
        return false;

    if (reel_symbol.size() != 5)
        return false;

    for (auto& reel : reel_symbol)
    {
        if (reel.size() != 3)
            return false;

        for (auto& symbolID : reel)
        {
            int symbolIdx = symbolID - beginSymbolInfoID;
            if (symbolIdx<0 || symbolIdx>symbolIndexMax)
                return false;
            symbolID = symbolIdx;//1~13->0~12 , 14~27->0~13
        }
    }
    return true;
}

int UISlotCleopatra::getOddsBySymbolCombo(int symbolId, int combo)
{
    SlotMachineData* pSlot = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (!pSlot)
        return 0;
    int GameSymbolBeginID = pSlot->GameSymbolBeginID;
    int GameSymbolEndID = pSlot->GameSymbolEndID;
    for (int GSIdx = GameSymbolBeginID; GSIdx <= GameSymbolEndID; GSIdx++)
    {
        GameSymbolData* pGameSymbolRecord = ExternalTable<GameSymbolData>::getRecordByID(GSIdx);
        if (!pGameSymbolRecord || pGameSymbolRecord->PayType != GameSymbolData::PAYTYPE_COINPAY || pGameSymbolRecord->CheckType != GameSymbolData::CHECKTYPE_SINGLECHECK)
            continue;
        if (pGameSymbolRecord->SymbolInfoID[0] == symbolId)
        {
            int tableIndex = combo - 1;
            if (tableIndex < 0 || tableIndex >= COMBOPAY_MAX)
                continue;
            return pGameSymbolRecord->ComboPay[tableIndex];
        }
    }
    return 0;
}

void UISlotCleopatra::performReelExpectBG(int reelOrder, EExpectType expectBG)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_reelExpect.size())
        return;

    ExpectBGGroup& rCurExpectBGGroup = _reelExpect.at(reelIndex);
    if (expectBG == EXPECT_NONE)
    {
        rCurExpectBGGroup.pScatterBg->setVisible(false);
        rCurExpectBGGroup.pBonusBg->setVisible(false);
        rCurExpectBGGroup.pHybridBg->setVisible(false);
        rCurExpectBGGroup.pParticles->removeAllChildren();
        return;
    }

    switch (expectBG)
    {
    case EXPECT_SCATTER:
        rCurExpectBGGroup.pScatterBg->setVisible(true);
        rCurExpectBGGroup.pBonusBg->setVisible(false);
        rCurExpectBGGroup.pHybridBg->setVisible(false);
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    case EXPECT_BONUS:
        rCurExpectBGGroup.pScatterBg->setVisible(false);
        rCurExpectBGGroup.pBonusBg->setVisible(true);
        rCurExpectBGGroup.pHybridBg->setVisible(false);
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    case EXPECT_HYBRID:
        rCurExpectBGGroup.pScatterBg->setVisible(false);
        rCurExpectBGGroup.pBonusBg->setVisible(false);
        rCurExpectBGGroup.pHybridBg->setVisible(true);
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    default:
        break;
    }
    if (rCurExpectBGGroup.pParticles->getChildrenCount() == 0)
    {
        ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("Particle/cleopatra_expect_fire.plist");
        pParticle1->setPosition(Vec2(-10, 355));
        rCurExpectBGGroup.pParticles->addChild(pParticle1);

        ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("Particle/cleopatra_expect_fire.plist");
        pParticle2->setPosition(Vec2(-10, 104));
        rCurExpectBGGroup.pParticles->addChild(pParticle2);

        ParticleSystemQuad* pParticle3 = ParticleSystemQuad::create("Particle/cleopatra_expect_fire.plist");
        pParticle3->setPosition(Vec2(145, 355));
        rCurExpectBGGroup.pParticles->addChild(pParticle3);

        ParticleSystemQuad* pParticle4 = ParticleSystemQuad::create("Particle/cleopatra_expect_fire.plist");
        pParticle4->setPosition(Vec2(145, 104));
        rCurExpectBGGroup.pParticles->addChild(pParticle4);

        ParticleSystemQuad* pParticlePoint = ParticleSystemQuad::create("Particle/cleopatra_expect_point.plist");
        pParticlePoint->setPosition(Vec2(84, 242));
        rCurExpectBGGroup.pParticles->addChild(pParticlePoint);

        ParticleSystemQuad* pParticleBG = ParticleSystemQuad::create("Particle/cleopatra_expect_bg.plist");
        pParticleBG->setPosition(Vec2(84, 242));
        rCurExpectBGGroup.pParticles->addChild(pParticleBG);
    }
}

void UISlotCleopatra::stopPerformExpect()
{
    for (auto& curBGs : _reelExpect)
    {
        curBGs.pScatterBg->setVisible(false);
        curBGs.pBonusBg->setVisible(false);
        curBGs.pHybridBg->setVisible(false);
        curBGs.pParticles->removeAllChildren();
    }

    colorAllSymbol(Color4B::WHITE);
}

void UISlotCleopatra::colorAllSymbol(const Color4B& color4B)
{
    for (auto pReel : _reels)
    {
        if (!pReel)
            continue;
        pReel->colorAllSymbol(color4B);
    }
}

void UISlotCleopatra::clearExpectSoundResult()
{
    //清除上次ExpectBG資料
    for (int i = 0; i < (int)_isExpectBgSoundPlayed.size(); i++)
        _isExpectBgSoundPlayed[i] = false;
    for (int i = 0; i < (int)_isMiddleExpectSoundPlayed.size(); i++)
        _isMiddleExpectSoundPlayed[i] = false;
}

void UISlotCleopatra::performSceneAnim(ECleopatraSceneAnim anim)
{
    switch (anim)
    {
    case SCENEANIM_CLEOPATRAIDLE:
        if (_sceneObjCleopatra->isPlayingAnimation("win01", 0))
        {
            _sceneObjCleopatra->setToSetupPose();
            _sceneObjCleopatra->addAnimation(0, "idle", true);
        }
        else if (_sceneObjCleopatra->isPlayingAnimation("win02_loop", 0))
        {
            _sceneObjCleopatra->setToSetupPose();
            _sceneObjCleopatra->setAnimation(0, "win02_out", false);
            _sceneObjCleopatra->addAnimation(0, "idle", true);
        }
        else if (_sceneObjCleopatra->isPlayingAnimation("idle", 0) ||
            _sceneObjCleopatra->isPlayingAnimation("win01", 0) ||
            _sceneObjCleopatra->isPlayingAnimation("win02_out", 0)
            )
        {
            return;
        }
        else
        {
            _sceneObjCleopatra->setToSetupPose();
            _sceneObjCleopatra->setAnimation(0, "idle", true);
        }
        break;
    case SCENEANIM_CLEOPATRAWIN01:
        _sceneObjCleopatra->setToSetupPose();
        _sceneObjCleopatra->addAnimation(0, "win01", true);
        //AudioManager::getInstance()->playEffectWithFileID(, false);
        break;
    case SCENEANIM_CLEOPATRAWIN02:
        _sceneObjCleopatra->setToSetupPose();
        _sceneObjCleopatra->setAnimation(0, "win02_in", false);
        _sceneObjCleopatra->addAnimation(0, "win02_loop", true);
        //AudioManager::getInstance()->playEffectWithFileID(, false);
        break;
    case SCENEANIM_CLEOPATRA_TOUCH:
        _sceneObjCleopatra->setToSetupPose();
        _sceneObjCleopatra->setAnimation(0, "touch01", false);
        _sceneObjCleopatra->addAnimation(0, "touch02", false);
        _sceneObjCleopatra->addAnimation(0, "idle", true);
        AudioManager::getInstance()->playEffectWithFileID(EFF_TOUCH_CLEOPATRA, false);
        break;
    default:
        break;
    }
}

void UISlotCleopatra::touchPayTable(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIPayTable* pPayTable = dynamic_cast<UIPayTable*>(UIController::getInstance()->getController(EUITAG_CLEOPATRA_PAYTABLE));
    if (!pPayTable)
        return;
    std::vector<int> symbolTypes = { SymbolInfoData::ESymbolType::SYMBOLTYPE_BONUS, SymbolInfoData::ESymbolType::SYMBOLTYPE_SCATTER, SymbolInfoData::ESymbolType::SYMBOLTYPE_WILD };
    std::vector<int> stringData1 = { 10035 };
    std::vector<int> stringData2 = { 10002, 10003, 10004, 10005 };
    std::vector<int> stringData3 = { 10007 };
    pPayTable->setPage1Data(symbolTypes, stringData1, stringData2, stringData3);
    pPayTable->openUI(_slotID, "GUI/UISlot_Cleopatra/UISlot_Cleopatra_PayTable.ExportJson");

    if (_isAutoPanelOn)
        setAutoPanel(!_isAutoPanelOn);
    if (_isBetOn)
        setBetPanel(!_isBetOn);
}

void UISlotCleopatra::touchMaxBet(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_isAutoPanelOn)
        setAutoPanel(!_isAutoPanelOn);
    if (_isBetOn)
        setBetPanel(!_isBetOn);

    setMaxLineAndBet();
    updateJackpotHint();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);

    requestSpin();
}

void UISlotCleopatra::touchSpin(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_autoSpinStatus == EAutoSpinStatus::AUTOSPINSTATUS_ON && _freeSpinStatus == EFreeSpinStatus::FREESPINSTATUS_OFF)
    {
        setAutoSpinStatus(EAutoSpinStatus::AUTOSPINSTATUS_OFF);
        return;
    }

    if (_isAutoPanelOn)
        setAutoPanel(!_isAutoPanelOn);
    if (_isBetOn)
        setBetPanel(!_isBetOn);

    switch (_spinBtnStatus)
    {
    case SLOTBTNSTATUS_IDLE:
        if (isOpen())
            AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
        requestSpin();
        break;
    case SLOTBTNSTATUS_WAITINGRESPONSE:
        break;
    case SLOTBTNSTATUS_STOPINGREEL:
        stopReel();
        break;
    case SLOTBTNSTATUS_STOPSUDDENDLY:
        break;
    default:
        break;
    }
}

void UISlotCleopatra::touchBet(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _touchMask->setTouchEnabled(true);

    setBetPanel(!_isBetOn);

    if (_isBetOn)
        updateBet();

    if (_isAutoPanelOn)
        setAutoPanel(!_isAutoPanelOn);

    stopPerformSpinResult();
}

void UISlotCleopatra::touchSelectBet(Ref* sender, Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    std::vector<int> betLevel;
    if (_isFixedBet)
        betLevel = _betLevel;
    else
        betLevel = Player::getInstance()->getBetLevel();
    if (index < 0 || index >= (int)betLevel.size())
        return;

    int betValue = betLevel.at(index);

    if (_isFixedBet && !UISlotUtils::isFixedBetUsable(_slotID, betValue))
    {
        auto unlockLevel = UISlotUtils::getFixedBetUnlockLevel(_slotID, betValue);
        auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
        messageController->clearMessage();
        messageController->pushMessage(composeStringData(safeGetStringData(34), StringUtils::toString(unlockLevel)));
        return;
    }
    else if (!_isFixedBet && !Player::getInstance()->isBetUsable(betValue))
    {
        auto unlockLevel = UISlotUtils::getBetUnlockLevel(betValue);
        auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
        messageController->clearMessage();
        messageController->pushMessage(composeStringData(safeGetStringData(34), StringUtils::toString(unlockLevel)));
        return;
    }
 
    std::vector<int> usableBet;
    if (_isFixedBet)
    {
        usableBet = _betLevel;
    }
    else
    {
        usableBet = Player::getInstance()->getUsableBetLevel();
    }

    if (index < 0 || index >= (int)usableBet.size())
        return;

    _curBet = usableBet.at(index);

    setBet(_curBet);

    updateJackpotHint();

    if (_isBetOn)
       setBetPanel(!_isBetOn);

    _touchMask->setTouchEnabled(false);
}

void UISlotCleopatra::touchBetAdd(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    setBetListPos(_betListPos - 1);
}

void UISlotCleopatra::touchBetMinus(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    setBetListPos(_betListPos + 1);
}

void UISlotCleopatra::touchAutoSpin(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _touchMask->setTouchEnabled(true);

    setAutoPanel(!_isAutoPanelOn);

    if (_isBetOn)
        setBetPanel(!_isBetOn);

    stopPerformSpinResult();
}

void UISlotCleopatra::touchAutoSpinTimes(Ref* sender, Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _remainAutoCount = autoSpineTimes[index - 1];
    setAutoSpinStatus(EAutoSpinStatus::AUTOSPINSTATUS_ON);
    setAutoPanel(!_isAutoPanelOn);
    setAutoSpinBtnStatus(true);
   
    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
    requestSpin();

    _touchMask->setTouchEnabled(false);
}

void UISlotCleopatra::touchAutoSpinUnlimited(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _isAutoSpinUnlimited = true;

    _remainAutoCount = 0;
    setAutoSpinStatus(EAutoSpinStatus::AUTOSPINSTATUS_ON);
    setAutoPanel(!_isAutoPanelOn);
    setAutoSpinBtnStatus(true);

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
    requestSpin();

    _touchMask->setTouchEnabled(false);
}

void UISlotCleopatra::touchAutoSpinSetting(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIController::getInstance()->getController(EUITAG_UIAUTOSPIN_SETTING)->openUI();
}

void UISlotCleopatra::touchMainPenal(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    _touchMask->setTouchEnabled(false);

    if (_isAutoPanelOn)
        setAutoPanel(!_isAutoPanelOn);
    if (_isBetOn)
        setBetPanel(!_isBetOn);
}

void UISlotCleopatra::touchSceneObjCleopatra(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    performSceneAnim(ECleopatraSceneAnim::SCENEANIM_CLEOPATRA_TOUCH);
}

void UISlotCleopatra::outOfCoinKeepPlayingCB()
{
    if (_isFixedBet)
        UISlotUtils::getSpinnableBetAndLine(_curBet, totalPayLineCount, UISlotUtils::getUsableFixedBetLevel(_slotID), false);
    else
        UISlotUtils::getSpinnableBetAndLine(_curBet, totalPayLineCount, Player::getInstance()->getUsableBetLevel(), false);
    setBet(_curBet);
}

bool UISlotCleopatra::isScrolling()
{
    for (int i = 0; i < _reelCount; ++i)
    {
        if (_reels.at(i) && _reels.at(i)->isScrolling())
            return true;
    }
    return false;
}
