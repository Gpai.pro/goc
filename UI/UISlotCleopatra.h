#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "UIReel.h"
#include "UISlotUtils.h"
#include "CustomParticleSystemGroup.h"
#include "../Animation/CustomSpAnim.h"

USING_NS_CC;

using namespace ui;

const int AUTOSPINNUM = 3;
const int autoSpineTimes[AUTOSPINNUM] = { 10, 50, 100 };
const int BET_BTN_COUNT = 3;


enum ECleopatraSceneAnim
{
    SCENEANIM_CLEOPATRAIDLE,     //豔后閒置
    SCENEANIM_CLEOPATRAWIN01,    //豔后win01
    SCENEANIM_CLEOPATRAWIN02,    //豔后win02
    SCENEANIM_CLEOPATRA_TOUCH,   //豔后touch
};

enum ECleopatraActionTag
{
    ETAG_PLAYALLLINE,
    ETAG_REPEATSINGLELINE,
    ETAG_PERFORMSPINRESULT,
    ETAG_STOPREEL,
};

class UISlotCleopatra : public UIBasicUnit
{
public:
    static UISlotCleopatra* create(int slotMachineID);

    UISlotCleopatra();
    ~UISlotCleopatra();

    virtual void destroyUI() override;
    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;
    virtual void UpdatePerSecond(float dt) override;
    virtual void playEnterUIAction() override;

    void onDispatchResponse(const std::string& requestTag, const std::string& json);

    // Expect
    void refreshReelExpectBG(bool hitScatter, bool hitBonus, int maxReelOrder, bool isResult);
    void refreshReelExpectBGByChance(int reelOrder);
    // Reel Callback
    void onReelEnterExpectCB(int reelOrder);
    void onReelEnterEndCB(int reelOrder);
    void onReelPendingStopCB(int reelOrder);
    void onReelStopCB(int reelOrder);
    void onReelTriggerStopSoundCB(int reelOrder);

    void stopMachine();

    void startSpin();
    void stopSpinWithResult(std::vector<std::vector<int>> spinResult);

    void setCurWin(uint64 win);
    void setBet(int bet);
    void setMaxLineAndBet();
    void setBetPanel(bool onOff);
    void setAutoPanel(bool onOff);
    void setBetListPos(int pos);
    void updateBet();
    void stopReel();

    bool isHit();
    bool isHitScatter();
    bool isHitBonus();

    // spin
    void setSpinBtnStatus(ESlotButtonStatus status);
    void setAutoSpinStatus(EAutoSpinStatus status);
    void setFreeSpinStatus(EFreeSpinStatus status);

    // auto spin
    bool isAutoSpining();
    void minusAutoConut();
    bool isUIOpen();
    void setSpinInUpdate(bool check){ _bSpinInUpdate = check; };
    void setAutoSpinRemainCount(int count);
    void setAutoSpinBtnStatus(bool start);

    // PayLine
    void initPayLine();
    int getPayLineOrder(int payLineID);
    void playPayLine(int payLineID, bool play, bool Loop);
    void playAllPayLineOnce(std::vector<SpinResponse_PayLine>& pay_line, SpinResponse_Win& winRcv, bool specialWin);
    void repeatSinglePayLines(std::vector<SpinResponse_PayLine>& pay_line);
    void stopPlayPayLine(int payLineID);
    void stopPlayAllPayLine();

    // spine
    void playAllPayLineSymbol(std::vector<SpinResponse_PayLine>& payLines);
    void playSinglePayLineSymbol(SpinResponse_PayLine& payLine);
    void clearPayLineSymbol();
    void visibleAllSymbol();

    // free spin
    void setFreeSpinVisible(bool visible);
    void setFreeSpinCount(int count);
    void addFreeSpinCount(int addCount);
    bool isFreeSpining();
    void minusFreeSpinCount();

    // Jackpot
    void setJPEnable(bool enable);
    void updateJackpotHint();
    void setJackpotScore(uint64 count);

    // Result
    void performSpinResult(std::vector<SpinResponse_PayLine>& pay_lineRcv,
                           SpinResponse_JP& jpRcv,
                           SpinResponse_Win& winRcv,
                           SpinResponse_FreeSpin& freeSpinRcv,
                           SpinResponse_Bonus& bonusRcv,
                           SpinResponse_Profile& profileRcv,
                           SpinResponse_LevelUp& levelupRcv); 
    void stopPerformSpinResult();
    bool isPerformSpinResult(EPerformResultType type = RESULTTYPE_NORMAL);

    void requestSpin();
    void clearAllSpinResultData();

    // 將symbolID轉為圖片Index
    bool transSymbolToIndex(std::vector<std::vector<int>>& reel_symbol);

    int getOddsBySymbolCombo(int symbolId, int combo);

    // expect
    void performReelExpectBG(int reelOrder, EExpectType expectBG);
    void stopPerformExpect();
    void colorAllSymbol(const cocos2d::Color4B& color4B);
    void clearExpectSoundResult();

    // scene anim
    void performSceneAnim(ECleopatraSceneAnim anim);

    // touch event
    void touchPayTable(Ref* sender, Widget::TouchEventType type);
    void touchMaxBet(Ref* sender, Widget::TouchEventType type);
    void touchSpin(Ref* sender, Widget::TouchEventType type);
    void touchBet(Ref* sender, Widget::TouchEventType type);
    void touchSelectBet(Ref* sender, Widget::TouchEventType type, int index);
    void touchBetAdd(Ref* sender, Widget::TouchEventType type);
    void touchBetMinus(Ref* sender, Widget::TouchEventType type);
    void touchAutoSpin(Ref* sender, Widget::TouchEventType type);
    void touchAutoSpinTimes(Ref* sender, Widget::TouchEventType type, int index);
    void touchAutoSpinUnlimited(Ref* sender, Widget::TouchEventType type);
    void touchAutoSpinSetting(Ref* sender, Widget::TouchEventType type);
    void touchMainPenal(Ref* sender, Widget::TouchEventType type);
    void touchSceneObjCleopatra(Ref* sender, Widget::TouchEventType type);

    // out of coin callback
    void outOfCoinKeepPlayingCB();

private:
    bool init(int slotMachineID);
    virtual bool createUI() override;
    bool isScrolling();

private:
    SlotMachineData* _pSlotData = nullptr;
    int _slotID = 3;
    int _reelCount = 0;
    int _payLineBeginID = 0;
    int _payLineEndID = 0;
    int _scatterID = 0;
    int _minScatterCombo = 0;
    int _bonusID = 0;
    int _minBonusCombo = 0;
    int totalPayLineCount = 50;
    bool _bSpinInUpdate = false;
    Layout* _uiSlotCleopatra = nullptr;
    Layout* _pLayoutGroup = nullptr;
    Button* _payTableBtn = nullptr;
    Button* _maxBetBtn = nullptr;
    Button* _spinBtn = nullptr;
    TextBMFont* _curWinScore = nullptr;
    TextBMFont* _totalBet = nullptr;
    TextBMFont* _winMoneyAmin = nullptr;
    Layout* _payLineAnim = nullptr;
    Layout* _touchMask = nullptr;

    std::vector<Layout*> _reelLayouts;
    std::vector<Node*> _symbolAnimations;

    // Animation
    cocostudio::ActionObject* _pActionEnterAnim = nullptr;

    // Spine
    CustomSpAnim* _sceneObjCleopatra = nullptr;              //場景物件 豔后

    // Jackpot
    uint64 _jackScore = 0;
    Layout* _jackpotPanel = nullptr;
    TextBMFont* _jackpotBM = nullptr;
    ImageView* _jackpot_hint_no = nullptr;
    ImageView* _jackpot_hint_yes = nullptr;

    // spin
    ImageView* _spinImg = nullptr;
    ImageView* _spinImgGray = nullptr;
    ImageView* _stopImg = nullptr;
    ImageView* _stopImgGray = nullptr;
    ESlotButtonStatus _spinBtnStatus = ESlotButtonStatus::SLOTBTNSTATUS_IDLE;
    EAutoSpinStatus _autoSpinStatus = EAutoSpinStatus::AUTOSPINSTATUS_OFF;
    EFreeSpinStatus _freeSpinStatus = EFreeSpinStatus::FREESPINSTATUS_OFF;

    // bet
    std::vector<int> _betLevel;
    bool _isFixedBet = false;
    int _betListPos = 0;
    int _betLevelIndex = 0;
    int _jpLimit = 0;
    int _curBet = 0;
    bool _isBetOn = false;
    Button* _betBtn = nullptr;
    TextBMFont* _betNowNum = nullptr;
    Layout* _betPanel = nullptr;
    Button* _betNextBtn = nullptr;
    Button* _betPreBtn = nullptr;
    ListView* _betListView = nullptr;

    // free spin
    int _freeSpinWin = 0;
    int _freeSpinCount = 0;
    int _remainFreeSpinCount = 0;
    bool _bFreeSpinWait = false;
    Layout* _freeSpinPanel = nullptr;
    TextBMFont* _remainFreeSpin = nullptr;

    // auto
    Button* _autoSpinBtn = nullptr;
    Layout* _autoPanel = nullptr;
    int _remainAutoCount = 0;
    Button* _autoSpinTimeBtn[AUTOSPINNUM];
    Button* _autoSpinUnlimited = nullptr;
    Button* _autoSpinSetting = nullptr;
    Button* _autoSpinCountBtn = nullptr;
    TextBMFont* _autoSpinCountBM = nullptr;
    bool _isAutoPanelOn = false;
    bool _isAutoSpinUnlimited = false;

    // Reel
    Layout* _symbolBorad = nullptr;
    std::vector<UIReel*> _reels;

    // Spin Result
    SlotIDList _slotIDListRcv;
    SpinResponse_Overall _overallRcv;
    std::vector<SpinResponse_PayLine> _paylineRcv;
    SpinResponse_JP _jpRcv;
    SpinResponse_Win _winRcv;
    SpinResponse_FreeSpin _freeSpinRcv;
    SpinResponse_Bonus _bonusRcv;
    SpinResponse_Bonuswild _bonuswildRcv;
    SpinResponse_Profile _profileRcv;
    SpinResponse_LevelUp _levelupRcv;
    SlotIDList _specialSlotListRcv;

    // Coin particle
    CustomParticleSystemGroup* _coinParticle;

    // Expect
    std::vector<ExpectBGGroup> _reelExpect;
    std::vector<bool> _isExpectBgSoundPlayed;       //是否播過特殊符號背景音效
    std::vector<bool> _isMiddleExpectSoundPlayed;   //是否播過特殊符號聽牌音效
    unsigned int _middleExpectSoundHandle;
    unsigned int _spinSoundHandle = 0;

    // Cheat Code
    std::vector<SlotIDList> _cheatsCodeList;
};