#include "UISlotCleopatraBonusGame.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataCenter/ExternalTable.h"
#include "../Animation/CustomActions.h"
#include "../Network/MessageProcess.h"
#include "../Animation/CustomSpAnim.h"
#include "AudioManager.h"
#include "UISlotUtils.h"
#include "UIController.h"
#include "UISlotCleopatra.h"
#include "UIMessage.h"
#include "../GlobalDefine/Player.h"

using namespace cocostudio;

#define EFF_SLOTCLEOPATRA_BONUSGAME_HIT 3101

UISlotCleopatraBonusGame* UISlotCleopatraBonusGame::create()
{
    UISlotCleopatraBonusGame* ui = new UISlotCleopatraBonusGame();
    if (ui && ui->init())
    {
        return ui;
    }

    CC_SAFE_DELETE(ui);
    return nullptr;
}

UISlotCleopatraBonusGame::UISlotCleopatraBonusGame()
{
    setUIShowType(EUISHOWTYPE::EUIST_BONUSGAME);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotCleopatraBonusGame::~UISlotCleopatraBonusGame()
{

}

void UISlotCleopatraBonusGame::notifyOpenUI()
{
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
}

void UISlotCleopatraBonusGame::notifyCloseUI()
{
    auto cSlotCleopatra = UIController::getInstance()->getController<UISlotCleopatra*>(EUITAG::EUITAG_SLOT_CLEOPATRA);
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (!cSlotCleopatra->isFreeSpining())
        AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

void UISlotCleopatraBonusGame::destroyUI()
{

}

void UISlotCleopatraBonusGame::updateUI()
{
    initData();
}

void UISlotCleopatraBonusGame::UpdatePerSecond(float dt)
{
    if (!_isAutoSpin)
        return;

    ++_counter;

    switch (_step)
    {
    case eBonusGamePokeStep::START:
        if (_counter >= 3)
        {
            setStep(eBonusGamePokeStep::GAMEING);
            _counter = 0;
        }
        break;
    case eBonusGamePokeStep::INTO_ANOMATION:
        break;
    case eBonusGamePokeStep::GAMEING:
        if (_counter < 2)
            break;

        if (_pokeChance == 0)
        {
            setStep(eBonusGamePokeStep::RESULT);
            break;
        }

        if (_pokeChance <= 0 || _isWaitResponse)
            break;

        _touchIndex = RandomHelper::random_int(0, (int)_gameObjects.size() - 1);

        while (_gameObjects.at(_touchIndex).isBePoke)
        {
            _touchIndex = RandomHelper::random_int(0, (int)_gameObjects.size() - 1);
        }
        _touchPos = _gameObjects.at(_touchIndex).score->getWorldPosition();

        sendPokeRequest(_touchIndex);

        break;
    case eBonusGamePokeStep::RESULT:
        // setStep(eBonusGamePokeStep::END);
        break;
    case eBonusGamePokeStep::END:
        break;
    default:
        break;
    }
}

bool UISlotCleopatraBonusGame::openUI(int bonusGameID, int bet)
{
    _bonusGameID = bonusGameID;
    _bet = bet;

    if (!UIBasicUnit::openUI())
    {
        return false;
    }

    return true;
}

void UISlotCleopatraBonusGame::setGameInfo(int bonusGameID, int bet, bool autoSpin)
{
    _bonusGameID = bonusGameID;
    _bet = bet;
    _isAutoSpin = autoSpin;
}

bool UISlotCleopatraBonusGame::init()
{
    return true;
}

bool UISlotCleopatraBonusGame::createUI()
{
    // Start
    _gameStart = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Cleopatra/UISlot_Cleopatra_BonusGame_Start.ExportJson"));
    _mainUINode->addChild(_gameStart);
    auto tempStart = _gameStart->getChildByName<Layout*>("bonusGame");

    auto text = tempStart->getChildByName<Text*>("txt_content");
    text->setString(safeGetStringData(10016));

    _startBtn = tempStart->getChildByName<Button*>("Button");
    _startBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatraBonusGame::touchStartGame, this));

    // Game
    _bonusGame = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Cleopatra/UISlot_Cleopatra_BonusGame.ExportJson"));
    _mainUINode->addChild(_bonusGame);
    setTraversalCascadeOpacityEnabled(_bonusGame, true);

    _leftChanceBM = _bonusGame->getChildByName<TextBMFont*>("BmLabel_PICKS_LEFT");
    _totalWinBM = _bonusGame->getChildByName<TextBMFont*>("BmLabel_TOTAL_WIN");

    auto queen = _bonusGame->getChildByName<Layout*>("CleopatraLayout");
    CustomSpAnim* pSpine = CustomSpAnim::createWithFile("UISlot_Cleopatra/Spine/cleopatra_obj_sp_cleopatra02", "");
    pSpine->playAnimation("standby");
    pSpine->setScale(1.3f);
    queen->addChild(pSpine);

    // End
    _gameEnd = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Cleopatra/UISlot_Cleopatra_BonusGame_Result.ExportJson"));
    _mainUINode->addChild(_gameEnd);
    setTraversalCascadeOpacityEnabled(_gameEnd, true);

    auto tempText = _gameEnd->getChildByName<Layout*>("text");
    _endResult = tempText->getChildByName<TextBMFont*>("type_01");
    _endCalculate = tempText->getChildByName<TextBMFont*>("type_02");

    auto content = tempText->getChildByName<Text*>("content");
    content->setString(safeGetStringData(10011));

    _backToGameBtn = _gameEnd->getChildByName<Button*>("Button");
    _backToGameBtn->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatraBonusGame::touchBackToGame, this));

    // Particle
    _particleDummy = _bonusGame->getChildByName<Layout*>("layout_particle_dummy");

    return true;
}

void UISlotCleopatraBonusGame::setStep(eBonusGamePokeStep step)
{
    if (_step == step)
        return;

    _step = step;

    switch (step)
    {
    case eBonusGamePokeStep::START:
        break;
    case eBonusGamePokeStep::INTO_ANOMATION:
        break;
    case eBonusGamePokeStep::GAMEING:
        if (_pokeChance == 0)
        {
            setStep(eBonusGamePokeStep::RESULT);
            break;
        }

        _bonusGame->setVisible(true);
        setTraversalCascadeOpacityEnabled(_bonusGame, true);
        _bonusGame->setOpacity(0.0f);
        _bonusGame->runAction(Sequence::create(FadeIn::create(1.0f),
            CallFunc::create(CC_CALLBACK_0(Widget::setTouchEnabled, _bonusGame, true)), nullptr));

        _gameStart->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create(CC_CALLBACK_0(Widget::setVisible, _gameStart, false)), nullptr));

        break;
    case eBonusGamePokeStep::RESULT:
    {
        setEndResult(_totalWinScore * _bet);
        setEndCalculate(_totalWinScore, _bet);

        _gameEnd->setVisible(true);
        _gameEnd->setOpacity(0.0f);
        Sequence* actionsSeq(nullptr);
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.clear();
        arrayOfActions.pushBack(DelayTime::create(0.7f));
        arrayOfActions.pushBack(FadeIn::create(1.0f));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Widget::setTouchEnabled, _gameEnd, true)));
        if (_isAutoSpin)
        {
            arrayOfActions.pushBack(DelayTime::create(3.0f));
            arrayOfActions.pushBack(CallFunc::create([this](){setStep(eBonusGamePokeStep::END); }));
        }
        actionsSeq = Sequence::create(arrayOfActions);

        _gameEnd->runAction(actionsSeq);

        _bonusGame->runAction(Sequence::create(DelayTime::create(0.7f), FadeOut::create(0.7f), CallFunc::create(CC_CALLBACK_0(Widget::setVisible, _bonusGame, false)), nullptr));
    }
        break;
    case eBonusGamePokeStep::END:
        // TODO::
        // bigwin
        Player::getInstance()->setCoin(_playerCoin);
        closeUI();
        break;
    default:
        break;
    }
}

void UISlotCleopatraBonusGame::setGameLeftChance(int chance)
{
    _leftChanceBM->setString(StringUtils::toString(chance));
}

void UISlotCleopatraBonusGame::setEndResult(int resultScore)
{
    //_endResult->stopAllActions();
    //CountTextBMFontToNum* action = CountTextBMFontToNum::create(1.5f, 0, resultScore);
    //action->setFormat(CountTextBMFontToNum::FORMAT_DOLLARCOMMA);
    //_endResult->runAction(Sequence::create(DelayTime::create(1.3f), action, nullptr));

    _endResult->setString(StringUtils::toString(resultScore));
}

void UISlotCleopatraBonusGame::setEndCalculate(int totalWin, int bet)
{
    _endCalculate->setString(StringUtils::format("%d * %d = %d", totalWin, bet, totalWin * bet));
}

void UISlotCleopatraBonusGame::updateTotalWin()
{
    _totalWinBM->stopAllActions();
    CountTextBMFontToUint64Num* action = CountTextBMFontToUint64Num::create(0.5f, _nowTotalWinScore, _totalWinScore);
    action->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    _totalWinBM->runAction(action);
}

void UISlotCleopatraBonusGame::initData()
{
    setStep(eBonusGamePokeStep::START);
    resetAllObject();
    _pokeChance = 0;
    _nowTotalWinScore = 0;
    _totalWinScore = 0;
    _counter = 0;

    // start
    _gameStart->setVisible(true);
    _gameStart->setTouchEnabled(true);

    // game
    _bonusGame->setVisible(false);
    _bonusGame->setTouchEnabled(false);
    _pokeChance = getPokeChance();

    _allScores.clear();
    int objectCount = getObjectCount();
    if (objectCount > TOTAL_OBJECT_COUNT)
    {
        log("SlotCleoPatraBonusGame Count Error");
        return;
    }

    std::vector<int> objectIndexs = shuffle(TOTAL_OBJECT_COUNT);

    _gameObjects.clear();
    for (int i = 0; i < objectCount; ++i)
    {
        auto objectLayout = _bonusGame->getChildByName<Layout*>(StringUtils::format("Object_%d", objectIndexs.at(i)));
        objectLayout->setVisible(true);

        BonusGamePokObject object;
        object.pic = objectLayout->getChildByName<ImageView*>("pic");
        object.pic->setColor(Color3B::WHITE);
        object.score = objectLayout->getChildByName<TextBMFont*>("score");
        object.score->setVisible(false);
        object.score->setScale(0.7f);

        bool hasMember = true;
        int count = 1;
        // get touch member
        while (hasMember)
        {
            auto touch = objectLayout->getChildByName<Layout*>(StringUtils::format("touch_%d", count));
            if (touch)
            {
                touch->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatraBonusGame::touchBonusGameObject, this, i));
                touch->setTouchEnabled(true);
                object.touchs.push_back(touch);

                ++count;
            }
            else
            {
                hasMember = false;
            }
        }
        // get attachment member
        hasMember = true;
        count = 1;
        while (hasMember)
        {
            auto attachment = objectLayout->getChildByName<ImageView*>(StringUtils::format("attachment_%d", count));

            if (attachment)
            {
                object.attachments.push_back(attachment);
                
                ++count;
            }
            else
            {
                hasMember = false;
            }
        }

        _gameObjects.push_back(object);
    }

    // end
    _gameEnd->setVisible(false);
    _gameEnd->setTouchEnabled(false);

    setGameLeftChance(_pokeChance);
    updateTotalWin();
    setEndResult(0);
    setEndCalculate(0, 0);

    // revome particle
    stopTouchParticle();
}

std::vector<int> UISlotCleopatraBonusGame::shuffle(int len)
{
    std::vector<int> indexVec;
    for (int i = 0; i < len; ++i)
        indexVec.push_back(i + 1);

    for (int i = len - 1; i > 0; --i)
    {
        int j = rand() % i + 1;
        int temp = indexVec[i];
        indexVec[i] = indexVec[j];
        indexVec[j] = temp;
    }

    std::vector<int> result(indexVec);
    return result;
}

void UISlotCleopatraBonusGame::resetAllObject()
{
    for (int i = 0; i < TOTAL_OBJECT_COUNT; ++i)
    {
        auto object = _bonusGame->getChildByName<Layout*>(StringUtils::format("Object_%d", i + 1));
        object->setVisible(false);
    }
}

int UISlotCleopatraBonusGame::getPokeChance()
{
    PokeGameData* pData = ExternalTable<PokeGameData>::getRecordByID(_bonusGameID);
    if (!pData)
    {
        CCLOG("UIBonusGamePoke :: Table BonusGame1Data ID = %d is Faild!!", _bonusGameID);
        return 0;
    }

    return pData->Chance;
}

int UISlotCleopatraBonusGame::getObjectCount()
{
    int chanceCount = 0;
    PokeGameData* pData = ExternalTable<PokeGameData>::getRecordByID(_bonusGameID);
    if (!pData)
    {
        CCLOG("UIBonusGamePoke :: Table BonusGame1Data ID = %d is Faild!!", _bonusGameID);
        return 0;
    }
    for (int i = 0; i < BONUSITEMCOUNT_MAX; ++i)
    {
        for (int j = 0; j < pData->items[i].Count; ++j)
            _allScores.push_back(pData->items[i].Coin);

        chanceCount += pData->items[i].Count;
    }

    return chanceCount;
}

void UISlotCleopatraBonusGame::sendPokeRequest(int index)
{
    if (_isWaitResponse || _pokeChance <= 0)
        return;

    _isWaitResponse = true;
    _touchIndex = index;

    RequestPokeResult(_pokeChance, _bonusGameID);
}

void UISlotCleopatraBonusGame::recvPokeResponse(int action, int score, int bet, uint64 totalScore, uint64 playerCoin)
{
    _playerCoin = playerCoin;
    _isWaitResponse = false;

    --_pokeChance;
    setGameLeftChance(_pokeChance);

    playPokeTreasure(_touchIndex, score);
    erasePokeScore(score);

    _nowTotalWinScore = _totalWinScore;
    _totalWinScore = totalScore;
    _mainUINode->runAction(Sequence::create(DelayTime::create(0.7f), CallFunc::create(CC_CALLBACK_0(UISlotCleopatraBonusGame::updateTotalWin, this)), nullptr));

    if (action == 1)//if (_pokeChance <= 0)
    {
        _bet = bet;
        _bonusGame->runAction(Sequence::create(DelayTime::create(1.5f), CallFunc::create(CC_CALLBACK_0(UISlotCleopatraBonusGame::playPokeDone, this)), nullptr));
    }

    // play particle
    playTouchParticle(_touchPos);
    AudioManager::getInstance()->playEffectWithFileID(EFF_SLOTCLEOPATRA_BONUSGAME_HIT, false);
}

void UISlotCleopatraBonusGame::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    int errorCode = 0;
    int action = 0;
    int gift = 0;
    int bet = 0;
    uint64 total_gift = 0;
    uint64 playerCoin = 0;

    if (!UISlotUtils::parsePokeResultResponse(json, errorCode, action, gift, bet, total_gift, playerCoin))
    {
        CCLOG("poke BonusGame response pasing error");
        closeUI();
        return;
    }

    recvPokeResponse(action, gift, bet, total_gift, playerCoin);
}

void UISlotCleopatraBonusGame::erasePokeScore(int score)
{
    auto it = std::find(_allScores.begin(), _allScores.end(), score);

    if (it != _allScores.end())
    {
        _allScores.erase(it);
    }
    else
    {
        _allScores.pop_back();
    }
}

void UISlotCleopatraBonusGame::playPokeTreasure(int index, int score)
{
    _gameObjects.at(index).isBePoke = true;
    _gameObjects.at(index).pic->runAction(TintTo::create(0.3f, Color3B::GRAY));

    _gameObjects.at(index).score->setOpacity(0);
    _gameObjects.at(index).score->setVisible(true);
    _gameObjects.at(index).score->setString(StringUtils::toString(score));
    _gameObjects.at(index).score->runAction(Spawn::createWithTwoActions(MoveBy::create(0.7f, Vec2(0.0f, 20.0f)), FadeIn::create(0.7f)));

    for (int i = 0; i < (int)_gameObjects.at(index).touchs.size(); ++i)
        _gameObjects.at(index).touchs.at(i)->setTouchEnabled(false);
}

void UISlotCleopatraBonusGame::playPokeDone()
{
    for (int i = 0; i < (int)_gameObjects.size(); ++i)
    {
        if (_gameObjects.at(i).isBePoke)
            continue;

        _gameObjects.at(i).pic->runAction(TintTo::create(0.3f, Color3B::GRAY));

        _gameObjects.at(i).score->setOpacity(0);
        _gameObjects.at(i).score->setVisible(true);

        int score = 0;
        if (!_allScores.empty())
        {
            score = _allScores.back();
            _allScores.pop_back();
        }
        _gameObjects.at(i).score->setScale(0.5);
        _gameObjects.at(i).score->setString(StringUtils::toString(score));
        _gameObjects.at(i).score->runAction(Spawn::createWithTwoActions(MoveBy::create(0.7f, Vec2(0.0f, 20.0f)), FadeTo::create(0.7f, 200)));

        _mainUINode->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create(CC_CALLBACK_0(UISlotCleopatraBonusGame::setStep, this, eBonusGamePokeStep::RESULT)), nullptr));
    }
}

void UISlotCleopatraBonusGame::playTouchParticle(Vec2 pos)
{
    ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("Particle/cleopatra_bonusgame_fx01.plist");
    pParticle1->setPosition(pos);
    _particleDummy->addChild(pParticle1);
    ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("Particle/cleopatra_bonusgame_fx02.plist");
    pParticle2->setPosition(pos);
    _particleDummy->addChild(pParticle2);
}

void UISlotCleopatraBonusGame::stopTouchParticle()
{
    _particleDummy->removeAllChildren();
}

void UISlotCleopatraBonusGame::touchStartGame(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_isAutoSpin)
    {
        UIController::getInstance()->getController<UIShortMessage*>(EUITAG::EUITAG_MESSAGE)->pushMessage(safeGetStringData(680));
        return;
    }

    setStep(eBonusGamePokeStep::GAMEING);
}

void UISlotCleopatraBonusGame::touchBonusGameObject(Ref* pSender, Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_isAutoSpin)
    {
        UIController::getInstance()->getController<UIShortMessage*>(EUITAG::EUITAG_MESSAGE)->pushMessage(safeGetStringData(680));
        return;
    }

    Widget* touch = (Widget*)pSender;
    _touchPos = touch->getTouchEndPosition();

    sendPokeRequest(index);
}

void UISlotCleopatraBonusGame::touchBackToGame(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_isAutoSpin)
    {
        UIController::getInstance()->getController<UIShortMessage*>(EUITAG::EUITAG_MESSAGE)->pushMessage(safeGetStringData(680));
        return;
    }

    Player::getInstance()->setCoin(_playerCoin);

    this->closeUI();
}