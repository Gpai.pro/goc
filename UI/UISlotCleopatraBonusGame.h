#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "UIBasicUnit.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/CustomTypeDef.h"

USING_NS_CC;

using namespace ui;

struct BonusGamePokObject
{
    ImageView* pic;
    std::vector<ImageView*> attachments;
    std::vector<Layout*> touchs;
    TextBMFont* score;
    bool isBePoke;

    BonusGamePokObject()
    {
        pic = nullptr;
        score = nullptr;
        attachments.clear();
        touchs.clear();
        isBePoke = false;
    }
};

class UISlotCleopatraBonusGame : public UIBasicUnit
{
public:
    static UISlotCleopatraBonusGame* create();
    UISlotCleopatraBonusGame();
    ~UISlotCleopatraBonusGame();

    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void destroyUI() override;
    virtual void updateUI() override;
    virtual void UpdatePerSecond(float dt) override;

public:
    bool openUI(int bonusGameID, int bet);
    void setGameInfo(int bonusGameID, int bet, bool autoSpin);
    void recvPokeResponse(int action, int score, int bet, uint64 totalScore, uint64 playerCoin);
    void onDispatchResponse(const std::string& requestTag, const std::string& json);

private:
    bool init();
    virtual bool createUI() override;

private:
    void setStep(eBonusGamePokeStep step);
    void setGameLeftChance(int chance);
    void setEndResult(int resultScore);
    void setEndCalculate(int totalWin, int bet);
    void updateTotalWin();
    void initData();
    std::vector<int> shuffle(int len);
    void resetAllObject();
    int getPokeChance();
    int getObjectCount();
    void sendPokeRequest(int index);
    void erasePokeScore(int score);
    void playPokeTreasure(int index, int score);
    void playPokeDone();
    void playTouchParticle(Vec2 pos);
    void stopTouchParticle();

    void touchStartGame(Ref* pSender, Widget::TouchEventType type);
    void touchBonusGameObject(Ref* pSender, Widget::TouchEventType type, int index);
    void touchBackToGame(Ref* pSender, Widget::TouchEventType type);

private:
    eBonusGamePokeStep _step = eBonusGamePokeStep::START;

    const int TOTAL_OBJECT_COUNT = 14;
    int _bonusGameID = 0;
    int _bet = 0;
    int _pokeChance = 0;
    uint64 _nowTotalWinScore = 0;
    uint64 _totalWinScore = 0;
    uint64 _playerCoin = 0;
    int _touchIndex = 0;
    bool _isWaitResponse = false;
    std::vector<int> _allScores;

    // Start
    Layout* _gameStart = nullptr;
    Button* _startBtn = nullptr;

    // Game
    Layout* _bonusGame = nullptr;
    std::vector<BonusGamePokObject> _gameObjects;
    TextBMFont* _leftChanceBM = nullptr;
    TextBMFont* _totalWinBM = nullptr;

    // End
    Layout* _gameEnd = nullptr;
    TextBMFont* _endResult = nullptr;
    TextBMFont* _endCalculate = nullptr;
    Button* _backToGameBtn = nullptr;

    // Particle
    Layout* _particleDummy = nullptr;
    Vec2 _touchPos;
    int _slotID = 3;

    // Auto
    bool _isAutoSpin = false;
    int _counter = 0;
};