#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "UIBasicUnit.h"
#include "../GlobalDefine/CustomTypeDef.h"

using namespace cocos2d;

class UISlotCleopatraFreeSpinResult : public UIBasicUnit
{

public:
    static UISlotCleopatraFreeSpinResult* create();

public:

    UISlotCleopatraFreeSpinResult();
    ~UISlotCleopatraFreeSpinResult();

    virtual void destroyUI() override;

    virtual void notifyCloseUI() override;

    virtual void updateUI() override;

    void setFreeSpinCounter(int freeSpinCounter){ _freeSpinCounter = freeSpinCounter; };

    void setWinMoney(uint64 winMoney){ _winMoney = winMoney; };

    void setAutoCloseUITime(int time){ _autoCloseUITime = time; };

    void reset()
    {
        _winMoney = 0;
        _freeSpinCounter = 0;
        _autoCloseUITime = 0.0f;
    }

private:
    bool init();

    virtual bool createUI() override;

    virtual void Update(float dt) override;

    void onTouchExitButton(Ref* sender, ui::Widget::TouchEventType event);

private:

    ui::Layout* _pLayoutFreeSpinResult = nullptr;

    cocos2d::ui::TextBMFont* _pBMFWinMoney = nullptr;;
    cocos2d::ui::TextBMFont* _pBMFFreeSpinCounter = nullptr;;
    cocos2d::ui::Button* _pBtnCloseUI = nullptr;;

    uint64 _winMoney = 0;
    int _freeSpinCounter = 0;
    float _autoCloseUITime = 0.0f;
    int _slotID = 3;
};