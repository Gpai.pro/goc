#include "UISlotCleopatraFreeSpinResult.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Animation/CustomActions.h"
#include "../DataCenter/ExternalTable.h"
#include "AudioManager.h"
#include "../Gameplay/GameLogic.h"


using namespace cocostudio;
using namespace ui;


UISlotCleopatraFreeSpinResult* UISlotCleopatraFreeSpinResult::create()
{
    UISlotCleopatraFreeSpinResult* unit = new UISlotCleopatraFreeSpinResult();

    if (unit && unit->init())
    {
        return unit;
    }

    CC_SAFE_DELETE(unit);
    return nullptr;
}

bool UISlotCleopatraFreeSpinResult::init()
{
    return true;
}

UISlotCleopatraFreeSpinResult::UISlotCleopatraFreeSpinResult()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotCleopatraFreeSpinResult::~UISlotCleopatraFreeSpinResult()
{

}

void UISlotCleopatraFreeSpinResult::notifyCloseUI()
{
    _pBMFFreeSpinCounter->stopAllActions();
    _pLayoutFreeSpinResult->stopAllActions();

    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (GameLogic::getInstance()->GetState() != E_GameState_StayBonusGame)
        AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

bool UISlotCleopatraFreeSpinResult::createUI()
{
    _pLayoutFreeSpinResult = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Cleopatra/UISlot_Cleopatra_FreeSpin_Result.ExportJson"));
    _mainUINode->addChild(_pLayoutFreeSpinResult);

    setTraversalCascadeOpacityEnabled(_mainUINode, true);

    auto text = _pLayoutFreeSpinResult->getChildByName<Layout*>("text");
    _pBMFWinMoney = text->getChildByName<TextBMFont*>("type_01");
    _pBMFWinMoney->setString("0");

    _pBMFFreeSpinCounter = text->getChildByName<TextBMFont*>("type_02");
    _pBMFFreeSpinCounter->setString("0");

    _pBtnCloseUI = _pLayoutFreeSpinResult->getChildByName<Button*>("Button");
    _pBtnCloseUI->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatraFreeSpinResult::onTouchExitButton, this));

    return true;
}

void UISlotCleopatraFreeSpinResult::destroyUI()
{
    _pLayoutFreeSpinResult->removeFromParent();
    _pLayoutFreeSpinResult = nullptr;
}

void UISlotCleopatraFreeSpinResult::updateUI()
{
    if (_winMoney<0)//�i��0��
        return;

    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(2.0f, 0, _winMoney);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_NONE);
    _pBMFWinMoney->runAction(pAction);

    _pBMFFreeSpinCounter->setString(Value(_freeSpinCounter).asString());

    if (_autoCloseUITime > 0.0f)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(_autoCloseUITime));
        arrayOfActions.pushBack(CallFunc::create([this](){ this->closeUI(); }));
        auto actionSeq = Sequence::create(arrayOfActions);
        _pLayoutFreeSpinResult->runAction(actionSeq);
    }
}

void UISlotCleopatraFreeSpinResult::Update(float dt)
{
}

void UISlotCleopatraFreeSpinResult::onTouchExitButton(Ref* sender, ui::Widget::TouchEventType event)
{
    if (event != ui::Widget::TouchEventType::ENDED)
        return;
    closeUI();
}