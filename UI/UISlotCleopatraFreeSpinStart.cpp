#include "UISlotCleopatraFreeSpinStart.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Animation/CustomActions.h"
#include "../DataCenter/ExternalTable.h"
#include "AudioManager.h"


using namespace cocostudio;
using namespace ui;


UISlotCleopatraFreeSpinStart* UISlotCleopatraFreeSpinStart::create()
{
    UISlotCleopatraFreeSpinStart* unit = new UISlotCleopatraFreeSpinStart();

    if (unit && unit->init())
    {
        return unit;
    }

    CC_SAFE_DELETE(unit);
    return nullptr;
}

bool UISlotCleopatraFreeSpinStart::init()
{
    return true;
}

UISlotCleopatraFreeSpinStart::UISlotCleopatraFreeSpinStart()
: _pBMFFreeSpinCounter(nullptr)
, _freeSpinCounter(0)
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotCleopatraFreeSpinStart::~UISlotCleopatraFreeSpinStart()
{

}

void UISlotCleopatraFreeSpinStart::notifyOpenUI()
{
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
}

void UISlotCleopatraFreeSpinStart::notifyCloseUI()
{
    _pLayoutFreeSpinStart->stopAllActions();
}

bool UISlotCleopatraFreeSpinStart::createUI()
{
    _pLayoutFreeSpinStart = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_Cleopatra/UISlot_Cleopatra_FreeSpin_Start.ExportJson"));
    _mainUINode->addChild(_pLayoutFreeSpinStart);

    _pLayoutFreeSpinStart->setTouchEnabled(true);
    _pLayoutFreeSpinStart->addTouchEventListener(CC_CALLBACK_2(UISlotCleopatraFreeSpinStart::onTouchCloseUIListener, this));

    setTraversalCascadeOpacityEnabled(_mainUINode, true);

    _pBMFFreeSpinCounter = _pLayoutFreeSpinStart->getChildByName<TextBMFont*>("freeSpinCounterLabel");
    CCASSERT(_pBMFFreeSpinCounter, "");
    _pBMFFreeSpinCounter->setString("0");

    return true;
}

void UISlotCleopatraFreeSpinStart::destroyUI()
{
    _pLayoutFreeSpinStart->removeFromParent();
    _pLayoutFreeSpinStart = nullptr;
}

void UISlotCleopatraFreeSpinStart::updateUI()
{
    if (_freeSpinCounter <= 0)
        return;

    _pBMFFreeSpinCounter->setString(Value(_freeSpinCounter).asString());

    if (_autoCloseUITime > 0.0f)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(_autoCloseUITime));
        arrayOfActions.pushBack(CallFunc::create([this](){ this->closeUI(); }));
        auto actionSeq = Sequence::create(arrayOfActions);
        _pLayoutFreeSpinStart->runAction(actionSeq);
    }
}

void UISlotCleopatraFreeSpinStart::Update(float dt)
{
}

void UISlotCleopatraFreeSpinStart::onTouchCloseUIListener(Ref* sender, ui::Widget::TouchEventType event)
{
    if (event != Widget::TouchEventType::ENDED)
        return;

    _pLayoutFreeSpinStart->stopAllActions();
    closeUI();
}