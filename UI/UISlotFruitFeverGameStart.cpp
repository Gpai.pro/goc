#include "UISlotFruitFeverGameStart.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Animation/CustomActions.h"
#include "../DataCenter/ExternalTable.h"
#include "AudioManager.h"


using namespace cocostudio;
using namespace ui;

#define EFF_SLOT5_FREESPIN_ENTER 5105

UISlotFruitFeverGameStart* UISlotFruitFeverGameStart::create()
{
    UISlotFruitFeverGameStart* unit = new UISlotFruitFeverGameStart();

    if (unit && unit->init())
    {
        return unit;
    }

    CC_SAFE_DELETE(unit);
    return nullptr;
}

bool UISlotFruitFeverGameStart::init()
{
    return true;
}

UISlotFruitFeverGameStart::UISlotFruitFeverGameStart()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotFruitFeverGameStart::~UISlotFruitFeverGameStart()
{

}

void UISlotFruitFeverGameStart::notifyOpenUI()
{
    AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT5_FREESPIN_ENTER, false);

    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
}

void UISlotFruitFeverGameStart::notifyCloseUI()
{
    _pLayoutFreeSpinStart->stopAllActions();
    if (_onCloseUICB)
    {
        _onCloseUICB();
        _onCloseUICB = nullptr;
    }
}

bool UISlotFruitFeverGameStart::createUI()
{
    _pLayoutFreeSpinStart = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("UISlot_DL_Machine0007_FruitMachine/UISlot_FruitMachine_Winnings.ExportJson"));
    _pLayoutFreeSpinStart->addTouchEventListener(CC_CALLBACK_2(UISlotFruitFeverGameStart::onTouchCloseUIListener, this));
    _pLayoutFreeSpinStart->setTouchEnabled(true);
    _mainUINode->addChild(_pLayoutFreeSpinStart);

    setTraversalCascadeOpacityEnabled(_mainUINode, true);

    _pActionBell = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_DL_Machine0007_FruitMachine/UISlot_FruitMachine_Winnings.ExportJson", "Animation_bell");
    _pActionBell->updateToFrameByTime(0);

    return true;
}

void UISlotFruitFeverGameStart::destroyUI()
{
    _pLayoutFreeSpinStart->removeFromParent();
    _pLayoutFreeSpinStart = nullptr;
}

void UISlotFruitFeverGameStart::updateUI()
{
    if (_autoCloseUITime > 0.0f)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(_autoCloseUITime));
        arrayOfActions.pushBack(CallFunc::create([this](){ this->closeUI(); }));
        auto actionSeq = Sequence::create(arrayOfActions);
        actionSeq->setTag(TAG_CLOSEUI);
        _pLayoutFreeSpinStart->runAction(actionSeq);
    }

    _pActionBell->updateToFrameByTime(0);
    _pActionBell->play();
}

void UISlotFruitFeverGameStart::Update(float dt)
{
}

void UISlotFruitFeverGameStart::onTouchCloseUIListener(Ref* sender, ui::Widget::TouchEventType event)
{
    if (event != Widget::TouchEventType::ENDED)
        return;
    _pLayoutFreeSpinStart->stopActionByTag(TAG_CLOSEUI);
    closeUI();
}