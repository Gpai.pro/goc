#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "UIBasicUnit.h"
#include "cocostudio/CocoStudio.h"

using namespace cocos2d;

class UISlotFruitFeverGameStart : public UIBasicUnit
{
public:
    const int TAG_CLOSEUI = 1;

    static UISlotFruitFeverGameStart* create();

    UISlotFruitFeverGameStart();
    ~UISlotFruitFeverGameStart();

    virtual void destroyUI() override;

    virtual void notifyOpenUI() override;

    virtual void notifyCloseUI() override;

    virtual void updateUI() override;

   // void setFreeSpinCounter(int freeSpinCounter){ _freeSpinCounter = freeSpinCounter; };

    void setAutoCloseUITime(int time){ _autoCloseUITime = time; };

   // void setOnCloseUICB(std::function<void()> cb){ _onCloseUICB = cb; };

    void reset()
    {
        _autoCloseUITime = 0.0f;
    }

    void onTouchCloseUIListener(Ref* sender, ui::Widget::TouchEventType event);

private:
    bool init();

    virtual bool createUI() override;

    virtual void Update(float dt) override;

private:
    ui::Layout* _pLayoutFreeSpinStart = nullptr;
    cocostudio::ActionObject* _pActionBell = nullptr;

    std::function<void()> _onCloseUICB = nullptr;

    float _autoCloseUITime = 0.0f;
    int _slotID = 5;
};