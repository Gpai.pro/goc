#include "UISlotFruitPayTable.h"
#include "../DataCenter/ExternalTable.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/ImageDefine.h"
#include "CustomRichText.h"
#include "AudioManager.h"


UISlotFruitPayTable* UISlotFruitPayTable::create()
{
    UISlotFruitPayTable* uiPayTable = new UISlotFruitPayTable();

    if (uiPayTable && uiPayTable->init())
    {
        return uiPayTable;
    }

    CC_SAFE_DELETE(uiPayTable);
    return nullptr;
}

UISlotFruitPayTable::UISlotFruitPayTable()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
}

UISlotFruitPayTable::~UISlotFruitPayTable()
{

}

void UISlotFruitPayTable::destroyUI()
{

}

void UISlotFruitPayTable::updateUI()
{
    _nowPage = 1;
    _pages[0]->setVisible(true);

    for (int i = 1; i < TOTAL_PAGE_COUNT; ++i)
    {
        _pages[i]->setVisible(false);
    }

    setLeftButtonVisible(false);
    setRightButtonVisible(true);

    // setting
    //setPage1();
    //setPage2();
    //setPage3();
    //setPage4();
}

bool UISlotFruitPayTable::openUI(int slotID)
{
    setSlotMachineID(slotID);

    if (!UIBasicUnit::openUI())
    {
        return false;
    }

    return true;
}

void UISlotFruitPayTable::setSlotMachineID(int ID)
{
    _slotMachineID = ID;

    SlotMachineData* pData = ExternalTable<SlotMachineData>::getRecordByID(_slotMachineID);
    if (!pData)
    {
        CCLOG("UISlotFruitPayTable Load SlotMachineDate ID = %d Error!!!", _slotMachineID);
        return;
    }

    _gameSymbolBeginID = pData->GameSymbolBeginID;
    _gameSymbolEndID = pData->GameSymbolEndID;

    _symbolInfoBeginID = pData->SymbolInfoBeginID;
    _symbolInfoEndID = pData->SymbolInfoEndID;

    _payLineBeginID = pData->PayLineBeginID;
    _payLineEndID = pData->PayLineEndID;
}

bool UISlotFruitPayTable::init()
{
    return true;
}

bool UISlotFruitPayTable::createUI()
{
    _payTable = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_DL_Machine0007_FruitMachine/UISlot_FruitMachine_Paytable.ExportJson"));
    _mainUINode->addChild(_payTable);
    setUIZOrder(ELZO_NORMAL);

    auto buttonLayout = _payTable->getChildByName<Layout*>("Button");
    buttonLayout->setLocalZOrder(50);

    _backButton = buttonLayout->getChildByName<Button*>("btn_back_to_game");
    _backButton->addTouchEventListener(CC_CALLBACK_2(UISlotFruitPayTable::touchBackToGame, this));
    _backButton->setSoundID(EFF_COMMON_CLOSE_WINDOW);

    _buttonRight = buttonLayout->getChildByName<Button*>("btn_right");
    _buttonRight->addTouchEventListener(CC_CALLBACK_2(UISlotFruitPayTable::touchRightButton, this));

    _buttonLeft = buttonLayout->getChildByName<Button*>("btn_left");
    _buttonLeft->addTouchEventListener(CC_CALLBACK_2(UISlotFruitPayTable::touchLeftButton, this));

    _attachment1 = buttonLayout->getChildByName<ImageView*>("img_chain_01");

    _attachment2 = buttonLayout->getChildByName<ImageView*>("img_chain_02");

    setLeftButtonVisible(false);

    // Page_01
    _pages[0] = _payTable->getChildByName<Layout*>("Page_1");
    _pages[0]->setVisible(true);

    // Page_02
    _pages[1] = _payTable->getChildByName<Layout*>("Page_2");
    _pages[1]->setVisible(false);

    // Page_03
    _pages[2] = _payTable->getChildByName<Layout*>("Page_3");
    _pages[2]->setVisible(false);

    for (int i = 0; i < 4; ++i)
    {
        _page3Texts[i] = _pages[2]->getChildByName<Text*>(StringUtils::format("txt_0%d", i + 1));
        _page3Texts[i]->setString(safeGetStringData(10045 + i));
    }

    // Page_04
    _pages[3] = _payTable->getChildByName<Layout*>("Page_4");
    _pages[3]->setVisible(false);

    for (int i = 0; i < 2; ++i)
    {
        _page4Texts[i] = _pages[3]->getChildByName<Text*>(StringUtils::format("txt_0%d", i + 1));
        _page4Texts[i]->setString(safeGetStringData(10049 + i));
    }

    //_stickyRule = _pages[3]->getChildByName<Text*>("lbl_content");

    _pages[4] = _payTable->getChildByName<Layout*>("Page_5");
    _pages[4]->setVisible(false);

    for (int i = 0; i < 2; ++i)
    {
        _page5Texts[i] = _pages[4]->getChildByName<Text*>(StringUtils::format("txt_0%d", i + 1));
        _page5Texts[i]->setString(safeGetStringData(10051 + i));
    }

    return true;
}

void UISlotFruitPayTable::setLeftButtonVisible(bool visible)
{
    _buttonLeft->setVisible(visible);
    if (_attachment2)
        _attachment2->setVisible(visible);
}

void UISlotFruitPayTable::setRightButtonVisible(bool visible)
{
    _buttonRight->setVisible(visible);

    if (_attachment1)
        _attachment1->setVisible(visible);
}

void UISlotFruitPayTable::setButtonTouchEnable(bool enable)
{
    _buttonLeft->setTouchEnabled(enable);
    _buttonRight->setTouchEnabled(enable);
    _backButton->setTouchEnabled(enable);

    if (_attachment2)
        _attachment2->setVisible(enable);
    if (_attachment1)
        _attachment1->setVisible(enable);
}

void UISlotFruitPayTable::touchBackToGame(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeUI();
}

void UISlotFruitPayTable::touchRightButton(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_nowPage >= TOTAL_PAGE_COUNT)
        return;

    _pages[_nowPage - 1]->setVisible(false);
    _pages[_nowPage]->setVisible(true);

    /* setButtonTouchEnable(false);
    _pages[_nowPage - 1]->runAction(MoveTo::create(0.7f, Vec2(1400.0f, _pages[_nowPage - 1]->getPositionY())));
    _mainUINode->runAction(Sequence::create(DelayTime::create(0.7f), CallFunc::create(CC_CALLBACK_0(UISlotFruitPayTable::setButtonTouchEnable, this, true)), nullptr));*/

    ++_nowPage;
    setLeftButtonVisible(true);

    if (_nowPage >= TOTAL_PAGE_COUNT)
        setRightButtonVisible(false);
}

void UISlotFruitPayTable::touchLeftButton(Ref* pSender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    if (_nowPage <= 1)
        return;

    _pages[_nowPage - 1]->setVisible(false);
    _pages[_nowPage - 2]->setVisible(true);

    /*  setButtonTouchEnable(false);
    _pages[_nowPage - 2]->runAction(MoveTo::create(0.7f, Vec2(0.0f, _pages[_nowPage - 1]->getPositionY())));
    _mainUINode->runAction(Sequence::create(DelayTime::create(0.7f), CallFunc::create(CC_CALLBACK_0(UISlotFruitPayTable::setButtonTouchEnable, this, true)), nullptr));*/

    --_nowPage;
    setRightButtonVisible(true);

    if (_nowPage <= 1)
        setLeftButtonVisible(false);
}