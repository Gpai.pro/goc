#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "UIBasicUnit.h"
#include "cocostudio/CocoStudio.h"


USING_NS_CC;

using namespace ui;
using namespace cocostudio;

class UISlotFruitPayTable : public UIBasicUnit
{
private:
    static const int TOTAL_PAGE_COUNT = 5;

public:


public:
    static UISlotFruitPayTable* create();

public:
    UISlotFruitPayTable();
    ~UISlotFruitPayTable();

    virtual void destroyUI() override;

    virtual void updateUI() override;

    bool openUI(int slotID);

    void setSlotMachineID(int ID);

private:
    bool init();

    virtual bool createUI() override;

    void setLeftButtonVisible(bool visible);

    void setRightButtonVisible(bool visible);

    void setButtonTouchEnable(bool enable);

    void touchBackToGame(Ref* pSender, Widget::TouchEventType type);

    void touchRightButton(Ref* pSender, Widget::TouchEventType type);

    void touchLeftButton(Ref* pSender, Widget::TouchEventType type);



private:
    Layout* _payTable = nullptr;

    Button* _backButton = nullptr;

    Button* _buttonLeft = nullptr;

    Button* _buttonRight = nullptr;

    ImageView* _attachment1 = nullptr;
    ImageView* _attachment2 = nullptr;

    int _nowPage = 1;

    int _slotMachineID;

    int _gameSymbolBeginID;

    int _gameSymbolEndID;

    int _symbolInfoBeginID;

    int _symbolInfoEndID;

    int _payLineBeginID;

    int _payLineEndID;

    Layout* _pages[TOTAL_PAGE_COUNT];

    // Page3
    Text* _page3Texts[4];

    // Page4
    Text* _page4Texts[2];

    // Page5
    Text* _page5Texts[2];
};