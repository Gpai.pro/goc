#include "UISlotSuperClassic.h"
#include "UISlotSuperClassicFreeSpinStart.h"
#include "UISlotSuperClassicFreeSpinResult.h"
#include "UIController.h"
#include "UIBigWin.h"
#include "UIFiveOfAKind.h"
#include "UIJackpot.h"
#include "UILevelup.h"
#include "UIOutOfCoin.h"
#include "UIPayTable.h"
#include "UIMessage.h"
#include "../GlobalDefine/Player.h"
#include "../GlobalDefine/GameFunc.h"
#include "../DataCenter/ExternalTable.h"
#include "../Network/MessageProcess.h"
#include "../GlobalDefine/ImageDefine.h"
#include "../Animation/CustomActions.h"
#include "../Animation/CustomSpAnim.h"
#include "../Animation/ProcedureFX.h"
#include "AudioManager.h"
#include "../Shader/ShaderManager.h"
#include "../Gameplay/GameLogic.h"
#include "UIAutoSpinSetting.h"

int UISlotSuperClassic::s_autoCountValue[AUTOCOUNTLEVEL] = { 10, 50, 100 };
const float UISlotSuperClassic::EDIDLINEBTN_HOLDTIME = 1.2f;
const float UISlotSuperClassic::LINEANIMTIME = 4.0f;
const float UISlotSuperClassic::DROPDOWNLIST_FADETIME = 0.2f;
const float UISlotSuperClassic::PERFORMSPIN_MINTIME = 1.0f;
const Color3B UISlotSuperClassic::BETSELECT_DARKCOLOR = Color3B(128, 128, 128);
const Color3B UISlotSuperClassic::PAYLINEBG_DARKCOLOR = Color3B(100, 100, 100);
const Color4B UISlotSuperClassic::PAYLINEBG_DARKCOLOR_WITHEXPECTBG = Color4B(255, 255, 255, 80);
const Size SYMBOLSIZE = Size(140, 140);

#define EFF_SLOT4_BAR_BEGIN     4003    //拉霸前音
#define EFF_SLOT4_BAR_MIDDLE    4004    //拉霸中音
#define EFF_SLOT4_BAR_END       4005    //拉霸後音
#define EFF_TOUCH_DEALER        4008    //戳荷官

UISlotSuperClassic* UISlotSuperClassic::create(int slotMachineID)
{
    UISlotSuperClassic* _ui = new UISlotSuperClassic();

    if (_ui && _ui->init(slotMachineID))
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UISlotSuperClassic::UISlotSuperClassic()
{
    setUIShowType(EUISHOWTYPE::EUIST_SLOTMACHINE);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotSuperClassic::~UISlotSuperClassic()
{

}

void UISlotSuperClassic::destroyUI()
{

}

void UISlotSuperClassic::notifyOpenUI()
{
    AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

void UISlotSuperClassic::notifyCloseUI()
{
    stopAutoSpin();
    performStopSuddendly();
    stopPerformSpinResult();

    SlotMachineInfo info = Player::getInstance()->getSlotMachineInfo(_slotID);
    info.freeSpin = _freeSpinCount;
    Player::getInstance()->setSlotMachineInfo(_slotID, info);

    _pLayoutArrow->setVisible(true);

    closeDropDownList();
}

void UISlotSuperClassic::updateUI()
{
    //play music
    SlotMachineInfo machineInfo = Player::getInstance()->getSlotMachineInfo(_slotID);

    //update jackpot
    setJackpot(machineInfo.jackpot);
    setJPEnable(machineInfo.isJackpotEnable);
    updateJackpotHint();

    //update Bet
    setMaxLineAndBet();

    _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    if (_pSlotData->DefaultBet>0)
    {
        int index = getBetIndexByBetValue(_pSlotData->DefaultBet);
        setCurBet(index);
    }

    // update FreeSpin
    if (machineInfo.freeSpin > 0)
    {
        showFreeSpinCount(true);
        setFreeSpinCount(machineInfo.freeSpin);
        setAutoCount(machineInfo.freeSpin);
        setAutoSpinStatus(AUTOSPINSTATUS_ON);
        setFreeSpinStatus(FREESPINSTATUS_ON);
        AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
    }
    else
    {
        showFreeSpinCount(false);
        setFreeSpinCount(machineInfo.freeSpin);
        setAutoCount(machineInfo.freeSpin);
        setAutoSpinStatus(AUTOSPINSTATUS_OFF);
        setFreeSpinStatus(FREESPINSTATUS_OFF);
    }

    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);

    // set win 0
    setWin(0);
}

void UISlotSuperClassic::Update(float dt)
{
    float totalTime = _pActionSpinBar->getTotalTime();
    float curTime = _pActionSpinBar->getCurrentTime();
    switch (_spinBarTouchType)
    {
    case cocos2d::ui::Widget::TouchEventType::BEGAN:
        break;
    case cocos2d::ui::Widget::TouchEventType::MOVED:
        _pActionSpinBar->updateToFrameByTime(totalTime * _spinBarPercent);
        break;
    case cocos2d::ui::Widget::TouchEventType::ENDED:
    case cocos2d::ui::Widget::TouchEventType::CANCELED:
        _pLayoutSpinBarBegin->setTouchEnabled(false);

        // 回彈
        if (curTime > 0.0f)
        {
            if (_spinBarTouchBottom)
                _pActionSpinBar->updateToFrameByTime(curTime - 0.5f);
            else
                _pActionSpinBar->updateToFrameByTime(curTime - 0.3f);
        }
        else
        {
            _pActionSpinBar->updateToFrameByTime(0.0f);
            _pLayoutSpinBarBegin->setTouchEnabled(true);

            _spinBarTouchBottom = false;
            _spinBarPercent = 0.0f;
        }

        if (_spinBarPercent > 0.85f)
        {
            if (_autoSpinStatus == AUTOSPINSTATUS_ON || _freeSpinStatus == FREESPINSTATUS_ON)
            {
                return;
            }

            if (_buttonStatus != SLOTBTNSTATUS_IDLE)
            {
                return;
            }

            stopPerformSpinResult();
            closeDropDownList();
            requestSpin();
            AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT4_BAR_END, false);
            _spinBarTouchBottom = true;
        }

        _spinBarPercent = 0.0f;

        break;
    default:
        break;
    }
}

void UISlotSuperClassic::UpdatePerSecond(float dt)
{
    auto info = Player::getInstance()->getSlotMachineInfo(_slotID);
    setJackpot(info.jackpot);

    if (_buttonStatus == SLOTBTNSTATUS_ONCELINEANDSTOREUI)  //正在播中獎結果視窗storeUI
    {
        if (!isPerformSpinResult(RESULTTYPE_BTNSTATUSIDLE)) //已播完中獎結果視窗storeUI,可以開始下一次spin
            setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
    }

    if (_pActionEnterAnim->isPlaying() || UIController::getInstance()->getController(EUITAG_UITRANSITIOIN)->isOpen())
        return;

    if (_autoSpinStatus == AUTOSPINSTATUS_ON && _buttonStatus == SLOTBTNSTATUS_IDLE)
    {
        //正在播AutoSpin結果
        if (!isPerformSpinResult(RESULTTYPE_AUTOSPIN))
        {
            stopPerformSpinResult();
            if (isOpen())
                AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
            requestSpin();
        }
    }
}

void UISlotSuperClassic::playEnterUIAction()
{
    _pActionEnterAnim->play();
}

bool UISlotSuperClassic::init(int slotMachineID)
{
    //set model data
    _slotID = slotMachineID;
    _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);

    //設定PayLine最大值, 最小值
    _maxLines = _pSlotData->PayLineEndID - _pSlotData->PayLineBeginID + 1;
    _minLines = (_pSlotData->isLockedPayLine == VALUESENSE_TRUE) ? _maxLines : 1;
    _beginPayLineID = _pSlotData->PayLineBeginID;
    _endPayLineID = _pSlotData->PayLineEndID;

    _isFixedBet = _pSlotData->FixedBetBeginID > 0;
    if (_isFixedBet)
    {
        for (int i = _pSlotData->FixedBetBeginID; i <= _pSlotData->FixedBetEndID; i++)
        {
            FixedBetData* pFixBetData = ExternalTable<FixedBetData>::getRecordByID(i);
            if (pFixBetData)
                _betLevelList.push_back(pFixBetData->Bet);
        }
    }
    else
    {
        std::map<int, LevelUpData*>& table = ExternalTable<LevelUpData>::raw();
        for (auto itr : table)
        {
            LevelUpData* curData = itr.second;
            if (curData->MaxBet)
                _betLevelList.push_back(curData->MaxBet);
        }
    }

    _scatterID = _bonusID = 0;
    for (int id = _pSlotData->SymbolInfoBeginID; id <= _pSlotData->SymbolInfoEndID; id++)
    {
        SymbolInfoData* pSymbolInfo = ExternalTable<SymbolInfoData>::getRecordByID(id);
        if (!pSymbolInfo)
            continue;
        if (pSymbolInfo->Type == SymbolInfoData::SYMBOLTYPE_SCATTER)
            _scatterID = pSymbolInfo->ID;
        else if (pSymbolInfo->Type == SymbolInfoData::SYMBOLTYPE_BONUS)
            _bonusID = pSymbolInfo->ID;
    }
    _minScatterCombo = _minBonusCombo = 0;
    _minScatterCombo = UISlotUtils::getMinHitCombo(_slotID, _scatterID);
    _minBonusCombo = UISlotUtils::getMinHitCombo(_slotID, _bonusID);

    _reelCount = _pSlotData->ReelEndID - _pSlotData->ReelBeginID + 1;

    return true;
}

bool UISlotSuperClassic::createUI()
{
    pRoot = static_cast<ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("GUI/UISlot_SuperClassic/UISlot_SuperClassic.ExportJson"));
    CCASSERT(pRoot, "");
    _mainUINode->addChild(pRoot);
    //Layout* pLayoutSceneBg = static_cast<Layout*>(pRoot->getChildByName("layout_scenebg"));
    //CCASSERT(pLayoutSceneBg, "");

    Layout* pLayoutSlotBackground = static_cast<Layout*>(pRoot->getChildByName("layout_slot_background"));
    CCASSERT(pLayoutSlotBackground, "");

    _pLayoutSlotReel = static_cast<Layout*>(getChildByNodePath(pRoot, "layout_slot_reel"));
    CCASSERT(_pLayoutSlotReel, "");

    Layout* pLayoutSlotForeground = static_cast<Layout*>(pRoot->getChildByName("layout_slot_foreground"));
    CCASSERT(pLayoutSlotForeground, "");

    Layout* pLayoutControls = static_cast<Layout*>(pRoot->getChildByName("layout_controls"));
    CCASSERT(pLayoutControls, "");

    _pLayoutArrow = pRoot->getChildByName<Layout*>("layout_arrow");

    //Layout* pLayoutSceneFg = static_cast<Layout*>(pRoot->getChildByName("layout_scenefg"));
    //CCASSERT(pLayoutSceneFg, "");

    //_pLayoutSeparator = static_cast<Layout*>(pLayoutSlotBackground->getChildByName("layout_separators"));
    //CCASSERT(_pLayoutSeparator, "");
    //setTraversalCascadeColorEnabled(_pLayoutSeparator, true);

    _pLayoutFreeSpin = static_cast<Layout*>(pLayoutControls->getChildByName("layout_freespin"));
    CCASSERT(_pLayoutFreeSpin, "");
    setTraversalCascadeOpacityEnabled(_pLayoutFreeSpin, true);
    _pLayoutFreeSpin->setVisible(true);
    _pLayoutFreeSpin->setOpacity(0);

    _pBMFFreeSpinRemain = static_cast<TextBMFont*>(_pLayoutFreeSpin->getChildByName("bmf_value_freespin"));
    CCASSERT(_pBMFFreeSpinRemain, "");
    _pBMFFreeSpinRemain->setString("");

    _pBMFWinMoneyAmin = static_cast<TextBMFont*>(pLayoutSlotForeground->getChildByName("bmf_winmoney_anim"));
    _pBMFWinMoneyAmin->setOpacity(0);
    _pLayoutDummyWinMoneyParticle = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_dummy_winmoney_particle"));

    //expectBg
    Layout* _pLayoutExpect = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_expect"));
    CCASSERT(_pLayoutExpect, "");
    for (int i = 0; i < _reelCount; i++)
    {
        ExpectBGGroup bgGroup;
        bgGroup.pParticles = static_cast<Layout*>(_pLayoutExpect->getChildByName(StringUtils::format("layout_expectbg_particle_%d", i + 1)));
        _expectBgs.push_back(bgGroup);
    }
    CCASSERT(_expectBgs.size() == _reelCount, "");
    _isExpectBgSoundPlayed.clear();
    for (int i = 0; i < _reelCount; i++)
        _isExpectBgSoundPlayed.push_back(false);
    _isMiddleExpectSoundPlayed.clear();
    for (int i = 0; i < _reelCount; i++)
        _isMiddleExpectSoundPlayed.push_back(false);
    _middleExpectSoundHandle = 0;

    //clipping layout for reels
    _pLayoutReels = static_cast<Layout*>(_pLayoutSlotReel->getChildByName("layout_reels"));
    _pLayoutReels->setClippingEnabled(true);
    _pLayoutReels->setClippingType(Layout::ClippingType::SCISSOR);

    _pLayoutSymbolAnim = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_dummy_symbolanim"));
    CCASSERT(_pLayoutSymbolAnim, "");

    Layout* pLayoutDummySceneobj = static_cast<Layout*>(pLayoutSlotForeground->getChildByName("layout_dummy_sceneobj"));
    CCASSERT(pLayoutDummySceneobj, "");

    _sceneObjDealer = CustomSpAnim::createWithFile("UISlot_SuperClassic/Spine/super_obj_sp_dealerg", "");
    //_sceneObjDealer->setMix("idle01", "win01", 0.2f);
    //_sceneObjDealer->setMix("idle01", "win02", 0.2f);
    //_sceneObjDealer->setMix("win01", "idle01", 0.5f);
    //_sceneObjDealer->setMix("win02", "idle01", 0.1f);
    _sceneObjDealer->setCompleteListener(CC_CALLBACK_2(UISlotSuperClassic::sceneObjAnimCompleteCB, this));
    pLayoutDummySceneobj->addChild(_sceneObjDealer);
    performSceneAnim(ESuperClassicSceneAnim::SCENEANIM_IDLE);

    _pLayoutSceneObjListener = pLayoutSlotForeground->getChildByName<Layout*>("layout_sceneobj_listener");
    _pLayoutSceneObjListener->setTouchEnabled(true);
    _pLayoutSceneObjListener->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onTouchSceneListener, this));
    


    iniPayLineComponent();

    for (int i = 0; i < _reelCount; ++i)
    {
        ReelLight reelLight;

        reelLight._light1 = pLayoutSlotForeground->getChildByName<ImageView*>(StringUtils::format("CoverLight_%d_1", i + 1));
        reelLight._light2 = pLayoutSlotForeground->getChildByName<ImageView*>(StringUtils::format("CoverLight_%d_2", i + 1));
        reelLight._light3 = pLayoutSlotForeground->getChildByName<ImageView*>(StringUtils::format("CoverLight_%d_3", i + 1));
        reelLight._light4 = pLayoutSlotForeground->getChildByName<ImageView*>(StringUtils::format("CoverLight_%d_4", i + 1));

        _reelLights.push_back(reelLight);
    }
    stopAllLight();

    //setup event of UI Component
    //_pImgSpinText = static_cast<ImageView*>(pLayoutControls->getChildByName("img_spintext"));
    //CCASSERT(_pImgSpinText, "");
    //_pImgStopText = static_cast<ImageView*>(pLayoutControls->getChildByName("img_stoptext"));
    //CCASSERT(_pImgStopText, "");

    _pLayoutAutoDisable = static_cast<Layout*>(pLayoutControls->getChildByName("layout_auto_disable"));
    CCASSERT(_pLayoutAutoDisable, "");

    _pBMFAutoCount = static_cast<TextBMFont*>(_pLayoutAutoDisable->getChildByName("bmf_autocount"));
    CCASSERT(_pBMFAutoCount, "");
    _pBMFAutoCount->setRotation3D(Vec3(-50.0f, 0.0f, 0.0f));

    _pBtnSpin = static_cast<Button*>(pLayoutControls->getChildByName("btn_spin"));
    CCASSERT(_pBtnSpin, "");
    _pBtnSpin->setTouchEnabled(false);
    _pLayoutSpinListener = pLayoutControls->getChildByName<Layout*>("layout_spin_listener");
    _pLayoutSpinListener->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onSpinBtnTouchEvent, this));

    _pBtnStop = pLayoutControls->getChildByName<Button*>("btn_stop");
    CCASSERT(_pBtnStop, "");
    _pBtnStop->setTouchEnabled(false);

    _pBtnAuto = static_cast<Button*>(pLayoutControls->getChildByName("btn_auto"));
    CCASSERT(_pBtnAuto, "");
    _pBtnAuto->setTouchEnabled(false);
    _pLayoutAutoListener = pLayoutControls->getChildByName<Layout*>("layout_auto_listener");
    _pLayoutAutoListener->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onAutoBtnClick, this));
    _pLayoutAutoListener->setVisible(true);
    _pLayoutAutoListener->setSoundID(EFF_COMMON_BUTTON_CLICK);

    _pBtnMaxBet = static_cast<Button*>(pLayoutControls->getChildByName("btn_maxbet"));
    CCASSERT(_pBtnMaxBet, "");
    _pBtnMaxBet->setTouchEnabled(false);
    _pLayoutMaxBetListener = pLayoutControls->getChildByName<Layout*>("layout_maxbet_listener");
    _pLayoutMaxBetListener->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onMaxBetBtnClick, this));
    _pLayoutMaxBetListener->setVisible(true);

    _pBtnBet = static_cast<Button*>(pLayoutControls->getChildByName("btn_bet"));
    CCASSERT(_pBtnBet, "");
    _pBtnBet->setTouchEnabled(false);
    _pLayoutBetListener = pLayoutControls->getChildByName<Layout*>("layout_bet_listener");
    _pLayoutBetListener->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onBetBtnClick, this));
    _pLayoutBetListener->setVisible(true);
    _pLayoutBetListener->setSoundID(EFF_COMMON_BUTTON_CLICK);

    _pBtnPayTable = static_cast<Button*>(pLayoutControls->getChildByName("btn_paytable"));
    CCASSERT(_pBtnPayTable, "");
    _pBtnPayTable->setTouchEnabled(false);
    _pLayoutPaytableListener = pLayoutControls->getChildByName<Layout*>("layout_paytable_listener");
    _pLayoutPaytableListener->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onPayTableBtnClick, this));
    _pLayoutPaytableListener->setSoundID(EFF_COMMON_BUTTON_CLICK);

    _pLayoutCloseDropdownListener = static_cast<Layout*>(pLayoutControls->getChildByName("layout_closedropdown_listener"));
    _pLayoutCloseDropdownListener->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onDropDownListBGTouch, this));

    _pLayoutAutoDropdownList = static_cast<Layout*>(pLayoutControls->getChildByName("layout_dropdown_auto"));
    CCASSERT(_pLayoutAutoDropdownList, "");
    setTraversalCascadeOpacityEnabled(_pLayoutAutoDropdownList, true);

    for (int i = 0; i < AUTOCOUNTLEVEL; ++i)
    {
        auto btnAuto = _pLayoutAutoDropdownList->getChildByName<Button*>(StringUtils::format("btn_autonum_%d", i + 1));
        CCASSERT(btnAuto, "");
        btnAuto->setSoundID(EFF_NONE);
        btnAuto->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onAutoCountValueClick, this, i));
        btnAuto->setTitleText(safeGetStringData(681 + i));
    }

    auto btnAutoUnlimited = _pLayoutAutoDropdownList->getChildByName<Button*>("btn_autonum_4");
    CCASSERT(btnAutoUnlimited, "");
    btnAutoUnlimited->setSoundID(EFF_NONE);
    btnAutoUnlimited->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onAutoUnlimitedClick, this));
    btnAutoUnlimited->setTitleText(safeGetStringData(684));

    auto btnAutoSpinSetting = _pLayoutAutoDropdownList->getChildByName<Button*>("btn_autonum_5");
    CCASSERT(btnAutoSpinSetting, "");
    btnAutoSpinSetting->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onAutoSpinSetting, this));
    btnAutoSpinSetting->setTitleText(safeGetStringData(685));

    _pLayoutBetDropdownList = static_cast<Layout*>(pLayoutControls->getChildByName("layout_dropdown_bet"));
    CCASSERT(_pLayoutBetDropdownList, "");

    _pNextBtn = static_cast<Button*>(_pLayoutBetDropdownList->getChildByName("btn_next_bet"));
    CCASSERT(_pNextBtn, "");
    _pNextBtn->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onNextBetBtnClick, this));

    _pPreviousBtn = static_cast<Button*>(_pLayoutBetDropdownList->getChildByName("btn_previous_bet"));
    CCASSERT(_pPreviousBtn, "");
    _pPreviousBtn->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onPreviousBetBtnClick, this));

    Layout* pLayoutBetLevel = static_cast<Layout*>(_pLayoutBetDropdownList->getChildByName("layout_betlevel"));
    CCASSERT(pLayoutBetLevel, "");

    _pListBetLevels = static_cast<ListView*>(_pLayoutBetDropdownList->getChildByName("list_betlevel_list"));
    CCASSERT(_pListBetLevels, "");
    _pListBetLevels->setTouchEnabled(false);
    _pListBetLevels->setScrollBarEnabled(false);
    _pListBetLevels->setBounceEnabled(true);
    _pListBetLevels->setGravity(ListView::Gravity::RIGHT);

    _pBMFWinNum = dynamic_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_win_num"));
    CCASSERT(_pBMFWinNum, "");

    _pBMFTotlaBetNum = static_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_totalbet_num"));
    CCASSERT(_pBMFTotlaBetNum, "");

    _pBMFCurrentBet = static_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_currentbet_num"));
    CCASSERT(_pBMFCurrentBet, "");
    _currentBetBMFPos = _pBMFCurrentBet->getPosition();
    _pBMFCurrentBet->setRotation3D(Vec3(-50.0f, 0.0f, 0.0f));

    _pBMFCurrentLines = static_cast<TextBMFont*>(pLayoutControls->getChildByName("bmf_currentlines_num"));
    CCASSERT(_pBMFCurrentLines, "");

    _pLayoutJPBanner = static_cast<Layout*>(pLayoutControls->getChildByName("layout_jpbanner"));
    CCASSERT(_pLayoutJPBanner, "");
    _pBMFJPNum = static_cast<TextBMFont*>(_pLayoutJPBanner->getChildByName("bmf_jp_num"));
    CCASSERT(_pBMFJPNum, "");

    _pImgJPCheck = _pLayoutJPBanner->getChildByName<ImageView*>("img_check");
    _pImgJPUncheck = _pLayoutJPBanner->getChildByName<ImageView*>("img_uncheck");

    // spin bar
    _pLayoutSpinBarBegin = pLayoutControls->getChildByName<Layout*>("layout_spinbar_begin");
    _pLayoutSpinBarBegin->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onDropSpinBarEvent, this));
    _pLayoutSpinBarEnd = pLayoutControls->getChildByName<Layout*>("layout_spinbar_end");

    //setup initial property of UI Component
    closeDropDownList(false);
    _pLayoutAutoDisable->setVisible(false);

    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
    setAutoSpinStatus(AUTOSPINSTATUS_OFF);
    setFreeSpinStatus(FREESPINSTATUS_OFF);

    //setup Reel
    UIReel* pReel;
    std::vector<SymbolSpec> symbolSpecs;
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_JACKPOT, "super_sym_st_jp.png", SPRITEFRAME, Vec2(24.0f, 0.0f), Size(167.0f, 140.0f), "UISlot_SuperClassic/Spine/super_sym_sp_jp", "", NONEBLUR, "", 0));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_WILD, "super_sym_st_wild.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_wild", "", NONEBLUR, "", 0));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_SCATTER, "super_sym_st_scatter.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_scatter", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_p1.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_p1", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_p2.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_p2", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_p3.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_p3", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_p4.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_p4", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_a.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_a", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_k.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_k", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_q.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_q", "", NONEBLUR, "", 4201));
    symbolSpecs.push_back(SymbolSpec(SymbolInfoData::SYMBOLTYPE_NORMAL, "super_sym_st_j.png", SPRITEFRAME, Vec2::ZERO, SYMBOLSIZE, "UISlot_SuperClassic/Spine/super_sym_sp_j", "", NONEBLUR, "", 4201));
  
    Layout* pLayoutReelDummy(nullptr);
    for (int i = 0; i < _reelCount; i++)
    {
        int reelOrder = i + 1;
        pLayoutReelDummy = static_cast<Layout*>(_pLayoutReels->getChildByName(StringUtils::format("layout_dummy_reel_%d", reelOrder)));
        CCASSERT(pLayoutReelDummy, "");

        pReel = UIReel::createWithReelView(Size(140, 420), 3, symbolSpecs, SYMBOLSIZE);
        if (pReel)
        {
            pReel->setReelOrder(reelOrder);
            pReel->setPosition(pLayoutReelDummy->getPosition());
            pReel->setOnSingleReelEnterMiddleExpectCB(CC_CALLBACK_1(UISlotSuperClassic::onSingleReelEnterMiddleExpectCB, this));
            pReel->setOnSingleReelEnterEndCB(CC_CALLBACK_1(UISlotSuperClassic::onSingleReelEnterEndCB, this));
            pReel->setOnSingleReelPendingStopCB(CC_CALLBACK_1(UISlotSuperClassic::onSingleReelPendingStopCB, this));
            pReel->setOnSingleReelStopCB(CC_CALLBACK_1(UISlotSuperClassic::onSingleReelStopCB, this));
            pReel->setOnSingleReelTriggerStopSoundCB(CC_CALLBACK_1(UISlotSuperClassic::onSingleReelTriggerStopSoundCB, this));
            _pLayoutReels->addChild(pReel);
            _reels.push_back(pReel);
        }
    }

    for (int i = (int)_betLevelList.size() - 1; i >= 0; i--)
    {
        Layout* pPrototypeBtn = static_cast<Layout*>(pLayoutBetLevel->clone());
        Button* pBtn = static_cast<Button*>(pPrototypeBtn->getChildByName("btn_betlevel"));
        ImageView* pJPImg = static_cast<ImageView*>(pPrototypeBtn->getChildByName("img_jpmark"));
        ImageView* pLockImg = static_cast<ImageView*>(pPrototypeBtn->getChildByName("img_lock"));
        pBtn->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassic::onBetListBtnClick, this, i));
        int betValue = _betLevelList.at(i);
        pBtn->setTitleText(GetMoneyFormat(betValue));
        pBtn->setTag(i);
        pPrototypeBtn->setTag(i);
        _pListBetLevels->pushBackCustomItem(pPrototypeBtn);
        pJPImg->setVisible(false);
        pLockImg->setVisible(false);
    }
    setTraversalCascadeOpacityEnabled(_pLayoutBetDropdownList, true);
    pLayoutBetLevel->setVisible(false);

    setCurLines(_maxLines);
    if (_isFixedBet && _betLevelList.size() > 0 )
        setCurBet(_betLevelList.back() - 1);
    else if (!_isFixedBet && Player::getInstance()->getUsableBetLevel().size())
        setCurBet(Player::getInstance()->getUsableBetLevel().size() - 1);
    else
        setCurBet(0);

    // Action
    _pActionEnterAnim = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_SuperClassic/UISlot_SuperClassic.ExportJson", "enter_animation");
    _pActionEnterAnim->updateToFrameByTime(0);
    _pActionSpinBar = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_SuperClassic/UISlot_SuperClassic.ExportJson", "spin_bar");
    _pActionSpinBar->updateToFrameByTime(0);
    _pActionSpinArrow = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_SuperClassic/UISlot_SuperClassic.ExportJson", "spin_arrow");
    _pActionSpinArrow->updateToFrameByTime(0);
    _pActionSpinArrow->play();
    _pActionWinLight = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_SuperClassic/UISlot_SuperClassic.ExportJson", "winning_light");
    _pActionWinLight->updateToFrameByTime(0);
    _pActionSpinLight = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_SuperClassic/UISlot_SuperClassic.ExportJson", "spining_light");
    _pActionSpinLight->updateToFrameByTime(0);
    _pActionNormalLight = ActionManagerEx::getInstance()->getActionByName("GUI/UISlot_SuperClassic/UISlot_SuperClassic.ExportJson", "normal_light");
    _pActionNormalLight->updateToFrameByTime(0);

    setMaxLineAndBet();

    _pListBetLevels->jumpToBottom();
    setAutoCount(0);
    setFreeSpinCount(0);//read from slotMachine Stat
    setWin(0);
    setJackpot(0);
    return true;
}

void UISlotSuperClassic::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    if (!this->isOpen())
        return;

    EMessageTag tagNum = (EMessageTag)Value(requestTag).asInt();
    if (tagNum == EMT_Slot_Spin || tagNum == EMT_Cheat_Codes)
    {
        if (tagNum == EMT_Cheat_Codes && !isScrolling())
        {
            closeDropDownList();
            stopPerformSpinResult();
            performSpin();
            _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
            DelayTime* pMinTimeAction = DelayTime::create(UISlotSuperClassic::PERFORMSPIN_MINTIME);
            pMinTimeAction->setTag(TAG_PERFORMSPIN_MINTIME);
            _mainUINode->runAction(pMinTimeAction);
        }

        int errCode = -1;
        SlotIDList slotIDListRcv;
        SpinResponse_Overall overallRcv;
        std::vector<SpinResponse_PayLine> paylineRcv;
        SpinResponse_JP jpRcv;
        SpinResponse_Win winRcv;
        SpinResponse_FreeSpin freeSpinRcv;
        SpinResponse_Bonus bonusRcv;
        SpinResponse_Bonuswild bonuswildRcv;
        SpinResponse_Profile profileRcv;
        SpinResponse_LevelUp levelupRcv;
        SlotIDList specialSlotListRcv;

        if (!UISlotUtils::parseResponse(
            _slotID,
            json,
            errCode,
            slotIDListRcv,
            overallRcv,
            paylineRcv,
            jpRcv,
            winRcv,
            freeSpinRcv,
            bonusRcv,
            bonuswildRcv,
            profileRcv,
            levelupRcv,
            specialSlotListRcv
            ))
        {
            CCLOG("spin response pasing error");
            stopPerformSpinResult();
            return;
        }

        clearLastResult();
        saveRcv(
            slotIDListRcv,
            overallRcv,
            paylineRcv,
            jpRcv,
            winRcv,
            freeSpinRcv,
            bonusRcv,
            bonuswildRcv,
            profileRcv,
            levelupRcv
            );
        Player::getInstance()->setLevel(profileRcv.level);
        Player::getInstance()->setExp(profileRcv.exp);
        Player::getInstance()->setVipLv(profileRcv.vip_level);
        Player::getInstance()->setVipPoint(profileRcv.vip_point);

        auto info = Player::getInstance()->getSlotMachineInfo(_slotID);
        info.jackpot = _jpRcv.winnings;
        Player::getInstance()->setSlotMachineInfo(_slotID, info);
        setJackpot(_jpRcv.winnings);

        performStopSpinWithResult(slotIDListRcv);
    }
}

bool UISlotSuperClassic::isHitJackpot()
{
    return _jpRcv.jackpot > 0;
}

bool UISlotSuperClassic::isHitScatter()
{
    //現在不在freespin狀態, 但有freespin次數表示進入freeSpin
    return isStartFreeSpin() || isAddFreeSpin();
}

bool UISlotSuperClassic::isHitBonus()
{
    return _bonusRcv.gameID > 0 || _bonuswildRcv.gameID > 0 || _overallRcv.bonus_gameID > 0;
}

bool UISlotSuperClassic::isHitFiveOfAKind()
{
    return _winRcv.fiveOfAKindID > 0;
}

bool UISlotSuperClassic::isHitBigWin()
{
    return _winRcv.win_level > WinLevel::WINLV_NORMAL;
}

bool UISlotSuperClassic::isHit()
{
    return (
        _paylineRcv.size() > 0 ||  //中scatter, 中bonus, 中bonuswild, 中一般符號, 中jp(jp會有假payline)
        _overallRcv.overall > 0 ||  //中overal
        isHitJackpot()              //中jp
        );
}

bool UISlotSuperClassic::isShowBigWin()
{
    return !isHitJackpot() && !isHitBonus() && !isHitScatter() && isHitBigWin();
}

bool UISlotSuperClassic::isLevelUp()
{
    return _levelupRcv.upgrade > 0;
}

bool UISlotSuperClassic::isStartFreeSpin()
{
    //現在不在freespin狀態, 但有freespin次數表示進入freeSpin
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::NORMAL && _freeSpinRcv.remain_count>0;
}

bool UISlotSuperClassic::isEndFreeSpin()
{
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::RESULT;
}

bool UISlotSuperClassic::isAddFreeSpin()
{
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::ADD;
}

bool UISlotSuperClassic::isDoingFreeSpin()
{
    return _freeSpinRcv.action == SpinResponse_FreeSpin::Action::INFREESPIN;
}

void UISlotSuperClassic::closeDropDownList(bool fadeOut)
{
    _pLayoutAutoDropdownList->stopAllActions();
    _pLayoutBetDropdownList->stopAllActions();

    if (fadeOut)
    {
        Sequence* actionsSeq(nullptr);
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.clear();
        arrayOfActions.pushBack(FadeOut::create(DROPDOWNLIST_FADETIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutAutoDropdownList, false)));
        actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutAutoDropdownList->runAction(actionsSeq);

        arrayOfActions.clear();
        arrayOfActions.pushBack(FadeOut::create(DROPDOWNLIST_FADETIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutBetDropdownList, false)));
        actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutBetDropdownList->runAction(actionsSeq);
    }
    else
    {
        _pLayoutAutoDropdownList->setVisible(false);
        _pLayoutAutoDropdownList->setOpacity(0.0f);
        _pLayoutBetDropdownList->setVisible(false);
        _pLayoutBetDropdownList->setOpacity(0.0f);
    }

    if (_pLayoutCloseDropdownListener)
        _pLayoutCloseDropdownListener->setVisible(false);

    hideAllPayline();
}

int UISlotSuperClassic::getAutoCountValueTitleByTag(int tag)
{
    int valueIndex = tag;
    if (valueIndex <= 0 || valueIndex > AUTOCOUNTLEVEL)
        return 0;
    return s_autoCountValue[valueIndex - 1];
}

bool UISlotSuperClassic::isScrolling()
{
    for (auto reel : _reels)
    {
        if (reel && reel->isScrolling())
            return true;
    }
    return false;
}

void UISlotSuperClassic::performSpin()
{
    if (isScrolling())
        return;

    for (auto reel : _reels)
    {
        reel->spin(0.2f, -1, SPINSTYLE_INFINITE_4);
    }

    AudioManager::getInstance()->stopEff(_spinSoundHandle);
    if (isOpen())
        _spinSoundHandle = AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinSoundID, false);

    playSpinLight();
}

void UISlotSuperClassic::performStopSpinWithResult(SlotIDList& result)
{
    //設定reel結果
    int i = 0;
    for (auto reel : _reels)
    {
        reel->setSpinResult(result.at(i));
        i++;
    }

    //開始停輪
    ActionInterval* pMinTimeAction = static_cast<ActionInterval*>(_mainUINode->getActionByTag(TAG_PERFORMSPIN_MINTIME));
    if (pMinTimeAction)
    {
        float waitTime = pMinTimeAction->getDuration() - pMinTimeAction->getElapsed();
        _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
        if (waitTime > 0.0f)
        {
            Vector<FiniteTimeAction*> arrayOfActions;
            arrayOfActions.pushBack(DelayTime::create(waitTime));
            arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, (*_reels.begin()))));
            auto actionsSeq = Sequence::create(arrayOfActions);
            actionsSeq->setTag(TAG_STOPFIRSTREEL);
            _mainUINode->runAction(actionsSeq);
        }
    }
    else
        (*_reels.begin())->stopInfinite();

    if (Player::getInstance()->getAutoSpinSetting().StopWinTimes)
    {
        if (_winRcv.win_coin >= _totalBet * Player::getInstance()->getAutoSpinSetting().WinTimes)
            _isAutoSpinUnlimited = false;
    }

    if (isAutoSpining() && Player::getInstance()->getAutoSpinSetting().StopSuddenly)
        performStopSuddendly();
    else
        setSlotButtonStatus(SLOTBTNSTATUS_STOPINGREEL);
}

void UISlotSuperClassic::performStopSuddendly()
{
    setSlotButtonStatus(SLOTBTNSTATUS_STOPSUDDENDLY);
    //for (auto& boolVaule : _isExpectBgSoundPlayed)
    //    boolVaule = true;
    AudioManager::getInstance()->stopEff(_middleExpectSoundHandle);
    _mainUINode->stopActionByTag(TAG_STOPFIRSTREEL);
    _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
    for (auto reel : _reels)
    {
        reel->stopActionByTag(TAG_STOPSINGLEREEL);
        reel->stopInfiniteSuddenly();
    }
}

void UISlotSuperClassic::requestSpin()
{
    if (isScrolling()/* || isPerformSpinResult()*/)
        return;

    _pLayoutArrow->setVisible(false);

    stopPerformSpinResult();

    stopAllLight();

    if (_freeSpinStatus == FREESPINSTATUS_OFF && !UISlotUtils::checkCoinAndOpenOutOfCoin(CC_CALLBACK_0(UISlotSuperClassic::outOfCoinKeepPlayingCB, this), _betLevelList.at(_curBet), _curLines))
    {
        if (_autoSpinStatus == AUTOSPINSTATUS_ON)
            stopAutoSpin();
        return;
    }

    setWin(0);
    setSlotButtonStatus(SLOTBTNSTATUS_WAITINGRESPONSE);
    performSpin();

    _mainUINode->stopActionByTag(TAG_PERFORMSPIN_MINTIME);
    DelayTime* pMinTimeAction = DelayTime::create(PERFORMSPIN_MINTIME);
    pMinTimeAction->setTag(TAG_PERFORMSPIN_MINTIME);
    _mainUINode->runAction(pMinTimeAction);

    RequestSlotSpin(_betLevelList.at(_curBet), _curLines);
}

void UISlotSuperClassic::showFreeSpinCount(bool show)
{
    if (show && _pLayoutFreeSpin->getOpacity() != 255)
        _pLayoutFreeSpin->runAction(FadeIn::create(0.2f));
    else if (!show && _pLayoutFreeSpin->getOpacity() != 0)
        _pLayoutFreeSpin->runAction(FadeOut::create(0.2f));
}

void UISlotSuperClassic::setFreeSpinCount(int count)
{
    _freeSpinCount = count;
    _pBMFFreeSpinRemain->setString(StringUtils::toString(count));
}

void UISlotSuperClassic::performAddFreeSpinCount(int addCount)
{
    CountTextBMFontToNum* pAction = CountTextBMFontToNum::create(0.5, _freeSpinCount - addCount, _freeSpinCount);
    pAction->setFormat(CountTextBMFontToNum::FORMAT_NONE);
    _pBMFFreeSpinRemain->runAction(pAction);
}

void UISlotSuperClassic::setSlotButtonStatus(ESlotButtonStatus status)
{
    _buttonStatus = status;

    //Paytable在任何時刻都enable
    if (_freeSpinStatus == FREESPINSTATUS_ON)
    {
        switch (status)
        {
        case SLOTBTNSTATUS_STOPINGREEL:     //只能按stop快速停輪
            _pLayoutSpinListener->setEnabled(true);
            _pBtnSpin->setVisible(false);
            _pBtnStop->setVisible(true);
            _pBtnSpin->setEnabled(true);
            _pBtnStop->setEnabled(true);

            _pLayoutAutoListener->setEnabled(false);
            _pBtnAuto->setEnabled(false);

            _pLayoutMaxBetListener->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);
          
            _pLayoutBetListener->setEnabled(false);
            _pBtnBet->setEnabled(false);
            break;
        default:                            //所有按鈕都disable
            _pLayoutSpinListener->setEnabled(false);
            _pBtnSpin->setVisible(false);
            _pBtnStop->setVisible(true);
            _pBtnSpin->setEnabled(false);
            _pBtnStop->setEnabled(false);

            _pLayoutAutoListener->setEnabled(false);
            _pBtnAuto->setEnabled(false);

            _pLayoutMaxBetListener->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);

            _pLayoutBetListener->setEnabled(false);
            _pBtnBet->setEnabled(false);
            break;
        }
    }
    else if (_autoSpinStatus == AUTOSPINSTATUS_ON)
    {
        //keep autoSpin Setting
    }
    else
    {
        switch (status)
        {
        case SLOTBTNSTATUS_IDLE:                //一般靜止狀態, 所有按鈕都enable
            _pLayoutSpinListener->setEnabled(true);
            _pBtnSpin->setVisible(true);
            _pBtnStop->setVisible(false);
            _pBtnSpin->setEnabled(true);
            _pBtnStop->setEnabled(true);

            _pLayoutAutoListener->setEnabled(true);
            _pBtnAuto->setEnabled(true);

            _pLayoutMaxBetListener->setEnabled(true);
            _pBtnMaxBet->setEnabled(true);

            _pLayoutBetListener->setEnabled(true);
            _pBtnBet->setEnabled(true);
            break;
        case SLOTBTNSTATUS_WAITINGRESPONSE:     //等待server回應, 所有按鈕都disable
            _pLayoutSpinListener->setEnabled(false);
            _pBtnSpin->setVisible(true);
            _pBtnStop->setVisible(false);
            _pBtnSpin->setEnabled(false);
            _pBtnStop->setEnabled(false);

            _pLayoutAutoListener->setEnabled(false);
            _pBtnAuto->setEnabled(false);

            _pLayoutMaxBetListener->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);

            _pLayoutBetListener->setEnabled(false);
            _pBtnBet->setEnabled(false);
            break;
        case SLOTBTNSTATUS_STOPINGREEL:         //server已回應, 只能按stop快速停輪
            _pLayoutSpinListener->setEnabled(true);
            _pBtnSpin->setVisible(false);
            _pBtnStop->setVisible(true);
            _pBtnSpin->setEnabled(true);
            _pBtnStop->setEnabled(true);

            _pLayoutAutoListener->setEnabled(false);
            _pBtnAuto->setEnabled(false);

            _pLayoutMaxBetListener->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);

            _pLayoutBetListener->setEnabled(false);
            _pBtnBet->setEnabled(false);
            break;
        case SLOTBTNSTATUS_STOPSUDDENDLY:       //server已回應, 按stop快速停輪中
            _pLayoutSpinListener->setEnabled(false);
            _pBtnSpin->setVisible(false);
            _pBtnStop->setVisible(true);
            _pBtnSpin->setEnabled(false);
            _pBtnStop->setEnabled(false);

            _pLayoutAutoListener->setEnabled(false);
            _pBtnAuto->setEnabled(false);

            _pLayoutMaxBetListener->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);

            _pLayoutBetListener->setEnabled(false);
            _pBtnBet->setEnabled(false);
            break;
        case SLOTBTNSTATUS_ONCELINEANDSTOREUI:  //當結果有StoreUI, 播onceAllPayLine和StoreUI時, 所有按鈕都disable
            _pLayoutSpinListener->setEnabled(false);
            _pBtnSpin->setVisible(true);
            _pBtnStop->setVisible(false);
            _pBtnSpin->setEnabled(false);
            _pBtnStop->setEnabled(false);

            _pLayoutAutoListener->setEnabled(false);
            _pBtnAuto->setEnabled(false);

            _pLayoutMaxBetListener->setEnabled(false);
            _pBtnMaxBet->setEnabled(false);

            _pLayoutBetListener->setEnabled(false);
            _pBtnBet->setEnabled(false);
            break;

        }
    }
}

void UISlotSuperClassic::setAutoSpinStatus(EAutoSpinStatus status)
{
    _autoSpinStatus = status;

    switch (status)
    {
    case AUTOSPINSTATUS_ON:      //自動轉輪中, 只能按stop停止自動轉輪
        _autoSpinStatus = AUTOSPINSTATUS_ON;
        _pLayoutSpinListener->setEnabled(true);
        _pBtnSpin->setVisible(false);
        _pBtnStop->setVisible(true);
        _pBtnSpin->setEnabled(true);
        _pBtnStop->setEnabled(true);

        _pLayoutAutoListener->setEnabled(false);
        _pBtnAuto->setEnabled(false);

        _pLayoutMaxBetListener->setEnabled(false);
        _pBtnMaxBet->setEnabled(false);

        _pLayoutBetListener->setEnabled(false);
        _pBtnBet->setEnabled(false);
        break;
    case AUTOSPINSTATUS_OFF:   //停止轉輪中, 回復buttonStatus
        _autoSpinStatus = AUTOSPINSTATUS_OFF;
        setSlotButtonStatus(_buttonStatus);
        _isAutoSpinUnlimited = false;
        break;
    }
}

void UISlotSuperClassic::setFreeSpinStatus(EFreeSpinStatus status)
{
    _freeSpinStatus = status;

    switch (status)
    {
    case FREESPINSTATUS_ON:     //自動轉輪中, 只有在slotButton為SLOTBTNSTATUS_STOPSUDDENDLY時可快速停輪
        _freeSpinStatus = FREESPINSTATUS_ON;
        setSlotButtonStatus(_buttonStatus);
        //setAutoSpinStatus(_autoSpinStatus);
        break;
    case FREESPINSTATUS_OFF:    //停止轉輪中, 回復buttonStatus
        _freeSpinStatus = FREESPINSTATUS_OFF;
        setSlotButtonStatus(_buttonStatus);
        setAutoSpinStatus(_autoSpinStatus);
        break;
    }
}

std::vector<cocos2d::Vec2> UISlotSuperClassic::getPaylinePtList(cocos2d::ui::Layout* paylineLayout, int payLineOrder)
{
    std::vector<cocos2d::Vec2> ptList;
    ui::Layout* singlePaylineLayout = static_cast<ui::Layout*>(_pLayoutPaylineRoot->getChildByName(StringUtils::format("layout_line%02d", payLineOrder)));
    for (int i = 0; i < singlePaylineLayout->getChildrenCount(); i++)
    {
        auto pChild = singlePaylineLayout->getChildren().at(i);
        ui::ImageView* pImageView = dynamic_cast<ui::ImageView*>(pChild);
        Vec2 headPt = _pLayoutDummyPaylineParticle->convertToNodeSpace(pImageView->convertToWorldSpace(Vec2(0, pImageView->getContentSize().height / 2.0f)));
        ptList.push_back(headPt);
        Vec2 tailPt = _pLayoutDummyPaylineParticle->convertToNodeSpace(pImageView->convertToWorldSpace(Vec2(pImageView->getContentSize().width, pImageView->getContentSize().height / 2.0f)));
        ptList.push_back(tailPt);
    }

    //payline的元件沒有按照順序
    sort(ptList.begin(), ptList.end(),
        [](const Vec2& first, const Vec2& second) {
        return (first.x < second.x);
    });

    return ptList;
}

void UISlotSuperClassic::iniPayLineComponent()
{
    _pLayoutPaylineRoot = static_cast<ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("UISlot_SuperClassic/UISlot_SuperClassic_Payline.ExportJson"));
    CCASSERT(_pLayoutPaylineRoot, "");
    //setTraversalCascadeOpacityEnabled(_pLayoutPaylineRoot, true);
    for (auto pLayout : _pLayoutPaylineRoot->getChildren())
    {
        if (!pLayout)
            continue;
        pLayout->setVisible(true);
        for (auto pImageView : pLayout->getChildren())
        {
            if (!pImageView)
                continue;
            pImageView->setOpacity(0.0f);
        }
    }

    Layout* pLayoutDummySymbolAnim = static_cast<Layout*>(_pLayoutSlotReel->getChildByName("layout_dummy_payline"));
    CCASSERT(pLayoutDummySymbolAnim, "");
    pLayoutDummySymbolAnim->addChild(_pLayoutPaylineRoot);

    _pLayoutDummyPaylineParticle = static_cast<Layout*>(_pLayoutSlotReel->getChildByName("layout_dummy_payline_particle"));
    CCASSERT(_pLayoutDummyPaylineParticle, "");
}                                          

void UISlotSuperClassic::showPayline(int payLineOrder, bool show)
{
    auto paylineLayout = static_cast<ui::Layout*>(_pLayoutPaylineRoot->getChildByName(StringUtils::format("layout_line%02d", payLineOrder)));
    if (!paylineLayout)
        return;
    for (auto pImageView : paylineLayout->getChildren())
    {
        if (!pImageView)
            continue;
        if (show)
            pImageView->runAction(FadeIn::create(0.2f));
        //pImageView->setOpacity(255);
        else
            pImageView->runAction(FadeOut::create(0.2f));
        //pImageView->setOpacity(0);
    }
}

void UISlotSuperClassic::showPaylineLessThanPayLineOrder(int payLineOrder, bool show)
{
    if (payLineOrder < _minLines || payLineOrder > _maxLines)
        return;

    for (int i = _maxLines; i >= _minLines; i--)
    {
        if (i <= payLineOrder)
            showPayline(i, show);
        else
            showPayline(i, false);
    }
}

void UISlotSuperClassic::hideAllPayline()
{
    showPaylineLessThanPayLineOrder(_maxLines, false);
}

void UISlotSuperClassic::playPayLine(int payLineID, bool play, bool Loop)
{
    if (!_pLayoutPaylineRoot)
        return;

    int payLineOrder = getPayLineOrder(payLineID);
    if (!payLineOrder)
        return;

    std::string lineAnimName = StringUtils::format("anim_line%02d", payLineOrder);
    auto lineAnim = cocostudio::ActionManagerEx::getInstance()->getActionByName("UISlot_SuperClassic/UISlot_SuperClassic_Payline.ExportJson", lineAnimName.c_str());
    if (!lineAnim)
        return;
    lineAnim->setLoop(Loop);
    if (play)
        lineAnim->play();
    else
        lineAnim->stop();
}

void UISlotSuperClassic::stopPlayPayLine(int payLineID)
{
    playPayLine(payLineID, false, false);
}

void UISlotSuperClassic::stopPlayAllPayLine()
{
    for (int i = _beginPayLineID; i <= _endPayLineID; i++)
        stopPlayPayLine(i);
    for (auto pNode : _pLayoutPaylineRoot->getChildren())
    {
        if (pNode)
        {
            for (auto pImageView : pNode->getChildren())
                pImageView->setOpacity(0.0f);
        }
    }
}

void UISlotSuperClassic::onceAllPayLines(std::vector<SpinResponse_PayLine>& payLines, SpinResponse_Win& winRcv)
{
    if (payLines.empty())
        return;

    if (isAutoSpining())
    {
        for (auto lines : payLines)
            playPayLine(lines.lineID, true, true);

        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::playAllPayLineSymbol, this, payLines)));
        arrayOfActions.pushBack(DelayTime::create(LINEANIMTIME));//onceAllPayLines中
        //if (UIController::getInstance()->isStoreUIEmpty())
        //    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);//有payline沒彈窗, allPalyLine播完時可以自動spin
        //需等待BinWgin等彈窗關閉才可request
        //arrayOfActions.pushBack(CallFunc::create(
        //    [&, this](){
        //    stopPerformSpinResult();
        //    requestSpin(); }
        //));
        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_ONCEALLPAYLINE);
        _mainUINode->runAction(actionsSeq);
    }
    else
    {
        for (auto lines : payLines)
            playPayLine(lines.lineID, true, false);

        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::playAllPayLineSymbol, this, payLines)));
        //if (UIController::getInstance()->isStoreUIEmpty())
        //    setSlotButtonStatus(SLOTBTNSTATUS_IDLE);//有payline沒彈窗, allPalyLine播出的瞬間開始可以按spin
        arrayOfActions.pushBack(DelayTime::create(LINEANIMTIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::clearPayLineSymbol, this)));
        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_ONCEALLPAYLINE);
        _mainUINode->runAction(actionsSeq);
    }

    if (!isShowBigWin())//若是bigwin會在UIBigWin中顯示
        performWinMoney(winRcv.win_coin);

    // Winning Light
    playWinLight();
    //場景慶祝動畫
    performSceneAnim((ESuperClassicSceneAnim)cocos2d::RandomHelper::random_int((int)ESuperClassicSceneAnim::SCENEANIM_WIN_1, (int)ESuperClassicSceneAnim::SCENEANIM_WIN_2));

    if (isHitJackpot())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
    else if (isHitScatter())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
    else if (isHitBonus())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
    else if (isHitFiveOfAKind())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
    else if (isHitBigWin())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpecialWinSoundID, false);
    else
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->WinSoundID, false);
}

void UISlotSuperClassic::repeatSinglePayLines(std::vector<SpinResponse_PayLine>& payLines)
{
    if (payLines.empty())
        return;

    Vector<FiniteTimeAction*> arrayOfActions;
    //重覆輪播每條payline
    for (auto line : payLines)
    {
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::playPayLine, this, line.lineID, true, false)));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::playSinglePayLineSymbol, this, line)));
        arrayOfActions.pushBack(DelayTime::create(LINEANIMTIME));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::clearPayLineSymbol, this)));
    }

    auto actionsSeq = Sequence::create(arrayOfActions);

    //自動spin中只播一次
    if (isAutoSpining())
    {
        //改為不播單條
        //actionsSeq->setTag(TAG_REPEATSINGLEPAYLINE);
        //runAction(actionsSeq);
    }
    //手動spin重覆播放
    else
    {
        auto rep = RepeatForever::create(actionsSeq);
        rep->setTag(TAG_REPEATSINGLEPAYLINE);
        _mainUINode->runAction(rep);
    }
}

void UISlotSuperClassic::playAllPayLineSymbol(std::vector<SpinResponse_PayLine>& payLines)
{
    std::map<Vec2, Vec2> focusPos;
    for (auto line : payLines)
    {
        if (/*line.lineID <= 0 || */line.lineID > _endPayLineID)//有些中獎沒有payline只有symbol
            continue;
        for (auto hitCoord : line.hitCoord)
        {
            Vec2 curPos = hitCoord;// getSymbolCoord(line.lineID, order);
            if (curPos.isZero())
                continue;
            if (focusPos.find(curPos) == focusPos.end())
                focusPos[curPos] = curPos;
        }
    }

    for (auto itr : focusPos)
    {
        Vec2 curPos = itr.second;
        Vec2 outVec = Vec2::ZERO;
        UISymbolBase* outSymbol = nullptr;
        getSymbolOnPayLine(curPos.x, curPos.y, outVec, &outSymbol);
        SymbolSpec outSymbolSpec = outSymbol->getSymbolSpec();
        outSymbol->setVisible(false);
        //symbol only use static sprite
        if (outSymbolSpec._animfile.empty())
        {
            Sprite* pSprite = Sprite::createWithSpriteFrameName(outSymbolSpec._filename);
            pSprite->setPosition(outVec);
            pSprite->setAnchorPoint(Vec2::ZERO);
            _pLayoutSymbolAnim->addChild(pSprite);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
        //symbol have animation file
        else
        {
            CustomSpAnim* pSpine = CustomSpAnim::createWithFile(outSymbolSpec._animfile, outSymbolSpec._maskfile);
            pSpine->playAnimationRoundInTime("win", LINEANIMTIME);
            pSpine->setPosition(outVec);
            _pLayoutSymbolAnim->addChild(pSpine);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
    }
}

void UISlotSuperClassic::playSinglePayLineSymbol(SpinResponse_PayLine& payLine)
{
    std::map<Vec2, Vec2> focusPos;

    if (/*payLine.lineID <= 0 || */payLine.lineID > _endPayLineID)//有些中獎沒有payline只有symbol
        return;

    for (auto hitCoord : payLine.hitCoord)
    {
        Vec2 curPos = hitCoord;// getSymbolCoord(payLine.lineID, order);
        if (curPos.isZero())
            continue;
        if (focusPos.find(curPos) == focusPos.end())
            focusPos[curPos] = curPos;
    }

    for (auto itr : focusPos)
    {
        Vec2 curPos = itr.second;
        Vec2 outVec = Vec2::ZERO;
        UISymbolBase* outSymbol = nullptr;
        getSymbolOnPayLine(curPos.x, curPos.y, outVec, &outSymbol);
        SymbolSpec outSymbolSpec = outSymbol->getSymbolSpec();
        outSymbol->setVisible(false);
        //symbol only use static sprite
        if (outSymbolSpec._animfile.empty())
        {
            Sprite* pSprite = Sprite::createWithSpriteFrameName(outSymbolSpec._filename);
            pSprite->setPosition(outVec);
            pSprite->setAnchorPoint(Vec2::ZERO);
            _pLayoutSymbolAnim->addChild(pSprite);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
        //symbol have animation file
        else
        {
            CustomSpAnim* pSpine = CustomSpAnim::createWithFile(outSymbolSpec._animfile, outSymbolSpec._maskfile);
            pSpine->playAnimationRoundInTime("win", LINEANIMTIME);
            pSpine->setPosition(outVec);
            _pLayoutSymbolAnim->addChild(pSpine);
            ProcedureFx::moveParticleAlongRect(Rect(outVec, SYMBOLSIZE), _pLayoutSymbolAnim, 0);
        }
    }
    int lineSymbolID = payLine.symbolID;
    SymbolInfoData* pSymbolData = ExternalTable<SymbolInfoData>::getRecordByID(lineSymbolID);
    if (!isPerformSpinResult(RESULTTYPE_SYMBOL_SOUND) && pSymbolData->SoundID)
        AudioManager::getInstance()->playEffectWithFileID(pSymbolData->SoundID, false);

}

void UISlotSuperClassic::clearPayLineSymbol()
{
    if (!_pLayoutSymbolAnim)
        return;
    _pLayoutSymbolAnim->removeAllChildrenWithCleanup(true);
    visibleAllSymbol();
}

void UISlotSuperClassic::visibleAllSymbol()
{
    for (auto pReel : _reels)
    {
        if (!pReel)
            continue;
        for (auto pSymobl : pReel->getSymbols())
        {
            if (!pSymobl)
                continue;
            pSymobl->setVisible(true);
        }
    }
}

void UISlotSuperClassic::colorAllSymbol(const cocos2d::Color4B& color4B)
{
    for (auto pReel : _reels)
    {
        if (!pReel)
            continue;
        pReel->colorAllSymbol(color4B);
    }
}

void UISlotSuperClassic::performWinMoney(int win)
{
    if (!_pBMFWinMoneyAmin || win <= 0)
        return;

    //_pBMFWinMoneyAmin->stopAllActions();

    ////數錢
    //CountTextBMFontToNum* pAction = CountTextBMFontToNum::create(1.5f, 0, win);
    //pAction->setFormat(CountTextBMFontToNum::FORMAT_DOLLARCOMMA);
    //_pBMFWinMoneyAmin->runAction(pAction);

    ////放大
    //Vector<FiniteTimeAction*> arrayOfActions;
    //arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setOpacity, _pBMFWinMoneyAmin, 255)));
    //arrayOfActions.pushBack(CallFunc::create(
    //    [this](){ _pBMFWinMoneyAmin->setScale(0.55f); }
    //));
    //arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    //arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    //arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    //arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    //arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 1.0f)));
    //arrayOfActions.pushBack(DelayTime::create(1.0f));
    //arrayOfActions.pushBack(FadeOut::create(0.5));
    //arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotPirate::setWin, this, win)));
    //auto scaleSeq = Sequence::create(arrayOfActions);
    //_pBMFWinMoneyAmin->runAction(scaleSeq);

    //_coinParticle->resetSystem();
}

void UISlotSuperClassic::performSceneAnim(ESuperClassicSceneAnim anim)
{
    switch (anim)
    {
    case SCENEANIM_IDLE:
        if (_sceneObjDealer->isPlayingAnimation("win01", 0))
        {
            //_sceneObjDealer->setToSetupPose();
            //_sceneObjDealer->setAnimation(0, "win01", false);
            //_sceneObjDealer->addAnimation(0, "idle", true);
            _dealerAnimName = "idle01";
        }
        else if (_sceneObjDealer->isPlayingAnimation("win02", 0))
        {
            //_sceneObjDealer->setToSetupPose();
            //_sceneObjDealer->setAnimation(0, "win02", false);
            //_sceneObjDealer->addAnimation(0, "idle", true);
            _dealerAnimName = "idle01";
        }
        else if (_sceneObjDealer->isPlayingAnimation("idle01", 0) ||
            _sceneObjDealer->isPlayingAnimation("idle02", 0) ||
            _sceneObjDealer->isPlayingAnimation("win01", 0) ||
            _sceneObjDealer->isPlayingAnimation("win02", 0)
            )
        {
            return;
        }
        else
        {
            _sceneObjDealer->setToSetupPose();
            _dealerAnimName = "idle01";
            _sceneObjDealer->setAnimation(0, "idle01", true);
        }
        break;
    case SCENEANIM_WIN_1:
        //_sceneObjDealer->setToSetupPose();
        //_sceneObjDealer->addAnimation(0, "win01", true);
        _dealerAnimName = "win01";
        break;
    case SCENEANIM_WIN_2:
        //_sceneObjDealer->setToSetupPose();
        //_sceneObjDealer->addAnimation(0, "win02", true);
        _dealerAnimName = "win02";
        break;
    case SCENEANIM_TOUCH:
        _dealerAnimName = "touch";
        _sceneObjDealer->setToSetupPose();
        _sceneObjDealer->setAnimation(0, "touch", false);
        AudioManager::getInstance()->playEffectWithFileID(EFF_TOUCH_DEALER, false);
        break;
    default:
        break;
    }
}

void UISlotSuperClassic::performExpectBG(int reelOrder, EExpectType expectBG)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_expectBgs.size())
        return;

    ExpectBGGroup& rCurExpectBGGroup = _expectBgs.at(reelIndex);
    if (expectBG == EXPECT_NONE)
    {
        rCurExpectBGGroup.pParticles->removeAllChildren();
        return;
    }

    switch (expectBG)
    {
    case EXPECT_SCATTER:
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    case EXPECT_BONUS:
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    case EXPECT_HYBRID:
        if (!_isExpectBgSoundPlayed.at(reelIndex))
        {
            _isExpectBgSoundPlayed.at(reelIndex) = true;
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_EXPECTBG, false);
        }
        break;
    default:
        break;
    }

    if (rCurExpectBGGroup.pParticles->getChildrenCount() == 0)
    {
        ParticleSystemQuad* pParticle1 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_boll_h.plist");
        pParticle1->setPosition(Vec2(70, 400));
        rCurExpectBGGroup.pParticles->addChild(pParticle1);
        ParticleSystemQuad* pParticle2 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_star_w.plist");
        pParticle2->setPosition(Vec2(70, 400));
        rCurExpectBGGroup.pParticles->addChild(pParticle2);

        ParticleSystemQuad* pParticle3 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_boll_w.plist");
        pParticle3->setPosition(Vec2(4, 198));
        rCurExpectBGGroup.pParticles->addChild(pParticle3);
        ParticleSystemQuad* pParticle4 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_star_h.plist");
        pParticle4->setPosition(Vec2(4, 198));
        rCurExpectBGGroup.pParticles->addChild(pParticle4);

        ParticleSystemQuad* pParticle5 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_boll_h.plist");
        pParticle5->setPosition(Vec2(70, 4));
        rCurExpectBGGroup.pParticles->addChild(pParticle5);
        ParticleSystemQuad* pParticle6 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_star_w.plist");
        pParticle6->setPosition(Vec2(70, 4));
        rCurExpectBGGroup.pParticles->addChild(pParticle6);

        ParticleSystemQuad* pParticle7 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_boll_w.plist");
        pParticle7->setPosition(Vec2(139, 199));
        rCurExpectBGGroup.pParticles->addChild(pParticle7);
        ParticleSystemQuad* pParticle8 = ParticleSystemQuad::create("Particle/UISlot_SuperClassic_Expect_star_h.plist");
        pParticle8->setPosition(Vec2(139, 199));
        rCurExpectBGGroup.pParticles->addChild(pParticle8);
    }
}

void UISlotSuperClassic::stopPerformExpectBG()
{
    for (auto& curBGs : _expectBgs)
    {
        curBGs.pParticles->removeAllChildren();
    }
    colorAllSymbol(Color4B::WHITE);
}

void UISlotSuperClassic::setPayLineBG(bool show)
{
    //if (show)
    //{
    //    _pLayoutSeparator->stopAllActions();
    //    _pLayoutSeparator->runAction(TintTo::create(0.2f, PAYLINEBG_DARKCOLOR));
    //}
    //else
    //{
    //    _pLayoutSeparator->stopAllActions();
    //    _pLayoutSeparator->runAction(TintTo::create(0.2f, Color3B::WHITE));
    //}
}

cocos2d::Vec2 UISlotSuperClassic::getSymbolCoord(int payLineID, int order)
{
    if (payLineID <= 0)
        return Vec2::ZERO;

    PayLineData* pPayLine = ExternalTable<PayLineData>::getRecordByID(payLineID);
    if (!pPayLine)
        return Vec2::ZERO;

    int reelIndex = order - 1;

    if (reelIndex < 0 || reelIndex >= (int)_reels.size() || reelIndex >= _reelCount)
    {
        CCLOG("illegal reelIndex");
        return Vec2::ZERO;
    }

    int symbolIndex = pPayLine->component[reelIndex].viewFrameIndex;

    return Vec2(order, symbolIndex);
}

void UISlotSuperClassic::getSymbolOnPayLine(int x, int y, cocos2d::Vec2& out_Position, UISymbolBase** out_Symbol)
{
    int reelIndex = x - 1;

    if (reelIndex < 0 || reelIndex >= (int)_reels.size() || reelIndex >= _reelCount)
    {
        CCLOG("illegal reelIndex");
        return;
    }

    UIReel* pReel = _reels.at(reelIndex);
    if (!pReel)
        return;

    cocos2d::Vector<UISymbolBase*>& symbols = pReel->getSymbols();

    int useSymbolIndex = symbols.size() - 1 - y;      //12321 -> 32123
    if (useSymbolIndex<1 || useSymbolIndex>symbols.size() - 2)  //1~3
        return;

    UISymbolBase* pSymbol = symbols.at(useSymbolIndex);
    if (!pSymbol)
        return;
    out_Position = Vec2(pReel->getPosition() + pSymbol->getPosition());
    *out_Symbol = pSymbol;
}

void UISlotSuperClassic::performSpinResult(std::vector<SpinResponse_PayLine>& pay_lineRcv,
                                            SpinResponse_JP& jpRcv,
                                            SpinResponse_Win& winRcv,
                                            SpinResponse_FreeSpin& freeSpinRcv,
                                            SpinResponse_Bonus& bonusRcv,
                                            SpinResponse_Profile& profileRcv,
                                            SpinResponse_LevelUp& levelupRcv)
{
    //必須放在前面, autoSpin中才扣1
    //放在後面可能扣到才剛加進autoSpin中的freeSpin
    if (isAutoSpining())
        setAutoCount(_autoCount - 1);

    stopPerformSpinResult();

    bool bStoreUI = false;

    //UIJackpot
    if (isHitJackpot())
    {
        auto cJackpot = UIController::getInstance()->getController<UIJackpot*>(EUITAG::EUITAG_JACKPOT);
        if (cJackpot)
        {
            cJackpot->reset();
            cJackpot->setShowMoney(jpRcv.winnings);
            cJackpot->openUILater();
            bStoreUI = true;
        }
    }

    //UIFiveOfAKind
    if (!isHitJackpot() && !isHitBonus() && !isHitScatter() && isHitFiveOfAKind())
    {
        auto cFiveOFAKind = UIController::getInstance()->getController<UIFiveOfAKind*>(EUITAG::EUITAG_FIVEOFAKIND);
        if (cFiveOFAKind)
        {
            cFiveOFAKind->reset();
            if (isAutoSpining())
                cFiveOFAKind->setAutoCloseUITime(5.0f);
            cFiveOFAKind->setShowSymbol("UISlot_SuperClassic/", winRcv.fiveOfAKindID, Size(140, 140), true); // symbol 實際size為140 * 140，但spine位置偏掉所以暫用165 * 165調整
            cFiveOFAKind->openUILater();
            bStoreUI = true;
        }
    }

    //UIBigWin
    if (isShowBigWin())
    {
        auto cBigWin = UIController::getInstance()->getController<UIBigWin*>(EUITAG::EUITAG_BIGWIN);
        if (cBigWin)
        {
            cBigWin->reset();
            if (isAutoSpining())
            {
                cBigWin->setAutoCloseUITime(5.0f);
                cBigWin->setShare(false);
            }
            cBigWin->setShowMoney(winRcv.win_coin);
            cBigWin->setSlotWinText(_pBMFWinNum);
            cBigWin->openUILater();
            bStoreUI = true;
        }
    }
    else
        setWin(winRcv.win_coin);

    //BonusGame
    //if (isHitBonus())
    //{
    //    auto bonusGame = UIController::getInstance()->getController<UIBonusGamePoke*>(EUITAG::EUITAG_BONUSGAMEPOKE);
    //    if (bonusGame)
    //    {
    //        bonusGame->setGameInfo(bonusRcv.gameID, bonusRcv.bet);
    //        bonusGame->openUILater();
    //        bStoreUI = true;
    //    }
    //}

    //FreeSpin: start
    if (isStartFreeSpin())
    {
        auto cFreeSpinStart = UIController::getInstance()->getController<UISlotSuperClassicFreeSpinStart*>(EUITAG::EUITAG_SUPPERCLASSIC_FREESPINSTART);
        if (cFreeSpinStart)
        {
            cFreeSpinStart->reset();
            cFreeSpinStart->setAutoCloseUITime(5.0f);
            cFreeSpinStart->setFreeSpinCounter(freeSpinRcv.remain_count);
            cFreeSpinStart->openUILater();
            bStoreUI = true;
            cFreeSpinStart->setOnCloseUICB(
                [this]()
            {
                //場景表現
                //performSceneAnim(ESceneAnim::SCENEANIM_BOATIN);
            }
            );
        }
        showFreeSpinCount(true);
        setFreeSpinCount(freeSpinRcv.remain_count);
        setAutoCount(_autoCount + freeSpinRcv.remain_count);
        setAutoSpinStatus(AUTOSPINSTATUS_ON);
        setFreeSpinStatus(FREESPINSTATUS_ON);
    }

    //FreeSpin: end
    if (isEndFreeSpin())
    {
        auto cFreeSpinReslut = UIController::getInstance()->getController<UISlotSuperClassicFreeSpinResult*>(EUITAG::EUITAG_SUPPERCLASSIC_FREESPINRESULT);
        if (cFreeSpinReslut)
        {
            cFreeSpinReslut->reset();
            if (isAutoSpining() && !Player::getInstance()->getAutoSpinSetting().StopWinBonus)
                cFreeSpinReslut->setAutoCloseUITime(5.0f);
            cFreeSpinReslut->setWinMoney(freeSpinRcv.coin_sum);
            cFreeSpinReslut->setFreeSpinCounter(freeSpinRcv.total_count);
            cFreeSpinReslut->openUILater();
            bStoreUI = true;
            cFreeSpinReslut->setOnCloseUICB(
                [this]()
            {
                //場景表現
                //performSceneAnim(ESceneAnim::SCENEANIM_BOATOUT);
            }
            );
        }
        showFreeSpinCount(false);
        setFreeSpinCount(freeSpinRcv.remain_count);
        setFreeSpinStatus(FREESPINSTATUS_OFF);
    }

    //FreeSpin: add more freespin
    if (isAddFreeSpin())
    {
        auto cFreeSpinStart = UIController::getInstance()->getController<UISlotSuperClassicFreeSpinStart*>(EUITAG::EUITAG_SUPPERCLASSIC_FREESPINSTART);
        if (cFreeSpinStart)
        {
            cFreeSpinStart->reset();
            cFreeSpinStart->setAutoCloseUITime(5.0f);
            cFreeSpinStart->setFreeSpinCounter(freeSpinRcv.add_count);
            cFreeSpinStart->openUILater();
            bStoreUI = true;
        }
        showFreeSpinCount(true);
        setFreeSpinCount(freeSpinRcv.remain_count);
        setAutoCount(_autoCount + freeSpinRcv.add_count);
        performAddFreeSpinCount(freeSpinRcv.add_count);
    }

    //FreeSpin: do freespin
    if (isDoingFreeSpin())
    {
        showFreeSpinCount(true);
        setFreeSpinCount(freeSpinRcv.remain_count);
    }

    //UILeveUp
    if (isLevelUp())
    {
        auto cLevelUp = UIController::getInstance()->getController<UILevelUp*>(EUITAG::EUITAG_LEVELUP);
        if (cLevelUp)
        {
            cLevelUp->reset();
            if (isAutoSpining())
            {
                cLevelUp->setAutoCloseUITime(5.0f);
                cLevelUp->setShare(false);
            }

            cLevelUp->setReward(levelupRcv.award_money + levelupRcv.award_fbmoney, levelupRcv.award_vip_point);
            cLevelUp->openUILater();
            bStoreUI = true;
        }
    }

    //必須放在後面
    //放在前面可能直接停止autoSpin, 但freeSpin可能會增加autoCount
    if (!isAutoSpining())
        stopAutoSpin();


    if (bStoreUI /*|| isHit()*/)
    {
        //storeUI開玩才可idle
        /*//等onceAllPayline(performWinMoney)後才idle*/
        setSlotButtonStatus(SLOTBTNSTATUS_ONCELINEANDSTOREUI);
    }
    else
    {
        setSlotButtonStatus(SLOTBTNSTATUS_IDLE);
    }

    //有中獎play payline, play symbol animation
    if (isHit())
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::setPayLineBG, this, true)));
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::onceAllPayLines, this, pay_lineRcv, winRcv)));
        arrayOfActions.pushBack(DelayTime::create(LINEANIMTIME));
        if (bStoreUI)
            arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIController::openStoredUI, UIController::getInstance())));

        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UISlotSuperClassic::repeatSinglePayLines, this, pay_lineRcv)));

        auto actionsSeq = Sequence::create(arrayOfActions);
        actionsSeq->setTag(TAG_PERFORMSPINRESULT);
        _mainUINode->runAction(actionsSeq);
    }
    //沒中獎但有UI要開,例如freeSpin結算, LevelUp
    else
    {
        if (bStoreUI)
            UIController::getInstance()->openStoredUI();
    }
}

void UISlotSuperClassic::stopPerformSpinResult()
{
    if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE))
        _mainUINode->stopActionByTag(TAG_ONCEALLPAYLINE);
    if (_mainUINode->getActionByTag(TAG_REPEATSINGLEPAYLINE))
        _mainUINode->stopActionByTag(TAG_REPEATSINGLEPAYLINE);
    if (_mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
        _mainUINode->stopActionByTag(TAG_PERFORMSPINRESULT);
    setPayLineBG(false);
    stopPlayAllPayLine();
    clearPayLineSymbol();
    performSceneAnim(ESuperClassicSceneAnim::SCENEANIM_IDLE);
    playNormalLight();
    stopPerformExpectBG();
}

bool UISlotSuperClassic::isPerformSpinResult(EPerformResultType type)
{
    switch (type)
    {
    case RESULTTYPE_NORMAL:
        if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE) ||
            _mainUINode->getActionByTag(TAG_REPEATSINGLEPAYLINE) ||
            _mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
            return true;
        break;
    case RESULTTYPE_AUTOSPIN:
        if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE) ||
            _mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
            return true;
    case RESULTTYPE_BTNSTATUSIDLE:
        if (_mainUINode->getActionByTag(TAG_ONCEALLPAYLINE) ||
            _mainUINode->getActionByTag(TAG_PERFORMSPINRESULT))
            return true;
        break;
    case RESULTTYPE_SYMBOL_SOUND:
        break;
    default:
        break;
    }

    auto cJackpot = UIController::getInstance()->getController<UIJackpot*>(EUITAG::EUITAG_JACKPOT);
    if (cJackpot->isOpen())
        return true;

    auto cBigWin = UIController::getInstance()->getController<UIBigWin*>(EUITAG::EUITAG_BIGWIN);
    if (cBigWin->isOpen())
        return true;

    auto cFiveOfAKind = UIController::getInstance()->getController<UIFiveOfAKind*>(EUITAG::EUITAG_FIVEOFAKIND);
    if (cFiveOfAKind->isOpen())
        return true;

    auto cFreeSpinStart = UIController::getInstance()->getController<UISlotSuperClassicFreeSpinStart*>(EUITAG::EUITAG_SUPPERCLASSIC_FREESPINSTART);//slot depend
    if (cFreeSpinStart->isOpen())
        return true;

    auto cFreeSpinResult = UIController::getInstance()->getController<UISlotSuperClassicFreeSpinResult*>(EUITAG::EUITAG_SUPPERCLASSIC_FREESPINRESULT);//slot depend
    if (cFreeSpinResult->isOpen())
        return true;

    auto cLevelUp = UIController::getInstance()->getController<UILevelUp*>(EUITAG::EUITAG_LEVELUP);
    if (cLevelUp->isOpen())
        return true;

    if (GameLogic::getInstance()->GetState() == E_GameState_StayBonusGame)
        return true;

    return false;
}

void UISlotSuperClassic::playWinLight()
{
    _pActionWinLight->updateToFrameByTime(0);
    _pActionWinLight->setLoop(true);
    _pActionWinLight->play();
}

void UISlotSuperClassic::playSpinLight()
{
    _pActionSpinLight->updateToFrameByTime(0);
    _pActionSpinLight->setLoop(true);
    _pActionSpinLight->play();
}

void UISlotSuperClassic::playNormalLight()
{
    _pActionWinLight->stop();
    _pActionSpinLight->stop();
    _pActionNormalLight->updateToFrameByTime(0);
    _pActionNormalLight->setLoop(false);
    _pActionNormalLight->play();
}

void UISlotSuperClassic::setAutoCount(int autoCount)
{
    if (!_pLayoutAutoDisable)
        return;

    if (_isAutoSpinUnlimited)
    {
        _pBMFAutoCount->setString("~");
        _pLayoutAutoDisable->setVisible(true);
        _pBtnAuto->setVisible(false);
    }
    else
    {
        _autoCount = autoCount;
        _pBMFAutoCount->setString(Value(_autoCount).asString());
        if (autoCount > 0)
        {
            _pLayoutAutoDisable->setVisible(true);
            _pBtnAuto->setVisible(false);
        }
        else
        {
            _pLayoutAutoDisable->setVisible(false);
            _pBtnAuto->setVisible(true);
        }
    }
}

void UISlotSuperClassic::stopAutoSpin()
{
    setAutoSpinStatus(AUTOSPINSTATUS_OFF);
    setAutoCount(0);
}

void UISlotSuperClassic::setWin(uint64 win)
{
    _win = win;
    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(1.0f, 0, win);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_COMMA);
    _pBMFWinNum->runAction(pAction);

    if (win <= 0)
    {
        return;
    }

    _pBMFWinMoneyAmin->stopAllActions();

    CountTextBMFontToUint64Num* pHUDWinAction = CountTextBMFontToUint64Num::create(1.0f, 0, win);
    pHUDWinAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    _pBMFWinMoneyAmin->runAction(pHUDWinAction);

    //放大
    Vector<FiniteTimeAction*> arrayOfActions;
    arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setOpacity, _pBMFWinMoneyAmin, 255)));
    arrayOfActions.pushBack(CallFunc::create(
        [this](){ _pBMFWinMoneyAmin->setScale(0.55f); }
    ));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.84f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 0.55f)));
    arrayOfActions.pushBack(EaseSineInOut::create(ScaleTo::create(0.2f, 1.0f)));
    arrayOfActions.pushBack(DelayTime::create(1.0f));
    arrayOfActions.pushBack(FadeOut::create(0.5));

    auto scaleSeq = Sequence::create(arrayOfActions);
    _pBMFWinMoneyAmin->runAction(scaleSeq);
}

void UISlotSuperClassic::updateTotalBet()
{
    _totalBet = _curLines*_betLevelList.at(_curBet);
    if (_pBMFTotlaBetNum)
        _pBMFTotlaBetNum->setString(GetMoneyFormat(_totalBet));
}

void UISlotSuperClassic::SetBetLevelListCurPos(int pos)
{
    int posMin = 0, posMax = _betLevelList.size() - BETLISTVIEW_ITEMCOUNT;
    if (pos < posMin)
        pos = posMin;
    if (pos > posMax)
        pos = posMax;
    if (pos == posMin)
    {
        _pNextBtn->setEnabled(false);
        _pNextBtn->setColor(BETSELECT_DARKCOLOR);

        _pPreviousBtn->setEnabled(true);
        _pPreviousBtn->setColor(Color3B(255, 255, 255));
    }
    else if (pos == posMax)
    {
        _pNextBtn->setEnabled(true);
        _pNextBtn->setColor(Color3B(255, 255, 255));

        _pPreviousBtn->setEnabled(false);
        _pPreviousBtn->setColor(BETSELECT_DARKCOLOR);
    }
    else
    {
        _pNextBtn->setColor(Color3B(255, 255, 255));
        _pNextBtn->setEnabled(true);

        _pPreviousBtn->setColor(Color3B(255, 255, 255));
        _pPreviousBtn->setEnabled(true);
    }

    _betLevelListCurPos = pos;
    _pListBetLevels->scrollToItem(_betLevelListCurPos, Vec2::ANCHOR_TOP_LEFT, Vec2::ANCHOR_TOP_LEFT);
}

void UISlotSuperClassic::setCurBet(int bet)
{
    if (bet < 0 || bet >= (int)_betLevelList.size())
        return;
    _curBet = bet;
    int betValue = _betLevelList.at(_curBet);
    if (_pBMFCurrentBet)
        _pBMFCurrentBet->setString(GetMoneyFormat(betValue));
    updateJackpotHint();
    updateTotalBet();
}

void UISlotSuperClassic::setCurLines(int lines)
{
    _curLines = lines;
    if (_pBMFCurrentLines)
        _pBMFCurrentLines->setString(StringUtils::toString(lines));
    if (_pBMFLineNumEdit)
        _pBMFLineNumEdit->setString(StringUtils::toString(lines));
}

void UISlotSuperClassic::setMaxLineAndBet()
{
    setCurLines(_maxLines);
    if (_isFixedBet && UISlotUtils::getUsableFixedBetLevel(_slotID).size())
        setCurBet(UISlotUtils::getUsableFixedBetLevel(_slotID).size() - 1);
    else if (!_isFixedBet && Player::getInstance()->getUsableBetLevel().size())
        setCurBet(Player::getInstance()->getUsableBetLevel().size() - 1);
    else
        setCurBet(0);
}

int UISlotSuperClassic::getPayLineOrder(int payLineID)
{
    if (payLineID < _beginPayLineID || payLineID > _endPayLineID)
        return 0;
    else
        return payLineID - _beginPayLineID + 1;
}

void UISlotSuperClassic::setJackpot(uint64 jackpot)
{
    int jackpotActionTag = 1;
    if (_pBMFJPNum->getActionByTag(jackpotActionTag))
        return;
    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(10.f, _jackpot, jackpot);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_DOLLARCOMMA);
    pAction->setTag(jackpotActionTag);
    _pBMFJPNum->stopAllActions();
    _pBMFJPNum->runAction(pAction);

    _jackpot = jackpot;
}

void UISlotSuperClassic::setJPEnable(bool enable)
{
    _pLayoutJPBanner->setVisible(enable);
    for (auto reel : _reels)
        reel->setJPEnable(enable);
    _jpBetLimit = enable ? _pSlotData->JackpotBetLimit : 0;
}

void UISlotSuperClassic::updateJackpotHint()
{
    bool overLimit = _betLevelList.at(_curBet) >= _jpBetLimit;
    _pImgJPCheck->setVisible(overLimit);
    _pImgJPUncheck->setVisible(!overLimit);
}

void UISlotSuperClassic::setJackpotHint(bool overLimit)
{
    _pImgJPCheck->setVisible(overLimit);
    _pImgJPUncheck->setVisible(!overLimit);
}

void UISlotSuperClassic::clearLastResult()
{
    //清除上次ExpectBG資料
    for (int i = 0; i < (int)_isExpectBgSoundPlayed.size(); i++)
        _isExpectBgSoundPlayed[i] = false;
    for (int i = 0; i < (int)_isMiddleExpectSoundPlayed.size(); i++)
        _isMiddleExpectSoundPlayed[i] = false;

    //清除上次暫存資料
    clearRcv();
}

void UISlotSuperClassic::clearRcv()
{
    _slotIDListRcv.clear();
    _paylineRcv.clear();
    _jpRcv.clear();
    _winRcv.clear();
    _freeSpinRcv.clear();
    _bonusRcv.clear();
    _profileRcv.clear();
    _levelupRcv.clear();
}

void UISlotSuperClassic::saveRcv(SlotIDList& slotIDListRcv,
                                SpinResponse_Overall& overallRcv,
                                std::vector<SpinResponse_PayLine>& paylineRcv,
                                SpinResponse_JP& jpRcv,
                                SpinResponse_Win& winRcv,
                                SpinResponse_FreeSpin& freeSpinRcv,
                                SpinResponse_Bonus& bonusRcv,
                                SpinResponse_Bonuswild& bonuswildRcv,
                                SpinResponse_Profile& profileRcv,
                                SpinResponse_LevelUp& levelupRcv)
{
    _slotIDListRcv = slotIDListRcv;
    _overallRcv = overallRcv;
    _paylineRcv = paylineRcv;
    _jpRcv = jpRcv;
    _winRcv = winRcv;
    _freeSpinRcv = freeSpinRcv;
    _bonusRcv = bonusRcv;
    _bonuswildRcv = bonuswildRcv;
    _profileRcv = profileRcv;
    _levelupRcv = levelupRcv;
}

void UISlotSuperClassic::outOfCoinKeepPlayingCB()
{
    int bet = 0;

    if (_isFixedBet)
        UISlotUtils::getSpinnableBetAndLine(bet, _curLines, UISlotUtils::getUsableFixedBetLevel(_slotID), false);
    else
        UISlotUtils::getSpinnableBetAndLine(bet, _curLines, Player::getInstance()->getUsableBetLevel(), false);

    int betIndex = getBetIndexByBetValue(bet);

    setCurLines(_curLines);
    setCurBet(betIndex);
}

int UISlotSuperClassic::getBetIndexByBetValue(int betValue)
{
    int betIndex = 0;
    for (int i = 0; i < (int)_betLevelList.size(); ++i)
    {
        if (betValue == _betLevelList.at(i))
        {
            betIndex = i;
            break;
        }
    }

    return betIndex;
}

void UISlotSuperClassic::stopAllLight()
{
    for (int i = 0; i < (int)_reelLights.size(); ++i)
        setReelLight(i + 1, false);
}

void UISlotSuperClassic::setReelLight(int reelIndex, bool light)
{
    if (reelIndex < 0 || reelIndex > (int)_reelLights.size())
        return;

    _reelLights.at(reelIndex - 1)._light1->setVisible(light);
    _reelLights.at(reelIndex - 1)._light2->setVisible(light);
    _reelLights.at(reelIndex - 1)._light3->setVisible(light);
    _reelLights.at(reelIndex - 1)._light4->setVisible(light);
}

void UISlotSuperClassic::sceneObjAnimCompleteCB(int trackIndex, int loopCount)
{
    if (_sceneObjDealer->isPlayingAnimation("idle01", trackIndex) && _dealerAnimName.compare("idle01") == 0)
    {
        if (_idleCount >= 3)
        {
            _dealerAnimName = "idle02";
            _idleCount = 0;
        }
        else
            ++_idleCount;

        return;
    }
    else if (_sceneObjDealer->isPlayingAnimation("idle02", trackIndex) && _dealerAnimName.compare("idle02") == 0)
    {
        _dealerAnimName = "idle01";
    }
    else if (_sceneObjDealer->isPlayingAnimation("win01", trackIndex) && _dealerAnimName.compare("win01") == 0)
    {
        return;
    }
    else if (_sceneObjDealer->isPlayingAnimation("win02", trackIndex) && _dealerAnimName.compare("win02") == 0)
    {
        return;
    }
    else if (_sceneObjDealer->isPlayingAnimation("touch", trackIndex) && _dealerAnimName.compare("touch") == 0)
    {
        _dealerAnimName = "idle01";
    }

    _sceneObjDealer->setToSetupPose();
    _sceneObjDealer->addAnimation(0, _dealerAnimName, true);
}

void UISlotSuperClassic::onSpinBtnTouchEvent(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::BEGAN)
    {
        _pBtnSpin->setHighlighted(true);
        _pBtnStop->setHighlighted(true);
    }
    else  if (type == Widget::TouchEventType::MOVED)
    {
        _pBtnSpin->setHighlighted(_pLayoutSpinListener->isHighlighted());
        _pBtnStop->setHighlighted(_pLayoutSpinListener->isHighlighted());
    }
    else  if (type == Widget::TouchEventType::CANCELED)
    {
        _pBtnSpin->setHighlighted(false);
        _pBtnStop->setHighlighted(false);
    }

    if (type != Widget::TouchEventType::ENDED)
        return;

    _pBtnSpin->setHighlighted(false);
    _pBtnStop->setHighlighted(false);

    if (_autoSpinStatus == AUTOSPINSTATUS_ON && _freeSpinStatus == FREESPINSTATUS_OFF)
    {
        stopAutoSpin();
        return;
    }

    if (_buttonStatus == SLOTBTNSTATUS_STOPINGREEL)
    {
        performStopSuddendly();
        return;
    }

    closeDropDownList();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);

    requestSpin();
}

void UISlotSuperClassic::onAutoBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::BEGAN)
    {
        _pBtnAuto->setHighlighted(true);
    }
    else  if (type == Widget::TouchEventType::MOVED)
    {
        _pBtnAuto->setHighlighted(_pLayoutAutoListener->isHighlighted());
    }
    else  if (type == Widget::TouchEventType::CANCELED)
    {
        _pBtnAuto->setHighlighted(false);
    }

    if (type != Widget::TouchEventType::ENDED)
        return;

    _pBtnAuto->setHighlighted(false);

    if (isScrolling())
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    stopPerformSpinResult();

    bool isAlreadyOpened = _pLayoutAutoDropdownList->isVisible();

    closeDropDownList();

    if (!_pLayoutAutoDropdownList->isVisible())
    {
        _pLayoutAutoDropdownList->stopAllActions();
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutAutoDropdownList, true)));
        arrayOfActions.pushBack(FadeIn::create(DROPDOWNLIST_FADETIME));
        auto actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutAutoDropdownList->runAction(actionsSeq);
        _pLayoutCloseDropdownListener->setVisible(true);
    }
}

void UISlotSuperClassic::onMaxBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::BEGAN)
    {
        _pBtnMaxBet->setHighlighted(true);
    }
    else  if (type == Widget::TouchEventType::MOVED)
    {
        _pBtnMaxBet->setHighlighted(_pLayoutMaxBetListener->isHighlighted());
    }
    else  if (type == Widget::TouchEventType::CANCELED)
    {
        _pBtnMaxBet->setHighlighted(false);
    }

    if (type != Widget::TouchEventType::ENDED)
        return;

    _pBtnMaxBet->setHighlighted(false);

    if (isAutoSpining())
        return;

    if (isScrolling())
        return;

    //停止播放spin result
    stopPerformSpinResult();

    closeDropDownList();

    setMaxLineAndBet();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);

    requestSpin();
}

void UISlotSuperClassic::onBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::BEGAN)
    {
        _pBtnBet->setHighlighted(true);
        _pBMFCurrentBet->setPosition(_currentBetBMFPos - Vec2(0.0f, 19.0f));
    }
    else  if (type == Widget::TouchEventType::MOVED)
    {
        if (_pLayoutBetListener->isHighlighted())
        {
            _pBtnBet->setHighlighted(true);
            _pBMFCurrentBet->setPosition(_currentBetBMFPos - Vec2(0.0f, 19.0f));
        }
        else
        {
            _pBtnBet->setHighlighted(false);
            _pBMFCurrentBet->setPosition(_currentBetBMFPos);
        }
    }
    else  if (type == Widget::TouchEventType::CANCELED)
    {
        _pBtnBet->setHighlighted(false);
        _pBMFCurrentBet->setPosition(_currentBetBMFPos);
    }

    if (type != Widget::TouchEventType::ENDED)
        return;

    _pBtnBet->setHighlighted(false);
    _pBMFCurrentBet->setPosition(_currentBetBMFPos);

    //自動spin中
    if (isAutoSpining())
        return;

    stopPerformSpinResult();

    if (!_pLayoutBetDropdownList)
        return;

    bool isAlreadyOpened = _pLayoutBetDropdownList->isVisible();

    closeDropDownList();


    for (auto pBetLevelLayout : _pListBetLevels->getChildren())
    {
        Button* pBtn = static_cast<Button*>(pBetLevelLayout->getChildByName("btn_betlevel"));
        ImageView* pJPImg = static_cast<ImageView*>(pBetLevelLayout->getChildByName("img_jpmark"));
        ImageView* pLockImg = static_cast<ImageView*>(pBetLevelLayout->getChildByName("img_lock"));
        int betValue = _betLevelList.at(pBtn->getTag());
        pJPImg->setVisible(betValue >= _jpBetLimit && _pLayoutJPBanner->isVisible());
        if ((_isFixedBet && !UISlotUtils::isFixedBetUsable(_slotID, betValue)) || (!_isFixedBet && !Player::getInstance()->isBetUsable(betValue)))
            pLockImg->setVisible(true);
        //pBtn->setEnabled(false);
        else
            pLockImg->setVisible(false);
        //pBtn->setEnabled(true);
    }

    if (!_pLayoutBetDropdownList->isVisible())
    {
        _pLayoutBetDropdownList->stopAllActions();
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(Node::setVisible, _pLayoutBetDropdownList, true)));
        arrayOfActions.pushBack(FadeIn::create(DROPDOWNLIST_FADETIME));
        auto actionsSeq = Sequence::create(arrayOfActions);
        _pLayoutBetDropdownList->runAction(actionsSeq);
        _pLayoutCloseDropdownListener->setVisible(true);

        //將目前bet設到ListView中間
        SetBetLevelListCurPos(_betLevelList.size() - 1 - _curBet - 1);
    }
    setCurBet(_curBet);
}

void UISlotSuperClassic::onPayTableBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type == Widget::TouchEventType::BEGAN)
    {
        _pBtnPayTable->setHighlighted(true);
    }
    else  if (type == Widget::TouchEventType::MOVED)
    {
        _pBtnPayTable->setHighlighted(_pLayoutPaytableListener->isHighlighted());
    }
    else  if (type == Widget::TouchEventType::CANCELED)
    {
        _pBtnPayTable->setHighlighted(false);
    }

    if (type != Widget::TouchEventType::ENDED)
        return;

    _pBtnPayTable->setHighlighted(false);

    closeDropDownList();
    UIPayTable* pPayTable = dynamic_cast<UIPayTable*>(UIController::getInstance()->getController(EUITAG_SUPERCLASSIC_PAYTABLE));
    if (!pPayTable)
        return;
    std::vector<int> symbolTypes = { SymbolInfoData::ESymbolType::SYMBOLTYPE_JACKPOT, SymbolInfoData::ESymbolType::SYMBOLTYPE_SCATTER, SymbolInfoData::ESymbolType::SYMBOLTYPE_WILD };
    std::vector<int> stringData1 = { 10034 };
    std::vector<int> stringData2 = { 10002, 10003, 10004, 10005 };
    std::vector<int> stringData3 = { 10007 };
    pPayTable->setPage1Data(symbolTypes, stringData1, stringData2, stringData3);
    pPayTable->openUI(_slotID, "GUI/UISlot_SuperClassic/UISlot_SuperClassic_PayTable.ExportJson");
}

void UISlotSuperClassic::onTouchSceneListener(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    performSceneAnim(ESuperClassicSceneAnim::SCENEANIM_TOUCH);
}

void UISlotSuperClassic::onAutoCountValueClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    //停止播放spin result
    stopPerformSpinResult();

    //設定autospin倒數, 關閉DropDownList
    setAutoCount(s_autoCountValue[index]);
    setAutoSpinStatus(AUTOSPINSTATUS_ON);

    closeDropDownList();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
    requestSpin();
}

void UISlotSuperClassic::onAutoUnlimitedClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //自動spin中
    if (isAutoSpining())
        return;

    _isAutoSpinUnlimited = true;

    //停止播放spin result
    stopPerformSpinResult();

    //設定autospin倒數, 關閉DropDownList
    setAutoCount(0);
    setAutoSpinStatus(AUTOSPINSTATUS_ON);

    closeDropDownList();

    if (isOpen())
        AudioManager::getInstance()->playEffectWithFileID(_pSlotData->SpinBtnSoundID, false);
    requestSpin();
}

void UISlotSuperClassic::onAutoSpinSetting(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    UIController::getInstance()->getController(EUITAG_UIAUTOSPIN_SETTING)->openUI();
}

void UISlotSuperClassic::onNextBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    SetBetLevelListCurPos(_betLevelListCurPos - 1);
}

void UISlotSuperClassic::onPreviousBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    SetBetLevelListCurPos(_betLevelListCurPos + 1);
}

void UISlotSuperClassic::onBetListBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type, int index)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    //選擇Bet
    int betIdx = static_cast<Button*>(pSender)->getTag();
    int betValue = _betLevelList.at(betIdx);
    if (_isFixedBet && !UISlotUtils::isFixedBetUsable(_slotID, betValue))
    {
        auto unlockLevel = UISlotUtils::getFixedBetUnlockLevel(_slotID, betValue);
        auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
        messageController->clearMessage();
        messageController->pushMessage(composeStringData(safeGetStringData(34), StringUtils::toString(unlockLevel)));
        return;
    }
    else if (!_isFixedBet && !Player::getInstance()->isBetUsable(betValue))
    {
        auto unlockLevel = UISlotUtils::getBetUnlockLevel(betValue);
        auto messageController = UIController::getInstance()->getController<UIShortMessage*>(EUITAG_MESSAGE);
        messageController->clearMessage();
        messageController->pushMessage(composeStringData(safeGetStringData(34), StringUtils::toString(unlockLevel)));
        return;
    }

    //關閉DropDownList
    closeDropDownList();

    setCurBet(betIdx);
}

void UISlotSuperClassic::onDropDownListBGTouch(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    closeDropDownList();
}

void UISlotSuperClassic::onDropSpinBarEvent(Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
    Widget* touch = (Widget*)pSender;

    float totalLength = _pLayoutSpinBarBegin->getPositionY() - _pLayoutSpinBarEnd->getPositionY();

    _spinBarTouchType = type;
    if (type == Widget::TouchEventType::BEGAN)
    {
        
    }
    else if (type == Widget::TouchEventType::MOVED)
    {
        _spinBarPercent = (_pLayoutSpinBarBegin->getPositionY() - touch->getTouchMovePosition().y) / totalLength;
        CCLOG("begin pos = %f, touch pos = %f, total length = %f, percent = %f",
            _pLayoutSpinBarBegin->getPositionY(), 
            touch->getTouchMovePosition().y,
            totalLength, 
            _spinBarPercent);
        const int DEGREE = 5;
        static float lastDegree = 0;
        int curDegree = (int)(_spinBarPercent*DEGREE);
        if (curDegree > DEGREE)
            curDegree = DEGREE;
        if (curDegree<0)
            curDegree = 0;
        if (curDegree > lastDegree && curDegree != lastDegree)
        {
            if (curDegree == DEGREE)
                AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT4_BAR_MIDDLE, false);
            else
                AudioManager::getInstance()->playEffectWithFileID(EFF_SLOT4_BAR_BEGIN, false);
        }
            
        lastDegree = curDegree;
        //if (_spinBarPercent <= 0.0f)
    }
    else if (type == Widget::TouchEventType::CANCELED)
    {

    }
    else if (type == Widget::TouchEventType::ENDED)
    {

    }


}

void UISlotSuperClassic::onSingleReelEnterMiddleExpectCB(int reelOrder)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !this->isOpen())//slot depend
        return;

    if (!_isMiddleExpectSoundPlayed.at(reelIndex))
    {
        _isMiddleExpectSoundPlayed.at(reelIndex) = true;
        _middleExpectSoundHandle = AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_MIDDLEEXPECT, false);
    }
}

void UISlotSuperClassic::onSingleReelEnterEndCB(int reelOrder)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !this->isOpen())//slot depend
        return;

    //加速或停止下一滾輪
    //必須在EnterEndCB處理, 若在PendingStopCB處理, 下一滾輪會多等一圈
    if (reelOrder != _reelCount)
    {
        int scatterCounter(0);
        bool haveChanceScatter = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _scatterID, _minScatterCombo, SymbolInfoData::SYMBOLTYPE_SCATTER, reelOrder, scatterCounter);

        int bonusCounter(0);
        bool haveChanceBonus = false;


        auto pNextReel = _reels.at(reelIndex + 1);
        //若已停輪或快停輪則不處理 "加速或停止下一滾輪"
        if (pNextReel->getSpinState() == ESPIN_INFINITE_RUNNING || pNextReel->getSpinState() == ESPIN_INFINITE_RUNNING_EXPECT)
        {
            bool expectHitScatter = haveChanceScatter && scatterCounter >= _minScatterCombo - 1;
            bool expectgHitBonus = haveChanceBonus && bonusCounter >= _minBonusCombo - 1;
            //聽牌
            if (expectHitScatter || expectgHitBonus)
            {
                Vector<FiniteTimeAction*> arrayOfActions;
                arrayOfActions.pushBack(DelayTime::create(UIReel::st4_secPerRound));
                arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::beginInfiniteExpect, pNextReel)));
                arrayOfActions.pushBack(DelayTime::create(2.0f));
                arrayOfActions.pushBack(CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, pNextReel)));
                Sequence* actionsSeq = Sequence::create(arrayOfActions);
                actionsSeq->setTag(TAG_STOPSINGLEREEL);
                pNextReel->runAction(actionsSeq);
            }
            else
            {
                //action for delay a frame
                //若沒有延遲一個frame, 全部輪會一起停止, 
                //一個frame足以讓下個reel進入下一圈, 或可設定一個DelayTime
                auto delayTime = DelayTime::create(0.3f);
                CallFunc* pStopNextReelAction = CallFunc::create(CC_CALLBACK_0(UIReel::stopInfinite, pNextReel));
                pStopNextReelAction->setTag(TAG_STOPSINGLEREEL);
                Sequence* actionsSeq = Sequence::create(delayTime, pStopNextReelAction, nullptr);
                pNextReel->runAction(actionsSeq);
            }
        }
    }
}

void UISlotSuperClassic::onSingleReelPendingStopCB(int reelOrder)
{
    if (!this->isOpen())
        return;
    //整理已停止Reel的ExpectBG
    //reelOrder前的reel都已停止
    //最新停止的一輪需要播放音效
    refineAllReelExpectBGByChance(reelOrder);
    //最後一輪停止
    if (reelOrder == _reelCount)
    {
        if (_buttonStatus == SLOTBTNSTATUS_STOPSUDDENDLY)//快速停輪過程不表現ExpectBG, 待停輪時一次表現
            refineGivenReelExpectBGByHit(isHitScatter(), isHitBonus(), _reelCount, false);
    }


    setReelLight(reelOrder, true);
}

void UISlotSuperClassic::onSingleReelStopCB(int reelOrder)
{
    int reelIndex = reelOrder - 1;
    if (reelIndex < 0 || reelIndex >= (int)_slotIDListRcv.size() || !this->isOpen())//slot depend
        return;


    //最後一輪停止
    if (reelOrder == _reelCount)
    {
        //有中獎表現中獎
        performSpinResult(_paylineRcv, _jpRcv, _winRcv, _freeSpinRcv, _bonusRcv, _profileRcv, _levelupRcv);
        //沒中獎如果正在autoSpin則繼續(沒中獎可能有升級界面)
        //if (!isHit())
        //{
        //    if (isAutoSpining())
        //    {
        //        Vector<FiniteTimeAction*> arrayOfActions;
        //        arrayOfActions.pushBack(DelayTime::create(1.0f));
        //        arrayOfActions.pushBack(CallFunc::create(
        //            [this](){
        //            stopPerformSpinResult();
        //            requestSpin();
        //        }
        //        ));
        //        Sequence* actionsSeq = Sequence::create(arrayOfActions);
        //        runAction(actionsSeq);
        //    }
        //};
        Player::getInstance()->setCoin(_profileRcv.coin);//停輪後才更新coin

        //若中scatter, bonus設定特殊背景
        refineGivenReelExpectBGByHit(isHitScatter(), isHitBonus(), _reelCount, true);//on this moment reelOrder==_reeCount
        if (!isHit())
            colorAllSymbol(Color4B::WHITE);
    }
}

void UISlotSuperClassic::onSingleReelTriggerStopSoundCB(int reelOrder)
{
    if (isOpen())
    {
        //快速停輪時只播最後一輪停輪聲
        if (_buttonStatus == SLOTBTNSTATUS_STOPSUDDENDLY)
        {
            if (reelOrder == _reelCount)
                AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_REEL_STOP, false);
        }
        //一般停輪播放每輪停輪聲
        else
            AudioManager::getInstance()->playEffectWithFileID(EFF_COMMON_REEL_STOP, false);
    }
}

void UISlotSuperClassic::refineAllReelExpectBGByChance(int reelOrder)
{
    if (_buttonStatus == SLOTBTNSTATUS_STOPSUDDENDLY)//快速停輪不表現ExpectBG
        return;

    int scatterCounter(0);
    bool haveChanceScatter = UISlotUtils::haveChanceHitSpecialSymbolOnSingleReelStop(_slotID, _slotIDListRcv, _scatterID, _minScatterCombo, SymbolInfoData::SYMBOLTYPE_SCATTER, reelOrder, scatterCounter);

    int bonusCounter(0);
    bool haveChanceBonus = false;

    refineGivenReelExpectBGByHit(haveChanceScatter, haveChanceBonus, reelOrder, false);
}

void UISlotSuperClassic::refineGivenReelExpectBGByHit(bool hitScatter, bool hitBonus, int maxReelOrder, bool isResult)
{
    int maxReelIndex = maxReelOrder - 1;
    if (maxReelIndex < 0 || maxReelIndex >= (int)_slotIDListRcv.size())//slot depend
        return;

    int beingSymbolInfoID = _pSlotData->SymbolInfoBeginID;
    bool haveChanceHybrid = hitScatter && hitBonus;
    bool noChance = !hitScatter && !hitBonus;
    for (int i = 0; i < (int)_reels.size(); i++)
    {
        auto& reel = _reels.at(i);

        int performReelOrder = i + 1;
        //不可能中Scatter和Bonus
        if (noChance)
        {
            performExpectBG(performReelOrder, EXPECT_NONE);
            if (isResult)
                reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            continue;
        }

        //可能中, 但還未停輪的reel不需背景
        if (i > maxReelIndex)
        {
            performExpectBG(performReelOrder, EXPECT_NONE);
            if (isResult)
                reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            continue;
        }

        //可能中, 已停輪的reel判斷需要顯示什麼背景
        bool haveScatter = false;
        bool haveBonus = false;
        if (!isResult)
        {
            for (auto id : _slotIDListRcv.at(i))
            {
                int symbolID = id + beingSymbolInfoID;
                if (symbolID == _scatterID)
                    haveScatter = true;
                else if (symbolID == _bonusID)
                    haveBonus = true;
            }
        }
        else
        {
            for (auto hitCoord : _freeSpinRcv.hitCoord)
            {
                if (hitCoord.x - 1 == i)
                {
                    haveScatter = true;
                    break;
                }
            }
            if (!haveBonus)
            {
                for (auto hitCoord : _bonusRcv.hitCoord)
                {
                    if (hitCoord.x - 1 == i)
                    {
                        haveBonus = true;
                        break;
                    }
                }
            }
            if (!haveBonus)
            {
                for (auto hitCoord : _bonuswildRcv.hitCoord)
                {
                    if (hitCoord.x - 1 == i)
                    {
                        haveBonus = true;
                        break;
                    }
                }
            }
        }

        bool haveHybrid = haveScatter&&haveBonus;

        if (haveChanceHybrid)       //可能中Scatter和Bonus
        {
            if (haveHybrid)
            {
                performExpectBG(performReelOrder, EXPECT_HYBRID);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else if (haveScatter)
            {
                performExpectBG(performReelOrder, EXPECT_SCATTER);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else if (haveBonus)
            {
                performExpectBG(performReelOrder, EXPECT_BONUS);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            }
            continue;
        }

        if (hitScatter)      //只可能中Scatter
        {
            if (haveScatter)
            {
                performExpectBG(performReelOrder, EXPECT_SCATTER);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(PAYLINEBG_DARKCOLOR));
            }

            continue;
        }
        else if (hitBonus)   //只可能中Bonus
        {
            if (haveBonus)
            {
                performExpectBG(performReelOrder, EXPECT_BONUS);
                if (isResult)
                    reel->colorAllSymbol(PAYLINEBG_DARKCOLOR_WITHEXPECTBG);
            }
            else
            {
                performExpectBG(performReelOrder, EXPECT_NONE);
                if (isResult)
                    reel->colorAllSymbol(Color4B(Color4B(PAYLINEBG_DARKCOLOR)));
            }

            continue;
        }
    }
}
