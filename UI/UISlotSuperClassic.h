#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"
#include "UIReel.h"
#include "UISlotUtils.h"
#include "CustomParticleSystemGroup.h"
#include "../Animation/CustomSpAnim.h"

USING_NS_CC;

using namespace ui;

struct ReelLight
{
    ImageView* _light1;
    ImageView* _light2;
    ImageView* _light3;
    ImageView* _light4;

    ReelLight()
    {
        _light1 = nullptr;
        _light2 = nullptr;
        _light3 = nullptr;
        _light4 = nullptr;
    }

    ~ReelLight()
    {}
};

enum ESuperClassicSceneAnim
{
    SCENEANIM_IDLE,   // 閒置
    SCENEANIM_WIN_1,  // win 1
    SCENEANIM_WIN_2,  // win 2
    SCENEANIM_TOUCH,  // touch
};

class UISlotSuperClassic : public UIBasicUnit
{
public:
    enum ESlotActionTag
    {
        TAG_ONCEALLPAYLINE,
        TAG_REPEATSINGLEPAYLINE,
        TAG_ONCEALLPAYLINESYMBOL,
        TAG_REPEATSINGLEPAYLINESYMBOL,
        TAG_PERFORMSPINRESULT,
        TAG_SETPAYTOMAX,
        TAG_SETPAYTOMIN,
        TAG_STOPSINGLEREEL,
        TAG_STOPFIRSTREEL,
        TAG_PERFORMSPIN_MINTIME,
    };

    static const int AUTOCOUNTLEVEL = 3;        //auto spin的階數
    static int s_autoCountValue[AUTOCOUNTLEVEL];//auto spin每階值
    static const int PAYLINECOMPONENT_MAX = 5;  //組成一條PayLine圖數量
    static const float EDIDLINEBTN_HOLDTIME;    //持續按多久add, minus可直接設成_maxLines, _minLines
    static const float LINEANIMTIME;            //PayLine動畫時間
    static const int BETLISTVIEW_ITEMCOUNT = 3; //bet選單顯示數量
    static const float DROPDOWNLIST_FADETIME;   //選單fadeIn, fadeOut時間
    static const float PERFORMSPIN_MINTIME;     //開始滾輪到開始停輪最小時間(server已回應仍需等待最小時間, 但可快速停輪)
    static const cocos2d::Color3B BETSELECT_DARKCOLOR;    //BetList無法向上或無法向下時按鈕暗色
    static const cocos2d::Color3B PAYLINEBG_DARKCOLOR;    //表現中獎時,沒中獎符號暗色
    static const cocos2d::Color4B PAYLINEBG_DARKCOLOR_WITHEXPECTBG;    //表現中獎時,沒中獎符號若有ExpectBG時暗色

    static UISlotSuperClassic* create(int slotMachineID);

public:
    UISlotSuperClassic();
    ~UISlotSuperClassic();

    virtual void destroyUI() override;
    virtual void notifyOpenUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;
    virtual void Update(float dt) override;
    virtual void UpdatePerSecond(float dt) override;
    virtual void playEnterUIAction() override;

public:
    virtual void onDispatchResponse(const std::string& requestTag, const std::string& json);

    //sense of SpinResponse
    bool isHitJackpot();    //SpinResponse是否中了jackpot
    bool isHitScatter();    //SpinResponse是否中了freeSpin( isStartFreeSpin() || isAddFreeSpin() )
    bool isHitBonus();      //SpinResponse是否中了bonus
    bool isHitFiveOfAKind();//SpinResponse是否中了FiveOfAKind
    bool isHitBigWin();     //SpinResponse是否中了BigWin
    bool isHit();           //SpinResponse是否中了任何獎項(特殊符號,一般符號, 會做playAllPayLineSymbol)
    bool isShowBigWin();    //SpinResponse是否會showBigWin, isHitBigWin不一定需要show, 可能因為中其它大獎省略
    bool isLevelUp();       //SpinResponse是否升級
    bool isStartFreeSpin(); //SpinResponse是否開始freeSpin(非freespin時中了freespin)
    bool isEndFreeSpin();   //SpinResponse是否結束freeSpin
    bool isAddFreeSpin();   //SpinResponse是否增加freeSpin(在freespin裡中了freespin)
    bool isDoingFreeSpin(); //SpinResponse是否為一次freespin

    //UI operate function
    void closeDropDownList(bool fadeOut = true);                  //關閉所有DropList選單
    int getAutoCountValueTitleByTag(int tag);                   //依Button tag取得AutoSpin的值
    bool isScrolling();                                         //是否正在表現滾輪
    void performSpin();                                         //表現持續spin
    void performStopSpinWithResult(SlotIDList& result);         //表現spin結果
    void performStopSuddendly();                                //表現手動快速停輪
    void requestSpin();                                         //發送一次spin請求
    void showFreeSpinCount(bool show);
    void setFreeSpinCount(int count);
    void performAddFreeSpinCount(int addCount);
    void setSlotButtonStatus(ESlotButtonStatus status);         //設定按鈕可否點擊
    void setAutoSpinStatus(EAutoSpinStatus status);             //設定按鈕可否點擊
    void setFreeSpinStatus(EFreeSpinStatus status);             //設定按鈕可否點擊

    std::vector<cocos2d::Vec2> getPaylinePtList(cocos2d::ui::Layout* paylineLayout, int payLineOrder);
    void iniPayLineComponent();                                             //初始化payline元件
    void showPayline(int payLineOrder, bool show);                          //顯示單條payline靜態圖
    void showPaylineLessThanPayLineOrder(int payLineOrder, bool show);      //顯示所有小於payLineID的payline靜態圖
    void hideAllPayline();                                                  //隱藏所有payline靜態圖
    void playPayLine(int payLineID, bool play, bool Loop);                  //播放單條payline動畫
    void stopPlayPayLine(int payLineID);                                    //停止播放單條palyline動畫
    void stopPlayAllPayLine();                                              //停止播放所有palyline動畫
    void onceAllPayLines(std::vector<SpinResponse_PayLine>& payLines, SpinResponse_Win& winRcv);  //中獎播放一次全部中獎線
    void repeatSinglePayLines(std::vector<SpinResponse_PayLine>& payLines); //中獎重覆輪播單條中獎線
    void playAllPayLineSymbol(std::vector<SpinResponse_PayLine>& payLines); //播放palyline上的symbol動畫
    void playSinglePayLineSymbol(SpinResponse_PayLine& payLine);            //播放palyline上的symbol動畫
    void clearPayLineSymbol();                                              //清除palyline上的symbol動畫
    void visibleAllSymbol();                                                //設定所有symbol可見
    void colorAllSymbol(const cocos2d::Color4B& color4B);                   //對所有symbol設定color
    void performWinMoney(int win);                                          //表現_pBMFWinMoneyAmin(放大, 數錢)
    void performSceneAnim(ESuperClassicSceneAnim anim);                                 //表現場景動畫
    void performExpectBG(int reelOrder, EExpectType expectBG);              //表現期待感背景, reelOrder = 1 ~ _reelCount
    void stopPerformExpectBG();                                             //停止表現期待感背景

    void setPayLineBG(bool show);                                           //設定PayLine半透明黑底
    cocos2d::Vec2 getSymbolCoord(int payLineID, int order);                 //回傳LineID的第x個是盤面的第x輪第y個
    void getSymbolOnPayLine(int x, int y, cocos2d::Vec2& out_Position, UISymbolBase** out_Symbol); //取得盤面第x輪第y個的Symbol

    void performSpinResult(
        std::vector<SpinResponse_PayLine>& pay_lineRcv,
        SpinResponse_JP& jpRcv,
        SpinResponse_Win& winRcv,
        SpinResponse_FreeSpin& freeSpinRcv,
        SpinResponse_Bonus& bonusRcv,
        SpinResponse_Profile& profileRcv,
        SpinResponse_LevelUp& levelupRcv);                                  //表現中獎: payline, symbol動畫, 特殊中獎
    void stopPerformSpinResult();                                           //中斷表現中獎
    bool isPerformSpinResult(EPerformResultType type = RESULTTYPE_NORMAL);

    void playWinLight();
    void playSpinLight();
    void playNormalLight();

    //set model data
    void setAutoCount(int autoCount);
    bool isAutoSpining(){ return _isAutoSpinUnlimited || _autoCount > 0; };
    void stopAutoSpin();
    void setWin(uint64 win);
    void updateTotalBet();

    void SetBetLevelListCurPos(int pos);
    void setCurBet(int bet);
    void setCurLines(int lines);
    void setMaxLineAndBet();
    int getPayLineOrder(int payLineID);
    void setJPEnable(bool enable);
    void updateJackpotHint();
    void setJackpot(uint64 jackpot);
    void hideJackpotHint();
    void setJackpotHint(bool overLimit);
    int getJackpot(){ return _jackpot; };
    void setReelCount(int reelCount){ _reelCount = reelCount; };
    int getReelCount(){ return _reelCount; };
    //const std::vector<int>& getViewFrameCounts(){ return _viewFrameCounts; };
    void clearLastResult();
    void clearRcv();
    void saveRcv(
        SlotIDList& slotIDListRcv,
        SpinResponse_Overall& overallRcv,
        std::vector<SpinResponse_PayLine>& paylineRcv,
        SpinResponse_JP& jpRcv,
        SpinResponse_Win& winRcv,
        SpinResponse_FreeSpin& freeSpinRcv,
        SpinResponse_Bonus& bonusRcv,
        SpinResponse_Bonuswild& bonuswildRcv,
        SpinResponse_Profile& profileRcv,
        SpinResponse_LevelUp& levelupRcv);

    // out of coin callback
    void outOfCoinKeepPlayingCB();
    int getBetIndexByBetValue(int betValue);

    // reel light
    void stopAllLight();
    void setReelLight(int reelIndex, bool light);

    void sceneObjAnimCompleteCB(int trackIndex, int loopCount);

private:
    bool init(int slotMachineID);
    virtual bool createUI() override;

private:
    //event of control panel
    void onSpinBtnTouchEvent(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onAutoBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onMaxBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onPayTableBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onTouchSceneListener(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //event of auto button in drop list
    void onAutoCountValueClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type, int index);
    void onAutoUnlimitedClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onAutoSpinSetting(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //event of bet button in drop list
    void onNextBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onPreviousBetBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);
    void onBetListBtnClick(Ref *pSender, cocos2d::ui::Widget::TouchEventType type, int index);

    //event of drop list bg
    void onDropDownListBGTouch(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    // event of spin bar
    void onDropSpinBarEvent(Ref *pSender, cocos2d::ui::Widget::TouchEventType type);

    //callback of a reelStop
    void onSingleReelEnterMiddleExpectCB(int reelOrder);
    void onSingleReelEnterEndCB(int reelOrder);
    void onSingleReelPendingStopCB(int reelOrder);  //reelOrder = 1 ~ _reelCount
    void onSingleReelStopCB(int reelOrder);         //reelOrder = 1 ~ _reelCount
    void onSingleReelTriggerStopSoundCB(int reelOrder); //reelOrder = 1 ~ _reelCount
    void refineAllReelExpectBGByChance(int reelOrder);                                                  //依目前停輪狀態的scatter, bonus中獎機會設定ExpectBG
    void refineGivenReelExpectBGByHit(bool hitScatter, bool hitBonus, int maxReelOrder, bool isResult); //依hit結果和reel中是否有scatter, bonus設定ExpectBG

private:
    std::vector<ExpectBGGroup> _expectBgs;          //特殊符號背景
    std::vector<bool> _isExpectBgSoundPlayed;       //是否播過特殊符號背景音效
    std::vector<bool> _isMiddleExpectSoundPlayed;   //是否播過特殊符號聽牌音效
    unsigned int _middleExpectSoundHandle = 0;
    unsigned int _spinSoundHandle = 0;
    std::vector<UIReel*> _reels;

    //model data
    SlotMachineData* _pSlotData = nullptr;
    int _slotID = 4;            //ID of SlotMachine Table
    int _autoCount;         //剩餘auto次數
    int _freeSpinCount;     //剩餘freespin次數
    uint64 _win;               //最近一次中獎金額
    int _totalBet;          //lines*bet
    int _curBet;            //目前每線壓注bet
    std::vector<int> _betLevelList; //bet選單顯示清單
    bool _isFixedBet = false;       // fixed bet
    int _betLevelListCurPos;        //bet選單目前位置, 非_curBet
    int _curLines;          //目前壓注線數
    int _minLines;          //最小壓注線數
    int _maxLines;          //最大壓注線數
    int _beginPayLineID;
    int _endPayLineID;
    int _scatterID;         //scatter symbol ID
    int _bonusID;           //bonus symbol ID
    int _minScatterCombo;   //scatter中獎最少combo
    int _minBonusCombo;     //bonus中獎最少combo
    uint64 _jackpot;           //目前jackpot
    int _jpBetLimit;        //多少bet以上可以參加jackpot
    int _reelCount;         //slot總共幾reel
    float _spinBarPercent;
    bool _spinBarTouchBottom = false;
    bool _isAutoSpinUnlimited = false;
    Widget::TouchEventType _spinBarTouchType;
    ESlotButtonStatus _buttonStatus;
    EAutoSpinStatus _autoSpinStatus;
    EFreeSpinStatus _freeSpinStatus;

    // 
    CustomSpAnim* _sceneObjDealer = nullptr;              //場景物件 女荷官
    Layout* _pLayoutSceneObjListener = nullptr;

    // UI compoment
    Layout* pRoot = nullptr;
    Layout* _pLayoutPaylineRoot = nullptr;
    //Layout* _pLayoutSeparator = nullptr;               //Reel分隔線圖
    Layout* _pLayoutReels = nullptr;                   //put reel, clip reel
    Layout* _pLayoutSymbolAnim = nullptr;              //a layout for play symbol spine animation
    Layout* _pLayoutSlotReel = nullptr;                //機台中景
    Layout* _pLayoutAutoDropdownList = nullptr;
    Layout* _pLayoutBetDropdownList = nullptr;
    Layout* _pLayoutCloseDropdownListener = nullptr;
    Layout* _pLayoutAutoDisable = nullptr;
    Layout* _pLayoutSpinListener = nullptr;
    Layout* _pLayoutAutoListener = nullptr;
    Layout* _pLayoutMaxBetListener = nullptr;
    Layout* _pLayoutBetListener = nullptr;
    Layout* _pLayoutPaytableListener = nullptr;
    Layout* _pLayoutFreeSpin = nullptr;
    Layout* _pLayoutDummyWinMoneyParticle = nullptr;   //少量金幣dummy
    Layout* _pLayoutDummyPaylineParticle = nullptr;    //payline particle dummy
    Layout* _pLayoutSpinBarBegin = nullptr;
    Layout* _pLayoutSpinBarEnd = nullptr;
    Layout* _pLayoutArrow = nullptr;
    Layout* _pLayoutJPBanner = nullptr;
    TextBMFont* _pBMFAutoCount = nullptr;
    TextBMFont* _pBMFWinNum = nullptr;
    TextBMFont* _pBMFWinMoneyAmin = nullptr;
    TextBMFont* _pBMFTotlaBetNum = nullptr;
    TextBMFont* _pBMFCurrentBet = nullptr;
    TextBMFont* _pBMFCurrentLines = nullptr;
    TextBMFont* _pBMFLineNumEdit = nullptr;
    TextBMFont* _pBMFJPNum = nullptr;
    TextBMFont* _pBMFFreeSpinRemain = nullptr;
    //ImageView* _pImgStopText = nullptr;
    //ImageView* _pImgSpinText = nullptr;
    ListView* _pListBetLevels = nullptr;
    Button* _pBtnSpin = nullptr;
    Button* _pBtnStop = nullptr;
    Button* _pBtnAuto = nullptr;
    Button* _pBtnMaxBet = nullptr;
    Button* _pBtnBet = nullptr;
    Button* _pBtnPayTable = nullptr;
    Button* _pNextBtn = nullptr;
    Button* _pPreviousBtn = nullptr;
    ImageView* _pImgJPCheck = nullptr;
    ImageView* _pImgJPUncheck = nullptr;

    Vec2 _currentBetBMFPos;
    std::vector<ReelLight> _reelLights;

    cocostudio::ActionObject* _pActionEnterAnim = nullptr;
    cocostudio::ActionObject* _pActionSpinBar = nullptr;
    cocostudio::ActionObject* _pActionSpinArrow = nullptr;
    cocostudio::ActionObject* _pActionWinLight = nullptr;
    cocostudio::ActionObject* _pActionSpinLight = nullptr;
    cocostudio::ActionObject* _pActionNormalLight = nullptr;

    //暫存server回應
    SlotIDList _slotIDListRcv;
    SpinResponse_Overall _overallRcv;
    std::vector<SpinResponse_PayLine> _paylineRcv;
    SpinResponse_JP _jpRcv;
    SpinResponse_Win _winRcv;
    SpinResponse_FreeSpin _freeSpinRcv;
    SpinResponse_Bonus _bonusRcv;
    SpinResponse_Bonuswild _bonuswildRcv;
    SpinResponse_Profile _profileRcv;
    SpinResponse_LevelUp _levelupRcv;

    std::string _dealerAnimName;
    int _idleCount = 0;
};