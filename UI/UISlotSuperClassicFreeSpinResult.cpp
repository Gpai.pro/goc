#include "UISlotSuperClassicFreeSpinResult.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Animation/CustomActions.h"
#include "../DataCenter/ExternalTable.h"
#include "AudioManager.h"


using namespace cocostudio;
using namespace ui;


UISlotSuperClassicFreeSpinResult* UISlotSuperClassicFreeSpinResult::create()
{
    UISlotSuperClassicFreeSpinResult* unit = new UISlotSuperClassicFreeSpinResult();

    if (unit && unit->init())
    {
        return unit;
    }

    CC_SAFE_DELETE(unit);
    return nullptr;
}

bool UISlotSuperClassicFreeSpinResult::init()
{
    return true;
}

UISlotSuperClassicFreeSpinResult::UISlotSuperClassicFreeSpinResult()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotSuperClassicFreeSpinResult::~UISlotSuperClassicFreeSpinResult()
{

}

void UISlotSuperClassicFreeSpinResult::notifyCloseUI()
{
    _pBMFFreeSpinCounter->stopAllActions();
    _pLayoutFreeSpinResult->stopAllActions();
    if (_onCloseUICB)
    {
        _onCloseUICB();
        _onCloseUICB = nullptr;
    }

    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->BGSoundID, true);
}

bool UISlotSuperClassicFreeSpinResult::createUI()
{
    _pLayoutFreeSpinResult = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("UISlot_SuperClassic/UISlot_SuperClassic_FreeSpin_Result.ExportJson"));
    _mainUINode->addChild(_pLayoutFreeSpinResult);

    setTraversalCascadeOpacityEnabled(_mainUINode, true);

    _pBMFWinMoney = _pLayoutFreeSpinResult->getChildByName<TextBMFont*>("bmf_winmoney");
    _pBMFWinMoney->setString("0");

    _pBMFFreeSpinCounter = _pLayoutFreeSpinResult->getChildByName<TextBMFont*>("bmf_freespin_counter");
    _pBMFFreeSpinCounter->setString("0");

    _pBtnCloseUI = _pLayoutFreeSpinResult->getChildByName<Button*>("btn_closeui");
    _pBtnCloseUI->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassicFreeSpinResult::onTouchExitButton, this));

    return true;
}

void UISlotSuperClassicFreeSpinResult::destroyUI()
{
    _pLayoutFreeSpinResult->removeFromParent();
    _pLayoutFreeSpinResult = nullptr;
}

void UISlotSuperClassicFreeSpinResult::updateUI()
{
    if (_winMoney<0)//�i��0��
        return;

    CountTextBMFontToUint64Num* pAction = CountTextBMFontToUint64Num::create(TIME_COUNTMONEY, 0, _winMoney);
    pAction->setFormat(CountTextBMFontToUint64Num::FORMAT_NONE);
    _pBMFWinMoney->runAction(pAction);

    _pBMFFreeSpinCounter->setString(Value(_freeSpinCounter).asString());

    if (_autoCloseUITime > 0.0f)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(_autoCloseUITime));
        arrayOfActions.pushBack(CallFunc::create([this](){ this->closeUI(); }));
        auto actionSeq = Sequence::create(arrayOfActions);
        actionSeq->setTag(TAG_CLOSEUI);
        _pLayoutFreeSpinResult->runAction(actionSeq);
    }
}

void UISlotSuperClassicFreeSpinResult::Update(float dt)
{
}

void UISlotSuperClassicFreeSpinResult::onTouchExitButton(Ref* sender, ui::Widget::TouchEventType event)
{
    if (event != ui::Widget::TouchEventType::ENDED)
        return;

    _pLayoutFreeSpinResult->stopActionByTag(TAG_CLOSEUI);
    closeUI();
}