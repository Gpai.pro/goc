#include "UISlotSuperClassicFreeSpinStart.h"
#include "cocostudio/CocoStudio.h"
#include "../GlobalDefine/GameFunc.h"
#include "../Animation/CustomActions.h"
#include "../DataCenter/ExternalTable.h"
#include "AudioManager.h"


using namespace cocostudio;
using namespace ui;


UISlotSuperClassicFreeSpinStart* UISlotSuperClassicFreeSpinStart::create()
{
    UISlotSuperClassicFreeSpinStart* unit = new UISlotSuperClassicFreeSpinStart();

    if (unit && unit->init())
    {
        return unit;
    }

    CC_SAFE_DELETE(unit);
    return nullptr;
}

bool UISlotSuperClassicFreeSpinStart::init()
{
    return true;
}

UISlotSuperClassicFreeSpinStart::UISlotSuperClassicFreeSpinStart()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UISlotSuperClassicFreeSpinStart::~UISlotSuperClassicFreeSpinStart()
{

}

void UISlotSuperClassicFreeSpinStart::notifyOpenUI()
{
    SlotMachineData* _pSlotData = ExternalTable<SlotMachineData>::getRecordByID(_slotID);
    AudioManager::getInstance()->playBGMusic(_pSlotData->FreeSpinBGSoundID, true);
}

void UISlotSuperClassicFreeSpinStart::notifyCloseUI()
{
    _pLayoutFreeSpinStart->stopAllActions();
    if (_onCloseUICB)
    {
        _onCloseUICB();
        _onCloseUICB = nullptr;
    }

    _pParticle1->stopSystem();
    _pParticle2->stopSystem();
    _pParticle3->stopSystem();
    _pParticle4->stopSystem();
}

bool UISlotSuperClassicFreeSpinStart::createUI()
{
    _pLayoutFreeSpinStart = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("UISlot_SuperClassic/UISlot_SuperClassic_FreeSpin_Start.ExportJson"));

    _mainUINode->addChild(_pLayoutFreeSpinStart);

    setTraversalCascadeOpacityEnabled(_mainUINode, true);

    _pLayoutCloseUIListenser = _pLayoutFreeSpinStart->getChildByName<Layout*>("layout_listener_closeui");
    CCASSERT(_pLayoutCloseUIListenser, "");
    _pLayoutCloseUIListenser->addTouchEventListener(CC_CALLBACK_2(UISlotSuperClassicFreeSpinStart::onTouchCloseUIListener, this));

    _pBMFFreeSpinCounter = _pLayoutFreeSpinStart->getChildByName<TextBMFont*>("bmf_freespin_counter");
    CCASSERT(_pBMFFreeSpinCounter, "");
    _pBMFFreeSpinCounter->setString("0");

    Layout* particleDummy = _pLayoutFreeSpinStart->getChildByName<Layout*>("freespins_win_fx");
    particleDummy->setVisible(true);
    _pParticle1 = ParticleSystemQuad::create("Particle/freespins_win01.plist");
    _pParticle2 = ParticleSystemQuad::create("Particle/freespins_win02.plist");
    _pParticle3 = ParticleSystemQuad::create("Particle/freespins_win03.plist");
    _pParticle4 = ParticleSystemQuad::create("Particle/freespins_win04.plist");
    particleDummy->addChild(_pParticle1);
    particleDummy->addChild(_pParticle2);
    particleDummy->addChild(_pParticle3);
    particleDummy->addChild(_pParticle4);

    return true;
}

void UISlotSuperClassicFreeSpinStart::destroyUI()
{
    _pLayoutFreeSpinStart->removeFromParent();
    _pLayoutFreeSpinStart = nullptr;
}

void UISlotSuperClassicFreeSpinStart::updateUI()
{
    if (_freeSpinCounter <= 0)
        return;

    _pBMFFreeSpinCounter->setString(Value(_freeSpinCounter).asString());

    if (_autoCloseUITime > 0.0f)
    {
        Vector<FiniteTimeAction*> arrayOfActions;
        arrayOfActions.pushBack(DelayTime::create(_autoCloseUITime));
        arrayOfActions.pushBack(CallFunc::create([this](){ this->closeUI(); }));
        auto actionSeq = Sequence::create(arrayOfActions);
        actionSeq->setTag(TAG_CLOSEUI);
        _pLayoutFreeSpinStart->runAction(actionSeq);
    }

    _pParticle1->resetSystem();
    _pParticle2->resetSystem();
    _pParticle3->resetSystem();
    _pParticle4->resetSystem();

    _pParticle1->setDuration(-1);
    _pParticle2->setDuration(-1);
    _pParticle3->setDuration(-1);
    _pParticle4->setDuration(-1);
}

void UISlotSuperClassicFreeSpinStart::Update(float dt)
{
}

void UISlotSuperClassicFreeSpinStart::onTouchCloseUIListener(Ref* sender, ui::Widget::TouchEventType event)
{
    if (event != Widget::TouchEventType::ENDED)
        return;
    _pLayoutFreeSpinStart->stopActionByTag(TAG_CLOSEUI);
    closeUI();
}