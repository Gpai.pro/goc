#include "UITutorialController.h"
#include "UIController.h"
#include "../GlobalDefine/Player.h"
#include "../Network/MessageProcess.h"

UITutorialController* UITutorialController::_instance = nullptr;

UITutorialController::UITutorialController()
{

}

UITutorialController::~UITutorialController()
{

}

UITutorialController* UITutorialController::getInstance()
{
    if (!_instance)
        _instance = new  UITutorialController();
    return _instance;
}

void UITutorialController::destroyInstance()
{
    CC_SAFE_DELETE(_instance);
}

void UITutorialController::initTutorial()
{
    if (!Player::getInstance()->getGameFlag(EGameFlagEntry::EGF_Tutorial_Done))
    {
        _step = ETutorial_01_Lobby;
    }
    else
    {
        _step = ETutorial_None;
    }
}

bool UITutorialController::isTutorialing()
{
    return !(_step == ETutorial_None);
}

void UITutorialController::openTutorial()
{
    if (_uiTutorial)
        return;

    switch (_step)
    {
    case ETutorial_01_Lobby:
        _uiTutorial = UIController::getInstance()->getController(EUITAG::EUITAG_TUTORIAL_01_LOBBY);
        _uiTutorial->openUILater();
        break;
    case ETutorial_02_SlotPirate:
        _uiTutorial = UIController::getInstance()->getController(EUITAG::EUITAG_TUTORIAL_02_SLOTPIRATE);
        _uiTutorial->openUI();
        break;
    case ETutorial_None:
        break;
    default:
        break;
    }
}

void UITutorialController::closeTutorial()
{
    if (_uiTutorial)
        _uiTutorial->closeUI();

    _uiTutorial = nullptr;

    switch (_step)
    {
    case ETutorial_01_Lobby:
        _step = ETutorialStep::ETutorial_02_SlotPirate;
        break;
    case ETutorial_02_SlotPirate:
        RequestTutorialDone();
        Player::getInstance()->setGameFlag(EGF_Tutorial_Done, true);
        _step = ETutorialStep::ETutorial_None;
        break;
    case ETutorial_None:
        break;
    default:
        break;
    }
}