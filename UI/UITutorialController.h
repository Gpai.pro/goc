#pragma once
#include "UIBasicUnit.h"

enum ETutorialStep
{
    ETutorial_01_Lobby = 0,
    ETutorial_02_SlotPirate,
    ETutorial_None,
};

class UITutorialController
{
public:
    UITutorialController();
    ~UITutorialController();

    static UITutorialController* getInstance();
    void destroyInstance();

    void initTutorial();
    bool isTutorialing();
    void openTutorial();
    void closeTutorial();
   
private:
    static UITutorialController* _instance;
    UIBasicUnit* _uiTutorial = nullptr;
    ETutorialStep _step = ETutorialStep::ETutorial_None;
};