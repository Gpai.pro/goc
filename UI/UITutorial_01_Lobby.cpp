#include "UITutorial_01_Lobby.h"
#include "../Auth/PlatformProxy.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/EnumType.h"
#include "../Network/MessageProcess.h"
#include "../UI/UICoinExplosion.h"
#include "../UI/UIController.h"

UITutorial_01_Lobby* UITutorial_01_Lobby::create()
{
    UITutorial_01_Lobby* _ui = new UITutorial_01_Lobby();

    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UITutorial_01_Lobby::UITutorial_01_Lobby()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UITutorial_01_Lobby::~UITutorial_01_Lobby()
{

}

void UITutorial_01_Lobby::destroyUI()
{

}

void UITutorial_01_Lobby::notifyCloseUI()
{

}

void UITutorial_01_Lobby::updateUI()
{

}

bool UITutorial_01_Lobby::init()
{
    return true;
}

bool UITutorial_01_Lobby::createUI()
{
    _uiTutorial = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UITutorial/Tutorial01_Lobby.ExportJson"));
    _uiTutorial->setVisible(true);
    _uiTutorial->setTouchEnabled(false);
    _mainUINode->addChild(_uiTutorial);
    setUIZOrder(ELZO_TUTORIAL);

    auto text = _uiTutorial->getChildByName<Text*>("txt_hint");
    text->setString(safeGetStringData(1151));
    text->enableOutline(Color4B::BLACK, 3);

    auto arrow = ActionManagerEx::getInstance()->getActionByName("GUI/UITutorial/Tutorial01_Lobby.ExportJson", "animation_arrow");
    arrow->updateToFrameByTime(0);
    arrow->play();

    return true;
}
