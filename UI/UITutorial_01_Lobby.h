#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"

USING_NS_CC;

using namespace ui;
using namespace cocostudio;

class UITutorial_01_Lobby : public UIBasicUnit
{
public:
    static UITutorial_01_Lobby* create();

public:
    UITutorial_01_Lobby();
    ~UITutorial_01_Lobby();

    virtual void destroyUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

private:
    bool init();
    virtual bool createUI() override;

private:
    Layout* _uiTutorial = nullptr;
};
