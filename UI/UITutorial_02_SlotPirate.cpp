#include "UITutorial_02_SlotPirate.h"
#include "../Auth/PlatformProxy.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/EnumType.h"
#include "../Network/MessageProcess.h"
#include "../UI/UICoinExplosion.h"
#include "../UI/UIController.h"
#include "../UI/UICommon.h"

UITutorial_02_SlotPirate* UITutorial_02_SlotPirate::create()
{
    UITutorial_02_SlotPirate* _ui = new UITutorial_02_SlotPirate();

    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UITutorial_02_SlotPirate::UITutorial_02_SlotPirate()
{
    setUIShowType(EUISHOWTYPE::EUIST_STAYALONE);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_FADE_IN);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UITutorial_02_SlotPirate::~UITutorial_02_SlotPirate()
{

}

void UITutorial_02_SlotPirate::destroyUI()
{

}

void UITutorial_02_SlotPirate::notifyCloseUI()
{

}

void UITutorial_02_SlotPirate::updateUI()
{

}

bool UITutorial_02_SlotPirate::init()
{
    return true;
}

bool UITutorial_02_SlotPirate::createUI()
{
    _uiTutorial = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/UITutorial/Tutorial02_UISlot_Pirate.ExportJson"));
    _uiTutorial->setVisible(true);
    _uiTutorial->setTouchEnabled(false);
    _mainUINode->addChild(_uiTutorial);
    setUIZOrder(ELZO_TUTORIAL);

    auto text = _uiTutorial->getChildByName<Text*>("txt_hint");
    text->setString(safeGetStringData(1152));
    text->enableOutline(Color4B::BLACK, 3);

    auto arrow = ActionManagerEx::getInstance()->getActionByName("GUI/UITutorial/Tutorial02_UISlot_Pirate.ExportJson", "animation_arrow");
    arrow->updateToFrameByTime(0);
    arrow->play();

    UICommon::getInstance()->fixUIPosition(_uiTutorial, EFixUIPositionType::EFP_DOWN);

    return true;
}
