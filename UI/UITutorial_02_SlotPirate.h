#pragma once

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"

USING_NS_CC;

using namespace ui;
using namespace cocostudio;

class UITutorial_02_SlotPirate : public UIBasicUnit
{
public:
    static UITutorial_02_SlotPirate* create();

public:
    UITutorial_02_SlotPirate();
    ~UITutorial_02_SlotPirate();

    virtual void destroyUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

private:
    bool init();
    virtual bool createUI() override;

private:
    Layout* _uiTutorial = nullptr;
};
