#include "UIWelcomeBonus.h"
#include "../Auth/PlatformProxy.h"
#include "../GlobalDefine/GameFunc.h"
#include "../GlobalDefine/EnumType.h"
#include "../Network/MessageProcess.h"
#include "../UI/UICoinExplosion.h"
#include "../UI/UIController.h"

UIWelcomeBonus* UIWelcomeBonus::create()
{
    UIWelcomeBonus* _ui = new UIWelcomeBonus();

    if (_ui && _ui->init())
    {
        return _ui;
    }

    CC_SAFE_DELETE(_ui);
    return nullptr;
}

UIWelcomeBonus::UIWelcomeBonus()
{
    setUIShowType(EUISHOWTYPE::EUIST_POPUP);
    setEnterAnimationType(EEnterUIAnimationType::EUAT_GROW_OUT);
    setExitAnimationType(EExitUIAnimationType::EXUAT_FADE_OUT);
}

UIWelcomeBonus::~UIWelcomeBonus()
{

}

void UIWelcomeBonus::destroyUI()
{

}

void UIWelcomeBonus::notifyCloseUI()
{

}

void UIWelcomeBonus::updateUI()
{
}

bool UIWelcomeBonus::init()
{
    return true;
}

bool UIWelcomeBonus::createUI()
{
    _uiWelcomeBonus = dynamic_cast<Layout*>(GUIReader::getInstance()->widgetFromJsonFile("GUI/SharedUI/WelcomeBonus.ExportJson"));
    _uiWelcomeBonus->setVisible(true);
    _uiWelcomeBonus->setTouchEnabled(true);
    _mainUINode->addChild(_uiWelcomeBonus);

    auto title = _uiWelcomeBonus->getChildByName<TextBMFont*>("bmf_welcome");
    title->setString(safeGetStringData(656));

    auto text1 = _uiWelcomeBonus->getChildByName<Text*>("text_1");
    text1->setString(safeGetStringData(657));
    auto text2 = _uiWelcomeBonus->getChildByName<Text*>("text_2");
    text2->setString(safeGetStringData(660));

    _moneyBM = _uiWelcomeBonus->getChildByName<TextBMFont*>("bmf_money");
    _moneyBM->setString(safeGetStringData(658));

    auto freeCoin = _uiWelcomeBonus->getChildByName<TextBMFont*>("bmf_free_coins");
    freeCoin->setString(safeGetStringData(659));

    _playNowBtn = _uiWelcomeBonus->getChildByName<Button*>("btn_playnow");
    _playNowBtn->setTouchEnabled(true);
    _playNowBtn->addTouchEventListener(CC_CALLBACK_2(UIWelcomeBonus::touchPlayNow, this));

    auto pLayoutListener = _uiWelcomeBonus->getChildByName<Layout*>("layout_touchlistener");
    pLayoutListener->addTouchEventListener(CC_CALLBACK_2(UIWelcomeBonus::onTouchWindow, this));

    auto BtnAnim = ActionManagerEx::getInstance()->getActionByName("GUI/SharedUI/WelcomeBonus.ExportJson", "btn");
    BtnAnim->updateToFrameByTime(0);
    BtnAnim->play();
    auto blingAnim = ActionManagerEx::getInstance()->getActionByName("GUI/SharedUI/WelcomeBonus.ExportJson", "blingbling");
    blingAnim->updateToFrameByTime(0);
    blingAnim->play();

    return true;
}

void UIWelcomeBonus::setMoney(int money)
{
    _moneyBM->setString(StringUtils::toString(money));
}

void UIWelcomeBonus::onDispatchResponse(const std::string& requestTag, const std::string& json)
{
    rapidjson::Document document;
    document.Parse(json.c_str());
    if (!document.IsObject())
        return;

    int errorCode = -1;
    if (document["ERR_CODE"].IsInt())
        errorCode = document["ERR_CODE"].GetInt();
    if (errorCode != 1)
    {
        closeUI();
        return;
    }

    int clickID = -1;
    if (document["clickID"].IsInt())
        clickID = document["clickID"].GetInt();
    else
    {
        closeUI();
        return;
    }

    if (clickID != EClickEventEntry::ECE_Welcome_Bonus)
    {
        return;
    }

    auto controller = UIController::getInstance()->getController<UICoinExplode*>(EUITAG::EUITAG_COINEXPLOSION);
    controller->openUI(_playNowBtn->getWorldPosition(), 10);
    closeUI();
}

void UIWelcomeBonus::touchPlayNow(Ref* sender, Widget::TouchEventType type)
{
    if (type != Widget::TouchEventType::ENDED)
        return;

    RequestClickEvent(EClickEventEntry::ECE_Welcome_Bonus);
}

void UIWelcomeBonus::onTouchWindow(Ref* sender, Widget::TouchEventType type)
{
    Layout* pLayoutListener = static_cast<Layout*>(sender);
    //按下視窗如同按下按鈕
    if (type == Widget::TouchEventType::BEGAN)
    {
        _playNowBtn->setHighlighted(true);
    }
    else if (type == Widget::TouchEventType::MOVED)
    {
        _playNowBtn->setHighlighted(pLayoutListener->isHighlighted());
    }
    else if (type == Widget::TouchEventType::ENDED)
    {
        _playNowBtn->setHighlighted(false);
        touchPlayNow(nullptr, ui::Widget::TouchEventType::ENDED);
    }
    else if (type == Widget::TouchEventType::CANCELED)
    {
        _playNowBtn->setHighlighted(false);
    }
}