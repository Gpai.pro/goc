#ifndef __UI_WELCOME_BONUS_H__
#define __UI_WELCOME_BONUS_H__

#include "cocos2d.h"
#include "UI/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "UIBasicUnit.h"

USING_NS_CC;

using namespace ui;
using namespace cocostudio;

class UIWelcomeBonus : public UIBasicUnit
{
public:
    static UIWelcomeBonus* create();

public:
    UIWelcomeBonus();
    ~UIWelcomeBonus();

    virtual void destroyUI() override;
    virtual void notifyCloseUI() override;
    virtual void updateUI() override;

    virtual void onDispatchResponse(const std::string& requestTag, const std::string& json);

private:
    bool init();
    virtual bool createUI() override;

    void setMoney(int money);

    void touchPlayNow(Ref* sender, Widget::TouchEventType type);
    void onTouchWindow(Ref* sender, Widget::TouchEventType type);

private:
    Layout* _uiWelcomeBonus = nullptr;
    TextBMFont* _moneyBM = nullptr;
    Button* _playNowBtn = nullptr;
};

#endif